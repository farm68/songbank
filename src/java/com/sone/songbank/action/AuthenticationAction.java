/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.action;

import com.sone.songbank.db.DmsAuthenticationDbMgr;
import com.sone.songbank.info.DmsArtistPortalInfo; 
import com.sone.songbank.info.login.CoverSongAccountInfo;
import com.sone.songbank.info.login.SessionUserInfo;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.songbank.util.Constants;
import com.sone.util.ThaiConvert;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author DELL
 */
public class AuthenticationAction extends DispatchAction {

    private final Logger logger = Logger.getLogger(AuthenticationAction.class);

    public ActionForward login(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String sUsername = ThaiConvert.nullToThaiString(request.getParameter("input_username"));
        String sPassword = ThaiConvert.nullToThaiString(request.getParameter("input_password"));
        String sMode = ThaiConvert.nullToThaiString(request.getParameter("mode"));

        logger.info("username: " + sUsername);
        logger.info("password: " + sPassword);

        if (sUsername != null && !sUsername.trim().equals("") && sPassword != null && !sPassword.trim().equals("")) {
            DmsAuthenticationDbMgr db = null;
            try {
                db = new DmsAuthenticationDbMgr();
                db.initializeConnection();

                String lastLogin = "Logged in: ";
                try {
                    DateFormat df = new SimpleDateFormat("HH:mm a , dd-MMMM");
                    Date date = new Date(System.currentTimeMillis());
                    lastLogin = lastLogin + df.format(date);
                } catch (Exception ex) {
                }

                SessionUserInfo userInfo = null;
                CoverSongAccountInfo accountInfo = db.authentication(sUsername, sPassword);
                if (accountInfo != null) {
                    userInfo = new SessionUserInfo();
                    userInfo.setCoverSongAccountInfo(accountInfo);
                    String channelId = accountInfo.getYoutubeChannelId();
                    ManageYoutubeChannelInfo youtubeChannelInfo = null;
                    DmsArtistPortalInfo artistPortalInfo = null;
                    try {
                        if (channelId != null && !channelId.trim().equals("")) {
                            youtubeChannelInfo = db.getYoutubeChannelInfo(channelId);
                            artistPortalInfo = db.getArtistInfo(channelId);
                        }
                    } catch (Exception ex) {
                    }
 
                    if (artistPortalInfo != null) {
                        userInfo.setArtistPortalInfo(artistPortalInfo);
                    }

                    if (youtubeChannelInfo != null) {
                        userInfo.setYoutubeChannelInfo(youtubeChannelInfo);
                    }
 
                }

                logger.info("userInfo : " + userInfo);
                if (userInfo != null) {
                    HttpSession session = request.getSession(false);
                    if ((session != null) && (!session.isNew())) {
                        session.invalidate();
                    }

                    session = request.getSession(true);
                    session.setAttribute(Constants.USER_HOME_PAGE, "");
                    session.setAttribute(Constants.USER_LOGIN_TIME, lastLogin);
                    session.setAttribute(Constants.USER_SESSION, userInfo);
                    return mapping.findForward("LOGIN_SUCCESS");
                } else {
                    request.setAttribute(Constants.DESCRIPTION_MESSAGE, "Invalid username or password. Please contact your account manager.");
                }
            } catch (Exception ex) { 
                logger.error("[AuthenticationAction] login : ", ex);
            } finally {
                if (db != null) {
                    db.releaseConnection();
                    db = null;
                }
            }
        } else {
            request.setAttribute(Constants.DESCRIPTION_MESSAGE, "Invalid username or password. Please contact your account manager.");
        }

        return mapping.findForward("LOGIN");
    }

    /**
     * This is the Struts action method called on
     * http://.../actionPath?method=myAction2, where "method" is the value
     * specified in <action> element : ( <action parameter="method" .../> )
     */
    public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        ArrayList removeList = new ArrayList();
        Enumeration enu = session.getAttributeNames();
        while (enu.hasMoreElements()) {
            try {
                removeList.add((String) enu.nextElement());
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        enu = null;
        int removeSize = removeList.size();
        for (int i = 0; i < removeSize; i++) {
            try {
                session.removeAttribute((String) removeList.get(i));
            } catch (Exception e) {
                logger.error("", e);
            }
        }
        removeSize = 0;
        removeList = null;

        return mapping.findForward("LOGIN");
    }
}
