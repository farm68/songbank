/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.db;

import com.sone.songbank.info.login.CoverSongAccountInfo;
import com.sone.songbank.util.Constants;
import com.sone.util.db.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author jiranuwat
 */
public class AccountDbMgr extends DBManager {
//public class AccountDbMgr extends DataSourceManager {

    private static Logger logger = Logger.getLogger(AccountDbMgr.class);
    
     static private String DB_CONNECTION_NAME = Constants.DB_CONNECTION_NAME;
//    static private String DB_CONNECTION_NAME = Constants.DATASOURCE_NAME;

    public AccountDbMgr() {
        this(Constants.DB_TIMEOUT, Constants.DB_CURSOR_SIZE);
    }

    public AccountDbMgr(long aTimeOut, int aCursorSize) {
        super(DB_CONNECTION_NAME, aTimeOut, aCursorSize);
    }

    public AccountDbMgr(String dbname, long aTimeOut, int aCursorSize) {
        super(dbname, aTimeOut, aCursorSize);
    }

    public ArrayList<CoverSongAccountInfo> listAccountInfo(String sKeyword) {
        ArrayList<CoverSongAccountInfo> listInfo = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT DMS_YTM_COVER_SONG_ACCOUNT.ACCOUNT_ID, DMS_YTM_COVER_SONG_ACCOUNT.USER_NAME, DMS_YTM_COVER_SONG_ACCOUNT.PASSWORD, DMS_YTM_COVER_SONG_ACCOUNT.DISPLAY_NAME,  ");
            sql.append("        DMS_YTM_COVER_SONG_ACCOUNT.DESCRIPTION, DMS_YTM_COVER_SONG_ACCOUNT.PREFIX_NAME, DMS_YTM_COVER_SONG_ACCOUNT.NAME, DMS_YTM_COVER_SONG_ACCOUNT.ALIAS_NAME,  ");
            sql.append("        DMS_YTM_COVER_SONG_ACCOUNT.EMAIL, DMS_YTM_COVER_SONG_ACCOUNT.TEL, DMS_YTM_COVER_SONG_ACCOUNT.YOUTUBE_CHANNEL_ID, DMS_YTM_COVER_SONG_ACCOUNT.CREATE_TIME,  ");
            sql.append("        YTM_YOUTUBE_CHANNEL.CHANNEL_NAME, YTM_YOUTUBE_CHANNEL.CHANNEL_THUMBNAIL_URL, YTM_YOUTUBE_CHANNEL.IS_ACTIVE, YTM_YOUTUBE_CHANNEL.LABEL_ID, ");
            sql.append("        YTM_YOUTUBE_CHANNEL.IS_ACTIVE, YTM_YOUTUBE_CHANNEL.IS_YT_CMS_LINK, YTM_YOUTUBE_CHANNEL.TOTAL_SUBSCRIBER, YTM_YOUTUBE_CHANNEL.TOTAL_VIDEOS, YTM_YOUTUBE_CHANNEL.TOTAL_VIEWS ");
            sql.append(" FROM   DMS_YTM_COVER_SONG_ACCOUNT LEFT OUTER JOIN ");
            sql.append("        YTM_YOUTUBE_CHANNEL WITH (NOLOCK) ON DMS_YTM_COVER_SONG_ACCOUNT.YOUTUBE_CHANNEL_ID = YTM_YOUTUBE_CHANNEL.CHANNEL_ID ");
            sql.append(" WHERE  (0=0) ");
            if (sKeyword != null && !sKeyword.trim().equals("")) {
                if (sKeyword.contains("*")) {
                    sKeyword = sKeyword.replaceAll("\\*", "%");
                }
                sql.append("AND ( ");
                sql.append("  DMS_YTM_COVER_SONG_ACCOUNT.USER_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR DMS_YTM_COVER_SONG_ACCOUNT.DISPLAY_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR DMS_YTM_COVER_SONG_ACCOUNT.DESCRIPTION " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR YTM_YOUTUBE_CHANNEL.CHANNEL_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR DMS_YTM_COVER_SONG_ACCOUNT.YOUTUBE_CHANNEL_ID " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append(") ");
            } 
            sql.append(" ORDER BY CREATE_TIME DESC ");
            pstmt = con.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            if (rs != null) {
                listInfo = new ArrayList<>();
                while (rs.next()) {
                    CoverSongAccountInfo info = new CoverSongAccountInfo();
                    info.setAccountId(rs.getInt("ACCOUNT_ID"));
                    info.setPassword(rs.getString("PASSWORD"));
                    info.setUsername(rs.getString("USER_NAME"));
                    info.setDisplayName(rs.getString("DISPLAY_NAME"));
                    info.setDescription(rs.getString("DESCRIPTION"));
                    info.setYoutubeChannelId(rs.getString("YOUTUBE_CHANNEL_ID"));
                    info.setYoutubeChannelName(rs.getString("CHANNEL_NAME"));
                    info.setYoutubeChannelImage(rs.getString("CHANNEL_THUMBNAIL_URL"));
                    info.setIsYoutubeChannelActive(rs.getBoolean("IS_ACTIVE"));
                    info.setIsYoutubeCmsLink(rs.getBoolean("IS_YT_CMS_LINK"));
                    info.setYoutubeSubscriber(rs.getLong("TOTAL_SUBSCRIBER"));
                    info.setYoutubeVideos(rs.getLong("TOTAL_VIDEOS"));
                    info.setYoutubeViews(rs.getLong("TOTAL_VIEWS"));
                    listInfo.add(info);
                }
            }

        } catch (Exception ex) {
            logger.error("[AccountDbMgr] listAccountInfo : ", ex);
        } finally {
            try {
                closeStatement(pstmt);
                closeResultSet(rs);
            } catch (Exception ex) {
            }
            rs = null;
            pstmt = null;
        }
        return listInfo;
    }

    public int createAccountInfo(String accountId,
            String username,
            String password,
            String displayName,
            String description,
            String channelId) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" INSERT INTO  DMS_YTM_COVER_SONG_ACCOUNT(ACCOUNT_ID, USER_NAME, PASSWORD, DISPLAY_NAME, DESCRIPTION, YOUTUBE_CHANNEL_ID, CREATE_TIME) ");
            sql.append(" VALUES (?,?,?,?,?,?,GETDATE()) ");

            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(i++, accountId);
            pstmt.setString(i++, username);
            pstmt.setString(i++, password);
            pstmt.setString(i++, displayName);
            pstmt.setString(i++, description);
            pstmt.setString(i++, channelId);
            resullt = pstmt.executeUpdate(); 

        } catch (Exception ex) {
            logger.error("[DmsYoutubeDbMgr] createAccountInfo : ", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int stampYoutubeChannelInfo(
            String channelId,
            String channelName,
            String channelImage) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {

            sql = new StringBuffer();
            sql.append(" INSERT INTO YTM_YOUTUBE_CHANNEL(CHANNEL_ID, CHANNEL_NAME, CHANNEL_THUMBNAIL_URL, IS_ACTIVE, IS_NOT_MONITOR, CREATE_TIME) ");
            sql.append(" VALUES (?,?,?,?,?,GETDATE()) ");
            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(i++, channelId);
            pstmt.setString(i++, channelName);
            pstmt.setString(i++, channelImage);
            pstmt.setBoolean(i++, false);
            pstmt.setBoolean(i++, false);
            resullt = pstmt.executeUpdate();

        } catch (Exception ex) {
            logger.error("[DmsYoutubeDbMgr] stampYoutubeChannelInfo : ", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int updateAccountInfo(String accountId, String username, String password, String displayName, String description) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" UPDATE DMS_YTM_COVER_SONG_ACCOUNT ");
            sql.append(" SET  CREATE_TIME = GETDATE() ");
            if (username != null && !username.equals("")) {
                sql.append(" , USER_NAME = '" + username + "' ");
            }

            if (password != null && !password.equals("")) {
                sql.append(" , PASSWORD = '" + password + "' ");
            }

            if (displayName != null && !displayName.equals("")) {
                sql.append(" , DISPLAY_NAME = '" + displayName + "' ");
            }

            if (description != null && !description.equals("")) {
                sql.append(" , DESCRIPTION = '" + description + "' ");
            }

            sql.append(" WHERE (ACCOUNT_ID = '" + accountId + "') ");
            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            resullt = pstmt.executeUpdate();
        } catch (Exception ex) {
            logger.error("[AccountDbMgr] updateAccountInfo : ", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

}
