/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.db;

import com.sone.songbank.info.coversong.ListSelectInfo;
import com.sone.songbank.info.dms.CmsUserInfo;
import com.sone.songbank.info.dms.ContentPagingInfo;
import com.sone.songbank.info.dms.DmsArtistInfo;
import com.sone.songbank.info.dms.ManageContentMusicCompositionInfo;
import com.sone.songbank.info.dms.ManageContentMusicServiceMapInfo;
import com.sone.songbank.info.dms.ManageSearchContentMusicInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.ContentPage;
import com.sone.util.db.*;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class DbDmsDbMgr extends DBManager {
//public class DbDmsDbMgr extends DataSourceManager {

    private static Logger logger = Logger.getLogger(DmsYoutubeDbMgr.class);

    /**
     * The database connection name.
     */
    static private String DB_CONNECTION_NAME = Constants.DB_CONNECTION_NAME;
//    static private String DB_CONNECTION_NAME = Constants.DATASOURCE_NAME;

    /**
     * Default Constructer.
     */
    public DbDmsDbMgr() {
        this(Constants.DB_TIMEOUT, Constants.DB_CURSOR_SIZE);
    }

    public DbDmsDbMgr(long aTimeOut, int aCursorSize) {
        super(DB_CONNECTION_NAME, aTimeOut, aCursorSize);
    }

    public DbDmsDbMgr(String dbname, long aTimeOut, int aCursorSize) {
        super(dbname, aTimeOut, aCursorSize);
    }

    private ContentPagingInfo queryContentPage(StringBuilder sql, int pageIndex, int limit) {

        ContentPagingInfo result = null;

        StringBuilder sqlMax = new StringBuilder();
        sqlMax.append("SELECT COUNT(*) FROM(" + sql.toString() + ")AS TMP");

//        logger.info("sqlMax: " + sqlMax.toString());
        try {
            int maxRecord = this.getMaxRecord(sqlMax.toString());
            int totalPage = 0;

            if (maxRecord < limit) {
                totalPage = 1;
            } else {
                double tmp = (double) maxRecord / (double) limit;
//                //logger.info("tmp: " + tmp);
                totalPage = (int) Math.ceil(tmp);
            }

            int startRow = 1;
            int endRow = maxRecord;
            if (pageIndex == 1) {
                startRow = 1;
                endRow = limit;
            } else if (pageIndex > 1) {
//                logger.info("0000 startRow" + startRow);
                startRow = (limit * (pageIndex - 1)) + 1;
                endRow = (limit * (pageIndex - 1)) + limit;

//                logger.info("1111 startRow" + startRow);
            }
//            logger.info("2222 startRow" + startRow);
//            (int pageIndex, int limit, int totalPage, int maxRecord, int queryStartRow, int queryEndRow)
            result = new ContentPagingInfo(pageIndex, limit, totalPage, maxRecord, startRow, endRow);
        } catch (Exception ex) {
            logger.error("", ex);
        }

        return result;
    }

    public ContentPage listSearchMusicContent(String artistPortalId, String sKeyword, Integer artistId, String startDate, String endDate, int pageIndex, int limit, String sKeysort, String sSortBy) {
        verifyConnection();
        ResultSet rs, rs2 = null;
        PreparedStatement pstmt = null;
        ArrayList<ManageSearchContentMusicInfo> aList = null;
        StringBuilder sql, cmd = new StringBuilder();
        ContentPage cp = null;

        long startQuery = System.currentTimeMillis();
        try {

            sql = new StringBuilder();

            sql.append("SELECT         MUSIC_ID, ISRC, MUSIC_NAME, MUSIC_NAME_EN, GENRE_ID, GENRE_NAME, ARTIST_NAME_TH, ARTIST_NAME_EN, UPC_CODE, ALBUM_NAME_TH, ALBUM_NAME_EN, RELEASE_DATE, LABEL_ID,  ");
            sql.append("LABEL_NAME_TH, LABEL_NAME_EN, ALBUM_ARTIST_NAME_TH, ALBUM_ARTIST_NAME_EN, EXCLUSIVE_MUSIC_SERVICE_ID, MUSIC_SERVICE_NAME, EXCLUSIVE_DATE,CREATE_TIME,UPDATE_TIME, ONE_LINK_URL, THUMBNAIL_URL ");
            sql.append("FROM            VIEW_DMS_CONTENT_MUSIC_INFO ");

            sql.append("WHERE 0=0 AND MUSIC_NAME NOT LIKE '%iTune Ringtone%' ");
            sql.append("AND (ARTIST_PORTAL_ID IS NOT NULL AND ARTIST_PORTAL_ID = N'" + artistPortalId + "') ");

            if (sKeyword != null && !sKeyword.trim().equals("")) {
                if (sKeyword.contains("*")) {
                    sKeyword = sKeyword.replaceAll("\\*", "%");
                }

                sql.append("AND ( ");
                sql.append("    MUSIC_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR MUSIC_NAME_EN " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR  ARTIST_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR  ARTIST_NAME_EN " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");

                sql.append("  OR UPC_CODE " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");

                sql.append("  OR  ALBUM_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR  ALBUM_NAME_EN " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR ISRC " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");

                sql.append("  OR REPLACE(ISRC,'-','') " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR LABEL_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
//                

                sql.append("  OR MUSIC_ID " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append(") ");
            }

            if (artistId != null && artistId.intValue() != -1) {
                sql.append("AND (ARTIST_ID IN( " + artistId + ")) ");
            }

            if (!Utilities.isBlankText(startDate)) {
                sql.append("AND (RELEASE_DATE >= N'" + startDate + " 00:00:00' ) ");
            }
            if (!Utilities.isBlankText(endDate)) {
                sql.append("AND (RELEASE_DATE <= N'" + endDate + " 23:59:59'  ) ");
            }

            logger.info("sql: " + sql.toString());

            String sortBy = "RELEASE_DATE";
            String aDesc = "DESC";

            if (sKeysort != null && !sKeysort.trim().equals("")) {
                sortBy = sKeysort;
                if (sSortBy != null && !sSortBy.trim().equals("") && sSortBy.trim().equalsIgnoreCase("asc")) {
                    aDesc = "ASC";
                }
            }

            int maxRecord = 0;
            int totalPage = 0;
            int startRow = 0;
            int endRow = 0;

            ContentPagingInfo contentPagingInfo = queryContentPage(sql, pageIndex, limit);

            if (contentPagingInfo != null) {
                maxRecord = contentPagingInfo.getMaxRecord();
                totalPage = contentPagingInfo.getTotalPage();
                startRow = contentPagingInfo.getQueryStartRow();
                endRow = contentPagingInfo.getQueryEndRow();

                cmd = new StringBuilder();
                cmd.append("{call getContentPage(?,?,?,?)}");

                CallableStatement callStmt = this.con.prepareCall(cmd.toString());
                callStmt.setString(1, sql.toString());
                callStmt.setInt(2, startRow);
                callStmt.setInt(3, endRow);
                callStmt.setString(4, "ORDER BY " + sortBy + " " + aDesc + " ");
                rs = callStmt.executeQuery();

                callStmt = null;
                cmd = null;

                if (rs != null) {
                    aList = new ArrayList<ManageSearchContentMusicInfo>();

                    while (rs.next()) {

                        String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                        String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));

                        String musicId = rs.getString("MUSIC_ID");
                        Integer labelId = rs.getInt("LABEL_ID");
                        String labelName = rs.getString("LABEL_NAME_TH");

                        Integer genreId = rs.getInt("GENRE_ID");
                        String genreName = rs.getString("GENRE_NAME");

                        artistId = -999;
                        String artistName = rs.getString("ARTIST_NAME_TH");

                        Integer albumId = -999;
                        String albumName = rs.getString("ALBUM_NAME_TH");

                        String isrc = rs.getString("ISRC");
                        String nameEn = rs.getString("MUSIC_NAME_EN");
                        String nameTh = rs.getString("MUSIC_NAME");

//                        Integer copyrightId = rs.getInt("COPYRIGHT_ID");
//                        String copyrightName = rs.getString("COPYRIGHT_NAME");
//
//                        Integer publisherId = rs.getInt("PUBLISHER_ID");
//                        String publisherName = rs.getString("PUBLISHER_NAME");
                        Integer copyrightId = -999;
                        String copyrightName = "";

                        Integer publisherId = -999;
                        String publisherName = "";

                        String upcCode = rs.getString("UPC_CODE");
                        String labelNameEn = rs.getString("LABEL_NAME_EN");

                        String releaseDate = rs.getString("RELEASE_DATE");
                        if (releaseDate != null && !releaseDate.trim().equals("")) {
                            releaseDate = Utilities.convertStrDbDateTimeToWebFormat(releaseDate);
                        }

                        Integer exclusiveMusicServiceId = rs.getInt("EXCLUSIVE_MUSIC_SERVICE_ID");
                        String exclusiveMusicServiceName = rs.getString("MUSIC_SERVICE_NAME");
                        String exclusiveDate = rs.getString("EXCLUSIVE_DATE");
                        if (exclusiveDate != null && !exclusiveDate.trim().equals("")) {
                            exclusiveDate = Utilities.convertStrDbDateTimeToWebFormat(exclusiveDate);
                        }

                        Integer musicStateId = 1;//rs.getInt("STATE_ID");
                        String musicStateName = "Active";//rs.getString("STATE_NAME");

                        String artistNameEn = rs.getString("ARTIST_NAME_EN");
                        String albumNameEn = rs.getString("ALBUM_NAME_EN");
                        String albumArtistName = rs.getString("ALBUM_ARTIST_NAME_TH");

                        String oneLinkUrl = rs.getString("ONE_LINK_URL");
                        String thumbnailUrl = rs.getString("THUMBNAIL_URL");
                        String parentLable = "";// rs.getString("PARENT_LABEL_ID");
//                        if (parentLable == null) {
//                            parentLable = "";
//                        }

                        /*List service Music*/
                        ArrayList<ManageContentMusicServiceMapInfo> listServiceMap = null;

                        sql = new StringBuilder();

                        sql.append("SELECT MUSIC_SERVICE_GROUP_ID, MUSIC_SERVICE_GROUP_NAME, MUSIC_SERVICE_ID, MUSIC_SERVICE_NAME, MUSIC_ID, DETAIL, STATE_ID, STATE_NAME,  ");
                        sql.append("REMARK, ACTION_TIME, RELEASE_TIME ");
                        sql.append("FROM VIEW_DMS_CONTENT_MUSIC_SERVICE_MAP_INFO WITH (NOLOCK) ");
                        sql.append("WHERE (MUSIC_ID = ?) ");
                        sql.append("ORDER BY MUSIC_SERVICE_GROUP_NAME, MUSIC_SERVICE_NAME ");

                        pstmt = con.prepareStatement(sql.toString());
                        pstmt.setString(1, musicId);
                        rs2 = pstmt.executeQuery();

                        sql = null;
                        pstmt = null;

                        if (rs2 != null) {
                            listServiceMap = new ArrayList<ManageContentMusicServiceMapInfo>();
                            while (rs2.next()) {
                                String innerMusicServiceGroupName = rs2.getString("MUSIC_SERVICE_GROUP_NAME");
                                String innerMusicServiceName = rs2.getString("MUSIC_SERVICE_NAME");
                                Integer innerMusicServiceId = rs2.getInt("MUSIC_SERVICE_ID");
                                String innerDetail = rs2.getString("DETAIL");
                                Integer stateId = rs2.getInt("STATE_ID");
                                String stateName = rs2.getString("STATE_NAME");
                                String remark = rs2.getString("REMARK");
                                String actionTime = rs2.getString("ACTION_TIME");
                                String releaseTime = rs2.getString("RELEASE_TIME");

                                if (!Utilities.isBlankText(actionTime)) {
                                    actionTime = Utilities.convertDateTimeDB2WebFormat(actionTime);
                                }

                                if (!Utilities.isBlankText(releaseTime)) {
                                    releaseTime = Utilities.convertDateTimeDB2WebFormat(releaseTime);
                                }

                                ManageContentMusicServiceMapInfo serviceInfo = new ManageContentMusicServiceMapInfo(innerMusicServiceGroupName, innerMusicServiceName, innerMusicServiceId, musicId, innerDetail, stateId, stateName, remark, actionTime);
                                serviceInfo.setReleaseTime(releaseTime);
                                listServiceMap.add(serviceInfo);
                            }
                        }

                        /*List service Music*/
                        ManageSearchContentMusicInfo manageSearch = new ManageSearchContentMusicInfo(musicId, labelId, labelName, genreId, genreName, artistId, artistName, albumId, albumName, upcCode,
                                isrc, nameEn, nameTh,
                                copyrightId, copyrightName, publisherId, publisherName,
                                releaseDate, exclusiveMusicServiceId, exclusiveMusicServiceName, exclusiveDate,
                                musicStateId, musicStateName, createTime, updateTime, listServiceMap);

                        manageSearch.setArtistNameEng(artistNameEn);
                        manageSearch.setAlbumNameEng(albumNameEn);
                        manageSearch.setAlbumArtistName(albumArtistName);
                        manageSearch.setParentLable(parentLable);
                        manageSearch.setLabelNameEn(labelNameEn);
                        manageSearch.setOneLinkUrl(oneLinkUrl);
                        manageSearch.setReleaseDate(releaseDate);
                        manageSearch.setThumbnailUrl(thumbnailUrl);

                        aList.add(manageSearch);

                    }

                    rs = null;
                    pstmt = null;
                    callStmt = null;
                    sql = null;

                    cp = new ContentPage();
                    cp.setDataList(aList);
                    cp.setPageIndex(pageIndex);
                    cp.setTotalPages(totalPage);
                    cp.setTotalAmount(maxRecord);
                }

            }

        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            try {
                pstmt.close();
                pstmt = null;
                rs = null;
                sql = null;
            } catch (Exception ex) {
            }
        }
        logger.info("query use time: " + (System.currentTimeMillis() - startQuery) + " milisec.");
        return cp;
    }

    public ArrayList<DmsArtistInfo> listArtistSelect(String artistPortalId) {
        ArrayList<DmsArtistInfo> result = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT ARTIST_ID, ARTIST_NAME_TH, ARTIST_NAME_EN, ARTIST_PORTAL_ID ");

            sql.append(" FROM   DMS_ARTIST WITH(NOLOCK) ");
            sql.append(" WHERE (ARTIST_PORTAL_ID = ?) ");
            sql.append(" ORDER BY ARTIST_NAME_TH ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, artistPortalId);
            rs = pstmt.executeQuery();
            pstmt = null;
            sql = null;
            if (rs != null) {
                result = new ArrayList<DmsArtistInfo>();
                while (rs.next()) {
                    DmsArtistInfo info = new DmsArtistInfo();
                    info.setArtistId(rs.getInt("ARTIST_ID"));
                    info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                    info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
                    info.setArtistPortalId(rs.getString("ARTIST_PORTAL_ID"));
                    result.add(info);
                }
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            sql = null;
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }

    public ArrayList<ListSelectInfo> listCoverStatusSelect() {
        ArrayList<ListSelectInfo> result = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT [STATUS_ID] ,[STATUS_NAME] FROM  [DMS_YTM_COVER_SONG_STATUS] ");
            pstmt = con.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            pstmt = null;
            sql = null;
            if (rs != null) {
                result = new ArrayList<ListSelectInfo>();
                while (rs.next()) {
                    ListSelectInfo info = new ListSelectInfo();
                    info.setId(rs.getInt("STATUS_ID") + "");
                    info.setName(rs.getString("STATUS_NAME"));
                    result.add(info);
                }
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            sql = null;
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }

    public ManageContentMusicCompositionInfo getMusicComposition(String musicId) {
        ManageContentMusicCompositionInfo result = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT MUSIC_ID, SHORT_LYRICS, LYRICS,  COMPOSER_WRITER_ID, COMPOSER_WRITER,  ");
            sql.append(" COMPOSER_MELODY_ID, COMPOSER_MELODY, COMPOSER_ARRANGE_ID, COMPOSER_ARRANGE, ");
            sql.append("PRODUCER_ID, PRODUCER, EXECUTIVE_PRODUCER_ID, EXCUTIVE_PRODUCER, URL_MASTER, MUSIC_NAME, ARTIST_NAME, ALBUM_NAME ");
            sql.append("FROM VIEW_DMS_CONTENT_MUSIC_COMPOSER WITH(NOLOCK) ");
            sql.append("WHERE (MUSIC_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, musicId);
            rs = pstmt.executeQuery();
            pstmt = null;
            sql = null;
            if (rs != null && rs.next()) {
                result = new ManageContentMusicCompositionInfo();
                result.setMusicId(rs.getString("MUSIC_ID"));
                result.setShortLyrics(rs.getString("SHORT_LYRICS"));
                result.setLyrics(rs.getString("LYRICS"));
                result.setPersonalLyricistId(rs.getInt("COMPOSER_WRITER_ID"));
                result.setPersonalLyricistName(rs.getString("COMPOSER_WRITER"));
//                    
                result.setPersonalComposerId(rs.getInt("COMPOSER_MELODY_ID"));
                result.setPersonalComposerName(rs.getString("COMPOSER_MELODY"));
                result.setPersonalArrangerId(rs.getInt("COMPOSER_ARRANGE_ID"));
                result.setPersonalArrangerName(rs.getString("COMPOSER_ARRANGE"));
                result.setPersonalProducerId(rs.getInt("PRODUCER_ID"));
                result.setPersonalProducerName(rs.getString("PRODUCER"));
                result.setPersonalExclusiveProducerId(rs.getInt("EXECUTIVE_PRODUCER_ID"));
                result.setPersonalExclusiveProducerName(rs.getString("EXCUTIVE_PRODUCER"));
                result.setUrlMaster(rs.getString("URL_MASTER"));
                result.setMusicName(rs.getString("MUSIC_NAME"));
                result.setArtistName(rs.getString("ARTIST_NAME"));
                result.setAlbumName(rs.getString("ALBUM_NAME"));

            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            sql = null;
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }

    public ArrayList<ListSelectInfo> listMusicCategorySelect() {
        ArrayList<ListSelectInfo> listInfo = null;
        PreparedStatement pstmtInner = null;
        ResultSet rsInner = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT  [MUSIC_CATEGORY_ID]");
            sql.append(" ,[MUSIC_CATEGORY_NAME]");
            sql.append(" ,[CREATE_TIME]");
            sql.append(" FROM  [DMS_CONTENT_MUSIC_CATEGORY]  WITH(NOLOCK) ");
            sql.append(" WHERE (MUSIC_CATEGORY_ID NOT IN(99001,9902,99003,9904)) ");
            sql.append(" AND MUSIC_CATEGORY_ID IN( SELECT        MUSIC_CATEGORY_ID ");
            sql.append(" FROM DMS_CONTENT_MUSIC_CATEGORY_MAP WITH(NOLOCK)) ");
            sql.append(" ORDER BY MUSIC_CATEGORY_NAME ");
            logger.info("listMusicCategorySelect : " + sql.toString());
            pstmtInner = con.prepareStatement(sql.toString());
            rsInner = pstmtInner.executeQuery();
            if (rsInner != null) {
                listInfo = new ArrayList<ListSelectInfo>();
                while (rsInner.next()) {
                    ListSelectInfo info = new ListSelectInfo();
                    info.setId(rsInner.getInt("MUSIC_CATEGORY_ID") + "");
                    info.setName(rsInner.getString("MUSIC_CATEGORY_NAME"));
                    listInfo.add(info);
                }
            }
        } catch (Exception ex) {
        } finally {
            try {
                closeResultSet(rsInner);
                closeStatement(pstmtInner);
            } catch (Exception ex) {
            }
            pstmtInner = null;
            rsInner = null;
        }

        return listInfo;
    }

    public ArrayList<ListSelectInfo> listCompLyrics() {
        ArrayList<ListSelectInfo> result = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT KEYWORD ");
            sql.append(" FROM VIEW_AUTO_COMPLETE_KEYWORD ");
            pstmt = con.prepareStatement(sql.toString());
            rs = pstmt.executeQuery();
            pstmt = null;
            sql = null;
            if (rs != null) {
                result = new ArrayList<ListSelectInfo>();
                while (rs.next()) {
                    ListSelectInfo info = new ListSelectInfo();
                    info.setName(rs.getString("KEYWORD"));
                    result.add(info);
                }
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            sql = null;
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }

    public ArrayList<CmsUserInfo> listCmsUserByGroup(String userGroupId) {
        ArrayList<CmsUserInfo> result = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT USER_ID, USER_GROUP_ID, DISPLAY_NAME, USER_NAME, EMAIL ");
            sql.append(" FROM     VIEW_ACCOUNT_MANAGER_SONG_BANK WITH(NOLOCK) ");
            sql.append(" WHERE (EMAIL<>'') AND (EMAIL IS NOT NULL)");
            if (!Utilities.isBlankText(userGroupId)) {
                sql.append("AND USER_GROUP_ID IN( " + userGroupId + " ) ");
            }
            sql.append("ORDER BY DISPLAY_NAME ");
            pstmt = con.prepareStatement(sql.toString());
            rs = pstmt.executeQuery(); 
            if (rs != null) {
                result = new ArrayList<CmsUserInfo>();
                while (rs.next()) {
                    CmsUserInfo info = new CmsUserInfo();
                    info.setUserId(rs.getInt("USER_ID"));
                    info.setUserGroupId(rs.getInt("USER_GROUP_ID"));
                    info.setDisplayName(rs.getString("DISPLAY_NAME"));
                    info.setUserName(rs.getString("USER_NAME"));
                    info.setEmail(rs.getString("EMAIL"));
                    result.add(info);
                }
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            sql = null;
            closeResultSet(rs);
            closeStatement(pstmt);
        }

        return result;
    }
}
