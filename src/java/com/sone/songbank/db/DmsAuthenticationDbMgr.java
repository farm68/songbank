/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.db;

import com.sone.songbank.info.DmsArtistPortalInfo; 
import com.sone.songbank.info.login.SessionUserInfo;
import com.sone.songbank.info.login.CoverSongAccountInfo;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.db.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author jiranuwat
 */
public class DmsAuthenticationDbMgr extends DBManager {
//public class DmsAuthenticationDbMgr extends DataSourceManager {

    private static Logger logger = Logger.getLogger(DmsAuthenticationDbMgr.class);

    /**
     * The database connection name.
     */
    static private String DB_CONNECTION_NAME = Constants.DB_CONNECTION_NAME;
//    static private String DB_CONNECTION_NAME = Constants.DATASOURCE_NAME;

    /**
     * Default Constructer.
     */
    public DmsAuthenticationDbMgr() {
        this(Constants.DB_TIMEOUT, Constants.DB_CURSOR_SIZE);
    }

    public DmsAuthenticationDbMgr(long aTimeOut, int aCursorSize) {
        super(DB_CONNECTION_NAME, aTimeOut, aCursorSize);
    }

    public DmsAuthenticationDbMgr(String dbname, long aTimeOut, int aCursorSize) {
        super(dbname, aTimeOut, aCursorSize);
    }

    public CoverSongAccountInfo authentication(String userName, String password) {
        CoverSongAccountInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT  [ACCOUNT_ID] ");
            sql.append("   ,[USER_NAME] ");
            sql.append("   ,[PASSWORD] ");
            sql.append("   ,[DISPLAY_NAME] ");
            sql.append("   ,[DESCRIPTION] ");
            sql.append("   ,[PREFIX_NAME] ");
            sql.append("   ,[NAME] ");
            sql.append("   ,[ALIAS_NAME] ");
            sql.append("   ,[EMAIL] ");
            sql.append("   ,[TEL] "); 
            sql.append("   ,[YOUTUBE_CHANNEL_ID] ");
            sql.append("   ,[CREATE_TIME] ");
            sql.append(" FROM [N-DataMusicService].[dbo].[DMS_YTM_COVER_SONG_ACCOUNT] ");
            sql.append(" WHERE (USER_NAME = ?) AND (PASSWORD = ?) ");
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, userName);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                info = new CoverSongAccountInfo();
                info.setAccountId(rs.getInt("ACCOUNT_ID"));
                info.setUsername(rs.getString("USER_NAME"));
                info.setPassword(rs.getString("PASSWORD"));
                info.setDisplayName(rs.getString("DISPLAY_NAME"));
                info.setDescription(rs.getString("DESCRIPTION"));
                info.setPrefixName(rs.getString("PREFIX_NAME"));
                info.setName(rs.getString("NAME"));
                info.setAliasName(rs.getString("ALIAS_NAME"));
                info.setTel(rs.getString("TEL"));
                info.setEmail(rs.getString("EMAIL"));
                info.setYoutubeChannelId(rs.getString("YOUTUBE_CHANNEL_ID"));
                info.setCreateTime(rs.getString("CREATE_TIME"));
            }
        } catch (Exception e) {
            logger.error("[DmsAuthenticationDbMgr] authentication : ", e);
        } finally {
            try {
                closeStatement(pstmt);
                closeResultSet(rs);
            } catch (Exception ex) {
            }

            rs = null;
            pstmt = null;
            sql = null;
        }

        return info;
    }
//    

    public ManageYoutubeChannelInfo getYoutubeChannelInfo(String channelId) {
        ManageYoutubeChannelInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" SELECT YTM_YOUTUBE_CHANNEL.[CHANNEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_NAME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_SHORT_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_IMAGE_URL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_ACTIVE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LABEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_TYPE_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_UGC_CHANNEL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_NOT_MONITOR] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LINKED_DATE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CREATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[UPDATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_LINKED] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[KEYWORDS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[COUNTRY] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[JOINED_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_SUBSCRIBER] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIDEOS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIEWS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_THUMBNAIL_URL] ");
//          
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_NAME] AS REQUEST_USER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PASSWORD] AS REQUEST_PASSWORD ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PREFIX_NAME] AS REQUEST_PREFIX_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[NAME] AS REQUEST_COVER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[ALIAS_NAME] AS REQUEST_COVER_ALIAS_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[EMAIL] AS REQUEST_COVER_EMAIL ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TEL] AS REQUEST_COVER_TEL ");
//
            sql.append("       ,CMS_USER.[USER_NAME] ");
            sql.append("       ,CMS_USER.DISPLAY_NAME ");
            sql.append("       ,CMS_USER.EMAIL ");
            sql.append("   FROM  YTM_YOUTUBE_CHANNEL   WITH (NOLOCK) INNER JOIN  ");
            sql.append("    CMS_USER WITH (NOLOCK) ON CMS_USER.USER_ID = YTM_YOUTUBE_CHANNEL.USER_ID   ");
            sql.append("   WHERE (YTM_YOUTUBE_CHANNEL.[CHANNEL_ID] = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, channelId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                String linkedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("LINKED_DATE"));
                String joinedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("JOINED_TIME"));

                String channelName = rs.getString("CHANNEL_NAME");
                String channelShortDescription = rs.getString("CHANNEL_SHORT_DESCRIPTION");
                String channelDescription = rs.getString("CHANNEL_DESCRIPTION");
                String channelImageUrl = rs.getString("CHANNEL_IMAGE_URL");
                Boolean isActive = rs.getBoolean("IS_ACTIVE");
                Integer labelId = rs.getInt("LABEL_ID");
                Integer channelUserId = rs.getInt("USER_ID");
                String username = rs.getString("USER_NAME");
                String displayName = rs.getString("DISPLAY_NAME");
                String email = rs.getString("EMAIL");
                Integer channelTypeId = rs.getInt("CHANNEL_TYPE_ID");
                Boolean isUgcChannel = rs.getBoolean("IS_UGC_CHANNEL");
                Boolean isNotMonitor = rs.getBoolean("IS_NOT_MONITOR");

                Boolean isLinked = rs.getBoolean("IS_LINKED");
                String keywords = rs.getString("KEYWORDS");
                String country = rs.getString("COUNTRY");
                Integer totalSubscriber = rs.getInt("TOTAL_SUBSCRIBER");
                Integer totalVideos = rs.getInt("TOTAL_VIDEOS");
                Integer totalViews = rs.getInt("TOTAL_VIEWS");
                String channelThumbnailUrl = rs.getString("CHANNEL_THUMBNAIL_URL");

                String requestUsername = rs.getString("REQUEST_USER_NAME");
                String requestPassword = rs.getString("REQUEST_PASSWORD");
                String requestPrefixName = rs.getString("REQUEST_PREFIX_NAME");
                String requestCoverName = rs.getString("REQUEST_COVER_NAME");
                String requestCoverAliasName = rs.getString("REQUEST_COVER_ALIAS_NAME");
                String requestCoverEmail = rs.getString("REQUEST_COVER_EMAIL");
                String requestCoverTel = rs.getString("REQUEST_COVER_TEL");

                info = new ManageYoutubeChannelInfo();
                info.setChannelId(channelId);
                info.setChannelName(channelName);
                info.setChannelShortDescription(channelShortDescription);
                info.setChannelDescription(channelDescription);
                info.setChannelImageUrl(channelImageUrl);
                info.setIsActive(isActive);
                info.setLabelUserId(channelUserId);
                info.setChannelUserId(channelUserId);
                info.setChannelTypeId(channelTypeId);
                info.setIsUgcChannel(isUgcChannel);
                info.setIsNotMonitor(isNotMonitor);
                info.setCreateTime(createTime);
                info.setUpdateTime(updateTime);

                info.setDisplayName(displayName);
                info.setChannelAccountManagerName(username);
                info.setChannelAccountEmail(email);

                info.setIsLinked(isLinked);
                info.setKeywords(keywords);
                info.setCountry(country);
                info.setTotalSubscripber(totalSubscriber);
                info.setTotalVideo(totalVideos);
                info.setTotalViews(totalViews);
                info.setJoinedTime(joinedTime);
                info.setChannelThumbnailUrl(channelThumbnailUrl);

                info.setUsername(requestUsername);
                info.setPassword(requestPassword);
                info.setPrefixName(requestPrefixName);
                info.setName(requestCoverName);
                info.setAliasName(requestCoverAliasName);
                info.setEmail(requestCoverEmail);
                info.setTel(requestCoverTel);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return info;
    }

    public DmsArtistPortalInfo getArtistInfo(String channelId) {
        DmsArtistPortalInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT  [ARTIST_PORTAL_ID] ,[ARTIST_NAME_TH] ,[ARTIST_NAME_EN] ");
            sql.append(" ,[PROFILE_SHORT_DESC] ,[PROFILE_DESC] ,[IMAGE_URL] ,[YT_CHANNEL_ID] ");
            sql.append(" ,[IS_YT_OAC]  ,[LABEL_ID] ,[STATUS_ID]  ,[USER_NAME]  ,[PASSWORD]  ,[DISPLAY_NAME] ");
            sql.append(" ,[USER_IMAGE_URL]  ,[EMAIL] ,[CREATE_TIME]  ,[UPDATE_TIME] ");
            sql.append(" FROM [N-DataMusicService].[dbo].[DMS_ARTIST_PORTAL] ");
            sql.append(" WHERE (YT_CHANNEL_ID = ?)  ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, channelId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String artistPortalId = rs.getString("ARTIST_PORTAL_ID");
                info = new DmsArtistPortalInfo();
                info.setArtistPortalId(artistPortalId);
                info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
                info.setProfileShortDesc(rs.getString("PROFILE_SHORT_DESC"));
                info.setProfileDesc(rs.getString("PROFILE_DESC"));
                info.setImageUrl(rs.getString("IMAGE_URL"));
                info.setYtChannelId(rs.getString("YT_CHANNEL_ID"));
                info.setIsYtOac(rs.getBoolean("IS_YT_OAC"));
                info.setLabelId(rs.getInt("LABEL_ID"));
                info.setStatusId(rs.getInt("STATUS_ID"));
                info.setUserName(rs.getString("USER_NAME"));
                info.setPassword(rs.getString("PASSWORD"));
                info.setDisplayName(rs.getString("DISPLAY_NAME"));
                info.setUserImageUrl(rs.getString("USER_IMAGE_URL"));
                info.setEmail(rs.getString("DISPLAY_NAME"));
                info.setCreateTime(rs.getString("CREATE_TIME"));
                info.setUpdateTime(rs.getString("UPDATE_TIME"));
            }
        } catch (Exception e) {
            logger.error("[DmsAuthenticationDbMgr] getArtistInfo : ", e);
        } finally {
            try {
                closeStatement(pstmt);
                closeResultSet(rs);
            } catch (Exception ex) {
            }

            rs = null;
            pstmt = null;
            sql = null;
        }

        return info;
    }
 
}
