/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.db;

import com.sone.songbank.info.DmsContentMusicCategoryMapInfo;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.songbank.info.coversong.DmsYtmRequestConverSongYtChannelMapInfo;
import com.sone.songbank.info.coversong.DmsYtmRequestCoverSongMusicMapInfo;
import com.sone.songbank.info.coversong.ManageRequestCoverSongLogInfo;
import com.sone.songbank.info.dms.ContentPagingInfo;
import com.sone.songbank.info.dms.ManageContentMusicServiceMapInfo;
import com.sone.songbank.info.coversong.ManageSearchContentMusicCoverInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.ContentPage;
import com.sone.util.db.*;
import com.sun.org.apache.bcel.internal.generic.Type;
import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author jiranuwat
 */
public class DmsCoverSongDbMgr extends DBManager {
//public class DmsCoverSongDbMgr extends DataSourceManager {

    private static Logger logger = Logger.getLogger(DmsCoverSongDbMgr.class);

    /**
     * The database connection name.
     */
    static private String DB_CONNECTION_NAME = Constants.DB_CONNECTION_NAME;
//    static private String DB_CONNECTION_NAME = Constants.DATASOURCE_NAME;

    /**
     * Default Constructer.
     */
    public DmsCoverSongDbMgr() {
        this(Constants.DB_TIMEOUT, Constants.DB_CURSOR_SIZE);
    }

    public DmsCoverSongDbMgr(long aTimeOut, int aCursorSize) {
        super(DB_CONNECTION_NAME, aTimeOut, aCursorSize);
    }

    public DmsCoverSongDbMgr(String dbname, long aTimeOut, int aCursorSize) {
        super(dbname, aTimeOut, aCursorSize);
    }

    public ContentPage listSearchMusicContentCover(String sKeyword, String sListMusicCategoryId, int pageIndex, int limit, String sKeysort, String sSortBy) {
        verifyConnection();
        ResultSet rs, rs2 = null;
        PreparedStatement pstmt = null;
        ArrayList<ManageSearchContentMusicCoverInfo> listInfo = null;
        StringBuilder sql, cmd = new StringBuilder();
        ContentPage cp = null;

        long startQuery = System.currentTimeMillis();
        try {
            sql = new StringBuilder();
            sql.append(" SELECT * FROM (	 ");
            sql.append(" SELECT VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_PUBLISHING_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ISRC, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME_EN, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.GENRE_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.GENRE_NAME, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_ID, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_TH, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_EN, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.UPC_CODE, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_ARTIST_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_ARTIST_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.RELEASE_DATE, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_ORIGI_VOCAL_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_ORIGI_VOCAL_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_LYRIC_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_LYRIC_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_MELODY_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_MELODY_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MELODY_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MELODY_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MASTER_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MASTER_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.SHORT_LYRICS, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LYRICS, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.URL_MASTER, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.THUMBNAIL_URL, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ONE_LINK_URL,VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.[COVER_OWNER_PERCENT_SHARING],VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.[COVER_PARTNER_PERCENT_SHARING],	 ");
            sql.append("    MIN(ISNULL(DMS_CONTENT_MUSIC_CATEGORY_MAP.PRIORITY, 999999)) AS PRIORITY  	 ");
            sql.append(" FROM      DMS_CONTENT_MUSIC_CATEGORY WITH (NOLOCK) INNER JOIN	 ");
            sql.append("    DMS_CONTENT_MUSIC_CATEGORY_MAP WITH (NOLOCK) ON DMS_CONTENT_MUSIC_CATEGORY.MUSIC_CATEGORY_ID = DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID RIGHT OUTER JOIN	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO WITH (NOLOCK) ON DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_ID = VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_ID	 ");
            sql.append(" WHERE (0=0) ");

            if (sListMusicCategoryId != null && !sListMusicCategoryId.trim().equals("")) {
                sql.append(" AND (DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID IN(" + sListMusicCategoryId + ")) ");
            }

//             else {
//                sql.append(" AND ((DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID IS NULL) OR ((DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID IS NULL) AND (DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID NOT IN(99001,99002,99003,99004)))  ");
//            }
            if (sKeyword != null && !sKeyword.trim().equals("")) {
                if (sKeyword.contains("*")) {
                    sKeyword = sKeyword.replaceAll("\\*", "%");
                }

                sql.append("AND (");
                sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME_EN " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_EN " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ISRC " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_NAME_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_LYRIC_TH " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.UPC_CODE " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append(")");
            }
            sql.append(" GROUP BY VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_PUBLISHING_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ISRC, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.MUSIC_NAME_EN, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.GENRE_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.GENRE_NAME, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_ID, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_TH, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ARTIST_NAME_EN, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.UPC_CODE, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_ARTIST_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ALBUM_ARTIST_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.RELEASE_DATE, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_ID, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_NAME_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LABEL_NAME_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_ORIGI_VOCAL_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_ORIGI_VOCAL_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_LYRIC_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_LYRIC_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_MELODY_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_MELODY_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MELODY_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MELODY_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MASTER_TH, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.COMP_OWNER_MASTER_EN, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.SHORT_LYRICS, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.LYRICS, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.URL_MASTER, VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.THUMBNAIL_URL, 	 ");
            sql.append("    VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.ONE_LINK_URL,VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.[COVER_OWNER_PERCENT_SHARING],VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO.[COVER_PARTNER_PERCENT_SHARING]	 ");
            sql.append("    ) AS TMP_VIEW_COVER 	 ");

            logger.info("listSearchMusicContentCover: " + sql.toString());
//            String sortBy = "PRIORITY,RELEASE_DATE";
//            String aDesc = "ASC";

//            if (sKeysort != null && !sKeysort.trim().equals("")) {
//                sortBy = sKeysort;
//                if (sSortBy != null && !sSortBy.trim().equals("") && sSortBy.trim().equalsIgnoreCase("asc")) {
//                    aDesc = "ASC";
//                }
//            }
            int maxRecord = 0;
            int totalPage = 0;
            int startRow = 0;
            int endRow = 0;

            ContentPagingInfo contentPagingInfo = queryContentPage(sql, pageIndex, limit);
            if (contentPagingInfo != null) {
                maxRecord = contentPagingInfo.getMaxRecord();
                totalPage = contentPagingInfo.getTotalPage();
                startRow = contentPagingInfo.getQueryStartRow();
                endRow = contentPagingInfo.getQueryEndRow();

                cmd = new StringBuilder();
                cmd.append("{call getContentPage(?,?,?,?)}");

                CallableStatement callStmt = this.con.prepareCall(cmd.toString());
                callStmt.setString(1, sql.toString());
                callStmt.setInt(2, startRow);
                callStmt.setInt(3, endRow);
                callStmt.setString(4, "ORDER BY PRIORITY,RELEASE_DATE DESC ");
                rs = callStmt.executeQuery();

                callStmt = null;
                cmd = null;

                if (rs != null) {
                    listInfo = new ArrayList<ManageSearchContentMusicCoverInfo>();
                    while (rs.next()) {
                        String releaseTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("RELEASE_DATE"));

                        String musicId = rs.getString("MUSIC_ID");
                        ManageSearchContentMusicCoverInfo info = new ManageSearchContentMusicCoverInfo();
                        info.setMusicId(musicId);
                        info.setMusicPublishingId(rs.getString("MUSIC_PUBLISHING_ID"));
                        info.setIsrc(rs.getString("ISRC"));
                        info.setTrackNameTh(rs.getString("MUSIC_NAME"));
                        info.setTrackNameEn(rs.getString("MUSIC_NAME_EN"));
                        info.setGenreId(rs.getInt("GENRE_ID"));
                        info.setGenreName(rs.getString("GENRE_NAME"));
                        info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                        info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
//
                        info.setUpcCode(rs.getString("UPC_CODE"));
                        info.setAlbumNameTh(rs.getString("ALBUM_NAME_TH"));
                        info.setAlbumNameEn(rs.getString("ALBUM_NAME_EN"));
                        info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                        info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
                        info.setLabelNameTh(rs.getString("LABEL_NAME_TH"));
                        info.setLabelNameEn(rs.getString("LABEL_NAME_EN"));
                        //
                        info.setCompOriginalVocalTh(rs.getString("COMP_ORIGI_VOCAL_TH"));
                        info.setCompOriginalVocalEn(rs.getString("COMP_ORIGI_VOCAL_EN"));
                        info.setCompLyricTh(rs.getString("COMP_LYRIC_TH"));
                        info.setCompLyricEn(rs.getString("COMP_LYRIC_EN"));
                        info.setCompMelodyTh(rs.getString("COMP_MELODY_TH"));
                        info.setCompMelodyEn(rs.getString("COMP_MELODY_EN"));
//                        
                        info.setCompOwnerTh(rs.getString("COMP_OWNER_TH"));
                        info.setCompOwnerEn(rs.getString("COMP_OWNER_EN"));
                        info.setCompOwnerMelodyTh(rs.getString("COMP_OWNER_MELODY_TH"));
                        info.setCompOwnerMelodyEn(rs.getString("COMP_OWNER_MELODY_EN"));
                        info.setCompOwnerMasterTh(rs.getString("COMP_OWNER_MASTER_TH"));
                        info.setCompOwnerMasterEn(rs.getString("COMP_OWNER_MASTER_EN"));
//                        
                        info.setShortLyrics(rs.getString("SHORT_LYRICS"));
                        info.setLyrics(rs.getString("LYRICS"));
                        info.setUrlMaster(rs.getString("URL_MASTER"));
                        info.setThumbnailUrl(rs.getString("THUMBNAIL_URL"));
                        info.setReleaseDate(releaseTime);

                        info.setCoverOwnerPercentSharing(rs.getDouble("COVER_OWNER_PERCENT_SHARING"));
                        info.setCoverPartnerPercentSharing(rs.getDouble("COVER_PARTNER_PERCENT_SHARING"));

                        ArrayList<DmsContentMusicCategoryMapInfo> listCmcInfo = listContentMusicCategoryMapInfo(musicId);
                        info.setListMusicCategoryMap(listCmcInfo);
                        listInfo.add(info);
                    }

                    rs = null;
                    pstmt = null;
                    callStmt = null;
                    sql = null;

                    cp = new ContentPage();
                    cp.setDataList(listInfo);
                    cp.setPageIndex(pageIndex);
                    cp.setTotalPages(totalPage);
                    cp.setTotalAmount(maxRecord);
                }

            }
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] listSearchMusicContentCover : ", ex);
        } finally {
            try {
                pstmt.close();
                pstmt = null;
                rs = null;
                sql = null;
            } catch (Exception ex) {
            }
        }
        logger.info("[DmsCoverSongDbMgr] listSearchMusicContentCover query use time: " + (System.currentTimeMillis() - startQuery) + " milisec.");
        return cp;
    }

    public ManageSearchContentMusicCoverInfo getMusicContentCover(String musicPublishingId) {
        ManageSearchContentMusicCoverInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null, rs2 = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT * FROM VIEW_CONTENT_MUSIC_PUBLISHING_COVER_SONG_INFO ");
            sql.append(" WHERE (MUSIC_PUBLISHING_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, musicPublishingId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String releaseTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("RELEASE_DATE"));

                info = new ManageSearchContentMusicCoverInfo();
                info.setMusicId(rs.getString("MUSIC_ID"));
                info.setMusicPublishingId(rs.getString("MUSIC_PUBLISHING_ID"));
                info.setIsrc(rs.getString("ISRC"));
                info.setTrackNameTh(rs.getString("MUSIC_NAME"));
                info.setTrackNameEn(rs.getString("MUSIC_NAME_EN"));
                info.setGenreId(rs.getInt("GENRE_ID"));
                info.setGenreName(rs.getString("GENRE_NAME"));
                info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
//
                info.setUpcCode(rs.getString("UPC_CODE"));
                info.setAlbumNameTh(rs.getString("ALBUM_NAME_TH"));
                info.setAlbumNameEn(rs.getString("ALBUM_NAME_EN"));
                info.setArtistNameTh(rs.getString("ARTIST_NAME_TH"));
                info.setArtistNameEn(rs.getString("ARTIST_NAME_EN"));
                info.setLabelNameTh(rs.getString("LABEL_NAME_TH"));
                info.setLabelNameEn(rs.getString("LABEL_NAME_EN"));
                //
                info.setCompOriginalVocalTh(rs.getString("COMP_ORIGI_VOCAL_TH"));
                info.setCompOriginalVocalEn(rs.getString("COMP_ORIGI_VOCAL_EN"));
                info.setCompLyricTh(rs.getString("COMP_LYRIC_TH"));
                info.setCompLyricEn(rs.getString("COMP_LYRIC_EN"));
                info.setCompMelodyTh(rs.getString("COMP_MELODY_TH"));
                info.setCompMelodyEn(rs.getString("COMP_MELODY_EN"));
//                        
                info.setCompOwnerTh(rs.getString("COMP_OWNER_TH"));
                info.setCompOwnerEn(rs.getString("COMP_OWNER_EN"));
                info.setCompOwnerMelodyTh(rs.getString("COMP_OWNER_MELODY_TH"));
                info.setCompOwnerMelodyEn(rs.getString("COMP_OWNER_MELODY_EN"));
                info.setCompOwnerMasterTh(rs.getString("COMP_OWNER_MASTER_TH"));
                info.setCompOwnerMasterEn(rs.getString("COMP_OWNER_MASTER_EN"));
//                        
                info.setShortLyrics(rs.getString("SHORT_LYRICS"));
                info.setLyrics(rs.getString("LYRICS"));
                info.setUrlMaster(rs.getString("URL_MASTER"));
                info.setThumbnailUrl(rs.getString("THUMBNAIL_URL"));
                info.setReleaseDate(releaseTime);

                info.setCoverOwnerPercentSharing(rs.getDouble("COVER_OWNER_PERCENT_SHARING"));
                info.setCoverPartnerPercentSharing(rs.getDouble("COVER_PARTNER_PERCENT_SHARING"));

                ArrayList<DmsContentMusicCategoryMapInfo> listCmcInfo = listContentMusicCategoryMapInfo(rs.getString("MUSIC_ID"));
                info.setListMusicCategoryMap(listCmcInfo);
            }
        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return info;
    }

    public ContentPage listSearchRequestCoverSongLog(String sKeyword, String channelId, Integer statusId, int pageIndex, int limit, String sKeysort, String sSortBy) {
        verifyConnection();
        ResultSet rs, rs2 = null;
        PreparedStatement pstmt = null;
        ArrayList<ManageRequestCoverSongLogInfo> listInfo = null;
        StringBuilder sql, cmd = new StringBuilder();
        ContentPage cp = null;

        long startQuery = System.currentTimeMillis();
        try {
            sql = new StringBuilder();
            sql.append(" SELECT [REQUEST_ID] ");
            sql.append(" ,[PREFIX_NAME] ");
            sql.append(" ,[NAME] ");
            sql.append(" ,[ARTIST_NAME] ");
            sql.append(" ,[ARTIST_ALIAS_NAME] ");
            sql.append(" ,[EMAIL] ");
            sql.append(" ,[TEL] ");
            sql.append(" ,[STATUS_ID] ");
            sql.append(" ,[PUBLISH_TIME] ");
            sql.append(" ,[CREATE_TIME] ");
            sql.append(" ,[UPDATE_TIME] ");
            sql.append(" ,[REQ_DOC_URL] ");
            sql.append(" ,[ACK_DOC_URL] ");
            sql.append(" ,[ACTION_REMARK] ");
            sql.append(" ,[ACTION_TIME] ");
            sql.append(" FROM DMS_YTM_REQUEST_COVER_SONG_LOG ");
            sql.append(" WHERE (0=0) AND (REQUEST_ID IN (SELECT REQUEST_ID FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP WHERE (CHANNEL_ID = '" + channelId + "')))");

            if (statusId != null) {
                sql.append(" AND (STATUS_ID = " + statusId + ") ");
            }

            if (sKeyword != null && !sKeyword.trim().equals("")) {
                if (sKeyword.contains("*")) {
                    sKeyword = sKeyword.replaceAll("\\*", "%");
                }

                sql.append("AND (");
                sql.append("    NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR ARTIST_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR ARTIST_ALIAS_NAME " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR EMAIL " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                sql.append("  OR TEL " + Constants.COLLATION_THAI_CASE_INSENSITIVE + " LIKE N'%" + sKeyword + "%'");
                if (Utilities.isNumeric(sKeyword)) {
                    sql.append("  OR (REQUEST_ID = " + sKeyword + ")");
                }
                sql.append(")");
            }

            logger.info("listSearchRequestCoverSongLog: " + sql.toString());
            String sortBy = "UPDATE_TIME";
            String aDesc = "DESC";

            if (sKeysort != null && !sKeysort.trim().equals("")) {
                sortBy = sKeysort;
                if (sSortBy != null && !sSortBy.trim().equals("") && sSortBy.trim().equalsIgnoreCase("asc")) {
                    aDesc = "ASC";
                }
            }

            int maxRecord = 0;
            int totalPage = 0;
            int startRow = 0;
            int endRow = 0;

            ContentPagingInfo contentPagingInfo = queryContentPage(sql, pageIndex, limit);
            if (contentPagingInfo != null) {
                maxRecord = contentPagingInfo.getMaxRecord();
                totalPage = contentPagingInfo.getTotalPage();
                startRow = contentPagingInfo.getQueryStartRow();
                endRow = contentPagingInfo.getQueryEndRow();

                cmd = new StringBuilder();
                cmd.append("{call getContentPage(?,?,?,?)}");

                CallableStatement callStmt = this.con.prepareCall(cmd.toString());
                callStmt.setString(1, sql.toString());
                callStmt.setInt(2, startRow);
                callStmt.setInt(3, endRow);
                callStmt.setString(4, "ORDER BY " + sortBy + " " + aDesc + "");
                rs = callStmt.executeQuery();

                callStmt = null;
                cmd = null;

                if (rs != null) {
                    listInfo = new ArrayList<ManageRequestCoverSongLogInfo>();
                    while (rs.next()) {
                        String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                        String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                        String publishTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("PUBLISH_TIME"));
                        String actionTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("ACTION_TIME"));

                        Long requestId = rs.getLong("REQUEST_ID");
                        ManageRequestCoverSongLogInfo info = new ManageRequestCoverSongLogInfo();
                        info.setRequestId(requestId);
                        info.setPrefixName(rs.getString("PREFIX_NAME"));
                        info.setName(rs.getString("NAME"));
                        info.setArtistName(rs.getString("ARTIST_NAME"));
                        info.setArtistAliasName(rs.getString("ARTIST_ALIAS_NAME"));
                        info.setEmail(rs.getString("EMAIL"));
                        info.setTel(rs.getString("TEL"));
                        info.setStatusId(rs.getInt("STATUS_ID"));
                        info.setPublishTime(publishTime);
                        info.setCreateTime(createTime);
                        info.setReqDocUrl(updateTime);
                        info.setAckDocUrl(rs.getString("ACK_DOC_URL"));
                        info.setReqDocUrl(rs.getString("REQ_DOC_URL"));
                        info.setActionRemark(rs.getString("ACTION_REMARK"));
                        info.setActionTime(actionTime);

                        ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicCover = null;
                        sql = new StringBuilder();
                        sql.append(" SELECT [REQUEST_ID] ");
                        sql.append(" ,[MUSIC_PUBLISHING_ID] ");
                        sql.append(" ,[PRIORITY] ");
                        sql.append(" ,[OWNER_PERCENT_SHARE] ");
                        sql.append(" ,[COVER_PERCENT_SHARE] ");
                        sql.append(" ,[CREATE_TIME] ");
                        sql.append(" ,[UPDATE_TIME] ");
                        sql.append(" FROM [DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP] ");
                        sql.append(" WHERE (REQUEST_ID = ?) ");
                        sql.append(" ORDER BY PRIORITY  ");

                        pstmt = con.prepareStatement(sql.toString());
                        pstmt.setLong(1, requestId);
                        rs2 = pstmt.executeQuery();
                        if (rs2 != null) {
                            listMusicCover = new ArrayList<DmsYtmRequestCoverSongMusicMapInfo>();
                            while (rs2.next()) {
                                String mCreateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                                String mUpdateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));

                                DmsYtmRequestCoverSongMusicMapInfo mInfo = new DmsYtmRequestCoverSongMusicMapInfo();
                                mInfo.setRequestId(requestId);
                                mInfo.setMusicPublishingId(rs2.getString("MUSIC_PUBLISHING_ID"));
                                mInfo.setPriority(rs2.getInt("PRIORITY"));
                                mInfo.setOwnerPercentShare(rs2.getDouble("OWNER_PERCENT_SHARE"));
                                mInfo.setCoverPercentShare(rs2.getDouble("COVER_PERCENT_SHARE"));
                                mInfo.setCreateTime(mCreateTime);
                                mInfo.setUpdateTime(mUpdateTime);
                                listMusicCover.add(mInfo);
                            }
                        }
                        info.setListMusicCover(listMusicCover);

                        ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelCover = null;
                        sql = new StringBuilder();
                        sql.append(" SELECT [REQUEST_ID]");
                        sql.append(" ,[CHANNEL_ID]");
                        sql.append(" ,[CHANNEL_NAME]");
                        sql.append(" ,[PRIORITY]");
                        sql.append(" ,[CREATE_TIME]");
                        sql.append(" ,[UPDATE_TIME]");
                        sql.append(" FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP");
                        sql.append(" WHERE (REQUEST_ID = ?) ");
                        sql.append(" ORDER BY PRIORITY  ");

                        pstmt = con.prepareStatement(sql.toString());
                        pstmt.setLong(1, requestId);
                        rs2 = pstmt.executeQuery();

                        sql = null;
                        pstmt = null;

                        if (rs2 != null) {
                            listChannelCover = new ArrayList<DmsYtmRequestConverSongYtChannelMapInfo>();
                            while (rs2.next()) {
                                String cCreateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                                String cUpdateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));

                                DmsYtmRequestConverSongYtChannelMapInfo cInfo = new DmsYtmRequestConverSongYtChannelMapInfo();
                                cInfo.setRequestId(requestId);
                                cInfo.setChannelId(rs2.getString("CHANNEL_ID"));
                                cInfo.setChannelName(rs2.getString("CHANNEL_NAME"));
                                cInfo.setPriority(rs2.getInt("PRIORITY"));
                                cInfo.setCreateTime(cCreateTime);
                                cInfo.setUpdateTime(cUpdateTime);
                                listChannelCover.add(cInfo);
                            }
                        }
                        info.setListChannelCover(listChannelCover);
                        listInfo.add(info);
                    }

                    rs = null;
                    pstmt = null;
                    callStmt = null;
                    sql = null;

                    cp = new ContentPage();
                    cp.setDataList(listInfo);
                    cp.setPageIndex(pageIndex);
                    cp.setTotalPages(totalPage);
                    cp.setTotalAmount(maxRecord);
                }

            }
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] listSearchRequestCoverSongLog : ", ex);
        } finally {
            try {
                pstmt.close();
                pstmt = null;
                rs = null;
                sql = null;
            } catch (Exception ex) {
            }
        }
        logger.info("[DmsCoverSongDbMgr] listSearchRequestCoverSongLog query use time: " + (System.currentTimeMillis() - startQuery) + " milisec.");
        return cp;
    }

    public ManageRequestCoverSongLogInfo getSearchRequestCoverSongLog(Long requestId) {
        ManageRequestCoverSongLogInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null, rs2 = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT [REQUEST_ID] ");
            sql.append(" ,[PREFIX_NAME] ");
            sql.append(" ,[NAME] ");
            sql.append(" ,[ARTIST_NAME] ");
            sql.append(" ,[ARTIST_ALIAS_NAME] ");
            sql.append(" ,[EMAIL] ");
            sql.append(" ,[TEL] ");
            sql.append(" ,[STATUS_ID] ");
            sql.append(" ,[PUBLISH_TIME] ");
            sql.append(" ,[CREATE_TIME] ");
            sql.append(" ,[UPDATE_TIME] ");
            sql.append(" ,[REQ_DOC_URL] ");
            sql.append(" ,[ACK_DOC_URL] ");
            sql.append(" ,[ACTION_REMARK] ");
            sql.append(" ,[ACTION_TIME] ");
            sql.append(" FROM DMS_YTM_REQUEST_COVER_SONG_LOG ");
            sql.append(" WHERE (REQUEST_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setLong(1, requestId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                String publishTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("PUBLISH_TIME"));
                String actionTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("ACTION_TIME"));

                info = new ManageRequestCoverSongLogInfo();
                info.setRequestId(requestId);
                info.setPrefixName(rs.getString("PREFIX_NAME"));
                info.setName(rs.getString("NAME"));
                info.setArtistName(rs.getString("ARTIST_NAME"));
                info.setArtistAliasName(rs.getString("ARTIST_ALIAS_NAME"));
                info.setEmail(rs.getString("EMAIL"));
                info.setTel(rs.getString("TEL"));
                info.setStatusId(rs.getInt("STATUS_ID"));
                info.setPublishTime(publishTime);
                info.setCreateTime(createTime);
                info.setReqDocUrl(updateTime);
                info.setAckDocUrl(rs.getString("ACK_DOC_URL"));
                info.setReqDocUrl(rs.getString("REQ_DOC_URL"));
                info.setActionRemark(rs.getString("ACTION_REMARK"));
                info.setActionTime(actionTime);

                ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicCover = null;
                sql = new StringBuilder();
                sql.append(" SELECT [REQUEST_ID] ");
                sql.append(" ,[MUSIC_PUBLISHING_ID] ");
                sql.append(" ,[PRIORITY] ");
                sql.append(" ,[OWNER_PERCENT_SHARE] ");
                sql.append(" ,[COVER_PERCENT_SHARE] ");
                sql.append(" ,[CREATE_TIME] ");
                sql.append(" ,[UPDATE_TIME] ");
                sql.append(" FROM [DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP] ");
                sql.append(" WHERE (REQUEST_ID = ?) ");
                sql.append(" ORDER BY PRIORITY  ");

                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(1, requestId);
                rs2 = pstmt.executeQuery();
                if (rs2 != null) {
                    listMusicCover = new ArrayList<DmsYtmRequestCoverSongMusicMapInfo>();
                    while (rs2.next()) {
                        String mCreateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                        String mUpdateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));

                        DmsYtmRequestCoverSongMusicMapInfo mInfo = new DmsYtmRequestCoverSongMusicMapInfo();
                        mInfo.setRequestId(requestId);
                        mInfo.setMusicPublishingId(rs2.getString("MUSIC_PUBLISHING_ID"));
                        mInfo.setPriority(rs2.getInt("PRIORITY"));
                        mInfo.setOwnerPercentShare(rs2.getDouble("OWNER_PERCENT_SHARE"));
                        mInfo.setCoverPercentShare(rs2.getDouble("COVER_PERCENT_SHARE"));
                        mInfo.setCreateTime(mCreateTime);
                        mInfo.setUpdateTime(mUpdateTime);
                        listMusicCover.add(mInfo);
                    }
                }
                info.setListMusicCover(listMusicCover);

                ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelCover = null;
                sql = new StringBuilder();
                sql.append(" SELECT [REQUEST_ID]");
                sql.append(" ,[CHANNEL_ID]");
                sql.append(" ,[CHANNEL_NAME]");
                sql.append(" ,[PRIORITY]");
                sql.append(" ,[CREATE_TIME]");
                sql.append(" ,[UPDATE_TIME]");
                sql.append(" FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP");
                sql.append(" WHERE (REQUEST_ID = ?) ");
                sql.append(" ORDER BY PRIORITY  ");

                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(1, requestId);
                rs2 = pstmt.executeQuery();

                sql = null;
                pstmt = null;

                if (rs2 != null) {
                    listChannelCover = new ArrayList<DmsYtmRequestConverSongYtChannelMapInfo>();
                    while (rs2.next()) {
                        String cCreateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                        String cUpdateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));

                        DmsYtmRequestConverSongYtChannelMapInfo cInfo = new DmsYtmRequestConverSongYtChannelMapInfo();
                        cInfo.setRequestId(requestId);
                        cInfo.setChannelId(rs2.getString("CHANNEL_ID"));
                        cInfo.setChannelName(rs2.getString("CHANNEL_NAME"));
                        cInfo.setPriority(rs2.getInt("PRIORITY"));
                        cInfo.setCreateTime(cCreateTime);
                        cInfo.setUpdateTime(cUpdateTime);
                        listChannelCover.add(cInfo);
                    }
                }
                info.setListChannelCover(listChannelCover);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return info;
    }

    public int updateRequestCoverSongLog(Long requestId,
            String prefixName,
            String requestName,
            String artistName,
            String artistAliasName,
            String requestEmail,
            String requestTel,
            Integer statusId,
            String publishTime,
            String reqDocUrl,
            String ackDocUrl,
            String actionRemark,
            String approveTime,
            ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelCover,
            ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicCover) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        setAutoCommit(false);
        try {
            sql = new StringBuffer();
            sql.append(" UPDATE DMS_YTM_REQUEST_COVER_SONG_LOG");
            sql.append(" SET  UPDATE_TIME = GETDATE()");

            if (prefixName != null && !prefixName.equals("")) {
                sql.append(" , PREFIX_NAME = '" + prefixName + "'");
            }

            if (requestName != null && !requestName.equals("")) {
                sql.append(" , NAME = '" + requestName + "'");
            }

            if (artistName != null && !artistName.equals("")) {
                sql.append(" , ARTIST_NAME = '" + artistName + "'");
            }

            if (artistAliasName != null && !artistAliasName.equals("")) {
                sql.append(" , ARTIST_ALIAS_NAME = '" + artistAliasName + "'");
            }

            if (requestEmail != null && !requestEmail.equals("")) {
                sql.append(" , EMAIL = '" + requestEmail + "'");
            }

            if (requestTel != null && !requestTel.equals("")) {
                sql.append(" , TEL = '" + requestTel + "'");
            }

            if (statusId != null && !statusId.equals("")) {
                sql.append(" , STATUS_ID = " + statusId + "");
            }

            if (publishTime != null && !publishTime.equals("")) {
                sql.append(" , PUBLISH_TIME = '" + publishTime + "'");
            }

            if (reqDocUrl != null && !reqDocUrl.equals("")) {
                sql.append(" , REQ_DOC_URL = '" + reqDocUrl + "'");
            }

            if (ackDocUrl != null && !ackDocUrl.equals("")) {
                sql.append(" , ACK_DOC_URL = '" + ackDocUrl + "'");
            }

            if (actionRemark != null && !actionRemark.equals("")) {
                sql.append(" , ACTION_REMARK = '" + actionRemark + "'");
            }

            if (approveTime != null && !approveTime.equals("")) {
                sql.append(" , ACTION_TIME = '" + approveTime + "'");
            }

            sql.append(" WHERE (REQUEST_ID = ?)");
            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setLong(i, requestId);
            resullt = pstmt.executeUpdate();

            if (listChannelCover != null && !listChannelCover.isEmpty()) {
                sql = new StringBuffer();
                sql.append(" DELETE FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP");
                sql.append(" WHERE (REQUEST_ID = ?) ");
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(1, requestId);
                resullt = pstmt.executeUpdate();
                for (DmsYtmRequestConverSongYtChannelMapInfo channelCoverMapInfo : listChannelCover) {
                    sql = new StringBuffer();
                    sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP ");
                    sql.append(" (REQUEST_ID, CHANNEL_ID, CHANNEL_NAME, PRIORITY, CREATE_TIME, UPDATE_TIME) ");
                    sql.append(" VALUES (?,?,?,?,GETDATE(),GETDATE()) ");
                    i = 1;
                    pstmt = con.prepareStatement(sql.toString());
                    pstmt.setLong(i++, requestId);
                    pstmt.setString(i++, channelCoverMapInfo.getChannelId());
                    pstmt.setString(i++, channelCoverMapInfo.getChannelName());
                    pstmt.setInt(i++, channelCoverMapInfo.getPriority());
                    resullt = pstmt.executeUpdate();
                }
            }

            if (listMusicCover != null && !listMusicCover.isEmpty()) {
                sql = new StringBuffer();
                sql.append(" DELETE FROM DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP");
                sql.append(" WHERE (REQUEST_ID = ?) ");
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(1, requestId);
                resullt = pstmt.executeUpdate();
                for (DmsYtmRequestCoverSongMusicMapInfo musicCoverMapInfo : listMusicCover) {
                    sql = new StringBuffer();
                    sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP ");
                    sql.append(" (REQUEST_ID, MUSIC_PUBLISHING_ID, PRIORITY, OWNER_PERCENT_SHARE, COVER_PERCENT_SHARE, CREATE_TIME, UPDATE_TIME) ");
                    sql.append(" VALUES (?,?,?,?,?,GETDATE(),GETDATE()) ");
                    i = 1;
                    pstmt = con.prepareStatement(sql.toString());
                    pstmt.setLong(i++, requestId);
                    pstmt.setString(i++, musicCoverMapInfo.getMusicPublishingId());
                    pstmt.setInt(i++, musicCoverMapInfo.getPriority());
                    pstmt.setDouble(i++, musicCoverMapInfo.getOwnerPercentShare());
                    pstmt.setDouble(i++, musicCoverMapInfo.getCoverPercentShare());
                    resullt = pstmt.executeUpdate();
                }
            }
            this.makeCommit();
        } catch (Exception ex) {
            resullt = -1;
            this.makeRollBack();
            logger.error("[DmsCoverSongDbMgr] updateRequestCoverSongLog", ex);
        } finally {
            this.setAutoCommit(true);
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int insertRequestCoverSongLog(Long requestId,
            String prefixName,
            String requestName,
            String artistName,
            String artistAliasName,
            String requestEmail,
            String requestTel,
            Integer statusId,
            String publishTime,
            String reqDocUrl,
            String ackDocUrl,
            String actionRemark,
            Integer accountManagerId,
            ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelCover,
            ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicCover) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        setAutoCommit(false);
        try {
            sql = new StringBuffer();
            sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_LOG ");
            sql.append(" (REQUEST_ID,PREFIX_NAME, NAME, ARTIST_NAME , ARTIST_ALIAS_NAME, EMAIL, TEL, STATUS_ID, CREATE_TIME, UPDATE_TIME, ACTION_REMARK, ACCOUNT_ID) ");
            sql.append(" VALUES (?,?,?,?,?,?,?,?,GETDATE(),GETDATE(),?,?) ");
            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setLong(i++, requestId);
            pstmt.setString(i++, prefixName);
            pstmt.setString(i++, requestName);
            pstmt.setString(i++, artistName);
            pstmt.setString(i++, artistAliasName);
            pstmt.setString(i++, requestEmail);
            pstmt.setString(i++, requestTel);
            pstmt.setInt(i++, statusId);
            pstmt.setString(i++, actionRemark);
            if (accountManagerId != null) {
                pstmt.setInt(i++, accountManagerId);
            } else {
                pstmt.setNull(i++, Types.INTEGER);
            }
            resullt = pstmt.executeUpdate();

            for (DmsYtmRequestConverSongYtChannelMapInfo channelCoverMapInfo : listChannelCover) {
                sql = new StringBuffer();
                sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP ");
                sql.append(" (REQUEST_ID, CHANNEL_ID, CHANNEL_NAME, PRIORITY, CREATE_TIME, UPDATE_TIME) ");
                sql.append(" VALUES (?,?,?,?,GETDATE(),GETDATE()) ");
                i = 1;
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(i++, requestId);
                pstmt.setString(i++, channelCoverMapInfo.getChannelId());
                pstmt.setString(i++, channelCoverMapInfo.getChannelName());
                pstmt.setInt(i++, channelCoverMapInfo.getPriority());
                resullt = pstmt.executeUpdate();
            }

            for (DmsYtmRequestCoverSongMusicMapInfo musicCoverMapInfo : listMusicCover) {
                sql = new StringBuffer();
                sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP ");
                sql.append(" (REQUEST_ID, MUSIC_PUBLISHING_ID, PRIORITY, OWNER_PERCENT_SHARE, COVER_PERCENT_SHARE, CREATE_TIME, UPDATE_TIME) ");
                sql.append(" VALUES (?,?,?,?,?,GETDATE(),GETDATE()) ");
                i = 1;
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(i++, requestId);
                pstmt.setString(i++, musicCoverMapInfo.getMusicPublishingId());
                pstmt.setInt(i++, musicCoverMapInfo.getPriority());
                pstmt.setDouble(i++, musicCoverMapInfo.getOwnerPercentShare());
                pstmt.setDouble(i++, musicCoverMapInfo.getCoverPercentShare());
                resullt = pstmt.executeUpdate();
            }

            this.makeCommit();
        } catch (Exception ex) {
            resullt = -1;
            this.makeRollBack();
            logger.error("[DmsCoverSongDbMgr] insertRequestCoverSongLog", ex);
        } finally {
            this.setAutoCommit(true);
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int updateRequestConverSongYtChannelMap(Long requestId,
            ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelCover) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" DELETE FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP");
            sql.append(" WHERE (REQUEST_ID = ?) ");
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setLong(1, requestId);
            resullt = pstmt.executeUpdate();
            for (DmsYtmRequestConverSongYtChannelMapInfo channelCoverMapInfo : listChannelCover) {
                sql = new StringBuffer();
                sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP ");
                sql.append(" (REQUEST_ID, CHANNEL_ID, CHANNEL_NAME, PRIORITY, CREATE_TIME, UPDATE_TIME) ");
                sql.append(" VALUES (?,?,?,?,GETDATE(),GETDATE()) ");
                int i = 1;
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(i++, requestId);
                pstmt.setString(i++, channelCoverMapInfo.getChannelId());
                pstmt.setString(i++, channelCoverMapInfo.getChannelName());
                pstmt.setInt(i++, channelCoverMapInfo.getPriority());
                resullt = pstmt.executeUpdate();
            }
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] updateRequestCoverSongLog", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int updateRequestCoverSongMusicMap(Long requestId,
            ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicCover) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" DELETE FROM DMS_YTM_REQUEST_COVER_SONG_YT_CHANNEL_MAP");
            sql.append(" WHERE (REQUEST_ID = ?) ");
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setLong(1, requestId);
            resullt = pstmt.executeUpdate();
            for (DmsYtmRequestCoverSongMusicMapInfo musicCoverMapInfo : listMusicCover) {
                sql = new StringBuffer();
                sql.append(" INSERT INTO DMS_YTM_REQUEST_COVER_SONG_MUSIC_MAP ");
                sql.append(" (REQUEST_ID, MUSIC_PUBLISHING_ID, PRIORITY, OWNER_PERCENT_SHARE, COVER_PERCENT_SHARE, CREATE_TIME, UPDATE_TIME) ");
                sql.append(" VALUES (?,?,?,?,?,GETDATE(),GETDATE()) ");
                int i = 1;
                pstmt = con.prepareStatement(sql.toString());
                pstmt.setLong(i++, requestId);
                pstmt.setString(i++, musicCoverMapInfo.getMusicPublishingId());
                pstmt.setInt(i++, musicCoverMapInfo.getPriority());
                pstmt.setDouble(i++, musicCoverMapInfo.getOwnerPercentShare());
                pstmt.setDouble(i++, musicCoverMapInfo.getCoverPercentShare());
                resullt = pstmt.executeUpdate();
            }
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] updateRequestCoverSongLog", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public ManageYoutubeChannelInfo getYoutubeChannelInfo(String channelId) {
        ManageYoutubeChannelInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" SELECT YTM_YOUTUBE_CHANNEL.[CHANNEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_NAME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_SHORT_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_IMAGE_URL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_ACTIVE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LABEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_TYPE_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_UGC_CHANNEL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_NOT_MONITOR] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LINKED_DATE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CREATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[UPDATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_LINKED] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[KEYWORDS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[COUNTRY] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[JOINED_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_SUBSCRIBER] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIDEOS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIEWS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_THUMBNAIL_URL] ");
//
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PREFIX_NAME] AS REQUEST_PREFIX_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[NAME] AS REQUEST_COVER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[ALIAS_NAME] AS REQUEST_COVER_ALIAS_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[EMAIL] AS REQUEST_COVER_EMAIL ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TEL] AS REQUEST_COVER_TEL ");
//
            sql.append("       ,CMS_USER.[USER_NAME] ");
            sql.append("       ,CMS_USER.DISPLAY_NAME ");
            sql.append("       ,CMS_USER.EMAIL ");
            sql.append("   FROM  YTM_YOUTUBE_CHANNEL   WITH (NOLOCK) INNER JOIN  ");
            sql.append("    CMS_USER WITH (NOLOCK) ON CMS_USER.USER_ID = YTM_YOUTUBE_CHANNEL.USER_ID   ");
            sql.append("   WHERE (YTM_YOUTUBE_CHANNEL.CHANNEL_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, channelId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                String linkedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("LINKED_DATE"));
                String joinedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("JOINED_TIME"));

                String channelName = rs.getString("CHANNEL_NAME");
                String channelShortDescription = rs.getString("CHANNEL_SHORT_DESCRIPTION");
                String channelDescription = rs.getString("CHANNEL_DESCRIPTION");
                String channelImageUrl = rs.getString("CHANNEL_IMAGE_URL");
                Boolean isActive = rs.getBoolean("IS_ACTIVE");
                Integer labelId = rs.getInt("LABEL_ID");
                Integer channelUserId = rs.getInt("USER_ID");
                String username = rs.getString("USER_NAME");
                String displayName = rs.getString("DISPLAY_NAME");
                String email = rs.getString("EMAIL");
                Integer channelTypeId = rs.getInt("CHANNEL_TYPE_ID");
                Boolean isUgcChannel = rs.getBoolean("IS_UGC_CHANNEL");
                Boolean isNotMonitor = rs.getBoolean("IS_NOT_MONITOR");

                Boolean isLinked = rs.getBoolean("IS_LINKED");
                String keywords = rs.getString("KEYWORDS");
                String country = rs.getString("COUNTRY");
                Integer totalSubscriber = rs.getInt("TOTAL_SUBSCRIBER");
                Integer totalVideos = rs.getInt("TOTAL_VIDEOS");
                Integer totalViews = rs.getInt("TOTAL_VIEWS");
                String channelThumbnailUrl = rs.getString("CHANNEL_THUMBNAIL_URL");

                String requestPrefixName = rs.getString("REQUEST_PREFIX_NAME");
                String requestCoverName = rs.getString("REQUEST_COVER_NAME");
                String requestCoverAliasName = rs.getString("REQUEST_COVER_ALIAS_NAME");
                String requestCoverEmail = rs.getString("REQUEST_COVER_EMAIL");
                String requestCoverTel = rs.getString("REQUEST_COVER_TEL");

                info = new ManageYoutubeChannelInfo();
                info.setChannelId(channelId);
                info.setChannelName(channelName);
                info.setChannelShortDescription(channelShortDescription);
                info.setChannelDescription(channelDescription);
                info.setChannelImageUrl(channelImageUrl);
                info.setIsActive(isActive);
                info.setLabelUserId(channelUserId);
                info.setChannelUserId(channelUserId);
                info.setChannelTypeId(channelTypeId);
                info.setIsUgcChannel(isUgcChannel);
                info.setIsNotMonitor(isNotMonitor);
                info.setCreateTime(createTime);
                info.setUpdateTime(updateTime);

                info.setDisplayName(displayName);
                info.setChannelAccountManagerName(username);
                info.setChannelAccountEmail(email);

                info.setIsLinked(isLinked);
                info.setKeywords(keywords);
                info.setCountry(country);
                info.setTotalSubscripber(totalSubscriber);
                info.setTotalVideo(totalVideos);
                info.setTotalViews(totalViews);
                info.setJoinedTime(joinedTime);
                info.setChannelThumbnailUrl(channelThumbnailUrl);

                info.setPrefixName(requestPrefixName);
                info.setName(requestCoverName);
                info.setAliasName(requestCoverAliasName);
                info.setEmail(requestCoverEmail);
                info.setTel(requestCoverTel);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return info;
    }

    public int updateYoutubeChannelInfo(String sChannelId,
            String channelName,
            String prefixName,
            String name,
            String aliasName,
            String email,
            String tel,
            String parentChannelId) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" UPDATE YTM_YOUTUBE_CHANNEL ");
            sql.append(" SET  UPDATE_TIME = GETDATE() ");
            if (channelName != null && !channelName.equals("")) {
                sql.append(" , CHANNEL_NAME = '" + channelName + "' ");
            }

            if (prefixName != null && !prefixName.equals("")) {
                sql.append(" , PREFIX_NAME = '" + prefixName + "' ");
            }

            if (name != null && !name.equals("")) {
                sql.append(" , NAME = '" + name + "' ");
            }

            if (aliasName != null && !aliasName.equals("")) {
                sql.append(" , ALIAS_NAME = '" + aliasName + "' ");
            }

            if (email != null && !email.equals("")) {
                sql.append(" , EMAIL = '" + email + "' ");
            }

            if (tel != null && !tel.equals("")) {
                sql.append(" , TEL = '" + tel + "' ");
            }

            if (parentChannelId != null && !parentChannelId.equals("")) {
                sql.append(" , PARENT_CHANNEL_ID = '" + parentChannelId + "' ");
            }

            sql.append(" WHERE (CHANNEL_ID = '" + sChannelId + "') ");

            pstmt = con.prepareStatement(sql.toString());
            resullt = pstmt.executeUpdate();
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] updateYoutubeChannelInfo", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int insertYoutubeChannelInfo(String sChannelId,
            String channelName,
            Integer userId,
            String prefixName,
            String name,
            String aliasName,
            String email,
            String tel,
            String parentChannelId) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" INSERT INTO YTM_YOUTUBE_CHANNEL (CHANNEL_ID, CHANNEL_NAME, USER_ID, PREFIX_NAME, NAME, ALIAS_NAME, EMAIL, TEL, PARENT_CHANNEL_ID ,CREATE_TIME, UPDATE_TIME) ");
            sql.append(" VALUES(?, ?, ?, ?, ?, ?, ?, ?, ");
            if (parentChannelId != null && !parentChannelId.trim().equals("")) {
                sql.append("  ,?) ");
            }
            sql.append(" ,GETDATE(), GETDATE()) ");
            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(i++, sChannelId);
            pstmt.setString(i++, channelName);
            pstmt.setInt(i++, userId);
            pstmt.setString(i++, prefixName);
            pstmt.setString(i++, name);
            pstmt.setString(i++, aliasName);
            pstmt.setString(i++, email);
            pstmt.setString(i++, tel);
            if (parentChannelId != null && !parentChannelId.trim().equals("")) {
                pstmt.setString(i++, parentChannelId);
            }
            resullt = pstmt.executeUpdate();
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] insertYoutubeChannelInfo", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public ArrayList<DmsContentMusicCategoryMapInfo> listContentMusicCategoryMapInfo(String musicId) {
        ArrayList<DmsContentMusicCategoryMapInfo> listCmcInfo = null;
        PreparedStatement pstmtInner = null;
        ResultSet rsInner = null;
        StringBuilder sql = null;
        try {
            sql = new StringBuilder();
            sql.append(" SELECT  DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_MAP_ID, DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_ID, DMS_CONTENT_MUSIC_CATEGORY_MAP.PRIORITY,  ");
            sql.append("       DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID, DMS_CONTENT_MUSIC_CATEGORY.MUSIC_CATEGORY_NAME ");
            sql.append(" FROM     DMS_CONTENT_MUSIC_CATEGORY_MAP INNER JOIN ");
            sql.append("       DMS_CONTENT_MUSIC_CATEGORY ON DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_CATEGORY_ID = DMS_CONTENT_MUSIC_CATEGORY.MUSIC_CATEGORY_ID ");
            sql.append(" WHERE  (DMS_CONTENT_MUSIC_CATEGORY_MAP.MUSIC_ID = ?) ");
            sql.append(" ORDER BY DMS_CONTENT_MUSIC_CATEGORY_MAP.PRIORITY ");

            pstmtInner = con.prepareStatement(sql.toString());
            pstmtInner.setString(1, musicId);
            rsInner = pstmtInner.executeQuery();
            if (rsInner != null) {
                listCmcInfo = new ArrayList<DmsContentMusicCategoryMapInfo>();
                while (rsInner.next()) {
                    DmsContentMusicCategoryMapInfo cmcInfo = new DmsContentMusicCategoryMapInfo();
                    cmcInfo.setMusicId(musicId);
                    cmcInfo.setPriority(rsInner.getInt("PRIORITY"));
                    cmcInfo.setMusicCategoryId(rsInner.getInt("MUSIC_CATEGORY_ID"));
                    cmcInfo.setMusicCategoryName(rsInner.getString("MUSIC_CATEGORY_NAME"));
                    listCmcInfo.add(cmcInfo);
                }
            }
        } catch (Exception ex) {
        } finally {
            try {
                closeResultSet(rsInner);
                closeStatement(pstmtInner);
            } catch (Exception ex) {
            }
            pstmtInner = null;
            rsInner = null;
        }

        return listCmcInfo;
    }

    private ContentPagingInfo queryContentPage(StringBuilder sql, int pageIndex, int limit) {
        ContentPagingInfo result = null;
        StringBuilder sqlMax = new StringBuilder();
        sqlMax.append("SELECT COUNT(*) FROM(" + sql.toString() + ")AS TMP");
        try {
            int maxRecord = this.getMaxRecord(sqlMax.toString());
            int totalPage = 0;

            if (maxRecord < limit) {
                totalPage = 1;
            } else {
                double tmp = (double) maxRecord / (double) limit;
                totalPage = (int) Math.ceil(tmp);
            }

            int startRow = 1;
            int endRow = maxRecord;
            if (pageIndex == 1) {
                startRow = 1;
                endRow = limit;
            } else if (pageIndex > 1) {
                startRow = (limit * (pageIndex - 1)) + 1;
                endRow = (limit * (pageIndex - 1)) + limit;
            }
            result = new ContentPagingInfo(pageIndex, limit, totalPage, maxRecord, startRow, endRow);
        } catch (Exception ex) {
            logger.error("[DmsCoverSongDbMgr] queryContentPage : ", ex);
        }

        return result;
    }

    public int execDmsCoverSongSendEmail(Long requestId) {
        int status = -1;
        StringBuilder sql = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        try {
            sql = new StringBuilder();
            sql.append("{CALL EXEC_DMS_COVERSONG_SEND_EMAIL(?)}");
            CallableStatement callStmt = this.con.prepareCall(sql.toString());
            callStmt.setLong(1, requestId);
            callStmt.executeQuery();
            status = 1;
        } catch (Exception ex) {
            logger.error("DmsCoverSongDbMgr [execDmsCoverSongSendEmail] ", ex);
        } finally {
            closeStatement(pstmt);
            sql = null;
            pstmt = null;
            sql = null;
        }
        return status;
    }

}
