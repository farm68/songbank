/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.db;

import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.db.*;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import org.apache.log4j.Logger;

/**
 *
 * @author DELL
 */
public class DmsYoutubeDbMgr extends DBManager {
//public class DmsYoutubeDbMgr extends DataSourceManager {

    private static Logger logger = Logger.getLogger(DmsYoutubeDbMgr.class);

    /**
     * The database connection name.
     */
    static private String DB_CONNECTION_NAME = Constants.DB_CONNECTION_NAME;
//    static private String DB_CONNECTION_NAME = Constants.DATASOURCE_NAME;

    /**
     * Default Constructer.
     */
    public DmsYoutubeDbMgr() {
        this(Constants.DB_TIMEOUT, Constants.DB_CURSOR_SIZE);
    }

    public DmsYoutubeDbMgr(long aTimeOut, int aCursorSize) {
        super(DB_CONNECTION_NAME, aTimeOut, aCursorSize);
    }

    public DmsYoutubeDbMgr(String dbname, long aTimeOut, int aCursorSize) {
        super(dbname, aTimeOut, aCursorSize);
    }

    public ManageYoutubeChannelInfo getYoutubeChannelInfo(String channelId) {
        ManageYoutubeChannelInfo info = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" SELECT YTM_YOUTUBE_CHANNEL.[CHANNEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_NAME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_SHORT_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_IMAGE_URL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_ACTIVE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LABEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_TYPE_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_UGC_CHANNEL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_NOT_MONITOR] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LINKED_DATE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CREATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[UPDATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_LINKED] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[KEYWORDS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[COUNTRY] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[JOINED_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_SUBSCRIBER] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIDEOS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIEWS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_THUMBNAIL_URL] ");
//          
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_NAME] AS REQUEST_USER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PASSWORD] AS REQUEST_PASSWORD ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PREFIX_NAME] AS REQUEST_PREFIX_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[NAME] AS REQUEST_COVER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[ALIAS_NAME] AS REQUEST_COVER_ALIAS_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[EMAIL] AS REQUEST_COVER_EMAIL ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TEL] AS REQUEST_COVER_TEL ");
//
            sql.append("       ,CMS_USER.[USER_NAME] ");
            sql.append("       ,CMS_USER.DISPLAY_NAME ");
            sql.append("       ,CMS_USER.EMAIL ");
            sql.append("   FROM  YTM_YOUTUBE_CHANNEL   WITH (NOLOCK) INNER JOIN  ");
            sql.append("    CMS_USER WITH (NOLOCK) ON CMS_USER.USER_ID = YTM_YOUTUBE_CHANNEL.USER_ID   ");
            sql.append("   WHERE (YTM_YOUTUBE_CHANNEL.CHANNEL_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, channelId);
            rs = pstmt.executeQuery();
            if (rs != null && rs.next()) {
                String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                String linkedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("LINKED_DATE"));
                String joinedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("JOINED_TIME"));

                String channelName = rs.getString("CHANNEL_NAME");
                String channelShortDescription = rs.getString("CHANNEL_SHORT_DESCRIPTION");
                String channelDescription = rs.getString("CHANNEL_DESCRIPTION");
                String channelImageUrl = rs.getString("CHANNEL_IMAGE_URL");
                Boolean isActive = rs.getBoolean("IS_ACTIVE");
                Integer labelId = rs.getInt("LABEL_ID");
                Integer channelUserId = rs.getInt("USER_ID");
                String username = rs.getString("USER_NAME");
                String displayName = rs.getString("DISPLAY_NAME");
                String email = rs.getString("EMAIL");
                Integer channelTypeId = rs.getInt("CHANNEL_TYPE_ID");
                Boolean isUgcChannel = rs.getBoolean("IS_UGC_CHANNEL");
                Boolean isNotMonitor = rs.getBoolean("IS_NOT_MONITOR");

                Boolean isLinked = rs.getBoolean("IS_LINKED");
                String keywords = rs.getString("KEYWORDS");
                String country = rs.getString("COUNTRY");
                Integer totalSubscriber = rs.getInt("TOTAL_SUBSCRIBER");
                Integer totalVideos = rs.getInt("TOTAL_VIDEOS");
                Integer totalViews = rs.getInt("TOTAL_VIEWS");
                String channelThumbnailUrl = rs.getString("CHANNEL_THUMBNAIL_URL");

                String requestUsername = rs.getString("REQUEST_USER_NAME");
                String requestPassword = rs.getString("REQUEST_PASSWORD");
                String requestPrefixName = rs.getString("REQUEST_PREFIX_NAME");
                String requestCoverName = rs.getString("REQUEST_COVER_NAME");
                String requestCoverAliasName = rs.getString("REQUEST_COVER_ALIAS_NAME");
                String requestCoverEmail = rs.getString("REQUEST_COVER_EMAIL");
                String requestCoverTel = rs.getString("REQUEST_COVER_TEL");

                info = new ManageYoutubeChannelInfo();
                info.setChannelId(channelId);
                info.setChannelName(channelName);
                info.setChannelShortDescription(channelShortDescription);
                info.setChannelDescription(channelDescription);
                info.setChannelImageUrl(channelImageUrl);
                info.setIsActive(isActive);
                info.setLabelUserId(channelUserId);
                info.setChannelUserId(channelUserId);
                info.setChannelTypeId(channelTypeId);
                info.setIsUgcChannel(isUgcChannel);
                info.setIsNotMonitor(isNotMonitor);
                info.setCreateTime(createTime);
                info.setUpdateTime(updateTime);

                info.setDisplayName(displayName);
                info.setChannelAccountManagerName(username);
                info.setChannelAccountEmail(email);

                info.setIsLinked(isLinked);
                info.setKeywords(keywords);
                info.setCountry(country);
                info.setTotalSubscripber(totalSubscriber);
                info.setTotalVideo(totalVideos);
                info.setTotalViews(totalViews);
                info.setJoinedTime(joinedTime);
                info.setChannelThumbnailUrl(channelThumbnailUrl);

                info.setUsername(requestUsername);
                info.setPassword(requestPassword);
                info.setPrefixName(requestPrefixName);
                info.setName(requestCoverName);
                info.setAliasName(requestCoverAliasName);
                info.setEmail(requestCoverEmail);
                info.setTel(requestCoverTel);
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return info;
    }

    public int updateYoutubeChannelInfo(String sChannelId,
            String channelName,
            String channelDes,
            String channelKeywords,
            String channelImageUrl,
            String channelThumbnailUrl,
            int isLinked,
            String country,
            String joinedTime,
            int totalSubscriber,
            int totalVideo,
            int totalViews) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" UPDATE YTM_YOUTUBE_CHANNEL ");
            sql.append(" SET  UPDATE_TIME = GETDATE() ");
            if (channelName != null && !channelName.equals("")) {
                sql.append(" , CHANNEL_NAME = '" + channelName + "' ");
            }

            if (channelDes != null && !channelDes.equals("")) {
                sql.append(" , CHANNEL_DESCRIPTION = '" + channelDes + "' ");
            }

            if (channelKeywords != null && !channelKeywords.equals("")) {
                sql.append(" , KEYWORDS = '" + channelKeywords + "' ");
            }

            if (channelImageUrl != null && !channelImageUrl.equals("")) {
                sql.append(" , CHANNEL_IMAGE_URL = '" + channelImageUrl + "' ");
            }

            if (channelThumbnailUrl != null && !channelThumbnailUrl.equals("")) {
                sql.append(" , CHANNEL_THUMBNAIL_URL = '" + channelImageUrl + "' ");
            }

            sql.append(" , IS_LINKED = " + isLinked + " ");

            if (country != null && !country.equals("")) {
                sql.append(" , COUNTRY = '" + country + "' ");
            }

            if (joinedTime != null && !joinedTime.equals("")) {
                sql.append(" , JOINED_TIME = '" + joinedTime + "' ");
            }

            if (totalSubscriber >= 0) {
                sql.append(" , TOTAL_SUBSCRIBER = " + totalSubscriber + " ");
            }

            if (totalVideo >= 0) {
                sql.append(" , TOTAL_VIDEOS = " + totalVideo + " ");
            }

            if (totalViews >= 0) {
                sql.append(" , TOTAL_VIEWS = " + totalViews + " ");
            }
            sql.append(" WHERE (CHANNEL_ID = '" + sChannelId + "') ");

            pstmt = con.prepareStatement(sql.toString());
            resullt = pstmt.executeUpdate();
        } catch (Exception ex) {
            logger.error("[DmsYoutubeDbMgr] updateYoutubeChannelInfo", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public int updateArtistPortalInfo(String artistPortalId,
            String userDisplayName,
            String userImageUrl,
            String userEmail) {
        Integer resullt = -1;
        verifyConnection();
        PreparedStatement pstmt = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" UPDATE DMS_ARTIST_PORTAL ");
            sql.append(" SET  UPDATE_TIME = GETDATE() ");
            if (userDisplayName != null && !userDisplayName.equals("")) {
                sql.append(" , DISPLAY_NAME = '" + userDisplayName + "' ");
            }

            if (userImageUrl != null && !userImageUrl.equals("")) {
                sql.append(" , USER_IMAGE_URL = '" + userImageUrl + "' ");
            }

            if (userEmail != null && !userEmail.equals("")) {
                sql.append(" , EMAIL = '" + userEmail + "' ");
            }
            sql.append(" WHERE (ARTIST_PORTAL_ID = '" + artistPortalId + "') ");

            int i = 1;
            pstmt = con.prepareStatement(sql.toString());
            resullt = pstmt.executeUpdate();
        } catch (Exception ex) {
            logger.error("[DmsYoutubeDbMgr] updateArtistPortalInfo", ex);
        } finally {
            try {
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
        }
        return resullt;
    }

    public ArrayList<ManageYoutubeChannelInfo> listYoutubeChannelInfo(String channelId) {
        ArrayList<ManageYoutubeChannelInfo> listInfo = null;
        verifyConnection();
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        StringBuffer sql = null;
        try {
            sql = new StringBuffer();
            sql.append(" SELECT YTM_YOUTUBE_CHANNEL.[CHANNEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_NAME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_SHORT_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_DESCRIPTION] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_IMAGE_URL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_ACTIVE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LABEL_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_TYPE_ID] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_UGC_CHANNEL] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_NOT_MONITOR] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[LINKED_DATE] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CREATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[UPDATE_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[IS_LINKED] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[KEYWORDS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[COUNTRY] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[JOINED_TIME] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_SUBSCRIBER] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIDEOS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TOTAL_VIEWS] ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[CHANNEL_THUMBNAIL_URL] ");
//          
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[USER_NAME] AS REQUEST_USER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PASSWORD] AS REQUEST_PASSWORD ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[PREFIX_NAME] AS REQUEST_PREFIX_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[NAME] AS REQUEST_COVER_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[ALIAS_NAME] AS REQUEST_COVER_ALIAS_NAME ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[EMAIL] AS REQUEST_COVER_EMAIL ");
            sql.append("       ,YTM_YOUTUBE_CHANNEL.[TEL] AS REQUEST_COVER_TEL ");
//
            sql.append("       ,CMS_USER.[USER_NAME] ");
            sql.append("       ,CMS_USER.DISPLAY_NAME ");
            sql.append("       ,CMS_USER.EMAIL ");
            sql.append("   FROM  YTM_YOUTUBE_CHANNEL   WITH (NOLOCK) INNER JOIN  ");
            sql.append("    CMS_USER WITH (NOLOCK) ON CMS_USER.USER_ID = YTM_YOUTUBE_CHANNEL.USER_ID   ");
            sql.append("   WHERE (YTM_YOUTUBE_CHANNEL.PARENT_CHANNEL_ID = ?) ");

            pstmt = con.prepareStatement(sql.toString());
            pstmt.setString(1, channelId);
            rs = pstmt.executeQuery();
            if (rs != null) {
                listInfo = new ArrayList<ManageYoutubeChannelInfo>();
                while (rs.next()) {
                    String createTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("CREATE_TIME"));
                    String updateTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("UPDATE_TIME"));
                    String linkedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("LINKED_DATE"));
                    String joinedTime = Utilities.convertStrDbDateTimeToWebFormat(rs.getString("JOINED_TIME"));

                    String channelName = rs.getString("CHANNEL_NAME");
                    String channelShortDescription = rs.getString("CHANNEL_SHORT_DESCRIPTION");
                    String channelDescription = rs.getString("CHANNEL_DESCRIPTION");
                    String channelImageUrl = rs.getString("CHANNEL_IMAGE_URL");
                    Boolean isActive = rs.getBoolean("IS_ACTIVE");
                    Integer labelId = rs.getInt("LABEL_ID");
                    Integer channelUserId = rs.getInt("USER_ID");
                    String username = rs.getString("USER_NAME");
                    String displayName = rs.getString("DISPLAY_NAME");
                    String email = rs.getString("EMAIL");
                    Integer channelTypeId = rs.getInt("CHANNEL_TYPE_ID");
                    Boolean isUgcChannel = rs.getBoolean("IS_UGC_CHANNEL");
                    Boolean isNotMonitor = rs.getBoolean("IS_NOT_MONITOR");

                    Boolean isLinked = rs.getBoolean("IS_LINKED");
                    String keywords = rs.getString("KEYWORDS");
                    String country = rs.getString("COUNTRY");
                    Integer totalSubscriber = rs.getInt("TOTAL_SUBSCRIBER");
                    Integer totalVideos = rs.getInt("TOTAL_VIDEOS");
                    Integer totalViews = rs.getInt("TOTAL_VIEWS");
                    String channelThumbnailUrl = rs.getString("CHANNEL_THUMBNAIL_URL");

                    String requestUsername = rs.getString("REQUEST_USER_NAME");
                    String requestPassword = rs.getString("REQUEST_PASSWORD");
                    String requestPrefixName = rs.getString("REQUEST_PREFIX_NAME");
                    String requestCoverName = rs.getString("REQUEST_COVER_NAME");
                    String requestCoverAliasName = rs.getString("REQUEST_COVER_ALIAS_NAME");
                    String requestCoverEmail = rs.getString("REQUEST_COVER_EMAIL");
                    String requestCoverTel = rs.getString("REQUEST_COVER_TEL");

                    ManageYoutubeChannelInfo info = new ManageYoutubeChannelInfo();
                    info.setChannelId(channelId);
                    info.setChannelName(channelName);
                    info.setChannelShortDescription(channelShortDescription);
                    info.setChannelDescription(channelDescription);
                    info.setChannelImageUrl(channelImageUrl);
                    info.setIsActive(isActive);
                    info.setLabelUserId(channelUserId);
                    info.setChannelUserId(channelUserId);
                    info.setChannelTypeId(channelTypeId);
                    info.setIsUgcChannel(isUgcChannel);
                    info.setIsNotMonitor(isNotMonitor);
                    info.setCreateTime(createTime);
                    info.setUpdateTime(updateTime);

                    info.setDisplayName(displayName);
                    info.setChannelAccountManagerName(username);
                    info.setChannelAccountEmail(email);

                    info.setIsLinked(isLinked);
                    info.setKeywords(keywords);
                    info.setCountry(country);
                    info.setTotalSubscripber(totalSubscriber);
                    info.setTotalVideo(totalVideos);
                    info.setTotalViews(totalViews);
                    info.setJoinedTime(joinedTime);
                    info.setChannelThumbnailUrl(channelThumbnailUrl);

                    info.setUsername(requestUsername);
                    info.setPassword(requestPassword);
                    info.setPrefixName(requestPrefixName);
                    info.setName(requestCoverName);
                    info.setAliasName(requestCoverAliasName);
                    info.setEmail(requestCoverEmail);
                    info.setTel(requestCoverTel);

                    listInfo.add(info);
                }
            }

        } catch (Exception e) {
            logger.error("", e);
        } finally {
            try {
                closeResultSet(rs);
                closeStatement(pstmt);
            } catch (Exception ex) {
            }
            sql = null;
            pstmt = null;
            rs = null;
        }

        return listInfo;
    }

}
