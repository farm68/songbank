/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.filter;
 
import com.sone.songbank.util.Constants;
import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse; 

/**
 *
 * @author Jiranuwat
 */
public class ManagementFilter implements Filter {

    private String mode = "DENY";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        String configMode = filterConfig.getInitParameter("mode");
        if (configMode != null) {
            mode = configMode;
        }
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletResponse res = (HttpServletResponse) response;
        HttpServletRequest req = (HttpServletRequest) request;
        res.addHeader("X-FRAME-OPTIONS", mode);
        res.addHeader("Referrer-Policy", "no-referrer-when-downgrade");
        res.addHeader("feature-policy", "fullscreen");
//        res.addHeader("Content-Security-Policy", "upgrade-insecure-requests");

        if (req.getSession().getAttribute(Constants.USER_SESSION) == null) {
            //forward request to login.jsp
            req.getRequestDispatcher("/login/login.jsp").forward(request, response);
        } else {
            chain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {
    }
}
