/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info;

/**
 *
 * @author jiranuwat
 */
public class DmsArtistPortalInfo {

    private String artistPortalId;
    private String artistNameTh;
    private String artistNameEn;
    private String profileShortDesc;
    private String profileDesc;
    private String imageUrl;
    private String ytChannelId;
    private boolean isYtOac;
    private Integer labelId;
    private Integer statusId;
    private String userName;
    private String password;
    private String displayName;
    private String userImageUrl;
    private String email;
    private String createTime;
    private String updateTime;

    /**
     * @return the artistPortalId
     */
    public String getArtistPortalId() {
        return artistPortalId;
    }

    /**
     * @param artistPortalId the artistPortalId to set
     */
    public void setArtistPortalId(String artistPortalId) {
        this.artistPortalId = artistPortalId;
    }

    /**
     * @return the artistNameTh
     */
    public String getArtistNameTh() {
        return artistNameTh;
    }

    /**
     * @param artistNameTh the artistNameTh to set
     */
    public void setArtistNameTh(String artistNameTh) {
        this.artistNameTh = artistNameTh;
    }

    /**
     * @return the artistNameEn
     */
    public String getArtistNameEn() {
        return artistNameEn;
    }

    /**
     * @param artistNameEn the artistNameEn to set
     */
    public void setArtistNameEn(String artistNameEn) {
        this.artistNameEn = artistNameEn;
    }

    /**
     * @return the profileShortDesc
     */
    public String getProfileShortDesc() {
        return profileShortDesc;
    }

    /**
     * @param profileShortDesc the profileShortDesc to set
     */
    public void setProfileShortDesc(String profileShortDesc) {
        this.profileShortDesc = profileShortDesc;
    }

    /**
     * @return the profileDesc
     */
    public String getProfileDesc() {
        return profileDesc;
    }

    /**
     * @param profileDesc the profileDesc to set
     */
    public void setProfileDesc(String profileDesc) {
        this.profileDesc = profileDesc;
    }

    /**
     * @return the imageUrl
     */
    public String getImageUrl() {
        return imageUrl;
    }

    /**
     * @param imageUrl the imageUrl to set
     */
    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    /**
     * @return the ytChannelId
     */
    public String getYtChannelId() {
        return ytChannelId;
    }

    /**
     * @param ytChannelId the ytChannelId to set
     */
    public void setYtChannelId(String ytChannelId) {
        this.ytChannelId = ytChannelId;
    }

    /**
     * @return the isYtOac
     */
    public boolean getIsYtOac() {
        return isIsYtOac();
    }

    /**
     * @param isYtOac the isYtOac to set
     */
    public void setIsYtOac(boolean isYtOac) {
        this.isYtOac = isYtOac;
    }

    /**
     * @return the labelId
     */
    public Integer getLabelId() {
        return labelId;
    }

    /**
     * @param labelId the labelId to set
     */
    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    /**
     * @return the statusId
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the isYtOac
     */
    public boolean isIsYtOac() {
        return isYtOac;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the userImageUrl
     */
    public String getUserImageUrl() {
        return userImageUrl;
    }

    /**
     * @param userImageUrl the userImageUrl to set
     */
    public void setUserImageUrl(String userImageUrl) {
        this.userImageUrl = userImageUrl;
    }

}
