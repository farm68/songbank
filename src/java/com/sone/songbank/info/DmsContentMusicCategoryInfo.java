/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info;

/**
 *
 * @author jiranuwat
 */
public class DmsContentMusicCategoryInfo {
    
    private Integer musicCategoryId;
    private String musicCategoryName;
    private String createTime;

    /**
     * @return the musicCategoryId
     */
    public Integer getMusicCategoryId() {
        return musicCategoryId;
    }

    /**
     * @param musicCategoryId the musicCategoryId to set
     */
    public void setMusicCategoryId(Integer musicCategoryId) {
        this.musicCategoryId = musicCategoryId;
    }

    /**
     * @return the musicCategoryName
     */
    public String getMusicCategoryName() {
        return musicCategoryName;
    }

    /**
     * @param musicCategoryName the musicCategoryName to set
     */
    public void setMusicCategoryName(String musicCategoryName) {
        this.musicCategoryName = musicCategoryName;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }
    
}
