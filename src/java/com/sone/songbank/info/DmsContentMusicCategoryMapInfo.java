/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info;

/**
 *
 * @author jiranuwat
 */
public class DmsContentMusicCategoryMapInfo {
    private String musicCategoryMapId;
    private String musicId;
    private Integer priority;
    private Integer musicCategoryId;
    
    //
    private String musicCategoryName;

    /**
     * @return the musicCategoryMapId
     */
    public String getMusicCategoryMapId() {
        return musicCategoryMapId;
    }

    /**
     * @param musicCategoryMapId the musicCategoryMapId to set
     */
    public void setMusicCategoryMapId(String musicCategoryMapId) {
        this.musicCategoryMapId = musicCategoryMapId;
    }

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return the musicCategoryId
     */
    public Integer getMusicCategoryId() {
        return musicCategoryId;
    }

    /**
     * @param musicCategoryId the musicCategoryId to set
     */
    public void setMusicCategoryId(Integer musicCategoryId) {
        this.musicCategoryId = musicCategoryId;
    }

    /**
     * @return the musicCategoryName
     */
    public String getMusicCategoryName() {
        return musicCategoryName;
    }

    /**
     * @param musicCategoryName the musicCategoryName to set
     */
    public void setMusicCategoryName(String musicCategoryName) {
        this.musicCategoryName = musicCategoryName;
    }
    
}
