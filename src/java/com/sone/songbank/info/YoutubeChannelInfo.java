/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info;

/**
 *
 * @author akapanpolsri
 */
public class YoutubeChannelInfo { 
    private String channelId;
    private String channelName;
    private Boolean isNotMonitor;
    private Boolean isActive; 
    
    private String createTime;
    private String updateTime; 
    private String displayName;
     
    // add new collumn 
    private String channelShortDescription;
    private String channelDescription;
    private String channelImageUrl;
    private Integer channelUserId;
    private String channelAccountManagerName;  
    private String channelAccountEmail;
    private Integer labelUserId;
    private String labelAccountManagerName;
    private Boolean isUgcChannel; // ในช่อง/นอกช่อง
    private Integer channelTypeId;
    private String channelTypeName;
    private String channelTypeUrl;
    
    //song bank
    private String username;
    private String password;
    private String name;
    private String prefixName;
    private String aliasName;
    private String email;
    private String tel;
    
    //add new collum artist portal
    private Boolean isLinked;
    private String keywords;
    private String country;
    private String joinedTime;
    private Long totalSubscripber;
    private Long totalVideo;
    private Long totalViews;
    private String channelThumbnailUrl;
    
    
    private String customUrl;
    private Boolean isHiddenSubCount;
    
    
    public YoutubeChannelInfo(){
    }

    /**
     * @return the channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the channelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * @return the isNotMonitor
     */
    public Boolean getIsNotMonitor() {
        return isNotMonitor;
    }

    /**
     * @param isNotMonitor the isNotMonitor to set
     */
    public void setIsNotMonitor(Boolean isNotMonitor) {
        this.isNotMonitor = isNotMonitor;
    }

    /**
     * @return the isActive
     */
    public Boolean getIsActive() {
        return isActive;
    }

    /**
     * @param isActive the isActive to set
     */
    public void setIsActive(Boolean isActive) {
        this.isActive = isActive;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the channelShortDescription
     */
    public String getChannelShortDescription() {
        return channelShortDescription;
    }

    /**
     * @param channelShortDescription the channelShortDescription to set
     */
    public void setChannelShortDescription(String channelShortDescription) {
        this.channelShortDescription = channelShortDescription;
    }

    /**
     * @return the channelDescription
     */
    public String getChannelDescription() {
        return channelDescription;
    }

    /**
     * @param channelDescription the channelDescription to set
     */
    public void setChannelDescription(String channelDescription) {
        this.channelDescription = channelDescription;
    }

    /**
     * @return the channelImageUrl
     */
    public String getChannelImageUrl() {
        return channelImageUrl;
    }

    /**
     * @param channelImageUrl the channelImageUrl to set
     */
    public void setChannelImageUrl(String channelImageUrl) {
        this.channelImageUrl = channelImageUrl;
    }

    /**
     * @return the channelUserId
     */
    public Integer getChannelUserId() {
        return channelUserId;
    }

    /**
     * @param channelUserId the channelUserId to set
     */
    public void setChannelUserId(Integer channelUserId) {
        this.channelUserId = channelUserId;
    }

    /**
     * @return the channelAccountManagerName
     */
    public String getChannelAccountManagerName() {
        return channelAccountManagerName;
    }

    /**
     * @param channelAccountManagerName the channelAccountManagerName to set
     */
    public void setChannelAccountManagerName(String channelAccountManagerName) {
        this.channelAccountManagerName = channelAccountManagerName;
    }

    /**
     * @return the channelAccountEmail
     */
    public String getChannelAccountEmail() {
        return channelAccountEmail;
    }

    /**
     * @param channelAccountEmail the channelAccountEmail to set
     */
    public void setChannelAccountEmail(String channelAccountEmail) {
        this.channelAccountEmail = channelAccountEmail;
    }

    /**
     * @return the labelUserId
     */
    public Integer getLabelUserId() {
        return labelUserId;
    }

    /**
     * @param labelUserId the labelUserId to set
     */
    public void setLabelUserId(Integer labelUserId) {
        this.labelUserId = labelUserId;
    }

    /**
     * @return the labelAccountManagerName
     */
    public String getLabelAccountManagerName() {
        return labelAccountManagerName;
    }

    /**
     * @param labelAccountManagerName the labelAccountManagerName to set
     */
    public void setLabelAccountManagerName(String labelAccountManagerName) {
        this.labelAccountManagerName = labelAccountManagerName;
    }

    /**
     * @return the isUgcChannel
     */
    public Boolean getIsUgcChannel() {
        return isUgcChannel;
    }

    /**
     * @param isUgcChannel the isUgcChannel to set
     */
    public void setIsUgcChannel(Boolean isUgcChannel) {
        this.isUgcChannel = isUgcChannel;
    }

    /**
     * @return the channelTypeId
     */
    public Integer getChannelTypeId() {
        return channelTypeId;
    }

    /**
     * @param channelTypeId the channelTypeId to set
     */
    public void setChannelTypeId(Integer channelTypeId) {
        this.channelTypeId = channelTypeId;
    }

    /**
     * @return the channelTypeName
     */
    public String getChannelTypeName() {
        return channelTypeName;
    }

    /**
     * @param channelTypeName the channelTypeName to set
     */
    public void setChannelTypeName(String channelTypeName) {
        this.channelTypeName = channelTypeName;
    }

    /**
     * @return the channelTypeUrl
     */
    public String getChannelTypeUrl() {
        return channelTypeUrl;
    }

    /**
     * @param channelTypeUrl the channelTypeUrl to set
     */
    public void setChannelTypeUrl(String channelTypeUrl) {
        this.channelTypeUrl = channelTypeUrl;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the prefixName
     */
    public String getPrefixName() {
        return prefixName;
    }

    /**
     * @param prefixName the prefixName to set
     */
    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    /**
     * @return the aliasName
     */
    public String getAliasName() {
        return aliasName;
    }

    /**
     * @param aliasName the aliasName to set
     */
    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the isLinked
     */
    public Boolean getIsLinked() {
        return isLinked;
    }

    /**
     * @param isLinked the isLinked to set
     */
    public void setIsLinked(Boolean isLinked) {
        this.isLinked = isLinked;
    }

    /**
     * @return the keywords
     */
    public String getKeywords() {
        return keywords;
    }

    /**
     * @param keywords the keywords to set
     */
    public void setKeywords(String keywords) {
        this.keywords = keywords;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) {
        this.country = country;
    }

    /**
     * @return the joinedTime
     */
    public String getJoinedTime() {
        return joinedTime;
    }

    /**
     * @param joinedTime the joinedTime to set
     */
    public void setJoinedTime(String joinedTime) {
        this.joinedTime = joinedTime;
    }

    /**
     * @return the totalSubscripber
     */
    public Long getTotalSubscripber() {
        return totalSubscripber;
    }

    /**
     * @param totalSubscripber the totalSubscripber to set
     */
    public void setTotalSubscripber(Long totalSubscripber) {
        this.totalSubscripber = totalSubscripber;
    }

    /**
     * @return the totalVideo
     */
    public Long getTotalVideo() {
        return totalVideo;
    }

    /**
     * @param totalVideo the totalVideo to set
     */
    public void setTotalVideo(Long totalVideo) {
        this.totalVideo = totalVideo;
    }

    /**
     * @return the totalViews
     */
    public Long getTotalViews() {
        return totalViews;
    }

    /**
     * @param totalViews the totalViews to set
     */
    public void setTotalViews(Long totalViews) {
        this.totalViews = totalViews;
    }

    /**
     * @return the channelThumbnailUrl
     */
    public String getChannelThumbnailUrl() {
        return channelThumbnailUrl;
    }

    /**
     * @param channelThumbnailUrl the channelThumbnailUrl to set
     */
    public void setChannelThumbnailUrl(String channelThumbnailUrl) {
        this.channelThumbnailUrl = channelThumbnailUrl;
    }

    /**
     * @return the customUrl
     */
    public String getCustomUrl() {
        return customUrl;
    }

    /**
     * @param customUrl the customUrl to set
     */
    public void setCustomUrl(String customUrl) {
        this.customUrl = customUrl;
    }

    /**
     * @return the isHiddenSubCount
     */
    public Boolean getIsHiddenSubCount() {
        return isHiddenSubCount;
    }

    /**
     * @param isHiddenSubCount the isHiddenSubCount to set
     */
    public void setIsHiddenSubCount(Boolean isHiddenSubCount) {
        this.isHiddenSubCount = isHiddenSubCount;
    }

}
