/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.coversong;

/**
 *
 * @author akapanpolsri
 */
public class DmsYtmCoverSongStatusInfo {
    private Integer statusId;
    private String statusName;
 
    public DmsYtmCoverSongStatusInfo(){}
    public DmsYtmCoverSongStatusInfo(Integer statusId,String statusName){
        this.statusId = statusId;
        this.statusName = statusName;
    }

    /**
     * @return the statusId
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the statusName
     */
    public String getStatusName() {
        return statusName;
    }

    /**
     * @param statusName the statusName to set
     */
    public void setStatusName(String statusName) {
        this.statusName = statusName;
    }
}
