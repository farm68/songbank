/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.coversong;

import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;

/**
 *
 * @author akapanpolsri
 */
public class DmsYtmRequestConverSongYtChannelMapInfo {

    /**
     * @return the requestId
     */
    public Long getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the channelId
     */
    public String getChannelId() {
        return channelId;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannelId(String channelId) {
        this.channelId = channelId;
    }

    /**
     * @return the channelName
     */
    public String getChannelName() {
        return channelName;
    }

    /**
     * @param channelName the channelName to set
     */
    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
    private Long requestId;
    private String channelId;
    private String channelName;
    private Integer priority;
    private String createTime;
    private String updateTime;
    
//    
    private ManageYoutubeChannelInfo channelInfo;
    
    public DmsYtmRequestConverSongYtChannelMapInfo(){}

    /**
     * @return the channelInfo
     */
    public ManageYoutubeChannelInfo getChannelInfo() {
        return channelInfo;
    }

    /**
     * @param channelInfo the channelInfo to set
     */
    public void setChannelInfo(ManageYoutubeChannelInfo channelInfo) {
        this.channelInfo = channelInfo;
    }
}
