/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.coversong;

/**
 *
 * @author akapanpolsri
 */
public class DmsYtmRequestCoverSongLogInfo {
    private Long requestId;
    private String name;
    private String artistName;
    private String artistAliasName;
    private String email;
    private String tel;
    private Integer statusId;
//    
    private String publishTime;
    private String createTime;
    private String updteTime;
//    
    private String reqDocUrl;
    private String ackDocUrl;
    private String actionRemark;
    private String actionTime;
    
    public DmsYtmRequestCoverSongLogInfo(){}

    /**
     * @return the requestId
     */
    public Long getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the artistName
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * @param artistName the artistName to set
     */
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /**
     * @return the artistAliasName
     */
    public String getArtistAliasName() {
        return artistAliasName;
    }

    /**
     * @param artistAliasName the artistAliasName to set
     */
    public void setArtistAliasName(String artistAliasName) {
        this.artistAliasName = artistAliasName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the statusId
     */
    public Integer getStatusId() {
        return statusId;
    }

    /**
     * @param statusId the statusId to set
     */
    public void setStatusId(Integer statusId) {
        this.statusId = statusId;
    }

    /**
     * @return the publishTime
     */
    public String getPublishTime() {
        return publishTime;
    }

    /**
     * @param publishTime the publishTime to set
     */
    public void setPublishTime(String publishTime) {
        this.publishTime = publishTime;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updteTime
     */
    public String getUpdteTime() {
        return updteTime;
    }

    /**
     * @param updteTime the updteTime to set
     */
    public void setUpdteTime(String updteTime) {
        this.updteTime = updteTime;
    }

    /**
     * @return the reqDocUrl
     */
    public String getReqDocUrl() {
        return reqDocUrl;
    }

    /**
     * @param reqDocUrl the reqDocUrl to set
     */
    public void setReqDocUrl(String reqDocUrl) {
        this.reqDocUrl = reqDocUrl;
    }

    /**
     * @return the ackDocUrl
     */
    public String getAckDocUrl() {
        return ackDocUrl;
    }

    /**
     * @param ackDocUrl the ackDocUrl to set
     */
    public void setAckDocUrl(String ackDocUrl) {
        this.ackDocUrl = ackDocUrl;
    }

    /**
     * @return the actionRemark
     */
    public String getActionRemark() {
        return actionRemark;
    }

    /**
     * @param actionRemark the actionRemark to set
     */
    public void setActionRemark(String actionRemark) {
        this.actionRemark = actionRemark;
    }

    /**
     * @return the actionTime
     */
    public String getActionTime() {
        return actionTime;
    }

    /**
     * @param actionTime the actionTime to set
     */
    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }
    
}
