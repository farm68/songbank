/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.coversong;

import com.sone.songbank.info.coversong.ManageSearchContentMusicCoverInfo;

/**
 *
 * @author akapanpolsri
 */
public class DmsYtmRequestCoverSongMusicMapInfo {
    private Long requestId;
    private String musicPublishingId;
    private Integer priority;
    private Double ownerPercentShare;
    private Double coverPercentShare;
    private String createTime;
    private String updateTime;
    
//    
    private ManageSearchContentMusicCoverInfo musicInfo;
    
    public DmsYtmRequestCoverSongMusicMapInfo(){}

    /**
     * @return the requestId
     */
    public Long getRequestId() {
        return requestId;
    }

    /**
     * @param requestId the requestId to set
     */
    public void setRequestId(Long requestId) {
        this.requestId = requestId;
    }

    /**
     * @return the musicPublishingId
     */
    public String getMusicPublishingId() {
        return musicPublishingId;
    }

    /**
     * @param musicPublishingId the musicPublishingId to set
     */
    public void setMusicPublishingId(String musicPublishingId) {
        this.musicPublishingId = musicPublishingId;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * @return the ownerPercentShare
     */
    public Double getOwnerPercentShare() {
        return ownerPercentShare;
    }

    /**
     * @param ownerPercentShare the ownerPercentShare to set
     */
    public void setOwnerPercentShare(Double ownerPercentShare) {
        this.ownerPercentShare = ownerPercentShare;
    }

    /**
     * @return the coverPercentShare
     */
    public Double getCoverPercentShare() {
        return coverPercentShare;
    }

    /**
     * @param coverPercentShare the coverPercentShare to set
     */
    public void setCoverPercentShare(Double coverPercentShare) {
        this.coverPercentShare = coverPercentShare;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the musicInfo
     */
    public ManageSearchContentMusicCoverInfo getMusicInfo() {
        return musicInfo;
    }

    /**
     * @param musicInfo the musicInfo to set
     */
    public void setMusicInfo(ManageSearchContentMusicCoverInfo musicInfo) {
        this.musicInfo = musicInfo;
    }
    
}
