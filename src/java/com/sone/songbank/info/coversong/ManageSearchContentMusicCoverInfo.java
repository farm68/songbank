/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.coversong;

import com.sone.songbank.info.DmsContentMusicCategoryMapInfo;
import com.sone.songbank.info.dms.ManageContentMusicServiceMapInfo;
import java.util.ArrayList;

/**
 *
 * @author jiranuwat
 */
public class ManageSearchContentMusicCoverInfo {

    /**
     * @return the listMusicCategoryMap
     */
    public ArrayList<DmsContentMusicCategoryMapInfo> getListMusicCategoryMap() {
        return listMusicCategoryMap;
    }

    /**
     * @param listMusicCategoryMap the listMusicCategoryMap to set
     */
    public void setListMusicCategoryMap(ArrayList<DmsContentMusicCategoryMapInfo> listMusicCategoryMap) {
        this.listMusicCategoryMap = listMusicCategoryMap;
    }

    private String musicPublishingId;
    private String musicId;
    private String isrc;
    private String trackNameTh;
    private String trackNameEn;
    private Integer genreId;
    private String genreName;
    private String artistNameTh;
    private String artistNameEn;
//    
    private String upcCode;
    private String albumNameTh;
    private String albumNameEn;
    private String albumArtistNameTh;
    private String albumArtistNameEn;
    private String releaseDate;
    private String labelNameTh;
    private String labelNameEn;
//    
    private String compOriginalVocalTh;
    private String compOriginalVocalEn;
    private String compLyricTh;
    private String compLyricEn;
    private String compMelodyTh;
    private String compMelodyEn;
    private String compOwnerTh;
    private String compOwnerEn;
    private String compOwnerMelodyTh;
    private String compOwnerMelodyEn;
    private String compOwnerMasterTh;
    private String compOwnerMasterEn;
//    
    private String shortLyrics;
    private String lyrics;
    private String urlMaster;
    private String oneLinkUrl;
    private String thumbnailUrl;
//    
    private Double coverOwnerPercentSharing;
    private Double coverPartnerPercentSharing;
//    
    private ArrayList<DmsContentMusicCategoryMapInfo> listMusicCategoryMap;
    private ArrayList<ManageContentMusicServiceMapInfo> listMusicServiceMap;

    /**
     * @return the musicPublishingId
     */
    public String getMusicPublishingId() {
        return musicPublishingId;
    }

    /**
     * @param musicPublishingId the musicPublishingId to set
     */
    public void setMusicPublishingId(String musicPublishingId) {
        this.musicPublishingId = musicPublishingId;
    }

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the isrc
     */
    public String getIsrc() {
        return isrc;
    }

    /**
     * @param isrc the isrc to set
     */
    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    /**
     * @return the trackNameTh
     */
    public String getTrackNameTh() {
        return trackNameTh;
    }

    /**
     * @param trackNameTh the trackNameTh to set
     */
    public void setTrackNameTh(String trackNameTh) {
        this.trackNameTh = trackNameTh;
    }

    /**
     * @return the trackNameEn
     */
    public String getTrackNameEn() {
        return trackNameEn;
    }

    /**
     * @param trackNameEn the trackNameEn to set
     */
    public void setTrackNameEn(String trackNameEn) {
        this.trackNameEn = trackNameEn;
    }

    /**
     * @return the genreId
     */
    public Integer getGenreId() {
        return genreId;
    }

    /**
     * @param genreId the genreId to set
     */
    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    /**
     * @return the genreName
     */
    public String getGenreName() {
        return genreName;
    }

    /**
     * @param genreName the genreName to set
     */
    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    /**
     * @return the artistNameTh
     */
    public String getArtistNameTh() {
        return artistNameTh;
    }

    /**
     * @param artistNameTh the artistNameTh to set
     */
    public void setArtistNameTh(String artistNameTh) {
        this.artistNameTh = artistNameTh;
    }

    /**
     * @return the artistNameEn
     */
    public String getArtistNameEn() {
        return artistNameEn;
    }

    /**
     * @param artistNameEn the artistNameEn to set
     */
    public void setArtistNameEn(String artistNameEn) {
        this.artistNameEn = artistNameEn;
    }

    /**
     * @return the upcCode
     */
    public String getUpcCode() {
        return upcCode;
    }

    /**
     * @param upcCode the upcCode to set
     */
    public void setUpcCode(String upcCode) {
        this.upcCode = upcCode;
    }

    /**
     * @return the albumNameTh
     */
    public String getAlbumNameTh() {
        return albumNameTh;
    }

    /**
     * @param albumNameTh the albumNameTh to set
     */
    public void setAlbumNameTh(String albumNameTh) {
        this.albumNameTh = albumNameTh;
    }

    /**
     * @return the albumNameEn
     */
    public String getAlbumNameEn() {
        return albumNameEn;
    }

    /**
     * @param albumNameEn the albumNameEn to set
     */
    public void setAlbumNameEn(String albumNameEn) {
        this.albumNameEn = albumNameEn;
    }

    /**
     * @return the albumArtistNameTh
     */
    public String getAlbumArtistNameTh() {
        return albumArtistNameTh;
    }

    /**
     * @param albumArtistNameTh the albumArtistNameTh to set
     */
    public void setAlbumArtistNameTh(String albumArtistNameTh) {
        this.albumArtistNameTh = albumArtistNameTh;
    }

    /**
     * @return the albumArtistNameEn
     */
    public String getAlbumArtistNameEn() {
        return albumArtistNameEn;
    }

    /**
     * @param albumArtistNameEn the albumArtistNameEn to set
     */
    public void setAlbumArtistNameEn(String albumArtistNameEn) {
        this.albumArtistNameEn = albumArtistNameEn;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the labelNameTh
     */
    public String getLabelNameTh() {
        return labelNameTh;
    }

    /**
     * @param labelNameTh the labelNameTh to set
     */
    public void setLabelNameTh(String labelNameTh) {
        this.labelNameTh = labelNameTh;
    }

    /**
     * @return the labelNameEn
     */
    public String getLabelNameEn() {
        return labelNameEn;
    }

    /**
     * @param labelNameEn the labelNameEn to set
     */
    public void setLabelNameEn(String labelNameEn) {
        this.labelNameEn = labelNameEn;
    }

    /**
     * @return the compOriginalVocalTh
     */
    public String getCompOriginalVocalTh() {
        return compOriginalVocalTh;
    }

    /**
     * @param compOriginalVocalTh the compOriginalVocalTh to set
     */
    public void setCompOriginalVocalTh(String compOriginalVocalTh) {
        this.compOriginalVocalTh = compOriginalVocalTh;
    }

    /**
     * @return the compOriginalVocalEn
     */
    public String getCompOriginalVocalEn() {
        return compOriginalVocalEn;
    }

    /**
     * @param compOriginalVocalEn the compOriginalVocalEn to set
     */
    public void setCompOriginalVocalEn(String compOriginalVocalEn) {
        this.compOriginalVocalEn = compOriginalVocalEn;
    }

    /**
     * @return the compLyricTh
     */
    public String getCompLyricTh() {
        return compLyricTh;
    }

    /**
     * @param compLyricTh the compLyricTh to set
     */
    public void setCompLyricTh(String compLyricTh) {
        this.compLyricTh = compLyricTh;
    }

    /**
     * @return the compLyricEn
     */
    public String getCompLyricEn() {
        return compLyricEn;
    }

    /**
     * @param compLyricEn the compLyricEn to set
     */
    public void setCompLyricEn(String compLyricEn) {
        this.compLyricEn = compLyricEn;
    }

    /**
     * @return the compMelodyTh
     */
    public String getCompMelodyTh() {
        return compMelodyTh;
    }

    /**
     * @param compMelodyTh the compMelodyTh to set
     */
    public void setCompMelodyTh(String compMelodyTh) {
        this.compMelodyTh = compMelodyTh;
    }

    /**
     * @return the compMelodyEn
     */
    public String getCompMelodyEn() {
        return compMelodyEn;
    }

    /**
     * @param compMelodyEn the compMelodyEn to set
     */
    public void setCompMelodyEn(String compMelodyEn) {
        this.compMelodyEn = compMelodyEn;
    }

    /**
     * @return the compOwnerTh
     */
    public String getCompOwnerTh() {
        return compOwnerTh;
    }

    /**
     * @param compOwnerTh the compOwnerTh to set
     */
    public void setCompOwnerTh(String compOwnerTh) {
        this.compOwnerTh = compOwnerTh;
    }

    /**
     * @return the compOwnerEn
     */
    public String getCompOwnerEn() {
        return compOwnerEn;
    }

    /**
     * @param compOwnerEn the compOwnerEn to set
     */
    public void setCompOwnerEn(String compOwnerEn) {
        this.compOwnerEn = compOwnerEn;
    }

    /**
     * @return the compOwnerMelodyTh
     */
    public String getCompOwnerMelodyTh() {
        return compOwnerMelodyTh;
    }

    /**
     * @param compOwnerMelodyTh the compOwnerMelodyTh to set
     */
    public void setCompOwnerMelodyTh(String compOwnerMelodyTh) {
        this.compOwnerMelodyTh = compOwnerMelodyTh;
    }

    /**
     * @return the compOwnerMelodyEn
     */
    public String getCompOwnerMelodyEn() {
        return compOwnerMelodyEn;
    }

    /**
     * @param compOwnerMelodyEn the compOwnerMelodyEn to set
     */
    public void setCompOwnerMelodyEn(String compOwnerMelodyEn) {
        this.compOwnerMelodyEn = compOwnerMelodyEn;
    }

    /**
     * @return the compOwnerMasterTh
     */
    public String getCompOwnerMasterTh() {
        return compOwnerMasterTh;
    }

    /**
     * @param compOwnerMasterTh the compOwnerMasterTh to set
     */
    public void setCompOwnerMasterTh(String compOwnerMasterTh) {
        this.compOwnerMasterTh = compOwnerMasterTh;
    }

    /**
     * @return the compOwnerMasterEn
     */
    public String getCompOwnerMasterEn() {
        return compOwnerMasterEn;
    }

    /**
     * @param compOwnerMasterEn the compOwnerMasterEn to set
     */
    public void setCompOwnerMasterEn(String compOwnerMasterEn) {
        this.compOwnerMasterEn = compOwnerMasterEn;
    }

    /**
     * @return the shortLyrics
     */
    public String getShortLyrics() {
        return shortLyrics;
    }

    /**
     * @param shortLyrics the shortLyrics to set
     */
    public void setShortLyrics(String shortLyrics) {
        this.shortLyrics = shortLyrics;
    }

    /**
     * @return the listMusicServiceMap
     */
    public ArrayList<ManageContentMusicServiceMapInfo> getListMusicServiceMap() {
        return listMusicServiceMap;
    }

    /**
     * @param listMusicServiceMap the listMusicServiceMap to set
     */
    public void setListMusicServiceMap(ArrayList<ManageContentMusicServiceMapInfo> listMusicServiceMap) {
        this.listMusicServiceMap = listMusicServiceMap;
    }

    /**
     * @return the lyrics
     */
    public String getLyrics() {
        return lyrics;
    }

    /**
     * @param lyrics the lyrics to set
     */
    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    /**
     * @return the oneLinkUrl
     */
    public String getOneLinkUrl() {
        return oneLinkUrl;
    }

    /**
     * @param oneLinkUrl the oneLinkUrl to set
     */
    public void setOneLinkUrl(String oneLinkUrl) {
        this.oneLinkUrl = oneLinkUrl;
    }

    /**
     * @return the thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * @param thumbnailUrl the thumbnailUrl to set
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

    /**
     * @return the urlMaster
     */
    public String getUrlMaster() {
        return urlMaster;
    }

    /**
     * @param urlMaster the urlMaster to set
     */
    public void setUrlMaster(String urlMaster) {
        this.urlMaster = urlMaster;
    }

    /**
     * @return the coverOwnerPercentSharing
     */
    public Double getCoverOwnerPercentSharing() {
        return coverOwnerPercentSharing;
    }

    /**
     * @param coverOwnerPercentSharing the coverOwnerPercentSharing to set
     */
    public void setCoverOwnerPercentSharing(Double coverOwnerPercentSharing) {
        this.coverOwnerPercentSharing = coverOwnerPercentSharing;
    }

    /**
     * @return the coverPartnerPercentSharing
     */
    public Double getCoverPartnerPercentSharing() {
        return coverPartnerPercentSharing;
    }

    /**
     * @param coverPartnerPercentSharing the coverPartnerPercentSharing to set
     */
    public void setCoverPartnerPercentSharing(Double coverPartnerPercentSharing) {
        this.coverPartnerPercentSharing = coverPartnerPercentSharing;
    }

}
