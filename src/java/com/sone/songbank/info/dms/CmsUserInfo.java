/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class CmsUserInfo {

    private Integer userId;
    private Integer userGroupId;
    private String userName;
    private String password;
    private String displayName;
    private String email;
    private String createTime;
    private String updateTime;
    private String lastAccessTime;

    public CmsUserInfo() {
    }

    public CmsUserInfo(Integer userId, Integer userGroupId, String displayName) {
        this.userId = userId;
        this.userGroupId = userGroupId;
        this.displayName = displayName;
    }

    public CmsUserInfo(Integer userId, Integer userGroupId, String displayName, String email, String lastAccessTime) {
        this.userId = userId;
        this.userGroupId = userGroupId;
        this.displayName = displayName;
        this.email = email;
        this.lastAccessTime = lastAccessTime;
    }

    public CmsUserInfo(Integer userId, Integer userGroupId, String username, String password, String displayName, String email) {
        this.userId = userId;
        this.userGroupId = userGroupId;
        this.userName = username;
        this.password = password;
        this.displayName = displayName;
        this.email = email;

    }

    public CmsUserInfo(Integer userId, Integer userGroupId, String username, String password, String displayName, String email, String createTime, String updateTime) {
        this.userId = userId;
        this.userGroupId = userGroupId;
        this.userName = username;
        this.password = password;
        this.displayName = displayName;
        this.email = email;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * @return the userId
     */
    public Integer getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    /**
     * @return the userGroupId
     */
    public Integer getUserGroupId() {
        return userGroupId;
    }

    /**
     * @param userGroupId the userGroupId to set
     */
    public void setUserGroupId(Integer userGroupId) {
        this.userGroupId = userGroupId;
    }

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the lastAccessTime
     */
    public String getLastAccessTime() {
        return lastAccessTime;
    }

    /**
     * @param lastAccessTime the lastAccessTime to set
     */
    public void setLastAccessTime(String lastAccessTime) {
        this.lastAccessTime = lastAccessTime;
    }

}
