/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author smileman
 */
public class ContentPagingInfo {

    private int pageIndex;
    private int limit;
    private int totalPage;
    private int maxRecord;
    private int queryStartRow;
    private int queryEndRow;

    public ContentPagingInfo(int pageIndex, int limit, int totalPage, int maxRecord, int queryStartRow, int queryEndRow) {
        this.pageIndex = pageIndex;
        this.limit = limit;
        this.totalPage = totalPage;
        this.maxRecord = maxRecord;
        this.queryStartRow = queryStartRow;
        this.queryEndRow = queryEndRow;
    }

    /**
     * @return the pageIndex
     */
    public int getPageIndex() {
        return pageIndex;
    }

    /**
     * @param pageIndex the pageIndex to set
     */
    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    /**
     * @return the limit
     */
    public int getLimit() {
        return limit;
    }

    /**
     * @param limit the limit to set
     */
    public void setLimit(int limit) {
        this.limit = limit;
    }

    /**
     * @return the totalPage
     */
    public int getTotalPage() {
        return totalPage;
    }

    /**
     * @param totalPage the totalPage to set
     */
    public void setTotalPage(int totalPage) {
        this.totalPage = totalPage;
    }

    /**
     * @return the maxRecord
     */
    public int getMaxRecord() {
        return maxRecord;
    }

    /**
     * @param maxRecord the maxRecord to set
     */
    public void setMaxRecord(int maxRecord) {
        this.maxRecord = maxRecord;
    }

    /**
     * @return the queryStartRow
     */
    public int getQueryStartRow() {
        return queryStartRow;
    }

    /**
     * @param queryStartRow the queryStartRow to set
     */
    public void setQueryStartRow(int queryStartRow) {
        this.queryStartRow = queryStartRow;
    }

    /**
     * @return the queryEndRow
     */
    public int getQueryEndRow() {
        return queryEndRow;
    }

    /**
     * @param queryEndRow the queryEndRow to set
     */
    public void setQueryEndRow(int queryEndRow) {
        this.queryEndRow = queryEndRow;
    }
}
