/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author USER
 */
public class DmsArtistInfo {

    /**
     * @return the artistPortalId
     */
    public String getArtistPortalId() {
        return artistPortalId;
    }

    /**
     * @param artistPortalId the artistPortalId to set
     */
    public void setArtistPortalId(String artistPortalId) {
        this.artistPortalId = artistPortalId;
    }

    private int artistId;
    private String artistNameTh;
    private String artistNameEn;
    private String createTime;
    private String updateTime;
    private String artistPortalId;

    public DmsArtistInfo() {
    }

    public DmsArtistInfo(int artistId, String artistNameTh) {
        this.artistId = artistId;
        this.artistNameTh = artistNameTh;
    }

    public DmsArtistInfo(int artistId, String artistNameTh, String artistNameEn) {
        this.artistId = artistId;
        this.artistNameTh = artistNameTh;
        this.artistNameEn = artistNameEn;
    }

    public DmsArtistInfo(int artistId, String artistNameTh, String artistNameEn, String createTime, String updateTime) {
        this.artistId = artistId;
        this.artistNameTh = artistNameTh;
        this.artistNameEn = artistNameEn;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public String getArtistNameTh() {
        return artistNameTh;
    }

    public void setArtistNameTh(String artistNameTh) {
        this.artistNameTh = artistNameTh;
    }

    public String getArtistNameEn() {
        return artistNameEn;
    }

    public void setArtistNameEn(String artistNameEn) {
        this.artistNameEn = artistNameEn;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

}
