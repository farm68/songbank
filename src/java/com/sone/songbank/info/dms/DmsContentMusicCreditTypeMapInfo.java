/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class DmsContentMusicCreditTypeMapInfo {

    private DmsContentMusicInfo contentMusicInfo;
    private DmsMusicCreditTypeInfo creditTypeInfo;
    private DmsPersonalInfo personalInfo;
    private Double percentSharing;
    private String createTime;
    private String updateTime;

    public DmsContentMusicCreditTypeMapInfo() {
    }

    public DmsContentMusicCreditTypeMapInfo(DmsContentMusicInfo contentMusicInfo, DmsMusicCreditTypeInfo creditTypeInfo, DmsPersonalInfo personalInfo, Double percentSharing) {
        this.contentMusicInfo = contentMusicInfo;
        this.creditTypeInfo = creditTypeInfo;
        this.personalInfo = personalInfo;
        this.percentSharing = percentSharing;
    }

    public DmsContentMusicCreditTypeMapInfo(DmsContentMusicInfo contentMusicInfo, DmsMusicCreditTypeInfo creditTypeInfo, DmsPersonalInfo personalInfo, Double percentSharing, String createTime, String updateTime) {
        this.contentMusicInfo = contentMusicInfo;
        this.creditTypeInfo = creditTypeInfo;
        this.personalInfo = personalInfo;
        this.percentSharing = percentSharing;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * @return the contentMusicInfo
     */
    public DmsContentMusicInfo getContentMusicInfo() {
        return contentMusicInfo;
    }

    /**
     * @param contentMusicInfo the contentMusicInfo to set
     */
    public void setContentMusicInfo(DmsContentMusicInfo contentMusicInfo) {
        this.contentMusicInfo = contentMusicInfo;
    }

    /**
     * @return the creditTypeInfo
     */
    public DmsMusicCreditTypeInfo getCreditTypeInfo() {
        return creditTypeInfo;
    }

    /**
     * @param creditTypeInfo the creditTypeInfo to set
     */
    public void setCreditTypeInfo(DmsMusicCreditTypeInfo creditTypeInfo) {
        this.creditTypeInfo = creditTypeInfo;
    }

    /**
     * @return the personalInfo
     */
    public DmsPersonalInfo getPersonalInfo() {
        return personalInfo;
    }

    /**
     * @param personalInfo the personalInfo to set
     */
    public void setPersonalInfo(DmsPersonalInfo personalInfo) {
        this.personalInfo = personalInfo;
    }

    /**
     * @return the percentSharing
     */
    public Double getPercentSharing() {
        return percentSharing;
    }

    /**
     * @param percentSharing the percentSharing to set
     */
    public void setPercentSharing(Double percentSharing) {
        this.percentSharing = percentSharing;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
