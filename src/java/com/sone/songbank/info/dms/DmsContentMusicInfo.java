/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author USER 
 */
public class DmsContentMusicInfo {

    private String musicId;
    private int labelId;
    private int genreId;
    private int artistId;
    private int albumId;
    private String isrc;
    private String nameEn;
    private String nameTh;
    private String releaseDate;
    private int exclusiveMusicServiceId;
    private String exclusiveDate;
    private int stateId;
    private String createTime;
    private String updateTime;

    public DmsContentMusicInfo() {
    }

    public DmsContentMusicInfo(String musicId, int labelId, int genreId, int artistId, int albumId, String isrc, String nameEn, String nameTh, String releaseDate, int exclusiveMusicServiceId, String exclusiveDate, int stateId, String createTime, String updateTime) {
        this.musicId = musicId;
        this.labelId = labelId;
        this.genreId = genreId;
        this.artistId = artistId;
        this.albumId = albumId;
        this.isrc = isrc;
        this.nameEn = nameEn;
        this.nameTh = nameTh;
        this.releaseDate = releaseDate;
        this.exclusiveMusicServiceId = exclusiveMusicServiceId;
        this.exclusiveDate = exclusiveDate;
        this.stateId = stateId;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    public String getMusicId() {
        return musicId;
    }

    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    public int getLabelId() {
        return labelId;
    }

    public void setLabelId(int labelId) {
        this.labelId = labelId;
    }

    public int getGenreId() {
        return genreId;
    }

    public void setGenreId(int genreId) {
        this.genreId = genreId;
    }

    public int getArtistId() {
        return artistId;
    }

    public void setArtistId(int artistId) {
        this.artistId = artistId;
    }

    public int getAlbumId() {
        return albumId;
    }

    public void setAlbumId(int albumId) {
        this.albumId = albumId;
    }

    public String getIsrc() {
        return isrc;
    }

    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    public String getNameEn() {
        return nameEn;
    }

    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    public String getNameTh() {
        return nameTh;
    }

    public void setNameTh(String nameTh) {
        this.nameTh = nameTh;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getExclusiveMusicServiceId() {
        return exclusiveMusicServiceId;
    }

    public void setExclusiveMusicServiceId(int exclusiveMusicServiceId) {
        this.exclusiveMusicServiceId = exclusiveMusicServiceId;
    }

    public String getExclusiveDate() {
        return exclusiveDate;
    }

    public void setExclusiveDate(String exclusiveDate) {
        this.exclusiveDate = exclusiveDate;
    }

    public int getStateId() {
        return stateId;
    }

    public void setStateId(int stateId) {
        this.stateId = stateId;
    }

    public String getCreateTime() {
        return createTime;
    }

    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
