/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

import com.sone.songbank.util.Utilities;
import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class DmsContentMusicPublishingInfo {

    private String musicPublishingId;
    private String musicId;
    private String vdoId;
    private String isrc;
    private String nameEn;
    private String nameTh;
    private String releaseDate;
    private String shortLyrics;
    private String createTime;
    private String updateTime;
    private ArrayList<DmsContentMusicCreditTypeMapInfo> contentMusicCreditType;
    private ManageContractPublishingInfo contractPublishingInfo;

    private Boolean isUploadYtPublishing;

    public DmsContentMusicPublishingInfo() {
    }

    private String appendSuffixLyrics(String shortLyrics) {
        if (!Utilities.isBlankText(shortLyrics)) {
//
            if (!shortLyrics.endsWith("...")) {
//                shortLyrics = shortLyrics.replaceAll("\\.", "");
                shortLyrics = shortLyrics + "...";
            }
////            if (!shortLyrics.endsWith("...")) {
////
////            }
        }
        return shortLyrics;
    }

    public DmsContentMusicPublishingInfo(String musicPublishingId, String musicId, String vdoId, String isrc,
            String nameEn, String nameTh, String releaseDate, String shortLyrics, Boolean isUploadYtPublishing) {
        this.musicPublishingId = musicPublishingId;
        this.musicId = musicId;
        this.vdoId = vdoId;
        this.isrc = isrc;
        this.nameEn = nameEn;
        this.nameTh = nameTh;
        this.releaseDate = releaseDate;
        this.shortLyrics = appendSuffixLyrics(shortLyrics);
        this.isUploadYtPublishing = isUploadYtPublishing;
    }

    public DmsContentMusicPublishingInfo(String musicPublishingId, String musicId, String vdoId, String isrc,
            String nameEn, String nameTh, String releaseDate, String shortLyrics, String createTime, String updateTime) {
        this.musicPublishingId = musicPublishingId;
        this.musicId = musicId;
        this.vdoId = vdoId;
        this.isrc = isrc;
        this.nameEn = nameEn;
        this.nameTh = nameTh;
        this.releaseDate = releaseDate;
        this.shortLyrics = appendSuffixLyrics(shortLyrics);
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * @return the musicPublishingId
     */
    public String getMusicPublishingId() {
        return musicPublishingId;
    }

    /**
     * @param musicPublishingId the musicPublishingId to set
     */
    public void setMusicPublishingId(String musicPublishingId) {
        this.musicPublishingId = musicPublishingId;
    }

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the vdoId
     */
    public String getVdoId() {
        return vdoId;
    }

    /**
     * @param vdoId the vdoId to set
     */
    public void setVdoId(String vdoId) {
        this.vdoId = vdoId;
    }

    /**
     * @return the isrc
     */
    public String getIsrc() {
        return isrc;
    }

    /**
     * @param isrc the isrc to set
     */
    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    /**
     * @return the nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn the nameEn to set
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return the nameTh
     */
    public String getNameTh() {
        return nameTh;
    }

    /**
     * @param nameTh the nameTh to set
     */
    public void setNameTh(String nameTh) {
        this.nameTh = nameTh;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the shortLyrics
     */
    public String getShortLyrics() {
        return shortLyrics;
    }

    /**
     * @param shortLyrics the shortLyrics to set
     */
    public void setShortLyrics(String shortLyrics) {
        this.shortLyrics = appendSuffixLyrics(shortLyrics);
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public ArrayList<DmsContentMusicCreditTypeMapInfo> getContentMusicCreditType() {
        return contentMusicCreditType;
    }

    public void setContentMusicCreditType(ArrayList<DmsContentMusicCreditTypeMapInfo> contentMusicCreditType) {
        this.contentMusicCreditType = contentMusicCreditType;
    }

    /**
     * @return the contractPublishingInfo
     */
    public ManageContractPublishingInfo getContractPublishingInfo() {
        return contractPublishingInfo;
    }

    /**
     * @param contractPublishingInfo the contractPublishingInfo to set
     */
    public void setContractPublishingInfo(ManageContractPublishingInfo contractPublishingInfo) {
        this.contractPublishingInfo = contractPublishingInfo;
    }

    /**
     * @return the isUploadYtPublishing
     */
    public Boolean getIsUploadYtPublishing() {
        return isUploadYtPublishing;
    }

    /**
     * @param isUploadYtPublishing the isUploadYtPublishing to set
     */
    public void setIsUploadYtPublishing(Boolean isUploadYtPublishing) {
        this.isUploadYtPublishing = isUploadYtPublishing;
    }
}
