/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class DmsMusicCreditTypeInfo {

    private Integer creditTypeId;
    private String creditTypeName;
    private String descpription;
    private Integer priority;

    public DmsMusicCreditTypeInfo() {
    }

    public DmsMusicCreditTypeInfo(Integer creditTypeId, String creditTypeName, String description, Integer priority) {
        this.creditTypeId = creditTypeId;
        this.creditTypeName = creditTypeName;
        this.descpription = description;
        this.priority = priority;
    }

    /**
     * @return the creditTypeId
     */
    public Integer getCreditTypeId() {
        return creditTypeId;
    }

    /**
     * @param creditTypeId the creditTypeId to set
     */
    public void setCreditTypeId(Integer creditTypeId) {
        this.creditTypeId = creditTypeId;
    }

    /**
     * @return the creditTypeName
     */
    public String getCreditTypeName() {
        return creditTypeName;
    }

    /**
     * @param creditTypeName the creditTypeName to set
     */
    public void setCreditTypeName(String creditTypeName) {
        this.creditTypeName = creditTypeName;
    }

    /**
     * @return the descpription
     */
    public String getDescpription() {
        return descpription;
    }

    /**
     * @param descpription the descpription to set
     */
    public void setDescpription(String descpription) {
        this.descpription = descpription;
    }

    /**
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * @param priority the priority to set
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }
}
