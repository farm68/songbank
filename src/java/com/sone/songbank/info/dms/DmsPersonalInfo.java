/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class DmsPersonalInfo {

    private Integer personalId;
    private DmsPersonalTypeInfo personalTypeInfo;
    private String personalNameTh;
    private String personalNameEn;
    private String createTime;
    private String updateTime;

    public DmsPersonalInfo() {
    }

    public DmsPersonalInfo(Integer personalId, DmsPersonalTypeInfo personalTypeInfo, String personalNameTh, String personalNameEn) {
        this.personalId = personalId;
        this.personalTypeInfo = personalTypeInfo;
        this.personalNameTh = personalNameTh;
        this.personalNameEn = personalNameEn;
    }

    public DmsPersonalInfo(Integer personalId, DmsPersonalTypeInfo personalTypeInfo, String personalNameTh, String personalNameEn, String createTime, String updateTime) {
        this.personalId = personalId;
        this.personalTypeInfo = personalTypeInfo;
        this.personalNameTh = personalNameTh;
        this.personalNameEn = personalNameEn;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }

    /**
     * @return the personalId
     */
    public Integer getPersonalId() {
        return personalId;
    }

    /**
     * @param personalId the personalId to set
     */
    public void setPersonalId(Integer personalId) {
        this.personalId = personalId;
    }

    /**
     * @return the personalTypeInfo
     */
    public DmsPersonalTypeInfo getPersonalTypeInfo() {
        return personalTypeInfo;
    }

    /**
     * @param personalTypeInfo the personalTypeInfo to set
     */
    public void setPersonalTypeInfo(DmsPersonalTypeInfo personalTypeInfo) {
        this.personalTypeInfo = personalTypeInfo;
    }

    /**
     * @return the personalNameTh
     */
    public String getPersonalNameTh() {
        return personalNameTh;
    }

    /**
     * @param personalNameTh the personalNameTh to set
     */
    public void setPersonalNameTh(String personalNameTh) {
        this.personalNameTh = personalNameTh;
    }

    /**
     * @return the personalNameEn
     */
    public String getPersonalNameEn() {
        return personalNameEn;
    }

    /**
     * @param personalNameEn the personalNameEn to set
     */
    public void setPersonalNameEn(String personalNameEn) {
        this.personalNameEn = personalNameEn;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
