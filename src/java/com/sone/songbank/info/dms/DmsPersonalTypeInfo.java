/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class DmsPersonalTypeInfo {

    private Integer personalTypeId;
    private String personalTypeName;
    private String createTime;
    private String updateTime;

    public DmsPersonalTypeInfo() {
    }

    public DmsPersonalTypeInfo(Integer personalTypeId, String personalTypeName, String createTime, String updateTime) {
        this.personalTypeId = personalTypeId;
        this.personalTypeName = personalTypeName;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }
    public DmsPersonalTypeInfo(Integer personalTypeId, String personalTypeName) {
        this.personalTypeId = personalTypeId;
        this.personalTypeName = personalTypeName;
    }

    /**
     * @return the personalTypeId
     */
    public Integer getPersonalTypeId() {
        return personalTypeId;
    }

    /**
     * @param personalTypeId the personalTypeId to set
     */
    public void setPersonalTypeId(Integer personalTypeId) {
        this.personalTypeId = personalTypeId;
    }

    /**
     * @return the personalTypeName
     */
    public String getPersonalTypeName() {
        return personalTypeName;
    }

    /**
     * @param personalTypeName the personalTypeName to set
     */
    public void setPersonalTypeName(String personalTypeName) {
        this.personalTypeName = personalTypeName;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }
}
