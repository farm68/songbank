/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author Jiranuwat
 */
public class ManageContentMusicCompositionInfo {

    

    private String musicId;
    private String shortLyrics;
    private String lyrics;
    private Integer personalLyricistId;
    private Integer personalComposerId;
    private Integer personalArrangerId;
    private Integer personalProducerId;
    private Integer personalExclusiveProducerId;
    private String urlMaster;
    private String createTime;
    private String updateTime;

    //
    private String personalLyricistName;
    private String personalComposerName;
    private String personalArrangerName;
    private String personalProducerName;
    private String personalExclusiveProducerName;
    private String musicName;
    private String artistName;

    private String albumName;
    
    

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the shortLyrics
     */
    public String getShortLyrics() {
        return shortLyrics;
    }

    /**
     * @param shortLyrics the shortLyrics to set
     */
    public void setShortLyrics(String shortLyrics) {
        this.shortLyrics = shortLyrics;
    }

    /**
     * @return the lyrics
     */
    public String getLyrics() {
        return lyrics;
    }

    /**
     * @param lyrics the lyrics to set
     */
    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    /**
     * @return the personalLyricistId
     */
    public Integer getPersonalLyricistId() {
        return personalLyricistId;
    }

    /**
     * @param personalLyricistId the personalLyricistId to set
     */
    public void setPersonalLyricistId(Integer personalLyricistId) {
        this.personalLyricistId = personalLyricistId;
    }

    /**
     * @return the personalComposerId
     */
    public Integer getPersonalComposerId() {
        return personalComposerId;
    }

    /**
     * @param personalComposerId the personalComposerId to set
     */
    public void setPersonalComposerId(Integer personalComposerId) {
        this.personalComposerId = personalComposerId;
    }

    /**
     * @return the personalArrangerId
     */
    public Integer getPersonalArrangerId() {
        return personalArrangerId;
    }

    /**
     * @param personalArrangerId the personalArrangerId to set
     */
    public void setPersonalArrangerId(Integer personalArrangerId) {
        this.personalArrangerId = personalArrangerId;
    }

    /**
     * @return the personalProducerId
     */
    public Integer getPersonalProducerId() {
        return personalProducerId;
    }

    /**
     * @param personalProducerId the personalProducerId to set
     */
    public void setPersonalProducerId(Integer personalProducerId) {
        this.personalProducerId = personalProducerId;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the personalLyricistName
     */
    public String getPersonalLyricistName() {
        return personalLyricistName;
    }

    /**
     * @param personalLyricistName the personalLyricistName to set
     */
    public void setPersonalLyricistName(String personalLyricistName) {
        this.personalLyricistName = personalLyricistName;
    }

    /**
     * @return the personalComposerName
     */
    public String getPersonalComposerName() {
        return personalComposerName;
    }

    /**
     * @param personalComposerName the personalComposerName to set
     */
    public void setPersonalComposerName(String personalComposerName) {
        this.personalComposerName = personalComposerName;
    }

    /**
     * @return the personalArrangerName
     */
    public String getPersonalArrangerName() {
        return personalArrangerName;
    }

    /**
     * @param personalArrangerName the personalArrangerName to set
     */
    public void setPersonalArrangerName(String personalArrangerName) {
        this.personalArrangerName = personalArrangerName;
    }

    /**
     * @return the personalProducerName
     */
    public String getPersonalProducerName() {
        return personalProducerName;
    }

    /**
     * @param personalProducerName the personalProducerName to set
     */
    public void setPersonalProducerName(String personalProducerName) {
        this.personalProducerName = personalProducerName;
    }
    
    /**
     * @return the personalExclusiveProducerId
     */
    public Integer getPersonalExclusiveProducerId() {
        return personalExclusiveProducerId;
    }

    /**
     * @param personalExclusiveProducerId the personalExclusiveProducerId to set
     */
    public void setPersonalExclusiveProducerId(Integer personalExclusiveProducerId) {
        this.personalExclusiveProducerId = personalExclusiveProducerId;
    }

    /**
     * @return the urlMaster
     */
    public String getUrlMaster() {
        return urlMaster;
    }

    /**
     * @param urlMaster the urlMaster to set
     */
    public void setUrlMaster(String urlMaster) {
        this.urlMaster = urlMaster;
    }

    /**
     * @return the personalExclusiveProducerName
     */
    public String getPersonalExclusiveProducerName() {
        return personalExclusiveProducerName;
    }

    /**
     * @param personalExclusiveProducerName the personalExclusiveProducerName to set
     */
    public void setPersonalExclusiveProducerName(String personalExclusiveProducerName) {
        this.personalExclusiveProducerName = personalExclusiveProducerName;
    }

    /**
     * @return the musicName
     */
    public String getMusicName() {
        return musicName;
    }

    /**
     * @param musicName the musicName to set
     */
    public void setMusicName(String musicName) {
        this.musicName = musicName;
    }

    /**
     * @return the artistName
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * @param artistName the artistName to set
     */
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /**
     * @return the albumName
     */
    public String getAlbumName() {
        return albumName;
    }

    /**
     * @param albumName the albumName to set
     */
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }


}
