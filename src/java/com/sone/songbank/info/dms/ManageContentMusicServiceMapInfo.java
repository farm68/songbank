/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class ManageContentMusicServiceMapInfo {

    /**
     * @return the path
     */
    public String getPath() {
        return path;
    }

    /**
     * @param path the path to set
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * @return the releaseTime
     */
    public String getReleaseTime() {
        return releaseTime;
    }

    /**
     * @param releaseTime the releaseTime to set
     */
    public void setReleaseTime(String releaseTime) {
        this.releaseTime = releaseTime;
    }
    private Integer musicStateId;
    private Integer musicServiceGroupId;
    private String musicServiceGroupName;
    private Integer musicServiceId;
    private String musicServiceName;

    private String musicId;
    private String detail;
    private Integer stateId;
    private String stateName;
    private String remark;
    private String actionTime;
    private String releaseTime;
    private String path;

    public ManageContentMusicServiceMapInfo() {
    }

    public ManageContentMusicServiceMapInfo(String musicServiceGroupName, String musicServiceName, Integer musicServiceId,
            String musicId, String detail, Integer stateId, String stateName, String remark, String actionTime) {
        this.musicServiceGroupName = musicServiceGroupName;
        this.musicServiceName = musicServiceName;
        this.musicServiceId = musicServiceId;

        this.musicId = musicId;
        this.detail = detail;
        this.stateId = stateId;
        this.stateName = stateName;
        this.remark = remark;
        this.actionTime = actionTime;
    }

    public ManageContentMusicServiceMapInfo(Integer musicServiceGroupId, String musicServiceGroupName, Integer musicServiceId, String musicServiceName,
            String musicId, String detail, Integer stateId, String stateName, String remark, String actionTime) {
        this.musicServiceGroupId = musicServiceGroupId;
        this.musicServiceGroupName = musicServiceGroupName;
        this.musicServiceId = musicServiceId;
        this.musicServiceName = musicServiceName;

        this.musicId = musicId;
        this.detail = detail;
        this.stateId = stateId;
        this.stateName = stateName;
        this.remark = remark;
        this.actionTime = actionTime;
    }

    /**
     * @return the musicServiceGroupId
     */
    public Integer getMusicServiceGroupId() {
        return musicServiceGroupId;
    }

    /**
     * @param musicServiceGroupId the musicServiceGroupId to set
     */
    public void setMusicServiceGroupId(Integer musicServiceGroupId) {
        this.musicServiceGroupId = musicServiceGroupId;
    }

    /**
     * @return the musicServiceGroupName
     */
    public String getMusicServiceGroupName() {
        return musicServiceGroupName;
    }

    /**
     * @param musicServiceGroupName the musicServiceGroupName to set
     */
    public void setMusicServiceGroupName(String musicServiceGroupName) {
        this.musicServiceGroupName = musicServiceGroupName;
    }

    /**
     * @return the musicServiceId
     */
    public Integer getMusicServiceId() {
        return musicServiceId;
    }

    /**
     * @param musicServiceId the musicServiceId to set
     */
    public void setMusicServiceId(Integer musicServiceId) {
        this.musicServiceId = musicServiceId;
    }

    /**
     * @return the musicServiceName
     */
    public String getMusicServiceName() {
        return musicServiceName;
    }

    /**
     * @param musicServiceName the musicServiceName to set
     */
    public void setMusicServiceName(String musicServiceName) {
        this.musicServiceName = musicServiceName;
    }

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the detail
     */
    public String getDetail() {
        return detail;
    }

    /**
     * @param detail the detail to set
     */
    public void setDetail(String detail) {
        this.detail = detail;
    }

    /**
     * @return the stateId
     */
    public Integer getStateId() {
        return stateId;
    }

    /**
     * @param stateId the stateId to set
     */
    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    /**
     * @return the stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName the stateName to set
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return the remark
     */
    public String getRemark() {
        return remark;
    }

    /**
     * @param remark the remark to set
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * @return the actionTime
     */
    public String getActionTime() {
        return actionTime;
    }

    /**
     * @param actionTime the actionTime to set
     */
    public void setActionTime(String actionTime) {
        this.actionTime = actionTime;
    }

    /**
     * @return the musicStateId
     */
    public Integer getMusicStateId() {
        return musicStateId;
    }

    /**
     * @param musicStateId the musicStateId to set
     */
    public void setMusicStateId(Integer musicStateId) {
        this.musicStateId = musicStateId;
    }

}
