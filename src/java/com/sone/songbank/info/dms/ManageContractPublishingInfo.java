/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

/**
 *
 * @author DELL
 */
public class ManageContractPublishingInfo {

    private Integer contractId;
    private Integer contractDetailId;
    private String contractName;

    private String contractDate;
    private String contractStartDate;
    private String contractEndDate;

    private String partnerName;
    private String partnerAddress;
    private String partnerIdCard;
    private String partnerLabel;

    private Float refShareYtMcn;
    private Float refShareYtCreator;

    public ManageContractPublishingInfo() {
    }

    public ManageContractPublishingInfo(Integer contractId, Integer contractDetailId, String contractName,
            String contractDate, String contractStartDate, String contractEndDate,
            String partnerName, String partnerAddress, String partnerIdCard, String partnerLabel,
            Float refShareYtMcn, Float refShareYtCreator) {
        this.contractId = contractId;
        this.contractDetailId = contractDetailId;
        this.contractName = contractName;

        this.contractDate = contractDate;
        this.contractStartDate = contractStartDate;
        this.contractEndDate = contractEndDate;

        this.partnerName = partnerName;
        this.partnerAddress = partnerAddress;
        this.partnerIdCard = partnerIdCard;
        this.partnerLabel = partnerLabel;

        this.refShareYtMcn = refShareYtMcn;
        this.refShareYtCreator = refShareYtCreator;
    }

    /**
     * @return the contractId
     */
    public Integer getContractId() {
        return contractId;
    }

    /**
     * @param contractId the contractId to set
     */
    public void setContractId(Integer contractId) {
        this.contractId = contractId;
    }

    /**
     * @return the contractDetailId
     */
    public Integer getContractDetailId() {
        return contractDetailId;
    }

    /**
     * @param contractDetailId the contractDetailId to set
     */
    public void setContractDetailId(Integer contractDetailId) {
        this.contractDetailId = contractDetailId;
    }

    /**
     * @return the contractName
     */
    public String getContractName() {
        return contractName;
    }

    /**
     * @param contractName the contractName to set
     */
    public void setContractName(String contractName) {
        this.contractName = contractName;
    }

    /**
     * @return the contractDate
     */
    public String getContractDate() {
        return contractDate;
    }

    /**
     * @param contractDate the contractDate to set
     */
    public void setContractDate(String contractDate) {
        this.contractDate = contractDate;
    }

    /**
     * @return the contractStartDate
     */
    public String getContractStartDate() {
        return contractStartDate;
    }

    /**
     * @param contractStartDate the contractStartDate to set
     */
    public void setContractStartDate(String contractStartDate) {
        this.contractStartDate = contractStartDate;
    }

    /**
     * @return the contractEndDate
     */
    public String getContractEndDate() {
        return contractEndDate;
    }

    /**
     * @param contractEndDate the contractEndDate to set
     */
    public void setContractEndDate(String contractEndDate) {
        this.contractEndDate = contractEndDate;
    }

    /**
     * @return the partnerName
     */
    public String getPartnerName() {
        return partnerName;
    }

    /**
     * @param partnerName the partnerName to set
     */
    public void setPartnerName(String partnerName) {
        this.partnerName = partnerName;
    }

    /**
     * @return the partnerAddress
     */
    public String getPartnerAddress() {
        return partnerAddress;
    }

    /**
     * @param partnerAddress the partnerAddress to set
     */
    public void setPartnerAddress(String partnerAddress) {
        this.partnerAddress = partnerAddress;
    }

    /**
     * @return the partnerIdCard
     */
    public String getPartnerIdCard() {
        return partnerIdCard;
    }

    /**
     * @param partnerIdCard the partnerIdCard to set
     */
    public void setPartnerIdCard(String partnerIdCard) {
        this.partnerIdCard = partnerIdCard;
    }

    /**
     * @return the partnerLabel
     */
    public String getPartnerLabel() {
        return partnerLabel;
    }

    /**
     * @param partnerLabel the partnerLabel to set
     */
    public void setPartnerLabel(String partnerLabel) {
        this.partnerLabel = partnerLabel;
    }

    /**
     * @return the refShareYtMcn
     */
    public Float getRefShareYtMcn() {
        return refShareYtMcn;
    }

    /**
     * @param refShareYtMcn the refShareYtMcn to set
     */
    public void setRefShareYtMcn(Float refShareYtMcn) {
        this.refShareYtMcn = refShareYtMcn;
    }

    /**
     * @return the refShareYtCreator
     */
    public Float getRefShareYtCreator() {
        return refShareYtCreator;
    }

    /**
     * @param refShareYtCreator the refShareYtCreator to set
     */
    public void setRefShareYtCreator(Float refShareYtCreator) {
        this.refShareYtCreator = refShareYtCreator;
    }

}
