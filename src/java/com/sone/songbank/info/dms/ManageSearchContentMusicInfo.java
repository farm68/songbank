/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.dms;

import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class ManageSearchContentMusicInfo {

    private String musicId;
    private Integer labelId;
    private String labelName;
    private Integer genreId;
    private String genreName;
    private String genreCode;
    private Integer artistId;
    private String artistName;
    private String artistNameEng;
    private Integer albumId;
    private String albumName;
    private String albumNameEng;
    private String albumArtistName;
    private String isrc;
    private String parentLable;
//    
    private String nameEn;
    private String nameTh;
//    
    private Integer copyrightId;
    private String copyrightName;
    //
    private Integer publisherId;
    private String publisherName;
    private String releaseDate;
    private Integer exclusiveMusicServiceId;
    private String exclusiveMusicServiceName;
    private String exclusiveDate;
//    
    private Integer stateId;
    private String stateName;
//    
    private String createTime;
    private String updateTime;
    private String upcCode;
    private String pathName;
    private String description;
    private String note;

    private String labelNameEn;

    private String artistAlbumNameTh;
    private String artistAlbumNameEn;

    private DmsContentMusicPublishingInfo musicPublishingInfo;
    private Integer contractDetailId;
    private Integer mapPriority;

    private String lyrics;
    private String oneLinkUrl;
    private String thumbnailUrl;

    private ArrayList<ManageContentMusicServiceMapInfo> listMusicServiceMap;

    public ManageSearchContentMusicInfo() {
    }

    public ManageSearchContentMusicInfo(String musicId, Integer labelId, String labelName, Integer genreId, String genreName, Integer artistId, String artistName, Integer albumId, String albumName, String upcCode, String isrc,
            String nameEn, String nameTh, Integer copyrightId, String copyrightName,
            Integer publisherId, String publisherName, String releaseDate, Integer exclusiveMusicServiceId, String exclusiveMusicServiceName, String exclusiveDate,
            Integer stateId, String stateName, String createTime, String updateTime, ArrayList<ManageContentMusicServiceMapInfo> listMusicServiceMap) {
        this.musicId = musicId;
        this.labelId = labelId;
        this.labelName = labelName;
        this.genreId = genreId;
        this.genreName = genreName;
        this.artistId = artistId;
        this.artistName = artistName;
        this.albumId = albumId;
        this.albumName = albumName;
        this.upcCode = upcCode;
        this.isrc = isrc;
//    
        this.nameEn = nameEn;
        this.nameTh = nameTh;
        this.copyrightId = copyrightId;
        this.copyrightName = copyrightName;
//    
        this.publisherId = publisherId;
        this.publisherName = publisherName;
        this.releaseDate = releaseDate;
        this.exclusiveMusicServiceId = exclusiveMusicServiceId;
        this.exclusiveMusicServiceName = exclusiveMusicServiceName;
        this.exclusiveDate = exclusiveDate;
//    
        this.stateId = stateId;
        this.stateName = stateName;
        this.createTime = createTime;
        this.updateTime = updateTime;
//        
        this.listMusicServiceMap = listMusicServiceMap;
    }

    /**
     * @return the musicId
     */
    public String getMusicId() {
        return musicId;
    }

    /**
     * @param musicId the musicId to set
     */
    public void setMusicId(String musicId) {
        this.musicId = musicId;
    }

    /**
     * @return the labelId
     */
    public Integer getLabelId() {
        return labelId;
    }

    /**
     * @param labelId the labelId to set
     */
    public void setLabelId(Integer labelId) {
        this.labelId = labelId;
    }

    /**
     * @return the labelName
     */
    public String getLabelName() {
        return labelName;
    }

    /**
     * @param labelName the labelName to set
     */
    public void setLabelName(String labelName) {
        this.labelName = labelName;
    }

    /**
     * @return the genreId
     */
    public Integer getGenreId() {
        return genreId;
    }

    /**
     * @param genreId the genreId to set
     */
    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    /**
     * @return the genreName
     */
    public String getGenreName() {
        return genreName;
    }

    /**
     * @param genreName the genreName to set
     */
    public void setGenreName(String genreName) {
        this.genreName = genreName;
    }

    /**
     * @return the artistId
     */
    public Integer getArtistId() {
        return artistId;
    }

    /**
     * @param artistId the artistId to set
     */
    public void setArtistId(Integer artistId) {
        this.artistId = artistId;
    }

    /**
     * @return the artistName
     */
    public String getArtistName() {
        return artistName;
    }

    /**
     * @param artistName the artistName to set
     */
    public void setArtistName(String artistName) {
        this.artistName = artistName;
    }

    /**
     * @return the albumId
     */
    public Integer getAlbumId() {
        return albumId;
    }

    /**
     * @param albumId the albumId to set
     */
    public void setAlbumId(Integer albumId) {
        this.albumId = albumId;
    }

    /**
     * @return the albumName
     */
    public String getAlbumName() {
        return albumName;
    }

    /**
     * @param albumName the albumName to set
     */
    public void setAlbumName(String albumName) {
        this.albumName = albumName;
    }

    /**
     * @return the isrc
     */
    public String getIsrc() {
        return isrc;
    }

    /**
     * @param isrc the isrc to set
     */
    public void setIsrc(String isrc) {
        this.isrc = isrc;
    }

    /**
     * @return the nameEn
     */
    public String getNameEn() {
        return nameEn;
    }

    /**
     * @param nameEn the nameEn to set
     */
    public void setNameEn(String nameEn) {
        this.nameEn = nameEn;
    }

    /**
     * @return the nameTh
     */
    public String getNameTh() {
        return nameTh;
    }

    /**
     * @param nameTh the nameTh to set
     */
    public void setNameTh(String nameTh) {
        this.nameTh = nameTh;
    }

    /**
     * @return the copyrightId
     */
    public Integer getCopyrightId() {
        return copyrightId;
    }

    /**
     * @param copyrightId the copyrightId to set
     */
    public void setCopyrightId(Integer copyrightId) {
        this.copyrightId = copyrightId;
    }

    /**
     * @return the copyrightName
     */
    public String getCopyrightName() {
        return copyrightName;
    }

    /**
     * @param copyrightName the copyrightName to set
     */
    public void setCopyrightName(String copyrightName) {
        this.copyrightName = copyrightName;
    }

    /**
     * @return the publisherId
     */
    public Integer getPublisherId() {
        return publisherId;
    }

    /**
     * @param publisherId the publisherId to set
     */
    public void setPublisherId(Integer publisherId) {
        this.publisherId = publisherId;
    }

    /**
     * @return the publisherName
     */
    public String getPublisherName() {
        return publisherName;
    }

    /**
     * @param publisherName the publisherName to set
     */
    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    /**
     * @return the releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the exclusiveMusicServiceId
     */
    public Integer getExclusiveMusicServiceId() {
        return exclusiveMusicServiceId;
    }

    /**
     * @param exclusiveMusicServiceId the exclusiveMusicServiceId to set
     */
    public void setExclusiveMusicServiceId(Integer exclusiveMusicServiceId) {
        this.exclusiveMusicServiceId = exclusiveMusicServiceId;
    }

    /**
     * @return the exclusiveMusicServiceName
     */
    public String getExclusiveMusicServiceName() {
        return exclusiveMusicServiceName;
    }

    /**
     * @param exclusiveMusicServiceName the exclusiveMusicServiceName to set
     */
    public void setExclusiveMusicServiceName(String exclusiveMusicServiceName) {
        this.exclusiveMusicServiceName = exclusiveMusicServiceName;
    }

    /**
     * @return the exclusiveDate
     */
    public String getExclusiveDate() {
        return exclusiveDate;
    }

    /**
     * @param exclusiveDate the exclusiveDate to set
     */
    public void setExclusiveDate(String exclusiveDate) {
        this.exclusiveDate = exclusiveDate;
    }

    /**
     * @return the stateId
     */
    public Integer getStateId() {
        return stateId;
    }

    /**
     * @param stateId the stateId to set
     */
    public void setStateId(Integer stateId) {
        this.stateId = stateId;
    }

    /**
     * @return the stateName
     */
    public String getStateName() {
        return stateName;
    }

    /**
     * @param stateName the stateName to set
     */
    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the updateTime
     */
    public String getUpdateTime() {
        return updateTime;
    }

    /**
     * @param updateTime the updateTime to set
     */
    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    /**
     * @return the listMusicServiceMap
     */
    public ArrayList<ManageContentMusicServiceMapInfo> getListMusicServiceMap() {
        return listMusicServiceMap;
    }

    /**
     * @param listMusicServiceMap the listMusicServiceMap to set
     */
    public void setListMusicServiceMap(ArrayList<ManageContentMusicServiceMapInfo> listMusicServiceMap) {
        this.listMusicServiceMap = listMusicServiceMap;
    }

    /**
     * @return the upcCode
     */
    public String getUpcCode() {
        return upcCode;
    }

    /**
     * @param upcCode the upcCode to set
     */
    public void setUpcCode(String upcCode) {
        this.upcCode = upcCode;
    }

    public String getArtistNameEng() {
        return artistNameEng;
    }

    public void setArtistNameEng(String artistNameEng) {
        this.artistNameEng = artistNameEng;
    }

    public String getAlbumNameEng() {
        return albumNameEng;
    }

    public void setAlbumNameEng(String albumNameEng) {
        this.albumNameEng = albumNameEng;
    }

    public String getAlbumArtistName() {
        return albumArtistName;
    }

    public void setAlbumArtistName(String albumArtistName) {
        this.albumArtistName = albumArtistName;
    }

    public String getParentLable() {
        return parentLable;
    }

    public void setParentLable(String parentLable) {
        this.parentLable = parentLable;
    }

    public String getPathName() {
        return pathName;
    }

    public void setPathName(String pathName) {
        this.pathName = pathName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    /**
     * @return the labelNameEn
     */
    public String getLabelNameEn() {
        return labelNameEn;
    }

    /**
     * @param labelNameEn the labelNameEn to set
     */
    public void setLabelNameEn(String labelNameEn) {
        this.labelNameEn = labelNameEn;
    }

    /**
     * @return the artistAlbumNameTh
     */
    public String getArtistAlbumNameTh() {
        return artistAlbumNameTh;
    }

    /**
     * @param artistAlbumNameTh the artistAlbumNameTh to set
     */
    public void setArtistAlbumNameTh(String artistAlbumNameTh) {
        this.artistAlbumNameTh = artistAlbumNameTh;
    }

    /**
     * @return the artistAlbumNameEn
     */
    public String getArtistAlbumNameEn() {
        return artistAlbumNameEn;
    }

    /**
     * @param artistAlbumNameEn the artistAlbumNameEn to set
     */
    public void setArtistAlbumNameEn(String artistAlbumNameEn) {
        this.artistAlbumNameEn = artistAlbumNameEn;
    }

    /**
     * @return the musicPublishingInfo
     */
    public DmsContentMusicPublishingInfo getMusicPublishingInfo() {
        return musicPublishingInfo;
    }

    /**
     * @param musicPublishingInfo the musicPublishingInfo to set
     */
    public void setMusicPublishingInfo(DmsContentMusicPublishingInfo musicPublishingInfo) {
        this.musicPublishingInfo = musicPublishingInfo;
    }

    /**
     * @return the contractDetailId
     */
    public Integer getContractDetailId() {
        return contractDetailId;
    }

    /**
     * @param contractDetailId the contractDetailId to set
     */
    public void setContractDetailId(Integer contractDetailId) {
        this.contractDetailId = contractDetailId;
    }

    /**
     * @return the mapPriority
     */
    public Integer getMapPriority() {
        return mapPriority;
    }

    /**
     * @param mapPriority the mapPriority to set
     */
    public void setMapPriority(Integer mapPriority) {
        this.mapPriority = mapPriority;
    }

    public String getGenreCode() {
        return genreCode;
    }

    public void setGenreCode(String genreCode) {
        this.genreCode = genreCode;
    }

    public String getLyrics() {
        return lyrics;
    }

    public void setLyrics(String lyrics) {
        this.lyrics = lyrics;
    }

    /**
     * @return the oneLinkUrl
     */
    public String getOneLinkUrl() {
        return oneLinkUrl;
    }

    /**
     * @param oneLinkUrl the oneLinkUrl to set
     */
    public void setOneLinkUrl(String oneLinkUrl) {
        this.oneLinkUrl = oneLinkUrl;
    }

    /**
     * @return the thumbnailUrl
     */
    public String getThumbnailUrl() {
        return thumbnailUrl;
    }

    /**
     * @param thumbnailUrl the thumbnailUrl to set
     */
    public void setThumbnailUrl(String thumbnailUrl) {
        this.thumbnailUrl = thumbnailUrl;
    }

}
