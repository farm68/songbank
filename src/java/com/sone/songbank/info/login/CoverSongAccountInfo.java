/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.login;

/**
 *
 * @author jiranuwat
 */
public class CoverSongAccountInfo {

    private Integer accountId;
    private String username;
    private String password;
    private String displayName;
    private String description;
    private String name;
    private String prefixName;
    private String aliasName;
    private String email;
    private String tel;

    private Integer cmsUserId;
    private String youtubeChannelId;
    private String createTime;
    
    private String youtubeChannelName;
    private String youtubeChannelImage;
    private Boolean isYoutubeChannelActive;
    private Boolean isYoutubeCmsLink;
    private Long youtubeSubscriber;
    private Long youtubeViews;
    private Long youtubeVideos;
   

    /**
     * @return the accountId
     */
    public Integer getAccountId() {
        return accountId;
    }

    /**
     * @param accountId the accountId to set
     */
    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the prefixName
     */
    public String getPrefixName() {
        return prefixName;
    }

    /**
     * @param prefixName the prefixName to set
     */
    public void setPrefixName(String prefixName) {
        this.prefixName = prefixName;
    }

    /**
     * @return the aliasName
     */
    public String getAliasName() {
        return aliasName;
    }

    /**
     * @param aliasName the aliasName to set
     */
    public void setAliasName(String aliasName) {
        this.aliasName = aliasName;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the tel
     */
    public String getTel() {
        return tel;
    }

    /**
     * @param tel the tel to set
     */
    public void setTel(String tel) {
        this.tel = tel;
    }

    /**
     * @return the cmsUserId
     */
    public Integer getCmsUserId() {
        return cmsUserId;
    }

    /**
     * @param cmsUserId the cmsUserId to set
     */
    public void setCmsUserId(Integer cmsUserId) {
        this.cmsUserId = cmsUserId;
    }

    /**
     * @return the youtubeChannelId
     */
    public String getYoutubeChannelId() {
        return youtubeChannelId;
    }

    /**
     * @param youtubeChannelId the youtubeChannelId to set
     */
    public void setYoutubeChannelId(String youtubeChannelId) {
        this.youtubeChannelId = youtubeChannelId;
    }

    /**
     * @return the createTime
     */
    public String getCreateTime() {
        return createTime;
    }

    /**
     * @param createTime the createTime to set
     */
    public void setCreateTime(String createTime) {
        this.createTime = createTime;
    }

    /**
     * @return the displayName
     */
    public String getDisplayName() {
        return displayName;
    }

    /**
     * @param displayName the displayName to set
     */
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    /**
     * @return the youtubeChannelName
     */
    public String getYoutubeChannelName() {
        return youtubeChannelName;
    }

    /**
     * @param youtubeChannelName the youtubeChannelName to set
     */
    public void setYoutubeChannelName(String youtubeChannelName) {
        this.youtubeChannelName = youtubeChannelName;
    }

    /**
     * @return the youtubeChannelImage
     */
    public String getYoutubeChannelImage() {
        return youtubeChannelImage;
    }

    /**
     * @param youtubeChannelImage the youtubeChannelImage to set
     */
    public void setYoutubeChannelImage(String youtubeChannelImage) {
        this.youtubeChannelImage = youtubeChannelImage;
    }
 

    /**
     * @return the isYoutubeCmsLink
     */
    public Boolean getIsYoutubeCmsLink() {
        return isYoutubeCmsLink;
    }

    /**
     * @param isYoutubeCmsLink the isYoutubeCmsLink to set
     */
    public void setIsYoutubeCmsLink(Boolean isYoutubeCmsLink) {
        this.isYoutubeCmsLink = isYoutubeCmsLink;
    }

    /**
     * @return the youtubeSubscriber
     */
    public Long getYoutubeSubscriber() {
        return youtubeSubscriber;
    }

    /**
     * @param youtubeSubscriber the youtubeSubscriber to set
     */
    public void setYoutubeSubscriber(Long youtubeSubscriber) {
        this.youtubeSubscriber = youtubeSubscriber;
    }

    /**
     * @return the youtubeViews
     */
    public Long getYoutubeViews() {
        return youtubeViews;
    }

    /**
     * @param youtubeViews the youtubeViews to set
     */
    public void setYoutubeViews(Long youtubeViews) {
        this.youtubeViews = youtubeViews;
    }

    /**
     * @return the youtubeVideos
     */
    public Long getYoutubeVideos() {
        return youtubeVideos;
    }

    /**
     * @param youtubeVideos the youtubeVideos to set
     */
    public void setYoutubeVideos(Long youtubeVideos) {
        this.youtubeVideos = youtubeVideos;
    }

    /**
     * @return the isYoutubeChannelActive
     */
    public Boolean getIsYoutubeChannelActive() {
        return isYoutubeChannelActive;
    }

    /**
     * @param isYoutubeChannelActive the isYoutubeChannelActive to set
     */
    public void setIsYoutubeChannelActive(Boolean isYoutubeChannelActive) {
        this.isYoutubeChannelActive = isYoutubeChannelActive;
    }
}
