/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.info.login;

import com.sone.songbank.info.DmsArtistPortalInfo;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import java.util.ArrayList;

/**
 *
 * @author DELL
 */
public class SessionUserInfo {
    
    private CoverSongAccountInfo coverSongAccountInfo;
    private ManageYoutubeChannelInfo youtubeChannelInfo; 
    private DmsArtistPortalInfo artistPortalInfo; 

    public SessionUserInfo() {
    }

    public SessionUserInfo(ManageYoutubeChannelInfo youtubeChannelInfo,DmsArtistPortalInfo artistPortalInfo) {
        this.youtubeChannelInfo = youtubeChannelInfo;
        this.artistPortalInfo = artistPortalInfo; 
    }

    /**
     * @return the youtubeChannelInfo
     */
    public ManageYoutubeChannelInfo getYoutubeChannelInfo() {
        return youtubeChannelInfo;
    }

    /**
     * @param youtubeChannelInfo the youtubeChannelInfo to set
     */
    public void setYoutubeChannelInfo(ManageYoutubeChannelInfo youtubeChannelInfo) {
        this.youtubeChannelInfo = youtubeChannelInfo;
    }

    /**
     * @return the artistPortalInfo
     */
    public DmsArtistPortalInfo getArtistPortalInfo() {
        return artistPortalInfo;
    }

    /**
     * @param artistPortalInfo the artistPortalInfo to set
     */
    public void setArtistPortalInfo(DmsArtistPortalInfo artistPortalInfo) {
        this.artistPortalInfo = artistPortalInfo;
    }

    /**
     * @return the coverSongAccountInfo
     */
    public CoverSongAccountInfo getCoverSongAccountInfo() {
        return coverSongAccountInfo;
    }

    /**
     * @param coverSongAccountInfo the coverSongAccountInfo to set
     */
    public void setCoverSongAccountInfo(CoverSongAccountInfo coverSongAccountInfo) {
        this.coverSongAccountInfo = coverSongAccountInfo;
    }
 

   
 
}
