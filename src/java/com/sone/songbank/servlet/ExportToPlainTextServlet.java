/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.sone.songbank.db.DbDmsDbMgr;
import com.sone.songbank.info.dms.ManageContentMusicCompositionInfo;
import com.sone.songbank.util.Utilities;
import com.sone.util.ThaiConvert;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URLDecoder;
import java.net.URLEncoder;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class ExportToPlainTextServlet extends HttpServlet {

    private static final String CONTENT_TYPE = "text/plain";
    private Logger logger = Logger.getLogger(ExportToPlainTextServlet.class);

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);

        logger.info(" ===== ExportToTextPlainServlet ===== ");
        String sMode = ThaiConvert.nullToString(request.getParameter("mode"));
        String sMusicId = ThaiConvert.nullToString(request.getParameter("musicId"));
        String result = null;
        DbDmsDbMgr dbMgr = null;
        try {
            dbMgr = new DbDmsDbMgr();
            dbMgr.initializeConnection();

            if (!Utilities.isBlankText(sMusicId)) {

                ManageContentMusicCompositionInfo compositionInfo = dbMgr.getMusicComposition(sMusicId);
                String fileName = "Lyrics_" + URLEncoder.encode(compositionInfo.getMusicName().trim()+"-"+compositionInfo.getArtistName().trim(), "UTF-8") + ".txt";
                if (sMode.trim().equals("EXPORT_MUSIC_LYRICS")) {
                    result = exportContentMusicLyric(request, dbMgr, compositionInfo);
                }

                response.setHeader("Content-Disposition", "attachment;filename=" + fileName + "");

                InputStream input = new ByteArrayInputStream(result.getBytes("UTF8"));

                int read = 0;
                byte[] bytes = new byte[result.getBytes("UTF-8").length * 2];
                OutputStream os = response.getOutputStream();
                while ((read = input.read(bytes)) != -1) {
                    os.write(bytes, 0, read);
                }

                os.flush();
                os.close();
            }
        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            try {
                dbMgr.releaseConnection();
                dbMgr = null;
            } catch (Exception ex) {
                logger.error("", ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String exportContentMusicLyric(HttpServletRequest request, DbDmsDbMgr dbMgr, ManageContentMusicCompositionInfo info) {
        StringBuilder result = new StringBuilder();
        try {

            if (info != null) {
                result.append("เพลง " + info.getMusicName() + "\n");
                result.append("ศิลปิน : " + info.getArtistName() + "\n");
                if (!Utilities.isBlankText(info.getPersonalLyricistName())) {
                    result.append("ผู้แต่งคำร้อง : " + info.getPersonalLyricistName() + "\n");
                }
                if (!Utilities.isBlankText(info.getPersonalComposerName())) {
                    result.append("ผู้แต่งทำนอง : " + info.getPersonalComposerName() + "\n");
                }
                if (!Utilities.isBlankText(info.getPersonalArrangerName())) {
                    result.append("เรียบเรียง : " + info.getPersonalArrangerName() + "\n");
                }
                if (!Utilities.isBlankText(info.getPersonalProducerName())) {
                    result.append("Producer : " + info.getPersonalProducerName() + "\n");
                }
                if (!Utilities.isBlankText(info.getPersonalExclusiveProducerName())) {
                    result.append("Exclusive Producer : " + info.getPersonalExclusiveProducerName() + "\n");
                }
                result.append("\nเนื้อเพลง " + "\n" + info.getLyrics() + "\n");

            }
        } catch (Exception ex) {
            logger.error("", ex);
        }

        return result.toString();
    }
}
