/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.sone.songbank.db.DbDmsDbMgr;
import com.sone.songbank.info.coversong.ListSelectInfo;
import com.sone.songbank.info.dms.CmsUserInfo;
import com.sone.songbank.info.dms.DmsArtistInfo;
import com.sone.songbank.info.login.SessionUserInfo;
import com.sone.songbank.util.Constants;
import com.sone.util.ThaiConvert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class GenerateListSelectServlet extends HttpServlet {

    Logger logger = Logger.getLogger(GenerateListSelectServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        String sMode = ThaiConvert.nullToThaiString(request.getParameter("mode"));
//        System.out.println("sMode: " + sMode);
        /*== search application list ==*/

        String sIsNotAll = ThaiConvert.nullToThaiString(request.getParameter("isNotAll"));
        String sListUserGroupId = ThaiConvert.nullToThaiString(request.getParameter("userGroupId"));

        int isNotAll = -1;
        try {
            isNotAll = Integer.parseInt(sIsNotAll);
        } catch (Exception e) {
        }

        String result = "";
        DbDmsDbMgr dbMgr = null;

        try {
            dbMgr = new DbDmsDbMgr();
            dbMgr.initializeConnection();

            if (sMode.trim().equals("LIST_ARTIST")) {
                result = listArtist(dbMgr, request, isNotAll);
            } else if (sMode.trim().equals("LIST_COVER_SONG_STATUS")) {
                result = listCoverSongStatus(dbMgr, isNotAll);
            } else if (sMode.trim().equals("LIST_MUSIC_CATEGORY")) {
                result = listMusicCategory(dbMgr, isNotAll);
            } else if (sMode.trim().equals("LIST_MUSIC_COMPOSER_SUGGEST")) {
                result = listMusicComposerSuggest(dbMgr, isNotAll);
            } else if (sMode.trim().equals("LIST_USER_ACCOUNT")) {
                result = listUserAccount(dbMgr, sListUserGroupId, isNotAll);
            }

            logger.info("GenerateListSelectServlet [" + sMode + "]");
            out.println(result);

//            logger.info(" [" + result + "]");
        } catch (Exception e) {
            logger.info("", e);
        } finally {
            out.close();

            try {
                dbMgr.releaseConnection();
                dbMgr = null;
            } catch (Exception e) {
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String listArtist(DbDmsDbMgr dbMgr, HttpServletRequest request, int notAll) {

        String artistPortalId = "";
        String channelId = "";
        SessionUserInfo sessionUserInfo = null;

        try {
            HttpSession session = request.getSession();
            sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
            artistPortalId = sessionUserInfo.getArtistPortalInfo().getArtistPortalId();

        } catch (Exception ex) {
        }

        JSONArray jArr = new JSONArray();

        if (notAll != 1) {
            JSONObject jObjAll = new JSONObject();
            jObjAll.element("id", "");
            jObjAll.element("text", "+++ select all +++");
            jArr.add(jObjAll);
        }

        ArrayList<DmsArtistInfo> aList = dbMgr.listArtistSelect(artistPortalId);
//
        if (aList != null && aList.size() > 0) {

            for (DmsArtistInfo info : aList) {
                JSONObject jObj = new JSONObject();
                jObj.element("id", info.getArtistId());
                jObj.element("text", info.getArtistNameTh());
                jArr.add(jObj);
            }
//        listArtistSelect

        }
        return jArr.toString();
    }

    private String listCoverSongStatus(DbDmsDbMgr dbMgr, int notAll) {
        JSONArray jArr = new JSONArray();

        if (notAll != 1) {
            JSONObject jObjAll = new JSONObject();
            jObjAll.element("id", "");
            jObjAll.element("text", "+++ select all +++");
            jArr.add(jObjAll);
        }

        ArrayList<ListSelectInfo> aList = dbMgr.listCoverStatusSelect();

        if (aList != null && aList.size() > 0) {
            for (ListSelectInfo info : aList) {
                JSONObject jObj = new JSONObject();
                jObj.element("id", info.getId());
                jObj.element("text", info.getName());
                jArr.add(jObj);
            }
        }

        return jArr.toString();
    }

    private String listMusicCategory(DbDmsDbMgr dbMgr, int notAll) {
        JSONArray jArr = new JSONArray();
        if (notAll != 1) {
            JSONObject jObjAll = new JSONObject();
            jObjAll.element("id", "");
            jObjAll.element("text", "+++ select all +++");
            jArr.add(jObjAll);
        }

        ArrayList<ListSelectInfo> aList = dbMgr.listMusicCategorySelect();
        if (aList != null && aList.size() > 0) {
            for (ListSelectInfo info : aList) {
                JSONObject jObj = new JSONObject();
                jObj.element("id", info.getId());
                jObj.element("text", info.getName());
                jArr.add(jObj);
            }
        }

        return jArr.toString();
    }

    private String listMusicComposerSuggest(DbDmsDbMgr dbMgr, int notAll) {
        JSONArray jArr = new JSONArray();
        ArrayList<ListSelectInfo> aList = dbMgr.listCompLyrics();
        if (aList != null && aList.size() > 0) {
            for (ListSelectInfo info : aList) {
                JSONObject jObj = new JSONObject();
                jObj.element("text", info.getName());
                jArr.add(jObj);
            }
        }

        return jArr.toString();
    }

    private String listUserAccount(DbDmsDbMgr dbMgr, String listUserAccount, int notAll) {
        JSONArray jArr = new JSONArray();
        if (notAll != 1) {
            JSONObject jObjAll = new JSONObject();
            jObjAll.element("id", "");
            jObjAll.element("text", "+++ select +++");
            jArr.add(jObjAll);
        }

        ArrayList<CmsUserInfo> aList = dbMgr.listCmsUserByGroup(listUserAccount);
        if (aList != null && aList.size() > 0) {
            for (CmsUserInfo info : aList) {
                JSONObject jObj = new JSONObject();
                jObj.element("id", info.getUserId());
                jObj.element("text", info.getDisplayName() + " : " + info.getUserName() + " (" + info.getEmail() + ")");
                jArr.add(jObj);
            }
        }

        return jArr.toString();
    }
}
