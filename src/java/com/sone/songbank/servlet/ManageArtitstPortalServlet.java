/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.google.gson.Gson;
import com.sone.songbank.db.DmsYoutubeDbMgr;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.util.ThaiConvert;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author DELL
 */
@WebServlet(name = "ManageArtitstPortalServlet", urlPatterns = {"/ManageArtitstPortalServlet"})
public class ManageArtitstPortalServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(ManageArtitstPortalServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        logger.info("########## Start ManageOneLinkServiceServlet ##########");
        final String ACCOUNT_USER_AGENT = request.getHeader("User-Agent");
        final String ACCOUNT_IP_ADDRESS = request.getRemoteAddr();
        logger.info("[ManageOneLinkServiceServlet]User Agent: " + ACCOUNT_USER_AGENT);
        logger.info("[ManageOneLinkServiceServlet]User IP: " + ACCOUNT_IP_ADDRESS);

        //################# GET PARAMETER //#################
        String sMode = ThaiConvert.nullToString(request.getParameter("mode"));
        //################################################### 
        String result = "";
        DmsYoutubeDbMgr dbMgr = null;
        try {

            dbMgr = new DmsYoutubeDbMgr();
            dbMgr.initializeConnection();

            if (sMode.trim().equals("GET_YOUTUBE_CHANNEL")) {
                result = getYoutubeChannel(dbMgr, request);
            } else if (sMode.trim().equals("UPDATE_YOUTUBE_CHANNEL")) {
                result = updateYoutubeChannel(dbMgr, request);
            } else if (sMode.trim().equals("UPDATE_ARTIST_PORTAL")) {
                result = updateArtistPortal(dbMgr, request);
            }
            out.print(result);
        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            try {
                dbMgr.releaseConnection();
            } catch (Exception e) {
            }
            dbMgr = null;
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getYoutubeChannel(DmsYoutubeDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jResult = new JSONObject();
        String sChannelId = ThaiConvert.nullToString(request.getParameter("channelId"));
        String jsonResult = "[]";
        ManageYoutubeChannelInfo info = dbMgr.getYoutubeChannelInfo(sChannelId);
        ArrayList<ManageYoutubeChannelInfo> listInfo = dbMgr.listYoutubeChannelInfo(sChannelId);
        if (listInfo != null) {
            info.setListChilChannelInfo(listInfo);
        }
        boolean isSyncData = false;
        if (info != null) {
            jsonResult = new Gson().toJson(info);
            try {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
                Date firstDate = sdf.parse(info.getUpdateTime());
                Date secondDate = sdf.parse(sdf.format(new Date()));
                long duration = firstDate.getTime() - secondDate.getTime();
                long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);
                System.out.println("diffInDays : " + diffInDays);
                if (diffInDays <= -30) {
                    isSyncData = true;
                }
            } catch (Exception ex) {
            }
        } else {
            isSyncData = true;
        }

        jResult.put("drList", jsonResult);

        return jResult.toString();
    }

    private String updateYoutubeChannel(DmsYoutubeDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        String sChannelId = ThaiConvert.nullToString(request.getParameter("channelId"));
        String sChannelTitle = ThaiConvert.nullToString(request.getParameter("channelTitle"));
        String sTotalSubscriber = ThaiConvert.nullToString(request.getParameter("totalSubscriber"));
        String sTotalVideos = ThaiConvert.nullToString(request.getParameter("totalVideos"));
        String sTotalViews = ThaiConvert.nullToString(request.getParameter("totalViews"));
        String sChannelDesc = ThaiConvert.nullToString(request.getParameter("channelDesc"));
        String sChannelKeywords = ThaiConvert.nullToString(request.getParameter("channelKeywords"));
        String sIsLinked = ThaiConvert.nullToString(request.getParameter("isLinked"));
        String sStatus = ThaiConvert.nullToString(request.getParameter("status"));
        String sCountry = ThaiConvert.nullToString(request.getParameter("country"));
        String sJoinedTime = ThaiConvert.nullToString(request.getParameter("joinedTime"));
        String sChannelCoverUrl = ThaiConvert.nullToString(request.getParameter("imageCoverUrl"));
        String sChannelThumbnailUrl = ThaiConvert.nullToString(request.getParameter("imageThumbnailUrl"));

        int isLinked = 1;
        if (sIsLinked != null && sIsLinked.trim().equals("false")) {
            isLinked = 0;
        }

        int totalSub = 0;
        try {
            totalSub = Integer.parseInt(sTotalSubscriber);
        } catch (Exception ex) {
        }

        int totalVideos = 0;
        try {
            totalVideos = Integer.parseInt(sTotalVideos);
        } catch (Exception ex) {
        }

        int totalViews = 0;
        try {
            totalViews = Integer.parseInt(sTotalViews);
        } catch (Exception ex) {
        }

        if (sJoinedTime != null) {
            sJoinedTime = sJoinedTime.replaceAll("T", " ").replaceAll("Z", "").substring(0, 19);
        }

        int tmpResult = dbMgr.updateYoutubeChannelInfo(sChannelId, sChannelTitle, sChannelDesc, sChannelKeywords, sChannelCoverUrl, sChannelThumbnailUrl, isLinked, sCountry, sJoinedTime, totalSub, totalVideos, totalViews);
        if (tmpResult != -1) {
            jsonResult.put("status", Boolean.TRUE);
        } else {
            jsonResult.put("status", Boolean.FALSE);
        }

//        String result = "[]";
//        ManageYoutubeChannelInfo info = dbMgr.getYoutubeChannelInfo(sChannelId);
//        if (info != null) {
//            result = new Gson().toJson(info);
//        }
//
//        jsonResult.put("drList", result);
        return jsonResult.toString();
    }

    private String updateArtistPortal(DmsYoutubeDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();

        String sArtistPortalId = ThaiConvert.nullToString(request.getParameter("artistPortalId"));
        String sUserDisplayName = ThaiConvert.nullToString(request.getParameter("userDisplayName"));
        String sUserImageUrl = ThaiConvert.nullToString(request.getParameter("userImageUrl"));
        String sUserEmail = ThaiConvert.nullToString(request.getParameter("userEmail"));

        //UPDATE USER  
        int tmpResult = dbMgr.updateArtistPortalInfo(sArtistPortalId, sUserDisplayName, sUserImageUrl, sUserEmail);
        if (tmpResult != -1) {
            jsonResult.put("status", Boolean.TRUE);
        } else {
            jsonResult.put("status", Boolean.FALSE);
        }
        return jsonResult.toString();
    }

}
