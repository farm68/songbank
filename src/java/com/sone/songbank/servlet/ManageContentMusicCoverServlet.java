/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.google.gson.Gson;
import com.sone.songbank.db.DmsCoverSongDbMgr;
import com.sone.songbank.info.youtube.ManageYoutubeChannelInfo;
import com.sone.songbank.info.coversong.DmsYtmRequestConverSongYtChannelMapInfo;
import com.sone.songbank.info.coversong.DmsYtmRequestCoverSongMusicMapInfo;
import com.sone.songbank.info.coversong.ManageRequestCoverSongLogInfo;
import com.sone.songbank.info.coversong.ManageSearchContentMusicCoverInfo;
import com.sone.songbank.info.login.SessionUserInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.ContentPage;
import com.sone.util.ThaiConvert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import org.apache.log4j.Logger;

/**
 *
 * @author jiranuwat
 */
@WebServlet(name = "ManageContentMusicCoverServlet", urlPatterns = {"/ManageContentMusicCoverServlet"})
public class ManageContentMusicCoverServlet extends HttpServlet {

    private Logger logger = Logger.getLogger(ManageArtitstPortalServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();
        logger.info("########## Start ManageContentMusicCoverServlet ##########");
        final String ACCOUNT_USER_AGENT = request.getHeader("User-Agent");
        final String ACCOUNT_IP_ADDRESS = request.getRemoteAddr();
        logger.info("[ManageContentMusicCoverServlet]User Agent: " + ACCOUNT_USER_AGENT);
        logger.info("[ManageContentMusicCoverServlet]User IP: " + ACCOUNT_IP_ADDRESS);

        //################# GET PARAMETER //#################
        String sMode = ThaiConvert.nullToString(request.getParameter("mode"));
        //################################################### 
        String result = "";
        DmsCoverSongDbMgr dbCoverMgr = null;
        try {

            dbCoverMgr = new DmsCoverSongDbMgr();
            dbCoverMgr.initializeConnection();

            if (sMode.trim().equals("LIST_SEARCH_MUSIC_CONTENT_COVER")) {
                result = listSearchMusicContentCover(dbCoverMgr, request);
            } else if (sMode.trim().equals("SUBMIT_REQUEST_MUSIC_CONTENT_COVER")) {
                result = requestMusicContentCover(dbCoverMgr, request);
            } else if (sMode.trim().equals("EDIT_REQUEST_MUSIC_CONTENT_COVER")) {
                result = updateMusicContentCover(dbCoverMgr, request);
            } else if (sMode.trim().equals("LIST_REQUEST_MUSIC_COVER_LOG")) {
                result = listSearchRequestMusicContentCoverLog(dbCoverMgr, request);
            } else if (sMode.trim().equals("GET_REQUEST_MUSIC_COVER_LOG")) {
                result = getRequestMusicContentCoverLog(dbCoverMgr, request);
            } else if (sMode.trim().equals("UPDATE_MUSIC_COVER_STATUS")) {
                result = updateMusicContentCoverStatus(dbCoverMgr, request);
            }
            out.print(result);
        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            try {
                dbCoverMgr.releaseConnection();
            } catch (Exception e) {
            }
            dbCoverMgr = null;
            out.close();
        }
    }

    private String listSearchMusicContentCover(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        String sKeywordSearch = ThaiConvert.nullToString(request.getParameter("keywordSearch"));
        String sListMusicCategoryId = ThaiConvert.nullToString(request.getParameter("listMusicCategoryId"));
        String sPageIndex = ThaiConvert.nullToString(request.getParameter("pageIndex"));
        String sLimit = ThaiConvert.nullToString(request.getParameter("limit"));

        logger.info("sKeywordSearch: " + sKeywordSearch);
        logger.info("sPageIndex: " + sPageIndex);
        logger.info("sLimit: " + sLimit);

        int pageIndex = 1;
        try {
            pageIndex = Integer.parseInt(sPageIndex);
        } catch (Exception e) {
        }

        int limit = 20;
        try {
            limit = Integer.parseInt(sLimit);
        } catch (Exception e) {
        }

        JSONObject jResult = new JSONObject();
        String jListData = "[]";

        int totalAmount = 0;
        int totalPage = 0;
        int currentPageAmount = 0;

        try {
            sKeywordSearch = Utilities.getDataFromEncode(sKeywordSearch);
            sKeywordSearch = sKeywordSearch.replaceAll("'", "''");
        } catch (Exception e) {
        }

        ContentPage cp = dbMgr.listSearchMusicContentCover(sKeywordSearch, sListMusicCategoryId, pageIndex, limit, "", "");
        if (cp != null && cp.getDataList() != null) {
            ArrayList<ManageSearchContentMusicCoverInfo> listInfo = cp.getDataList();
            totalAmount = Integer.parseInt(cp.getTotalAmount() + "");
            totalPage = Integer.parseInt(cp.getTotalPages() + "");
            currentPageAmount = listInfo.size();

            jListData = new Gson().toJson(listInfo);
        }
        jResult.put("drList", jListData);
        jResult.put("pageIndex", pageIndex);
        jResult.put("fCurrentPageAmount", ((pageIndex - 1) * limit) + 1);
        jResult.put("tCurrentPageAmount", ((pageIndex - 1) * limit) + currentPageAmount);
        jResult.put("totalAmount", totalAmount);
        jResult.put("totalPage", totalPage);

        return jResult.toString();
    }

    private String requestMusicContentCover(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jResult = new JSONObject();
        Boolean resStatus = false;
        String resMessage = "ขออภัยไม่สามารถทำรายการได้ในขณะนี้!";

        String parentChannelId = "";
        SessionUserInfo sessionUserInfo = null;
        try {
            HttpSession session = request.getSession();
            sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
            parentChannelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
        } catch (Exception ex) {
        }

        String requestPrefixName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("prefixName")));
        String requestName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestName")));
        String requestemail = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestemail")));
        String requestTel = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestTel")));
        String requestArtistName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestArtistName")));
        String requestArtistAliasName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestArtistAliasName")));
        String sListMusicInfo = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("listMusicInfo")));
        String sListChannelInfo = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("listChannelInfo")));
        String sRemark = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("remark")));
        String requestAccManager = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("channelAccManagerUser")));

        Integer userAccManagerId = 51;
        try {
            userAccManagerId = Integer.parseInt(requestAccManager);
        } catch (Exception ex) {
        }

        logger.info("sRequestName: " + requestName);
        logger.info("sRequestemail: " + requestemail);
        logger.info("sRequestTel: " + requestTel);
        logger.info("sRequestArtistName: " + requestArtistName);
        logger.info("sRequestArtistAliasName: " + requestArtistAliasName);
        logger.info("sRemark: " + sRemark);
        logger.info("sListMusicInfo: " + sListMusicInfo);
        logger.info("sListChannelInfo: " + sListChannelInfo);
        logger.info("requestAccManager: " + userAccManagerId);

        Long requestId = new Date().getTime();
        ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelInfo = null;
        try {
            listChannelInfo = new ArrayList<DmsYtmRequestConverSongYtChannelMapInfo>();
            JSONObject jsonTmp = (JSONObject) JSONSerializer.toJSON(sListChannelInfo);
            JSONArray listChannelArr = jsonTmp.getJSONArray("list_channel");
//            JSONObject channelObj = jsonTmp.getJSONObject("list_channel");
            int priority = 1;
            for (int i = 0; i < listChannelArr.size(); i++) {
                DmsYtmRequestConverSongYtChannelMapInfo infoChannel = new DmsYtmRequestConverSongYtChannelMapInfo();
                String channelId = "";
                try {
                    channelId = listChannelArr.getJSONObject(i).getString("channelId");
                    logger.info("channelId: " + channelId);
                } catch (Exception e) {
                }

                String channelName = "";
                try {
                    channelName = listChannelArr.getJSONObject(i).getString("channelName");
                    logger.info("channelName: " + channelName);
                } catch (Exception e) {
                }

                infoChannel.setChannelId(channelId);
                infoChannel.setChannelName(channelName);
                infoChannel.setPriority(priority);
                priority++;

                listChannelInfo.add(infoChannel);

                ManageYoutubeChannelInfo ytChannelInfo = dbMgr.getYoutubeChannelInfo(channelId);
                if (ytChannelInfo != null) {
                    int statusUpdateChannel = dbMgr.updateYoutubeChannelInfo(channelId, channelName, requestPrefixName, requestName, requestArtistAliasName, requestemail, requestTel, parentChannelId);
                    logger.info("statusUpdateChannel: " + statusUpdateChannel);
                } else {
                    Integer userId = ytChannelInfo.getChannelUserId();
                    if (userId == null) {
                        userId = 51;
                    }
                    int statusInsertChannel = dbMgr.insertYoutubeChannelInfo(channelId, channelName, userId, requestPrefixName, requestName, requestArtistAliasName, requestemail, requestTel, parentChannelId);
                    logger.info("statusInsertChannel: " + statusInsertChannel);
                }
            }
        } catch (Exception ex) {
        }

        ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicInfo = null;
        try {
            listMusicInfo = new ArrayList<DmsYtmRequestCoverSongMusicMapInfo>();
            JSONObject jsonTmp = (JSONObject) JSONSerializer.toJSON(sListMusicInfo);
            JSONArray listMusicJArr = jsonTmp.getJSONArray("list_music");
            int priority = 1;
            for (int i = 0; i < listMusicJArr.size(); i++) {
                DmsYtmRequestCoverSongMusicMapInfo infoMusic = new DmsYtmRequestCoverSongMusicMapInfo();
                String musicPublishingId = "";
                try {
                    musicPublishingId = listMusicJArr.getJSONObject(i).getString("musicPublishingId");
                } catch (Exception e) {
                }

                infoMusic.setMusicPublishingId(musicPublishingId);
                infoMusic.setPriority(priority);
//                infoMusic.setOwnerPercentShare(80d);
//                infoMusic.setCoverPercentShare(20d);
                Double ownerPercentShare = 20d;
                try {
                    ownerPercentShare = listMusicJArr.getJSONObject(i).getDouble("coverOwnerPercentSharing");
                } catch (Exception e) {
                }

                Double setCoverPercentShare = 80d;
                try {
                    setCoverPercentShare = listMusicJArr.getJSONObject(i).getDouble("coverPartnerPercentSharing");
                } catch (Exception e) {
                }
                infoMusic.setOwnerPercentShare(ownerPercentShare);
                infoMusic.setCoverPercentShare(setCoverPercentShare);
                //XXXXX
                priority++;

                listMusicInfo.add(infoMusic);
            }
        } catch (Exception ex) {
        }

        int statusInsert = dbMgr.insertRequestCoverSongLog(requestId, requestPrefixName, requestName, requestArtistName, requestArtistAliasName, requestemail, requestTel, Constants.COVER_SONG_NEW_REQUEST, null, null, null, sRemark, userAccManagerId, listChannelInfo, listMusicInfo);
        if (statusInsert > -1) {
            resStatus = true;
            resMessage = "ทำรายการสำเร็จ";

            int sentMail = dbMgr.execDmsCoverSongSendEmail(requestId);
            logger.info("execDmsCoverSongSendEmail : " + sentMail);
        }
        jResult.put("status", resStatus);
        jResult.put("message", resMessage);
        return jResult.toString();
    }

    private String updateMusicContentCover(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jResult = new JSONObject();
        Boolean resStatus = false;
        String resMessage = "ขออภัยไม่สามารถทำรายการได้ในขณะนี้!";

        String parentChannelId = "";
        SessionUserInfo sessionUserInfo = null;
        try {
            HttpSession session = request.getSession();
            sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
            parentChannelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
        } catch (Exception ex) {
        }

        String sRequestId = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestId")));
        String requestPrefixName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("prefixName")));
        String requestName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestName")));
        String requestEmail = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestemail")));
        String requestTel = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestTel")));
        String requestArtistName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestArtistName")));
        String requestArtistAliasName = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestArtistAliasName")));
        String sListMusicInfo = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("listMusicInfo")));
        String sListChannelInfo = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("listChannelInfo")));
        String sRemark = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("remark")));

        logger.info("sRequestName: " + requestName);
        logger.info("sRequestemail: " + requestEmail);
        logger.info("sRequestTel: " + requestTel);
        logger.info("sRequestArtistName: " + requestArtistName);
        logger.info("sRequestArtistAliasName: " + requestArtistAliasName);
        logger.info("sRemark: " + sRemark);
        logger.info("sListMusicInfo: " + sListMusicInfo);
        logger.info("sListChannelInfo: " + sListChannelInfo);

        Long requestId = null;
        try {
            requestId = Long.parseLong(sRequestId);
        } catch (Exception ex) {
        }
        ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> listChannelInfo = null;
        try {
            listChannelInfo = new ArrayList<DmsYtmRequestConverSongYtChannelMapInfo>();
            JSONObject jsonTmp = (JSONObject) JSONSerializer.toJSON(sListChannelInfo);
//            JSONArray listChannelArr = jsonTmp.getJSONArray("list_channel");
            JSONObject channelObj = jsonTmp.getJSONObject("list_channel");
            int priority = 1;

            DmsYtmRequestConverSongYtChannelMapInfo infoChannel = new DmsYtmRequestConverSongYtChannelMapInfo();
            String channelId = "";
            try {
                channelId = channelObj.getString("channelId");
                logger.info("channelId: " + channelId);
            } catch (Exception e) {
            }

            String channelName = "";
            try {
                channelName = channelObj.getString("channelName");
                logger.info("channelName: " + channelName);
            } catch (Exception e) {
            }

            infoChannel.setChannelId(channelId);
            infoChannel.setChannelName(channelName);
            infoChannel.setPriority(priority);
            priority++;

            listChannelInfo.add(infoChannel);
            try {
                int statusUpdateChannel = dbMgr.updateYoutubeChannelInfo(channelId, channelName, requestName, requestName, requestArtistAliasName, requestEmail, requestTel, parentChannelId);
                logger.info("statusUpdateChannel: " + statusUpdateChannel);
            } catch (Exception ex) {
            }
        } catch (Exception ex) {
        }

        ArrayList<DmsYtmRequestCoverSongMusicMapInfo> listMusicInfo = null;
        try {
            listMusicInfo = new ArrayList<DmsYtmRequestCoverSongMusicMapInfo>();
            JSONObject jsonTmp = (JSONObject) JSONSerializer.toJSON(sListMusicInfo);
            JSONArray listMusicJArr = jsonTmp.getJSONArray("list_music");
            int priority = 1;
            for (int i = 0; i < listMusicJArr.size(); i++) {
                DmsYtmRequestCoverSongMusicMapInfo infoMusic = new DmsYtmRequestCoverSongMusicMapInfo();
                String musicPublishingId = "";
                try {
                    musicPublishingId = listMusicJArr.getJSONObject(i).getString("musicPublishingId");
                } catch (Exception e) {
                }

                Double ownerPercentShare = 20d;
                try {
                    ownerPercentShare = listMusicJArr.getJSONObject(i).getDouble("coverOwnerPercentSharing");
                } catch (Exception e) {
                }

                Double setCoverPercentShare = 80d;
                try {
                    setCoverPercentShare = listMusicJArr.getJSONObject(i).getDouble("coverPartnerPercentSharing");
                } catch (Exception e) {
                }

                infoMusic.setMusicPublishingId(musicPublishingId);
                infoMusic.setPriority(priority);
                infoMusic.setOwnerPercentShare(ownerPercentShare);
                infoMusic.setCoverPercentShare(setCoverPercentShare);
                priority++;

                listMusicInfo.add(infoMusic);
            }
        } catch (Exception ex) {
        }

        int statusInsert = dbMgr.updateRequestCoverSongLog(requestId,
                requestPrefixName,
                requestName,
                requestArtistName,
                requestArtistAliasName,
                requestEmail,
                requestTel,
                null, //statusId,
                null, //publishTime,
                null, //reqDocUrl,
                null, //ackDocUrl,
                sRemark,
                null, //approveTime,
                listChannelInfo,
                listMusicInfo);
        if (statusInsert > -1) {
            resStatus = true;
            resMessage = "ทำรายการแก้ไขสำเร็จ";
        }
        jResult.put("status", resStatus);
        jResult.put("message", resMessage);
        return jResult.toString();
    }

    private String listSearchRequestMusicContentCoverLog(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        String sKeywordSearch = ThaiConvert.nullToString(request.getParameter("keywordSearch"));
        String sChannelId = ThaiConvert.nullToString(request.getParameter("channelId"));
        String sStatus = ThaiConvert.nullToString(request.getParameter("statusId"));
        String sPageIndex = ThaiConvert.nullToString(request.getParameter("pageIndex"));
        String sLimit = ThaiConvert.nullToString(request.getParameter("limit"));

        logger.info("sPageIndex: " + sPageIndex);
        logger.info("sLimit: " + sLimit);

        int pageIndex = 1;
        try {
            pageIndex = Integer.parseInt(sPageIndex);
        } catch (Exception e) {
        }

        Integer statusId = null;
        try {
            statusId = Integer.parseInt(sStatus);
        } catch (Exception ex) {
        }

        int limit = 20;
        try {
            limit = Integer.parseInt(sLimit);
        } catch (Exception e) {
        }

        try {
            sKeywordSearch = Utilities.getDataFromEncode(sKeywordSearch);
            sKeywordSearch = sKeywordSearch.replaceAll("'", "''");
        } catch (Exception e) {
        }

        JSONObject jResult = new JSONObject();
        String jListData = "[]";

        int totalAmount = 0;
        int totalPage = 0;
        int currentPageAmount = 0;

        ContentPage cp = dbMgr.listSearchRequestCoverSongLog(sKeywordSearch, sChannelId, statusId, pageIndex, limit, "", "");
        if (cp != null && cp.getDataList() != null) {
            ArrayList<ManageRequestCoverSongLogInfo> listInfo = cp.getDataList();
            logger.info("listInfo: " + listInfo);
            if (listInfo != null && !listInfo.isEmpty()) {
                for (ManageRequestCoverSongLogInfo info : listInfo) {
                    ArrayList<DmsYtmRequestCoverSongMusicMapInfo> mInfoArr = info.getListMusicCover();
                    if (mInfoArr != null && !mInfoArr.isEmpty()) {
                        for (DmsYtmRequestCoverSongMusicMapInfo mInfo : mInfoArr) {
                            String musicPublishingId = mInfo.getMusicPublishingId();
                            ManageSearchContentMusicCoverInfo objMuisc = dbMgr.getMusicContentCover(musicPublishingId);
                            //SET OBJECT MUSIC
                            mInfo.setMusicInfo(objMuisc);
                        }
                    }

                    ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> cInfoArr = info.getListChannelCover();
                    if (mInfoArr != null && !mInfoArr.isEmpty()) {
                        for (DmsYtmRequestConverSongYtChannelMapInfo cInfo : cInfoArr) {
                            String channelId = cInfo.getChannelId();
                            ManageYoutubeChannelInfo objChannel = dbMgr.getYoutubeChannelInfo(channelId);
                            //SET OBJECT MUSIC
                            cInfo.setChannelInfo(objChannel);
                        }
                    }
                }
            }

            totalAmount = Integer.parseInt(cp.getTotalAmount() + "");
            totalPage = Integer.parseInt(cp.getTotalPages() + "");
            currentPageAmount = listInfo.size();

            jListData = new Gson().toJson(listInfo);
        }
        jResult.put("drList", jListData);
        jResult.put("pageIndex", pageIndex);
        jResult.put("fCurrentPageAmount", ((pageIndex - 1) * limit) + 1);
        jResult.put("tCurrentPageAmount", ((pageIndex - 1) * limit) + currentPageAmount);
        jResult.put("totalAmount", totalAmount);
        jResult.put("totalPage", totalPage);

        return jResult.toString();
    }

    private String getRequestMusicContentCoverLog(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        String sRequestId = ThaiConvert.nullToString(request.getParameter("requestId"));
        JSONObject jResult = new JSONObject();
        String jListData = "[]";

        Long requestId = null;
        try {
            requestId = Long.parseLong(sRequestId);
        } catch (Exception ex) {
        }

        ManageRequestCoverSongLogInfo info = dbMgr.getSearchRequestCoverSongLog(requestId);
        if (info != null) {
            ArrayList<DmsYtmRequestCoverSongMusicMapInfo> mInfoArr = info.getListMusicCover();
            if (mInfoArr != null && !mInfoArr.isEmpty()) {
                for (DmsYtmRequestCoverSongMusicMapInfo mInfo : mInfoArr) {
                    String musicPublishingId = mInfo.getMusicPublishingId();
                    ManageSearchContentMusicCoverInfo objMuisc = dbMgr.getMusicContentCover(musicPublishingId);
                    //SET OBJECT MUSIC
                    mInfo.setMusicInfo(objMuisc);
                }
            }

            ArrayList<DmsYtmRequestConverSongYtChannelMapInfo> cInfoArr = info.getListChannelCover();
            if (mInfoArr != null && !mInfoArr.isEmpty()) {
                for (DmsYtmRequestConverSongYtChannelMapInfo cInfo : cInfoArr) {
                    String channelId = cInfo.getChannelId();
                    ManageYoutubeChannelInfo objChannel = dbMgr.getYoutubeChannelInfo(channelId);
                    //SET OBJECT MUSIC
                    cInfo.setChannelInfo(objChannel);
                }
            }
            jListData = new Gson().toJson(info);
        }
        jResult.put("drList", jListData);

        return jResult.toString();
    }

    private String updateMusicContentCoverStatus(DmsCoverSongDbMgr dbMgr, HttpServletRequest request) {
        JSONObject jResult = new JSONObject();
        Boolean resStatus = false;
        String resMessage = "ขออภัยไม่สามารถทำรายการได้ในขณะนี้!";

        String sRequestId = ThaiConvert.nullToString(Utilities.getDataFromEncode(request.getParameter("requestId")));

        Long requestId = null;
        try {
            requestId = Long.parseLong(sRequestId);
        } catch (Exception ex) {
        }

        int statusInsert = dbMgr.updateRequestCoverSongLog(requestId,
                null,
                null,
                null,
                null,
                null,
                null,
                Constants.COVER_SONG_CANCEL_REQUEST, //statusId,
                null, //publishTime,
                null, //reqDocUrl,
                null, //ackDocUrl,
                null,
                null, //approveTime,
                null,
                null);
        if (statusInsert > -1) {
            resStatus = true;
            resMessage = "ทำรายการยกเลิกสำเร็จ";
        }
        jResult.put("status", resStatus);
        jResult.put("message", resMessage);
        return jResult.toString();
    }

// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
