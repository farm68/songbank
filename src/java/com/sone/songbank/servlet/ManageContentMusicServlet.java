/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.google.gson.Gson;
import com.sone.songbank.db.DbDmsDbMgr;
import com.sone.songbank.info.dms.ManageContentMusicCompositionInfo;
import com.sone.songbank.info.dms.ManageContentMusicServiceMapInfo;
import com.sone.songbank.info.dms.ManageSearchContentMusicInfo;
import com.sone.songbank.info.login.SessionUserInfo;
import com.sone.songbank.util.Constants;
import com.sone.songbank.util.Utilities;
import com.sone.util.ContentPage;
import com.sone.util.ThaiConvert;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class ManageContentMusicServlet extends HttpServlet {

    Logger logger = Logger.getLogger(ManageContentMusicServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();

        logger.info(" ===== ManageCmsUserServlet ===== ");

        String sMode = ThaiConvert.nullToString(request.getParameter("mode"));
        String sContentName = ThaiConvert.nullToString(request.getParameter("contentName"));
        String sPageIndex = ThaiConvert.nullToString(request.getParameter("pageIndex"));
        String sLimit = ThaiConvert.nullToString(request.getParameter("limit"));

        String sArtistId = ThaiConvert.nullToString(request.getParameter("artistId"));
        String sStartDate = ThaiConvert.nullToThaiString(request.getParameter("startDate"));
        String sEndDate = ThaiConvert.nullToThaiString(request.getParameter("endDate"));
        String sMusicId = ThaiConvert.nullToThaiString(request.getParameter("musicId"));

        int artistId = -1;
        try {
            artistId = Integer.parseInt(sArtistId);
        } catch (Exception e) {
        }

        int pageIndex = 1;
        try {
            pageIndex = Integer.parseInt(sPageIndex);
        } catch (Exception e) {
        }

        int limit = 20;
        try {
            limit = Integer.parseInt(sLimit);
        } catch (Exception e) {
        }

        logger.info("mode: " + sMode);
        String result = "";
        DbDmsDbMgr dbMgr = null;

        try {
            dbMgr = new DbDmsDbMgr();
            dbMgr.initializeConnection();
            if (sMode.trim().equals("LIST_SEARCH_MUSIC_CONTENT")) {
                result = listSearchMusicContent(dbMgr, request, sContentName, artistId, sStartDate, sEndDate, pageIndex, limit);
            } else if (sMode.trim().equals("GET_MUSIC_COMPOSITION")) {
                result = getMusicComposition(dbMgr, request, sMusicId);
            }

            out.print(result);
        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            out.close();
            try {
                dbMgr.releaseConnection();
                dbMgr = null;
            } catch (Exception e) {
            }

        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String listSearchMusicContent(DbDmsDbMgr dbMgr, HttpServletRequest request, String sContentName, int artistId, String sStartDate, String sEndDate, int pageIndex, int limit) {
        JSONObject jResult = new JSONObject();
        JSONArray jArr = new JSONArray();

        int totalAmount = 0;
        int totalPage = 0;
        int currentPageAmount = 0;

        try {
            sContentName = Utilities.getDataFromEncode(sContentName);

            sContentName = sContentName.replaceAll("'", "''");
        } catch (Exception e) {
        }
        sStartDate = Utilities.getDataFromEncode(sStartDate);
        sEndDate = Utilities.getDataFromEncode(sEndDate);

        logger.info("sStartDate: " + sStartDate);
        logger.info("sEndDate: " + sEndDate);
        sStartDate = Utilities.convertDateTimeWebNoTime2DBFormat(sStartDate);
        sEndDate = Utilities.convertDateTimeWebNoTime2DBFormat(sEndDate);
        logger.info("sStartDate: " + sStartDate);
        logger.info("sEndDate: " + sEndDate);

        String artistPortalId = "";
        String channelId = "";
        SessionUserInfo sessionUserInfo = null;

        try {
            HttpSession session = request.getSession();
            sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
            artistPortalId = sessionUserInfo.getArtistPortalInfo().getArtistPortalId();

        } catch (Exception ex) {
        }

//        listSearchMusicContent(String sKeyword, Integer artistId, String startDate, String endDate, int pageIndex, int limit, String sKeysort, String sSortBy)
        ContentPage cp = dbMgr.listSearchMusicContent(artistPortalId, sContentName, artistId, sStartDate, sEndDate, pageIndex, limit, null, null);

//        logger.info("cp.getDataList(): " + cp.getDataList());
        if (cp != null && cp.getDataList() != null) {

            ArrayList<ManageSearchContentMusicInfo> aList = cp.getDataList();
            if (aList != null && aList.size() > 0) {

//                logger.info("aList.size(): " + aList.size());
                totalAmount = Integer.parseInt(cp.getTotalAmount() + "");
                totalPage = Integer.parseInt(cp.getTotalPages() + "");
                currentPageAmount = aList.size();

                for (ManageSearchContentMusicInfo info : aList) {

                    JSONObject jObject = new JSONObject();
                    jObject.put("music_id", info.getMusicId());
                    jObject.put("label_id", info.getLabelId());
                    jObject.put("label_name", info.getLabelName());

                    jObject.put("genre_id", info.getGenreId());
                    jObject.put("genre_name", info.getGenreName());
                    jObject.put("artist_id", info.getArtistId());
                    jObject.put("artist_name", info.getArtistName());
                    jObject.put("artist_name_en", info.getArtistNameEng());
                    jObject.put("album_id", info.getAlbumId());
                    jObject.put("album_name", info.getAlbumName());
                    jObject.put("upc_code", info.getUpcCode());

                    jObject.put("isrc", info.getIsrc());
                    jObject.put("name_en", info.getNameEn());
                    jObject.put("name_th", info.getNameTh());

                    jObject.put("copyright_id", info.getCopyrightId());
                    jObject.put("copyright_name", info.getCopyrightName());
                    jObject.put("publisher_id", info.getPublisherId());
                    jObject.put("publisher_name", info.getPublisherName());
                    jObject.put("release_date", info.getReleaseDate());

                    jObject.put("ex_music_service_id", info.getExclusiveMusicServiceId());
                    jObject.put("ex_music_service_name", info.getExclusiveMusicServiceName());
                    jObject.put("ex_date", info.getExclusiveDate());

                    jObject.put("state_id", info.getStateId());
                    jObject.put("state_name", info.getStateName());

                    jObject.put("create_time", info.getCreateTime());
                    jObject.put("update_time", info.getUpdateTime());
                    jObject.put("one_link_url", info.getOneLinkUrl());
                    jObject.put("thumbnail_url", info.getThumbnailUrl());

                    JSONArray jArrServiceMap = new JSONArray();
                    ArrayList<ManageContentMusicServiceMapInfo> listServiceMap = info.getListMusicServiceMap();
                    if (listServiceMap != null && listServiceMap.size() > 0) {
                        for (ManageContentMusicServiceMapInfo serviceInfo : listServiceMap) {
                            JSONObject jObjectInner = new JSONObject();

                            jObjectInner.put("service_group_name", serviceInfo.getMusicServiceGroupName());
                            jObjectInner.put("service_name", serviceInfo.getMusicServiceName());
                            jObjectInner.put("service_id", serviceInfo.getMusicServiceId());
                            jObjectInner.put("detail", serviceInfo.getDetail());
                            jObjectInner.put("state_id", serviceInfo.getStateId());
                            jObjectInner.put("state_name", serviceInfo.getStateName());
                            jObjectInner.put("release_time", serviceInfo.getReleaseTime());
                            jObjectInner.put("action_time", serviceInfo.getActionTime());
                            jArrServiceMap.add(jObjectInner);
                        }
                    }

                    jObject.put("list_service", jArrServiceMap);

                    //COVER MUSIC DETAIL
//                    boolean isHaveComposer = dbMgrContentMusic.checkIsMusicComposer(info.getMusicId());
//                    jObject.put("is_composer", isHaveComposer);
                    jArr.add(jObject);
                }
            }

        }
        jResult.put("drList", jArr);
        jResult.put("pageIndex", pageIndex);
        jResult.put("fCurrentPageAmount", ((pageIndex - 1) * limit) + 1);
        jResult.put("tCurrentPageAmount", ((pageIndex - 1) * limit) + currentPageAmount);
        jResult.put("totalAmount", totalAmount);
        jResult.put("totalPage", totalPage);

        return jResult.toString();
    }

    private String getMusicComposition(DbDmsDbMgr dbMgr, HttpServletRequest request, String sMusicId) {
        StringBuilder strResJson = new StringBuilder();
        strResJson.append("{");
        strResJson.append("\"statusId\":\"0\",");
        strResJson.append("\"statusName\":\"failure\",");
        strResJson.append("\"datas\": []");
        strResJson.append("}");

        logger.info("sMusicId: " + sMusicId);

        String result = null;
        String logMessage = "";
        HttpSession session = request.getSession();
        try {
            SessionUserInfo sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
            if (sessionUserInfo != null) {
                ManageContentMusicCompositionInfo info = dbMgr.getMusicComposition(sMusicId);
                if (info != null) {
                    result = new Gson().toJson(info);
                    strResJson = new StringBuilder();
                    strResJson.append("{");
                    strResJson.append("\"statusId\":\"1\",");
                    strResJson.append("\"statusName\":\"success\",");
                    strResJson.append("\"statusMessage\":\"success\",");
                    strResJson.append("\"datas\": " + result + "");
                    strResJson.append("}");
                } else {
                    strResJson = new StringBuilder();
                    strResJson.append("{");
                    strResJson.append("\"statusId\":\"1\",");
                    strResJson.append("\"statusName\":\"success\",");
                    strResJson.append("\"statusMessage\":\"Data not found.\",");
                    strResJson.append("\"datas\": []");
                    strResJson.append("}");
                }
            } else {
                strResJson = new StringBuilder();
                strResJson.append("{");
                strResJson.append("\"statusId\":\"0\",");
                strResJson.append("\"statusName\":\"failure\",");
                strResJson.append("\"statusMessage\":\"Session time out.\",");
                strResJson.append("\"datas\": []");
                strResJson.append("}");
            }
        } catch (Exception ex) {
            logger.error("reportTransaction : ", ex);
        }
        return strResJson.toString();
    }

}
