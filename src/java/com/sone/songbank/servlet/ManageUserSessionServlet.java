/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.sone.songbank.util.Constants;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class ManageUserSessionServlet extends HttpServlet {
private Logger logger = Logger.getLogger(ManageUserSessionServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";
    private String mode = "DENY";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = null;
        try {
            out = response.getWriter();
        } catch (Exception ex) {
//            ex.printStackTrace();
        }
        String result = "";

        try {
            JSONObject json = new JSONObject();

            HttpServletResponse res = (HttpServletResponse) response;
            HttpServletRequest req = (HttpServletRequest) request;
            res.addHeader("X-FRAME-OPTIONS", mode);
            res.addHeader("Referrer-Policy", "no-referrer-when-downgrade");
            res.addHeader("feature-policy", "fullscreen");
//        res.addHeader("Content-Security-Policy", "upgrade-insecure-requests");ˆ

            if (req.getSession().getAttribute(Constants.USER_SESSION) == null) {
                //forward request to login.jsp
                json.put("status", Boolean.FALSE);
                json.put("msg", "Session time out!!!");
            } else {
                json.put("status", Boolean.TRUE);
            }

            result = json.toString();

            out.print(result);
        } catch (Exception ex) {
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    
    
    
}
