/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.servlet;

import com.google.gson.Gson;
import com.sone.songbank.db.AccountDbMgr;
import com.sone.songbank.db.DbDmsDbMgr;
import com.sone.songbank.info.YoutubeChannelInfo;
import com.sone.songbank.info.login.CoverSongAccountInfo;
import com.sone.songbank.util.YoutubeUtil;
import com.sone.util.ThaiConvert;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.json.JSONObject;
import org.apache.log4j.Logger;

/**
 *
 * @author jiranuwat
 */
@WebServlet(name = "ManagerAccountServlet", urlPatterns = {"/ManagerAccountServlet"})
public class ManagerAccountServlet extends HttpServlet {

    Logger logger = Logger.getLogger(ManagerAccountServlet.class);
    private static final String CONTENT_TYPE = "application/json;charset=UTF-8";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType(CONTENT_TYPE);
        PrintWriter out = response.getWriter();

        logger.info(" ####### ManagerAccountServlet ####### ");
        String sMode = ThaiConvert.nullToString(request.getParameter("mode"));

        logger.info("mode: " + sMode);
        String result = "";
        AccountDbMgr accountDb = null;
        try {
            accountDb = new AccountDbMgr();
            accountDb.initializeConnection();
            if (sMode.trim().equals("LIST_ACCOUNT")) {
                result = listSearchAccountInfo(accountDb, request);
            } else if (sMode.trim().equals("CREATE_ACCOUNT")) {
                result = createAccountInfo(accountDb, request);
            } else if (sMode.trim().equals("UPDATE_ACCOUNT")) {
                result = updateAccountInfo(accountDb, request);
            }
            out.print(result);
        } catch (Exception ex) {
            logger.error("", ex);
        } finally {
            out.close();
            try {
                accountDb.releaseConnection();
                accountDb = null;
            } catch (Exception e) {
            }
        }
    }

    private String listSearchAccountInfo(AccountDbMgr accountDb, HttpServletRequest request) {
        String sKeyword = ThaiConvert.nullToString(request.getParameter("keyword"));
        String response = null;
        try {
            ArrayList<CoverSongAccountInfo> listInfo = accountDb.listAccountInfo(sKeyword);
            if (listInfo != null && !listInfo.isEmpty()) {
                response = new Gson().toJson(listInfo);
            }
        } catch (Exception ex) {
            logger.error("[ManagerAccountServlet] listSearchAccountInfo : ", ex);
        }
        return response;
    }

    private String createAccountInfo(AccountDbMgr accountDb, HttpServletRequest request) {
        String sChannelId = ThaiConvert.nullToString(request.getParameter("channelId"));
        String sChannelName = ThaiConvert.nullToString(request.getParameter("channelName"));
        String sUsername = ThaiConvert.nullToString(request.getParameter("username"));
        String sPassword = ThaiConvert.nullToString(request.getParameter("password"));
        String sDisplayName = ThaiConvert.nullToString(request.getParameter("displayName"));
        String sDescription = ThaiConvert.nullToString(request.getParameter("description")); 
        String accountId = null;
        JSONObject jResult = new JSONObject();
        jResult.put("status", false);
        jResult.put("message", "Create Fail!");
        try {
            Date date = Calendar.getInstance().getTime();
            DateFormat dateFormat = new SimpleDateFormat("yyyyMMddss");
            accountId = dateFormat.format(date);
            accountId = accountId.substring(2, accountId.length());
            
            if(sDisplayName == null || sDisplayName.trim().equals("")){
                sDisplayName = sChannelName;
            }
            
            if(sDescription == null || sDescription.trim().equals("")){
                sDescription = sChannelName;
            }

            int createStatus = accountDb.createAccountInfo(accountId,
                    sUsername,
                    sPassword,
                    sDisplayName,
                    sDescription,
                    sChannelId);
            if (createStatus != -1) {
                jResult = new JSONObject();
                jResult.put("status", true);
                jResult.put("message", "Create Success!");
            }
            try {
                String YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/channels?part=statistics,snippet";
                String youtubeKey = "AIzaSyAbxcUaMRe_IiYDLZ8b4IeOSnuL-Kck4ms";
                YoutubeChannelInfo youtubeInfo = YoutubeUtil.walker(youtubeKey, sChannelId); 
                String channelName = youtubeInfo.getChannelName();
                if(channelName == null || channelName.trim().equals("")){
                    channelName = sChannelName;
                }
                String sChannelImage = youtubeInfo.getChannelImageUrl();
                if(sChannelImage == null){
                    sChannelImage = "";
                }
                int stampStatus = accountDb.stampYoutubeChannelInfo(
                        sChannelId,
                        channelName,
                        sChannelImage); 
            } catch (Exception ex) {
            }
        } catch (Exception ex) {
            logger.error("[ManagerAccountServlet] createAccount : ", ex);
        }
        return jResult.toString();
    }

    private String updateAccountInfo(AccountDbMgr accountDb, HttpServletRequest request) {
        String sAccountId = ThaiConvert.nullToString(request.getParameter("accountId"));
        String sUsername = ThaiConvert.nullToString(request.getParameter("username"));
        String sPassword = ThaiConvert.nullToString(request.getParameter("password"));
        String sDisplayName = ThaiConvert.nullToString(request.getParameter("displayname"));
        String sDescription = ThaiConvert.nullToString(request.getParameter("description"));
        JSONObject jResult = new JSONObject();
        jResult.put("status", false);
        jResult.put("message", "Update Fail!");
        String response = null;
        try {
            int stuatusUpdate = accountDb.updateAccountInfo(sAccountId, sUsername, sPassword, sDisplayName, sDescription);
            if (stuatusUpdate != -1) {
                jResult = new JSONObject();
                jResult.put("status", true);
                jResult.put("message", "Update Success!");
            }
        } catch (Exception ex) {
            logger.error("[ManagerAccountServlet] updateAccount : ", ex);
        }
        return jResult.toString();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
