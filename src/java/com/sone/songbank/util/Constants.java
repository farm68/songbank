/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.util;

import com.sone.util.MessageCache;
import java.io.File;

/**
 *
 * @author smileman
 */
public class Constants {

    // ======================================================================== //
    // DATABASE CONSTANTS                                                       //
    // ======================================================================== //
    /**
     * Database connection name.
     *
     */
    public static final String DB_CONNECTION_NAME = "mssql";
    // data source
    public static final String DATASOURCE_NAME = "java:comp/env/jdbc/N-DataMusicService";
    /**
     * Database connection time out 10 seconds.
     */
    public static final long DB_TIMEOUT = 10000L;
    /**
     * Database connection open cursor size.
     */
    public static final int DB_CURSOR_SIZE = 500;
    // ======================================================================== //
    // CACHE OBJECT CONSTANTS                                                   //
    // ======================================================================== //
    /**
     * Cache object refresh period time
     */
    public static final long CACHE_OBJECT_PERIOD = 300000L;
    public static final int HTTP_SESSION_PERIOD = 30;
    // ======================================================================== //
    /**
     * HTTP user session.
     *
     */
//    public static final String USER_CMS_SESSION = "USER_CMS_SESSION";
    public static final String USER_SESSION = "USER_SESSION";
    public static final String ROLE_SESSION = "ROLE_SESSION";
    public static final String LOGIN_MESSAGE = "LOGIN_MESSAGE";
    public static final String CFG_WEB_SESSION_TIME = "1800";
    public static final String USER_SESSION_GROUP_ID = "USER_SESSION_GROUP_ID";
    public static final String CMS_MENU_SESSION = "CMS_MENU_SESSION";
    public static final String DESCRIPTION_MESSAGE = "DESCRIPTION_MESSAGE";
    public static final String STATUS_MESSAGE = "STATUS_MESSAGE";
    public static final String USER_HOME_PAGE = "USER_HOME_PAGE";
    public static final String ACCOUNT_NAME = "ACCOUNT_NAME";
    public static final String USER_LOGIN_TIME = "USER_LOGIN_TIME"; 
    /**
     * High single instance reload value period time 10 minutes 600000L
     */
    public static final long PERIOD_HIGH = 600000L;
    /**
     * Medium single instance reload value period time 5 minutes
     */
    public static final long PERIOD_MEDIUM = 300000L;
    /**
     * Low single instance reload value period time 1 minute
     */
    public static final long PERIOD_LOW = 60000L;

    public static final String USER_GROUP_MUSIC_TEAM = "2,3";
    public static final String USER_GROUP_GRAPHICS_TEAM = "5";
    public static final int USER_GROUP_ADMIN_GRAPHICS_TEAM = 4;
    public static final int USER_GROUP_USER_GRAPHICS_TEAM = 5;
    public static final int USER_GROUP_ADMIN_STUDIO_TEAM = 6;

    /*Manage Action*/
    //manage cms user
    public static final int ACTION_ADD_NEW_CMS_USER = 10;
    public static final int ACTION_EDIT_INFO_CMS_USER = 11;
    public static final int ACTION_EDIT_PASSWORD_CMS_USER = 12;

    public static final int ACTION_ADD_CMS_USER_GROUP = 13;
    public static final int ACTION_EDIT_CMS_USER_GROUP = 14;

//    manage label
    public static final int ACTION_ADD_NEW_LABEL = 20;
    public static final int ACTION_EDIT_LABEL = 21;
    public static final int ACTION_DELETE_LABEL = 22;

    //    manage genre
    public static final int ACTION_ADD_NEW_GENRE = 30;
    public static final int ACTION_EDIT_GENRE = 31;

    //    manage Artist
    public static final int ACTION_ADD_NEW_ARTIST = 40;
    public static final int ACTION_EDIT_ARTIST = 41;

    //    manage Publisher
    public static final int ACTION_ADD_NEW_PUBLISHER = 42;
    public static final int ACTION_EDIT_PUBLISHER = 43;

    //    manage Publisher
    public static final int ACTION_ADD_NEW_COPYRIGHT = 44;
    public static final int ACTION_EDIT_COPYRIGHT = 45;

    //    manage Job
    public static final int ACTION_ADD_NEW_JOB = 50;
    public static final int ACTION_EDIT_JOB_INFO = 51;
    public static final int ACTION_CLOSE_JOB = 52;
    public static final int ACTION_ADD_DETAIL_MUSIC_SERVICE = 53;
    public static final int ACTION_EDIT_DETAIL_MUSIC_SERVICE = 54;
    public static final int ACTION_CONFIRM_DETAIL_MUSIC_SERVICE = 55;
    public static final int ACTION_ADD_NEW_CONTENT_MUSIC = 56;
    public static final int ACTION_EDIT_CONTENT_MUSIC = 57;
    public static final int ACTION_ADD_NEW_ALBUM = 58;
    public static final int ACTION_SEND_TO_SONE = 59;

    //    manage Graphics Job
    public static final int ACTION_ADD_NEW_GRAPHICS_JOB = 60;
    public static final int ACTION_EDIT_JOB_GRAPHICS_JOB = 61;
    public static final int ACTION_ASSIGN_GRAPHICS_JOB = 62;
    public static final int ACTION_SEND_GRAPHICS_JOB = 63;
    public static final int ACTION_DEFECT_GRAPHICS_JOB = 64;
    public static final int ACTION_RESOLVE_GRAPHICS_JOB = 65;
    public static final int ACTION_CLOSE_GRAPHICS_JOB = 66;
    public static final int ACTION_CANCEL_GRAPHICS_JOB = 67;

    public static final int ACTION_ADD_NEW_BOOKING_STUDIO = 70;
    public static final int ACTION_EDIT_BOOKING_STUDIO = 71;
    public static final int ACTION_DELETE_BOOKING_STUDIO = 72;
    public static final int ACTION_APPROVE_BOOKING_STUDIO = 73;

    public static final int ACTION_UPLOAD_MUSIC_PREVIEW_FILE = 90;

    /*Default ID*/
    public static final int DEFAULT_ARTIST_ID = -999;
    public static final String DEFAULT_ARTIST_NAME = "Default Artist";
    public static final int DEFAULT_ALBUM_ID = -999;
    public static final String DEFAULT_ALBUM_NAME = "Default Album";
    public static final int DEFAULT_GENRE_ID = -999;
    public static final String DEFAULT_GENRE_NAME = "Default Album";
    public static final int DEFAULT_COPYRIGHT_ID = -999;
    public static final String DEFAULT_COPYRIGHT_NAME = "Default Copyright";
    public static final int DEFAULT_PUBLISHER_ID = -999;
    public static final String DEFAULT_PUBLISHER_NAME = "Default Publisher";
    public static final int DEFAULT_PERSONAL_TYPE_ID = 1;
    public static final int DEFAULT_PERSONAL_ID = -999;
    public static final String DEFAULT_PERSONAL_NAME = "Default Personal";

    /*JOB Status*/
    public static final int JOB_STATUS_OPEN_JOB = 0;
    public static final int JOB_STATUS_PROCESSING_JOB = 1;
    public static final int JOB_STATUS_CLOSE_JOB = -999;
    public static final int JOB_STATUS_ISRC_NOT_FOUND_JOB = -998;

    /*Content State*/
    public static final int MUSIC_STATE_INACTIVE = -999;
    public static final int MUSIC_STATE_PENDING = 0;
    public static final int MUSIC_STATE_ACTIVE = 1;
    public final static int MUSIC_STATE_SEND2SONE = 100;

    /*Booking Studio State*/
    public static final int STATE_BOOKING_STUDIO_REQUEST = 0;
    public static final int STATE_BOOKING_STUDIO_CONFIRM = 1;

    public static final int SERVICE_GROUP_ITUNE_RINGTONE = 18;
    public static final int SERVICE_ITUNE_RINGTONE = 20;

    public static final int PERSON_YOUTUBE_MCN_TYPE = 1;
    public static final int COMPANY_YOUTUBE_MCN_TYPE = 2;
    public static final int DIGITAL_PERSON_TYPE = 3;
    public static final int DIGITAL_COMPANY_TYPE = 4;
    public static final int DIGITAL_PERSON_EN_TYPE = 5;
    public static final int RBT_PERSON_EN_TYPE = 6;
    public static final int YOUTUBE_PERSON_MCN_EN_TYPE = 7;
    public static final int COMPANY_AUTHORIZE_TYPE = 8;
    public static final int PERSON_AUTHORIZE_TYPE = 9;
    public static final int PUBLISHING_TYPE = 10;
    public static final int COMPANY_PUBLISHING_TYPE = 11;
    public static final int COMPANY_PUBLISHING_NEW_VERSION_TYPE = 12;
    public static final int COMPANY_PUBLISHING_ENG_TYPE = 13;

    public static final String YOUTUBE_PERSON = "YOUTUBE_PERSON";
    public static final String DIGITAL_PERSON = "DIGITAL_PERSON";
    public static final String COMPANY_YOUTUBE_MCN = "COMPANY_YOUTUBE_MCN";
    public static final String DIGITAL_COMPANY = "DIGITAL_COMPANY";
    public static final String DIGITAL_PERSON_EN = "DIGITAL_PERSON_EN";
    public static final String RBT_PERSON_EN = "RBT_PERSON_EN";
    public static final String YOUTUBE_PERSON_MCN_EN = "YOUTUBE_PERSON_MCN_EN";
    public static final String COMPANY_AUTHORIZE = "COMPANY_AUTHORIZE";
    public static final String PERSON_AUTHORIZE = "PERSON_AUTHORIZE";
    public static final String PUBLISHING = "PUBLISHING";
    public static final String PUBLISHING_COMPANY = "PUBLISHING_COMPANY";
    public static final String PUBLISHING_COMPANY_NEW_VERSION = "PUBLISHING_COMPANY_NEW_VERSION";
    public static final String PUBLISHING_COMPANY_ENG = "PUBLISHING_COMPANY_ENG";

    public static final String COLLATION_THAI_CASE_INSENSITIVE = " collate Thai_CI_AS ";

    public static final String UPC_SONE_CUSTOMER_ID = MessageCache.getMessage("upc.sone.customer.id");

    /*Content State*/
    public static final int GRAPHICS_JOB_STATE_NEW_JOB = 1;
    public static final int GRAPHICS_JOB_STATE_ASSIGN_JOB = 2;
    public static final int GRAPHICS_JOB_WAITING_APPROVE_JOB = 3;
    public static final int GRAPHICS_JOB_DEFECT_JOB = 4;
    public static final int GRAPHICS_JOB_RESOLVE_JOB = 5;
    public static final int GRAPHICS_JOB_JOB_DONE = 200;
    public static final int GRAPHICS_JOB_CANCEL_DONE = -999;

    public static final int ACTION_ADD_NEW_STREAMING_YOUTUBE = 80;
    public static final int ACTION_ADD_NEW_STREAMING_OTHER = 81;
    public static final int ACTION_UPDATE_STREAMING_OTHER = 82;
    public static final int ACTION_DELETE_STREAMING_YOUTUBE = 83;
    public static final int ACTION_DELETE_STREAMING_OTHER = 84;
    public static final int ACTION_UPDATE_STREAMING_YOUTUBE = 85;

    //public static final String ROOT_DIRECTORY_FOLDER_TEMPLATE = "D:\\My Work\\SoneWork\\Document\\Music Team\\Youtube\\DDEX";
    public static final String ROOT_DIRECTORY_FOLDER_TEMPLATE = File.separator + "home" + File.separator + "tomcat" + File.separator + "ddex_xml_resource" + File.separator;

    public final static String SENDER_PARTY_ID = "PADPIDA2014090901R";
    public final static String SENDER_PARTY_NAME = "SOLUTION ONE HOLDING COMPANY LIMITED(THAILAND)";
    public final static String RECIPIENT_PARTY_ID = "PADPIDA2013020802I";
    public final static String RECIPIENT_PARTY_NAME = "YouTube";
    public final static String RECIPIENT_PARTY_ID2 = "PADPIDA2015120100H";
    public final static String RECIPIENT_PARTY_NAME2 = "YouTube_ContentID";
    public final static String PLINE_YEAR = "2009";

    public final static int MUSIC_SERVICE_YOUTUBE = 12;
    public final static int MUSIC_SERVICE_YOUTUBE_MUSIC = 21;
    public final static int MUSIC_SERVICE_SPOTIFY = 3;
    public final static int MUSIC_SERVICE_ITUNE_AND_APPLE_MUSIC = 1;
    public final static int MUSIC_SERVICE_JOOX = 2;
    public final static int VOCAL_CREDIT_TYPE = 1;
    public final static int LYRIC_CREDIT_TYPE = 2;
    public final static int MELODY_CREDIT_TYPE = 3;
    public final static int OWNER_CREDIT_TYPE = 4;
    public final static int OWNER_LYRIC_CREDIT_TYPE = 5;
    public final static int OWNER_MELODY_CREDIT_TYPE = 6;

    public final static String MAIL_FROM = "jiranuwat@s-onetelecom.com";
    public final static String MAIL_TO = "jiranuwat@s-onetelecom.com,akapan@s-onetelecom.com";
    public final static String MAIL_CC = "jiranuwat@s-onetelecom.com,akapan@s-onetelecom.com";

    public static void main(String[] args) {
        System.out.println("UPC_SONE_CUSTOMER_ID: " + UPC_SONE_CUSTOMER_ID);
    }

    public final static int USER_GROUP_ADMIN_TEAM = 1;
    public final static int USER_GROUP_SONE_ACCOUNT_MANAGER = 999;
//
    //COVER SONG
    public final static int COVER_SONG_REJECT_REQUEST = -999;
    public final static int COVER_SONG_CANCEL_REQUEST = -998;
    public final static int COVER_SONG_NEW_REQUEST = 0;
    public final static int COVER_SONG_APPROVE_REQUEST = 1;
    public final static int COVER_SONG_WAITING_APPROVE = 2;
    
    public final static int MUSIC_CATEGORY_METRO = 88015;
    public final static int MUSIC_CATEGORY_THAI_GROOVE = 99001;
    public final static int MUSIC_CATEGORY_THAI_STRING = 99002;
    public final static int MUSIC_CATEGORY_THAI_LUKTHUNG = 99003;
    public final static int MUSIC_CATEGORY_THAI_SOUTERN = 99004;
    
}
