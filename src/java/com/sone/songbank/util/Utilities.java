/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.util;

import com.sone.songbank.db.DmsCoverSongDbMgr;
import java.net.URLDecoder;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

/**
 *
 * @author DELL
 */
public class Utilities {

    private static final java.text.SimpleDateFormat DATE_DB_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat DATE_DB_REPORT_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);

    private static final java.text.SimpleDateFormat WEB_FORM_DATE_TIME_FORMAT = new java.text.SimpleDateFormat("dd/MM/yyyy HH:mm", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat WEB_FORM_DATE_TIME_JS_FORMAT = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat RELEASE_START_TIME_FORMAT = new java.text.SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_1 = new java.text.SimpleDateFormat("yyyy.MM.dd", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_2 = new java.text.SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_3 = new java.text.SimpleDateFormat("dd/M/yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_4 = new java.text.SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_5 = new java.text.SimpleDateFormat("yyyy/MM/dd", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_6 = new java.text.SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_7 = new java.text.SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_8 = new java.text.SimpleDateFormat("dd-M-yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_9 = new java.text.SimpleDateFormat("dd-MM-yy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_10 = new java.text.SimpleDateFormat("yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_11 = new java.text.SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat FTP_FLODER_NAME = new java.text.SimpleDateFormat("yyyyMMddHHmmssSSS", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat DDEX_HEADER_CREATE_DATE = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat EXCEL_META_DATE_PATTERN_THAI_TIME_ZONE = new java.text.SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat DB_PEROID_TIME = new java.text.SimpleDateFormat("HH:mm:ss", Locale.ENGLISH);

    public static String convertStrDbDateTimeToWebFormat(String dateTime) {
        String tmp = "";
        try {
            if (!Utilities.isBlankText(dateTime)) {
                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                calDateTime.setTime(DATE_DB_FORMAT.parse(dateTime));

                tmp = WEB_FORM_DATE_TIME_FORMAT.format(calDateTime.getTime());
            }
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertStrDbDateTimeToJsFormat(String dateTime) {
        String tmp = "";
        try {
            if (!Utilities.isBlankText(dateTime)) {
                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));

                tmp = WEB_FORM_DATE_TIME_JS_FORMAT.format(calDateTime.getTime());
            }
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String getDataFromEncode(String tmp2) {
        String tmp = "";

        try {
            if (!Utilities.isBlankText(tmp2)) {
                tmp = URLDecoder.decode(tmp2, "UTF-8");
            }
        } catch (Exception e) {
        }

        //trim string before, end
        return tmp.trim();
    }

    public static String convertDateTimeWeb2DBFormat(String dateTime) {
        String tmp = "";
        try {
            if (!Utilities.isBlankText(dateTime)) {
                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));

                tmp = DATE_DB_FORMAT.format(calDateTime.getTime());
            }

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertDateTimeWebNoTime2DBFormat(String dateTime) {
        String tmp = "";
        try {
            if (!Utilities.isBlankText(dateTime)) {
                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                calDateTime.setTime(RELEASE_START_TIME_FORMAT.parse(dateTime));

                tmp = EXCEL_META_DATE_PATTERN_11.format(calDateTime.getTime());
            }

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convert2Csv(String dateTime) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = EXCEL_META_DATE_PATTERN_6.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convert2Csv(String dateTime, int dateAdd) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));

                if (dateAdd > 0) {
                    calDateTime.add(Calendar.DATE, dateAdd);
                }
            }
            tmp = EXCEL_META_DATE_PATTERN_6.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convert2CsvForBangkokTimeZone(String dateTime, int dateAdd) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));

                if (dateAdd > 0) {
                    calDateTime.add(Calendar.DATE, dateAdd);
                }
            }
//            datetimeformat 2015-12-01T09:30:00-07:00
            tmp = EXCEL_META_DATE_PATTERN_THAI_TIME_ZONE.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertDateTimeWebSearch2DBFormat(String dateTime) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(EXCEL_META_DATE_PATTERN_3.parse(dateTime));
            }
            tmp = DATE_DB_FORMAT.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertDateTimeDB2WebFormat(String dateTime) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(DATE_DB_FORMAT.parse(dateTime));
            }
            tmp = WEB_FORM_DATE_TIME_FORMAT.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static boolean isBlankText(String tmp) {
        boolean result = Boolean.FALSE;

        if (tmp == null || tmp.trim().equalsIgnoreCase("")
                || tmp.trim().equalsIgnoreCase("-")
                || tmp.trim().equalsIgnoreCase("_")
                || tmp.trim().equalsIgnoreCase("/")
                || tmp.trim().equalsIgnoreCase("null")
                || tmp.trim().equalsIgnoreCase("NULL")) {
            result = Boolean.TRUE;
        }

        return result;
    }

    public static String converNewLineXmlTag(String tmp) {
        String[] lines = tmp.split("\r\n|\r|\n");
        String result = "";
        for (String str : lines) {//&lt;br&gt;
            result = result + str + "<br>";
        }

        return result;
    }

    private static final java.text.SimpleDateFormat DATE_FTP_SHORT_FORMAT = new java.text.SimpleDateFormat("yyyyMMdd", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat YEAR_FORMAT = new java.text.SimpleDateFormat("yyyy", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat DATE_FTP_LONG_FORMAT = new java.text.SimpleDateFormat("yyyyMMddHHmmss", Locale.ENGLISH);
    private static final java.text.SimpleDateFormat DATE_REPORT_SUMMARY_FORMAT = new java.text.SimpleDateFormat("dd/MM/yyyy", Locale.ENGLISH);

    public static String convertStrQueryDbDateToReportFormat(String dateTime) {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(DATE_DB_REPORT_FORMAT.parse(dateTime));

                tmp = DATE_FTP_SHORT_FORMAT.format(calDateTime.getTime());
            }
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getYearFormDbDate(String dateTime) {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = YEAR_FORMAT.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getReleaseTimeFormDbDate(String dateTime) {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = RELEASE_START_TIME_FORMAT.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getReleaseTimeFormDbDateJooxFormat(String dateTime) {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = EXCEL_META_DATE_PATTERN_6.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getReleaseTimeFormDbDateJooxFullFormat(String dateTime) {

        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getReleaseTimeFormDbDateTrueIdMusicFormat(String dateTime) {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = EXCEL_META_DATE_PATTERN_9.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getCurrentDateToReportFormat() {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            tmp = DATE_FTP_SHORT_FORMAT.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String getCurrentDateToReportLongFormat() {
        String tmp = "";
        Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
        try {
            tmp = DATE_FTP_LONG_FORMAT.format(calDateTime.getTime());
        } catch (Exception ex) {
        }

        return tmp;
    }

    public static String convertDateFromExcel2DBFormat(String dateTime) {

        String tmp = "";
        try {

            //find datePattern
            //1. .
            if (!Utilities.isBlankText(dateTime)) {

                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                String separateTxt = ".";
                if (dateTime.contains(separateTxt)) {
                    String[] dateTimeArr = dateTime.split("\\" + separateTxt);
//                    System.out.println("dateTimeArr: " + dateTimeArr.length);
                    if (dateTimeArr.length == 3) {
                        String firstSeperate = dateTimeArr[0];
//                        System.out.println("dateTime: " + dateTime);
//                        System.out.println("firstSeperate: " + firstSeperate);
                        if (firstSeperate.length() == 4) {
                            //format yyyy.MM.dd
                            calDateTime.setTime(EXCEL_META_DATE_PATTERN_1.parse(dateTime));
                            tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                        } else {
                            //format 
                            calDateTime.setTime(EXCEL_META_DATE_PATTERN_2.parse(dateTime));
                            tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                        }
                    }
                } else {
                    separateTxt = "/";
                    if (dateTime.contains(separateTxt)) {
                        String[] dateTimeArr = dateTime.split(separateTxt);
                        if (dateTimeArr.length == 3) {
                            String firstSeperate = dateTimeArr[0];

                            if (firstSeperate.length() == 4) {
                                //format yyyy.MM.dd
                                calDateTime.setTime(EXCEL_META_DATE_PATTERN_5.parse(dateTime));
                                tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                            } else {
                                String secoundSeperate = dateTimeArr[1];

                                if (secoundSeperate.length() < 2) {
                                    //format 
                                    calDateTime.setTime(EXCEL_META_DATE_PATTERN_3.parse(dateTime));
                                    tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                                } else {
                                    //format 
                                    calDateTime.setTime(EXCEL_META_DATE_PATTERN_4.parse(dateTime));
                                    tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                                }

                            }
                        }
                    } else {
                        separateTxt = "-";
                        if (dateTime.contains(separateTxt)) {
                            String[] dateTimeArr = dateTime.split(separateTxt);
                            if (dateTimeArr.length == 3) {
                                String firstSeperate = dateTimeArr[0];

                                if (firstSeperate.length() == 4) {
                                    //format yyyy.MM.dd
                                    calDateTime.setTime(EXCEL_META_DATE_PATTERN_6.parse(dateTime));
                                    tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                                } else {
                                    String secoundSeperate = dateTimeArr[1];

                                    if (secoundSeperate.length() < 2) {
                                        //format 
                                        calDateTime.setTime(EXCEL_META_DATE_PATTERN_7.parse(dateTime));
                                        tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                                    } else {
                                        //format 
                                        calDateTime.setTime(EXCEL_META_DATE_PATTERN_8.parse(dateTime));
                                        tmp = DATE_DB_REPORT_FORMAT.format(calDateTime.getTime());
                                    }

                                }
                            }
                        }

                    }
                }

            }

//            if (dateTime != null && !dateTime.trim().equals("")) {
//                calDateTime.setTime(DATE_DB_FORMAT.parse(dateTime));
//            }
//           
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String getCurrentDateFormatHeaderEmail() {
//    
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            tmp = EXCEL_META_DATE_PATTERN_1.format(calDateTime.getTime());
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertMssqlSpecialTextCondition(String text) {
//    
        String tmp = text;
        try {
            if (tmp.contains("'")) {
                tmp = tmp.replaceAll("'", "''");
            }
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String toTxtDigits(int tmpNumb, int digits) {
        String result = "" + tmpNumb;

        if (result != null) {
            if (digits >= 0) {
                int tmpDigit = result.length();
                while (tmpDigit < digits) {
                    result = "0" + result;
                    tmpDigit = result.length();
                }
            } else {
                digits = digits * (-1);
                int tmpDigit = result.length();

                if (tmpDigit > digits) {
                    result = result.substring((result.length() - digits), result.length());
                }
            }
        }
        return result;
    }

    public static String toDecimal(Double d) {
        DecimalFormat df = new DecimalFormat("0.00##");
        String result = df.format(d).replace(".00", "");
        return result;
    }

    public static String convertNullToEmptyString(String str) {
        if (str == null) {
            return " - ";
        }
        return str;
    }

    public static String toThaiDate(String date) {
        String listMonth[] = {"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
        String result = "";
        try {
            String textSplit[] = date.split("-");
            String sYear = textSplit[0];
            String sMonth = textSplit[1];
            String day = textSplit[2].replace(" 00:00:00.0", "");
            Integer tempMouth = 0;
            try {
                tempMouth = Integer.parseInt(sMonth);
            } catch (Exception e) {

            }
            String mouth = "";
            try {
                mouth = listMonth[tempMouth - 1];
            } catch (Exception e) {

            }

            Integer year = 0;
            try {
                year = Integer.parseInt(sYear) + 543;
            } catch (Exception e) {

            }
            result = day + " " + mouth + " " + year;

        } catch (Exception e) {

        }
        return result;
    }

    public static String toThaiDateWithDetail(String date) {
        String listMonth[] = {"มกราคม", "กุมภาพันธ์", "มีนาคม", "เมษายน", "พฤษภาคม", "มิถุนายน", "กรกฎาคม", "สิงหาคม", "กันยายน", "ตุลาคม", "พฤศจิกายน", "ธันวาคม"};
        String result = "";
        try {
            String textSplit[] = date.split("-");
            String sYear = textSplit[0];
            String sMonth = textSplit[1];
            String day = textSplit[2].replace(" 00:00:00.0", "");
            Integer tempMouth = 0;
            try {
                tempMouth = Integer.parseInt(sMonth);
            } catch (Exception e) {

            }
            String mouth = "";
            try {
                mouth = listMonth[tempMouth - 1];
            } catch (Exception e) {

            }

            Integer year = 0;
            try {
                year = Integer.parseInt(sYear) + 543;
            } catch (Exception e) {

            }
            result = day + " " + mouth + " พ.ศ. " + year;

        } catch (Exception e) {

        }
        return result;
    }

    public static String toEngDate(String date) {
        String listMonth[] = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
        String result = "";
        try {
            String textSplit[] = date.split("-");
            String sYear = textSplit[0];
            String sMonth = textSplit[1];
            String day = textSplit[2].replace(" 00:00:00.0", "");
            Integer tempMouth = 0;
            try {
                tempMouth = Integer.parseInt(sMonth);
            } catch (Exception e) {

            }
            String mouth = "";
            try {
                mouth = listMonth[tempMouth - 1];
            } catch (Exception e) {

            }

            Integer year = 0;
            try {
                year = Integer.parseInt(sYear);
            } catch (Exception e) {

            }
            result = day + " " + mouth + " " + year;

        } catch (Exception e) {

        }
        return result;
    }

    public static String toDateShortFormat(String fullDate) {
        String result = "";
        try {
            String date = fullDate.split(" ")[0];
            result = date;

        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }

    public static String toThaiDateWithDetailShort(String fullDate) {
        String listMonth[] = {"ม.ค.", "ก.พ.", "มี.ค.", "เม.ย.", "พ.ค.", "มิ.ย.", "ก.ค.", "ส.ค.", "ก.ย.", "ต.ค.", "พ.ย.", "ธ.ค."};
        String result = "";
        try {
            String date = fullDate.split(" ")[0];
            String textSplit[] = date.split("/");
            String sYear = textSplit[2];
            String sMonth = textSplit[1];
            String day = textSplit[0];
            Integer tempMouth = 0;
            try {
                tempMouth = Integer.parseInt(sMonth);
            } catch (Exception e) {
                System.out.println(e);
            }
            String mouth = "";
            try {
                mouth = listMonth[tempMouth - 1];
            } catch (Exception e) {
                System.out.println(e);
            }

            Integer year = 0;
            try {
                year = Integer.parseInt(sYear) + 543;
            } catch (Exception e) {
                System.out.println(e);
            }
            result = day + " " + mouth + " " + year;

        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }

    public static String toEnDate(String date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        SimpleDateFormat formatEn = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH);
        String result = "";
        try {
            Date dateEn = sdf.parse(date);
            result = formatEn.format(dateEn);
        } catch (Exception e) {
            System.out.println(e);
        }
        return result;
    }

    public static String toEnDay(String date) {
        String result = "";
        try {
            result = date.split("-")[0];

        } catch (Exception e) {

        }
        return result;
    }

    public static String toEnMonth(String date) {
        String result = "";
        try {
            result = date.split("-")[1];

        } catch (Exception e) {

        }
        return result;
    }

    public static String toEnYear(String date) {
        String result = "";
        try {
            result = date.split("-")[2];

        } catch (Exception e) {

        }
        return result;
    }

    public static String deleteMinusSignFromIsrc(String isrc) {
        return isrc.replaceAll("-", "");
    }

    public static String concatMinusSigntoIsrc(String isrc) {
        String result = "";
        //THSOH1805688 => TH-SOH-18-05688
        if (isrc.length() == 12) {
            result = isrc.substring(0, 2) + "-" + isrc.substring(2, 5) + "-" + isrc.substring(5, 7) + "-" + isrc.substring(7, 12);
        } else {
            result = isrc;
        }
        return result;
    }

    public static String generateDdexFtpFloderName(Calendar calDateTime) {

        String tmp = "";
        try {
            if (calDateTime == null) {
                calDateTime = Calendar.getInstance(Locale.ENGLISH);
            }

            tmp = FTP_FLODER_NAME.format(calDateTime.getTime());
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String generateDdexHeaderCreateDate(Calendar calDateTime) {

        String tmp = "";
        try {
            if (calDateTime == null) {
                calDateTime = Calendar.getInstance(Locale.ENGLISH);
            }

            tmp = DDEX_HEADER_CREATE_DATE.format(calDateTime.getTime());
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertPeroidOfTime(String peroidTime) {
        String tmp = "PT";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (peroidTime != null && !peroidTime.trim().equals("")) {
                calDateTime.setTime(DB_PEROID_TIME.parse(peroidTime));

                int hour = calDateTime.get(Calendar.HOUR_OF_DAY);
                if (hour > 0) {
                    tmp += calDateTime.get(Calendar.HOUR_OF_DAY) + "H";
                } else {
                    tmp += "0H";
                }
                int minute = calDateTime.get(Calendar.MINUTE);
                if (minute > 0) {
                    tmp += calDateTime.get(Calendar.MINUTE) + "M";
                } else {
                    tmp += "0M";
                }

                int secound = calDateTime.get(Calendar.SECOND);
                if (secound > 0) {
                    tmp += calDateTime.get(Calendar.SECOND) + "S";
                } else {
                    tmp += "0S";
                }
            } else {
                tmp += "0S";
            }
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertStrDbDateTimeToWebShortFormat(String dateTime) {
        String tmp = "";
        try {
            if (!Utilities.isBlankText(dateTime)) {
                Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);

                calDateTime.setTime(DATE_DB_FORMAT.parse(dateTime));

                tmp = EXCEL_META_DATE_PATTERN_6.format(calDateTime.getTime());
            }
        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convert2CLineYearFormat(String dateTime) {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(WEB_FORM_DATE_TIME_FORMAT.parse(dateTime));
            }
            tmp = EXCEL_META_DATE_PATTERN_10.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String getYesterday() {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            calDateTime.add(Calendar.DATE, -1);
            calDateTime.set(Calendar.HOUR, 23);
            calDateTime.set(Calendar.MINUTE, 59);
            calDateTime.set(Calendar.SECOND, 59);
            calDateTime.set(Calendar.MILLISECOND, 59);
//            tmp = EXCEL_META_DATE_PATTERN_11.format(calDateTime.getTime());
            tmp = EXCEL_META_DATE_PATTERN_THAI_TIME_ZONE.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String getTodayday() {
        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
//            calDateTime.add(Calendar.DATE, 0);
            tmp = WEB_FORM_DATE_TIME_FORMAT.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static String convertStrReportDateTimeToDbFormat(String dateTime) {

        String tmp = "";
        try {
            Calendar calDateTime = Calendar.getInstance(Locale.ENGLISH);
            if (dateTime != null && !dateTime.trim().equals("")) {
                calDateTime.setTime(DATE_REPORT_SUMMARY_FORMAT.parse(dateTime));
            }
            tmp = DATE_DB_FORMAT.format(calDateTime.getTime());

        } catch (Exception ex) {
        }
        return tmp;
    }

    public static ArrayList<String> generateFeatList(String s) {
        ArrayList<String> listResult = new ArrayList<String>();
        String tmpLower = s.toLowerCase();
        if (tmpLower.contains("ft.")) {
            String splitAristFeat[] = tmpLower.split("ft.");
            String listFeat = splitAristFeat[1];

            if (listFeat.contains(",")) {
                String tmpSplit[] = listFeat.split(",");
                for (String tmpS : tmpSplit) {
                    listResult.add(tmpS);
                }
            } else if (listFeat.contains("&")) {
                String tmpSplit[] = listFeat.split("&");
                for (String tmpS : tmpSplit) {
                    listResult.add(tmpS);
                }
            } else {
                listResult.add(listFeat);
            }

        } else if (tmpLower.contains("feat")) {
            String splitAristFeat[] = tmpLower.split("feat");
            String listFeat = splitAristFeat[1];
            if (listFeat.contains(",")) {
                String tmpSplit[] = listFeat.split(",");
                for (String tmpS : tmpSplit) {
                    listResult.add(tmpS);
                }
            } else if (listFeat.contains("&")) {
                String tmpSplit[] = listFeat.split("&");
                for (String tmpS : tmpSplit) {
                    listResult.add(tmpS);
                }
            } else {
                listResult.add(listFeat);
            }
        }
        listResult = trimText(listResult);

        return listResult;
    }

    public static ArrayList<String> trimText(ArrayList<String> list) {
        ArrayList<String> listResult = new ArrayList<String>();
        for (String s : list) {
            boolean leftBacket = false;
            boolean rightBacket = false;
            for (int i = 0; i < s.length(); i++) {
                if (s.charAt(i) == '(') {
                    leftBacket = true;
                } else if (s.charAt(i) == ')') {
                    rightBacket = true;
                }
            }
            if (rightBacket && !leftBacket) {
                s = s.replace(")", "");
            }
            if (s.charAt(0) == '.') {
                s = s.substring(1, s.length());
            }
            if (s.charAt(0) == ' ') {
                s = s.substring(1, s.length());
            }
            listResult.add(s);
        }
        return listResult;
    }

    public static void main(String[] args) {
         System.out.println("FICK");
        
//       DmsCoverSongDbMgr dbCoverMgr = null;
//        try {
//
//            dbCoverMgr = new DmsCoverSongDbMgr();
//            dbCoverMgr.initializeConnection();
//            
//            int resutl = dbCoverMgr.execDmsCoverSongSendEmail(1633417509130L);
//            System.out.println(resutl);
//            
//        }catch(Exception ex){
//        }finally {
//            try {
//                dbCoverMgr.releaseConnection();
//            } catch (Exception e) {
//            }
//            dbCoverMgr = null; 
//        }

    }

    public static String convertNumberConfigDigits(String numberTxt, int digits) {
        String result = numberTxt;

        while (result.length() < digits) {
            result = '0' + result;
        }
        return result;
    }

    public static boolean isNumeric(String strNum) {
        if (strNum == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }
}
