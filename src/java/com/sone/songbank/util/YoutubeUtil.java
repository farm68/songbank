/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sone.songbank.util;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser; 
import com.sone.songbank.info.YoutubeChannelInfo;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import javax.net.ssl.HttpsURLConnection;
import org.apache.log4j.Logger;

/**
 *
 * @author akapanpolsri
 */
public class YoutubeUtil {

    private static final Logger logger = Logger.getLogger(YoutubeUtil.class);
    private static final String YOUTUBE_API_URL = "https://www.googleapis.com/youtube/v3/channels?part=statistics,snippet";

    public static void main(String[] args) {
        String channelId = "UCDGZtKscHE6dTU3XCxpC2ag";
        YoutubeUtil youtubeUtil = new YoutubeUtil();
        YoutubeChannelInfo ytChannelInfo = youtubeUtil.walker("AIzaSyAbxcUaMRe_IiYDLZ8b4IeOSnuL-Kck4ms&id=", channelId);
        System.out.println("ID: " + ytChannelInfo.getChannelId());
        System.out.println("Name: " + ytChannelInfo.getChannelName());
        System.out.println("custom url: " + ytChannelInfo.getChannelImageUrl());
        System.out.println("stat: " + ytChannelInfo.getTotalVideo() + " videos | " + ytChannelInfo.getTotalSubscripber() + " subscribes | " + ytChannelInfo.getTotalViews() + " views");

    }

    public static YoutubeChannelInfo walker(String youtubeKey, String channelId) {
        YoutubeChannelInfo ytChannelInfo = null;
        String urlYt = YOUTUBE_API_URL + "&key=" + youtubeKey + "&id=" + channelId; 

        String response = getUrlConnection(urlYt);
        if (!Utilities.isBlankText(response)) {
            ytChannelInfo = convertToObject(response);
        }
        return ytChannelInfo;

    }

    private static YoutubeChannelInfo convertToObject(String response) {
        YoutubeChannelInfo ytChannelInfo = null;
        try {
            ytChannelInfo = new YoutubeChannelInfo();
            JsonObject jsonObject = new JsonParser().parse(response).getAsJsonObject();
            JsonArray jsonArray = null;
            try {
                jsonArray = jsonObject.get("items").getAsJsonArray();
            } catch (Exception ex) {
            }

            if (jsonArray != null) {
                JsonObject item = jsonArray.get(0).getAsJsonObject();
                JsonObject statistics = item.get("statistics").getAsJsonObject();
                String ytChannelId = null;
                try {
                    ytChannelId = item.get("id").getAsString();
                } catch (Exception ex) {
                }

                JsonObject snippet = item.get("snippet").getAsJsonObject();

                String ytTitle = null;
                try {
                    ytTitle = getTextFromJson(snippet.get("title").getAsString());
                } catch (Exception ex) {
                }

                String ytDescription = "";
                try {
                    ytDescription = getTextFromJson(snippet.get("description").getAsString());
                } catch (Exception ex) {
                }

                String ytPublishedAt = null;
                try {
                    ytPublishedAt = getTextFromJson(snippet.get("publishedAt").getAsString());
                } catch (Exception ex) {
                }

                String imageYt = "";
                try {
                    JsonObject thumbnails = snippet.get("thumbnails").getAsJsonObject();
                    JsonObject imgHigh = thumbnails.get("high").getAsJsonObject();
                    imageYt = getTextFromJson(imgHigh.get("url").getAsString());
                } catch (Exception ex) {
                }

                String ytCountry = "";
                try {
                    ytCountry = getTextFromJson(snippet.get("country").getAsString());
                } catch (Exception ex) {
                }

                String ytCustomUrl = "";
                try {
                    ytCustomUrl = getTextFromJson(snippet.get("customUrl").getAsString());
                } catch (Exception ex) {
                }

                Boolean isHiddenSubCount = false;
                try {
                    isHiddenSubCount = statistics.get("hiddenSubscriberCount").getAsBoolean();
                } catch (Exception ex) {
                }

                String tmpViewCount = "";
                try {
                    tmpViewCount = getTextFromJson(statistics.get("viewCount").getAsString());
                } catch (Exception ex) {
                }

                String tmpSubscriberCount = "";
                try {
                    tmpSubscriberCount = getTextFromJson(statistics.get("subscriberCount").getAsString());
                } catch (Exception ex) {
                }

                String tmpVideoCount = "";
                try {
                    tmpVideoCount = getTextFromJson(statistics.get("videoCount").getAsString());
                } catch (Exception ex) {
                }

//                System.out.println("imageYt: " + imageYt);
//                System.out.println("isHiddenSubCount: " + isHiddenSubCount);
//                System.out.println("ytCustomUrl: " + ytCustomUrl);
//                System.out.println("ytPublishedAt: " + ytPublishedAt);
//                System.out.println("tmpViewCount: " + tmpViewCount);
//                System.out.println("tmpSubscriberCount: " + tmpSubscriberCount);
//                System.out.println("tmpVideoCount: " + tmpVideoCount);

                Long viewCount = 0L;
                try {
                    viewCount = Long.parseLong(tmpViewCount);
                } catch (Exception e) {
                }
                Long subscriberCount = 0L;
                try {
                    subscriberCount = Long.parseLong(tmpSubscriberCount);
                } catch (Exception e) {
                }
                Long videoCount = 0L;
                try {
                    videoCount = Long.parseLong(tmpVideoCount);
                } catch (Exception e) {
                }
                ytChannelInfo.setChannelId(ytChannelId);
                ytChannelInfo.setChannelName(ytTitle);
                ytChannelInfo.setChannelDescription(ytDescription);
                ytChannelInfo.setChannelImageUrl(ytCustomUrl);
                ytChannelInfo.setJoinedTime(ytPublishedAt);
                ytChannelInfo.setCustomUrl(ytCustomUrl);
                ytChannelInfo.setCountry(ytCountry);
                ytChannelInfo.setIsHiddenSubCount(isHiddenSubCount);
                ytChannelInfo.setChannelThumbnailUrl(imageYt);
                ytChannelInfo.setTotalViews(viewCount);
                ytChannelInfo.setTotalSubscripber(subscriberCount);
                ytChannelInfo.setTotalVideo(videoCount);
            }
        } catch (Exception ex) {
            logger.error("[YoutubeChannelInfo] for loop  : ", ex);
        }

        return ytChannelInfo;
    }

    private static String getTextFromJson(String tmp2) {
        String result = "";

        try {
            if (!Utilities.isBlankText(tmp2)) {
                result = tmp2;
            }
        } catch (Exception e) {
        }

        //trim string before, end
        return result;
    }
    
     public static String getUrlConnection(String url) { 
        StringBuilder xml = new StringBuilder();
        HttpsURLConnection connection = null;
        try {
            connection = (HttpsURLConnection) new URL(url).openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String resInputLing = "";
            while ((resInputLing = in.readLine()) != null) {
                xml.append(resInputLing);
            }

            in.close();
        } catch (IOException ex) {
        } finally {
            if (connection != null) {
                connection.disconnect();
            }
            connection = null;
        }
        
        logger.info("response : "+xml.toString());
        return xml.toString();
    }

}
