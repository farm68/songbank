<%-- 
    Document   : account
    Created on : Jul 8, 2022, 9:37:22 AM
    Author     : jiranuwat
--%> 

<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.util.ThaiConvert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<!DOCTYPE html>

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
%>

<%//     
    String channelId = "";
    SessionUserInfo sessionUserInfo = null;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
    } catch (Exception ex) {
    }
%>
<tiles:insert page="/assets/template/mainTemplate.jsp" flush="true">
    <tiles:put name="page.title" type="String" >Content Music Cover</tiles:put>
    <tiles:put name="page.body.script" type="String">   
        <script type="text/javascript">
//          ###################### DEFAULT PARAMETER  #######################   
            const htmlLoading = '<div class="text-center  p-xl-5"><img src="<%=request.getContextPath()%>/assets/images/loading-dot/64x64.gif" alt=""></div>';
//          #################################################################      
            $(document).ready(function () {
                search();
            });

            var tempData;
            function search() {
                let keyword = $("#iKeywordSearch").val();
                var datas = ({
                    mode: "LIST_ACCOUNT",
                    keyword: keyword
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManagerAccountServlet",
                    beforeSend: function () {
                        $('#resultSearchDataForm').html(htmlLoading);
                    }, success: function (data) {
                        var dataSet = data;
                        tempData = data;
                        if (dataSet!=null && dataSet.length > 0) {
                            var row = '<div class="row justify-content-center"><div class="col-lg-8 col-md-10 col-sm-12"><table class="table table-sm table-striped" style="width:60%:"> ';
                            row += '<tbody>';
                            var index = 1;
                            for (var i = 0; i < dataSet.length; i++) {
                                var accountId = dataSet[i].accountId;
                                var password = dataSet[i].password || "-";
                                var username = dataSet[i].username || "-";
                                var displayName = dataSet[i].displayName;
                                var youtubeChannelId = dataSet[i].youtubeChannelId || "-";
                                var youtubeChannelName = dataSet[i].youtubeChannelName || "-";
                                var youtubeChannelImage = dataSet[i].youtubeChannelImage || '<%=request.getContextPath()%>/SongBank/assets/images/logoMCN400x400.png';
                                var isYoutubeChannelActive = dataSet[i].isYoutubeChannelActive;
                                var isYoutubeCmsLink = dataSet[i].isYoutubeCmsLink;
                                var youtubeSubscriber = dataSet[i].youtubeSubscriber;
                                var youtubeVideos = dataSet[i].youtubeVideos;
                                var youtubeViews = dataSet[i].youtubeViews;

                                row += ' <tr> ';
                                row += ' <td class="text-center" style="vertical-align: middle;width: 60px;"> ';
                                row += ' <a href="https://www.youtube.com/channel/' + youtubeChannelId + '"><img src="' + youtubeChannelImage + '" class="rounded" style="width: 60px;" alt=""></a>';
                                row += ' </td>';
                                row += ' <td style="vertical-align: middle;"> ';
                                row += ' <div class="text-overflow-lg text-sm"><b>Name:</b> ' + displayName + '</div>';
                                row += ' <div class="text-overflow-lg text-sm"><b>User:</b> ' + username + '</div>';
                                row += ' <div class="text-overflow-lg text-sm"><b>Pass:</b> ' + password + '</div>';
//                                row += ' <div class="">' + username + '</div>  ';
                                row += ' </td>';
                                row += ' <td style="vertical-align: middle;"> ';
                                row += ' <div class="text-overflow-lg text-sm"><b>Channel:</b> <a href="https://www.youtube.com/channel/' + youtubeChannelId + '">' + youtubeChannelName + '</a> <i class="fa fa-link"></i></div>';
                                row += ' <div class="text-overflow-lg text-sm"><b>ID:</b> <small>' + youtubeChannelId + '</small></div>';
                                row += ' <div class="text-overflow-lg text-sm"><b>isActive:</b> ' + isYoutubeChannelActive + '</div>';
//                                row += ' <div class="">' + username + '</div>  ';
                                row += ' </td>';
                                row += ' <td class="text-center" style="vertical-align: middle;width: 80px;"><button type="button" class="btn btn-sm btn-warning" data-toggle="modal" data-target="#modalUpdateAccount" onclick="getEditInfo(' + i + ')"><i class="fa fa-edit"></i> Edit </button></td>';
                                row += ' </tr> '; 
                                index++;
                            }
                            row += '</tbody>'; 
                            row += '</table>';
                            row += '</div>';
                            row += '</div>';
                            $("#resultSearchDataForm").html(row);
                        } else {
                            var row = '<div class="text-center">';
                            row += ' <h5 class="text-black-25 p-xl-5">No List Music</h5>';
                            row += '<div>';
                            row += '<button type="button" class="btn btn-info" data-toggle="modal" data-target="#modalCreateAccount"><i class="fas fa-plus"></i> Create Account </button>';
                            row += '</div>';
                            row += '</div>';
                            $("#resultSearchDataForm").html(row);
                        }
                    }, complete: function () {
                    }
                });
            }

            function getEditInfo(index) {
                let uAccountId = tempData[index].accountId || "";
                let uUsername = tempData[index].username || "";
                let uPassword = tempData[index].password || "";
                let uDisplayname = tempData[index].displayName || "";
                let uDescription = tempData[index].description || "";

                $("#uAccountId").val(uAccountId);
                $("#uUsername").val(uUsername);
                $("#uPassword").val(uPassword);
                $("#uDisplayname").val(uDisplayname);
                $("#uDescription").val(uDescription);
            }

            function updateAccountData() {
                $('#modalUpdateAccount').modal('hide');
                let uAccountId = $("#uAccountId").val();
                let uUsername = $("#uUsername").val();
                let uPassword = $("#uPassword").val();
                let uDisplayname = $("#uDisplayname").val();
                let uDescription = $("#uDescription").val();
                var datas = ({
                    mode: "UPDATE_ACCOUNT",
                    accountId: uAccountId,
                    username: uUsername,
                    password: uPassword,
                    displayname: uDisplayname,
                    description: uDescription
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManagerAccountServlet",
                    beforeSend: function () {
                    }, success: function (data) {
                        alert(data.message);
                        if (data.status) {
                            search();
                        }
                    }, complete: function () {
                    }
                });
            }

            function createAccountData() {
                $('#modalCreateAccount').modal('hide');
                let iChannelName = $("#iChannelName").val();
                let iChannelId = $("#iChannelId").val();
                let iUsername = $("#iUsername").val();
                let iPassword = $("#iPassword").val();

                if (iChannelId != null && iChannelId != "" 
                        && iUsername != null && iUsername != "" 
                        && iPassword != null && iPassword != ""
                        && iChannelName !=null && iChannelName!="") {
                    if (iChannelId.length === 24 && iChannelId.startsWith("UC")) {
                        var datas = ({
                            mode: "CREATE_ACCOUNT",
                            channelId: iChannelId,
                            channelName:iChannelName,
                            username: iUsername,
                            password: iPassword
                        });
                        $.ajax({
                            cache: false,
                            type: 'POST',
                            data: datas,
                            dataType: 'json',
                            url: "<%=request.getContextPath()%>/ManagerAccountServlet",
                            beforeSend: function () {
                            }, success: function (data) {
                                alert(data.message);
                                if (data.status) {
                                    search();
                                }
                            }, complete: function () {
                            }
                        });
                    } else {
                        alert("Incorrect data");
                    }
                }
            }
        </script>  

    </tiles:put>

    <tiles:put name="page.body.head" type="String"></tiles:put>
    <tiles:put name="page.body.data" type="String"> 
        <input type="hidden" name="channelId" id="channelId" value="<%=channelId%>"/>  
        <div class="row"> 
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12  order-lg-first order-md-first order-sm-last">
                <div class="box box-secondary" style="min-height:80vh;">
                    <div class="box-body" style="background:rgba(116, 129, 141, 0.3);border-bottom:1px solid rgba(116, 129, 141, 0.3);color:#464E55;"> 
                        <div class="p-xl-3 row justify-content-center" style="width: 100%;">
                            <div class="col-lg-6 col-md-8 col-sm-10"> 
                                <div class="input-group">  
                                    <input id="iKeywordSearch" type="text" class="form-control form-control-lg rounded" placeholder="Channel-Id">  
                                    <button class="box-actions-item" onclick="search()"><i class="fas fa-2x fa-search"></i></button> 
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="box-body" style="min-height:70vh">
                        <div id="resultSearchDataForm">

                        </div>
                    </div> 
                </div>
            </div>  
        </div>   

        <!--Modal Add-->
        <div class="modal" id="modalCreateAccount" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Account</h5> 
                    </div>
                    <div class="modal-body">
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Channel ID</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="iChannelId" name="iChannelId">
                            </div>
                        </div>
                         <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Channel Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="iChannelName" name="iChannelName">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Username</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="iUsername" name="iUsername">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Password</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="iPassword" name="iPassword">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="createAccountData()">Create</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>


        <!--Modal Edit-->
        <div class="modal" id="modalUpdateAccount" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Create Account</h5> 
                    </div>
                    <div class="modal-body"> 
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Account ID</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="uAccountId" name="uAccountId" readonly="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Username</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="uUsername" name="uUsername">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Password</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="uPassword" name="uPassword">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Display Name</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="uDisplayname" name="uDisplayname">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label text-right">Description</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="uDescription" name="uDescription">
                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info" onclick="updateAccountData()">Update</button>
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .table td {
                padding:5px 5px;
                text-align:left;
            }
            .form-group {
                margin-bottom:0.75rem;
            }
            .form-label-sm[class*="col-"] {
                padding-top:6px;
            }

        </style>

    </tiles:put>
</tiles:insert>

