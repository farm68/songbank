<%-- 
    Document   : add
    Created on : Sep 21, 2021, 1:37:49 PM
    Author     : jiranuwat
--%>

<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.util.ThaiConvert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<!DOCTYPE html>

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
%>

<%//     
    String channelId = "";
    SessionUserInfo sessionUserInfo = null;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        channelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
    } catch (Exception ex) {
    }

    String jsonObjListMusicBase64 = "";
    try {
        jsonObjListMusicBase64 = request.getParameter("minfo");
    } catch (Exception ex) {
    }

%>
<tiles:insert page="/assets/template/mainTemplate.jsp" flush="true">
    <tiles:put name="page.title" type="String" >Content Music Cover</tiles:put>
    <tiles:put name="page.body.script" type="String">   
        <script type="text/javascript">
//          ###################### DEFAULT PARAMETER  #######################  
            var TemplistMusicSelectedArr = [];
            var TemplistChannelArr = [];
            var listAddChannelArr = [];
            var TempDataList;
            const htmlLoading = '<div class="text-center  p-xl-5"><img src="<%=request.getContextPath()%>/assets/images/loading-dot/64x64.gif" alt=""></div>';
            //          #################################################################      
            $(document).ready(function () {
                selectOptionListUserMusic();
                $("#musicCartHeader").hide();
                try {
                    var decodeBase64 = decodeURIComponent(Base64.decode("<%=jsonObjListMusicBase64%>"));
                    var jsonListMusicArr = JSON.parse(decodeBase64);
                    if (jsonListMusicArr != null && jsonListMusicArr.length > 0) {
                        TemplistMusicSelectedArr = jsonListMusicArr;
                    }
                } catch (error) {
                }

                diplayMusicSelectedInner();
                getChannelData();
                $('#linkYtChannel').on('change', function () {
                    var tmpVal = $(this).val();
                    if (!$.isNullText(tmpVal)) {
                        tmpVal = tmpVal.replaceAll("https://www.youtube.com/channel/", "");
                        tmpVal = tmpVal.replaceAll("https://www.youtube.com/c/", "");
                        $("#ytChannelId").val(tmpVal);
                        findYtChannelInfo();
                    }

                });
            });

            function selectOptionListUserMusic() {
                var datas = ({
                    mode: 'LIST_USER_ACCOUNT',
                    userGroupId: '',
                    isNotAll: 1
                });
                var id = "channelAccManagerUser";
                var url = "<%=request.getContextPath()%>/GenerateListSelectServlet";
                var selectValue = "";

                ajax_generate_option_callfnc(id, url, datas, selectValue, "setSelectSearch(\"" + id + "\")");
            }


            function getChannelData() {
                var channelId = encodeURIComponent($("#channelId").val());
                var datas = ({
                    mode: "GET_YOUTUBE_CHANNEL",
                    channelId: channelId,
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageArtitstPortalServlet",
                    beforeSend: function () {
                    }, success: function (data) {
                        var listData = data.drList;
                        TemplistChannelArr = listData.listChilChannelInfo;
                        if (listData != null) {
                            var channelName = listData.channelName;
                            var channelId = listData.channelId;
                            var requestCoverName = listData.name;
                            var requestCoverAliasName = listData.aliasName;
                            var requestCoverEmail = listData.email;
                            var requestCoverTel = listData.tel;
                            var prefixName = listData.prefixName;
                            $('#iRequestCoverName').val(requestCoverName);
//                            $('#iRequestCoverArtistName').val(requestCoverName);
                            $('#selectPrefixName').val(prefixName);
                            $('#iRequestCoverArtistAliasName').val(requestCoverAliasName);
                            $('#iRequestCoverEmail').val(requestCoverEmail);
                            $('#iRequestCoverTel').val(requestCoverTel);
//                            $('#iRequestCoverChannel').val(title); 
                            if (TemplistChannelArr != null && TemplistChannelArr.length > 0) {
                                for (var i = 0; i < TemplistChannelArr.length; i++) {
                                    var ytChannelName = TemplistChannelArr[i].channelName;
                                    var ytChannelId = TemplistChannelArr[i].channelId;
                                    listAddChannelArr.push({
                                        'linkYtChannel': "",
                                        'channelId': ytChannelId,
                                        'channelName': ytChannelName
                                    });
                                }
                            } else {
                                listAddChannelArr.push({
                                    'linkYtChannel': "",
                                    'channelId': channelId,
                                    'channelName': channelName
                                });
                            }
                            diplayListYoutubeChannel();
                        }
                    }, complete: function () {
                    }
                });
            }

            function initContentYoutube() {
                var videoSrc;
                $('.video-btn').click(function () {
                    videoSrc = $(this).data("src");
                });
                $('#playYoutubeModal').on('shown.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
                });
                $('#playYoutubeModal').on('hide.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc);
                });
            }

            function addMusicCover(index) {
                var musicId = tempDataList[index].musicId;
                var elementBtnAddId = "btnAdd" + musicId;
                document.getElementById(elementBtnAddId).disabled = true;
                TemplistMusicSelectedArr.push(tempDataList[index]);
                diplayMusicSelectedInner();
            }

            function removeMusicCover(index) {
                var tempArr = [];
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        if (index != i) {
                            tempArr.push(TemplistMusicSelectedArr[i]);
                        } else {
                            try {
                                var musicId = TemplistMusicSelectedArr[i].musicId;
                                var elementBtnAddId = "btnAdd" + musicId;
                                document.getElementById(elementBtnAddId).disabled = false;
                            } catch (err) {
                            }
                        }
                    }
                }
                TemplistMusicSelectedArr = tempArr;
                diplayMusicSelectedInner();
            }

            function diplayMusicSelectedInner() {
                var index = 1;
                var row = "";
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    row += '<table class="table table-bordered table-striped" > ';
                    row += '<tbody>';
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        var albumNameEn = TemplistMusicSelectedArr[i].albumNameEn || "";
                        var albumNameTh = TemplistMusicSelectedArr[i].albumNameTh || " - ";
                        var artistNameEn = TemplistMusicSelectedArr[i].artistNameEn || " - ";
                        var artistNameTh = TemplistMusicSelectedArr[i].artistNameTh || " - ";
                        var compLyricEn = TemplistMusicSelectedArr[i].compLyricEn || " - ";
                        var compLyricTh = TemplistMusicSelectedArr[i].compLyricTh || " - ";
                        var compMelodyEn = TemplistMusicSelectedArr[i].compMelodyEn || " - ";
                        var compMelodyTh = TemplistMusicSelectedArr[i].compMelodyTh || " - ";
                        var compOriginalVocalEn = TemplistMusicSelectedArr[i].compOriginalVocalEn;
                        var compOriginalVocalTh = TemplistMusicSelectedArr[i].compOriginalVocalTh;
                        var compOwnerEn = TemplistMusicSelectedArr[i].compOwnerEn;
                        var compOwnerMasterEn = TemplistMusicSelectedArr[i].compOwnerMasterEn;
                        var compOwnerMasterTh = TemplistMusicSelectedArr[i].compOwnerMasterTh;
                        var compOwnerMelodyEn = TemplistMusicSelectedArr[i].compOwnerMelodyEn;
                        var compOwnerMelodyTh = TemplistMusicSelectedArr[i].compOwnerMelodyTh;
                        var compOwnerTh = TemplistMusicSelectedArr[i].compOwnerTh;
                        var genreId = TemplistMusicSelectedArr[i].genreId;
                        var genreName = TemplistMusicSelectedArr[i].genreName;
                        var isrc = TemplistMusicSelectedArr[i].isrc;
                        var labelNameEn = TemplistMusicSelectedArr[i].labelNameEn || " - ";
                        var abelNameTh = TemplistMusicSelectedArr[i].abelNameTh || " - ";
                        var musicId = TemplistMusicSelectedArr[i].musicId;
                        var shortLyrics = TemplistMusicSelectedArr[i].shortLyrics;
                        var thumbnailUrl = TemplistMusicSelectedArr[i].thumbnailUrl;
                        var trackNameEn = TemplistMusicSelectedArr[i].trackNameEn || "";
                        var trackNameTh = TemplistMusicSelectedArr[i].trackNameTh || " - ";
                        var upcCode = TemplistMusicSelectedArr[i].upcCode;
                        var coverOwnerPercentSharing = TemplistMusicSelectedArr[i].coverOwnerPercentSharing;
                        var coverPartnerPercentSharing = TemplistMusicSelectedArr[i].coverPartnerPercentSharing;
                        var trackName = trackNameTh;
                        if (trackNameEn != null && trackNameEn != "") {
                            trackName = trackName + " (" + trackNameEn + ")";
                        }

                        var tagAction = '<span onclick="removeMusicCover(' + i + ')"  style="cursor:pointer;"><i class="fe fa-2x fe-x text-danger"></i></span>';
                        row += ' <tr> ';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + index + ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;width: 45px;"> ';
                        row += ' <img src="' + thumbnailUrl + '" class="rounded" style="width: 45px;" alt="">';
                        row += ' </td>';
                        row += ' <td style="vertical-align: middle;"> ';
                        row += ' <div class="text-overflow-lg "><b>เพลง : </b>' + trackName + '</div>';
                        row += ' <div class="text-overflow-lg "><b>อัลบัม : </b>' + albumNameTh + '</div>  ';
                        row += ' <div class="text-overflow-lg "><b>ศิลปิน : </b>' + artistNameTh + '</div>  '; 
                        row += ' </td>';
                        row += ' <td>'
                        row += ' <div class="text-center ">ส่วนแบ่ง Owner</div>';
                        row += ' <div class="text-center "><input form-control " type="text" size="10" value="' + coverOwnerPercentSharing + '%" placeholder="" disabled=""/></div>';
                        row += ' </td>';
                        row += ' <td>'
                        row += ' <div class="text-center ">ส่วนแบ่งผู้ Cover</div>';
                        row += ' <div class="text-center "><input form-control " type="text" size="10" value="' + coverPartnerPercentSharing + '%" placeholder="" disabled=""/></div>';
                        row += ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + tagAction + ' </td>';
                        row += ' </tr> ';
                        index++;
                    }
                    row += '</tbody>';
                    row += '</table>';
                } else {
                    row = '<div class="text-center"><h5 class="text-black-25 p-xl-2">No Music Request</h5></div>';
                }
                $('#divMusicSelectedInner').html(row);
            }

            function addYoutubeChannelCover() {
                if (validateYtChannel()) {
                    var linkYtChannel = $("#linkYtChannel").val();
                    var ytChannelId = $("#ytChannelId").val();
                    var ytChannelName = $("#ytChannelName").val();
                    var isDuplicate = false;
                    if (listAddChannelArr != null && listAddChannelArr.length > 0) {
                        for (var i = 0; i < listAddChannelArr.length; i++) {
                            if (ytChannelId === listAddChannelArr[i].channelId) {
                                isDuplicate = true;
                                break;
                            }
                        }
                    }

                    if (isDuplicate) {
                        displayYtChannelWarn("Warning!", "Channel ID is duplicate");
                    } else {
                        listAddChannelArr.push({
                            'linkYtChannel': linkYtChannel,
                            'channelId': ytChannelId,
                            'channelName': ytChannelName
                        });
                        diplayListYoutubeChannel();
                        closeModalYtChannel();
                    }
                }
            }

            function removeYoutubeChannelCover(index) {
                if (!$.isNullText(index) && index >= 0) {
                    listAddChannelArr.splice(index, 1);
                    diplayListYoutubeChannel();
                }
            }

            function closeModalYtChannel() {
                //clear form
                $("#linkYtChannel").val("");
                $("#ytChannelId").val("");
                $("#ytChannelName").val("");
                //alert("closeModalComposer");
                $("#displayWarning").html("");
                $('#ytChannelModal').modal('toggle');
            }

            function diplayListYoutubeChannel() {
                $('#divSelectYoutubeChannel').html("");
                var row = "";
                var num = 1;
                if (listAddChannelArr != null && listAddChannelArr.length > 0) {
                    for (var jj = 0; jj < listAddChannelArr.length; jj++) {
                        var linkYtChannel = listAddChannelArr[jj].linkYtChannel;
                        var ytChannelId = listAddChannelArr[jj].channelId;
                        var ytChannelName = listAddChannelArr[jj].channelName;
                        var actionDelTag = 'removeYoutubeChannelCover(' + jj + ')';
                        row += '<tr class="dataChoice" >';
                        row += '<td class="text-center" style="vertical-align: middle;">' + num + '</td>';
                        row += '<td>';
                        row += '<div class=" text-black-75"><b>Channel Name</b> : <span>' + ytChannelName + '</span></div>';
                        row += '<div class=" text-black-75"><b>Channel ID</b>   : <span>' + ytChannelId + '</span></div>';
                        row += '</td>';
                        row += '<td class="text-center" style="vertical-align: middle;">' +
                                '<div class="btn-group-vertical">' +
//                                '<button type="button" class="btn btn-sm btn-warning" onclick="' + actionEditTag + '"><i class="fa fa-pencil" aria-hidden="true"></i></button>' +
                                '<button type="button" class="btn btn-sm btn-danger" onclick="' + actionDelTag + '"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                                '</div>'
                                + '</td>';
                        row += '</tr>';
                        num++;
                    }

                    var tmpRow = "<table class=\"table table-striped table-bordered\">";
                    tmpRow += row;
                    tmpRow += "</table>";
                    $('#divSelectYoutubeChannel').html(tmpRow);
                } else {
                    $('#divSelectYoutubeChannel').html('<div class="text-center"><h6 class="p-xl-2" style="color:#C1C1C1">No List Channel</h6></div>');
                }
            }

            function validateYtChannel() {
                var linkYtChannel = $("#linkYtChannel").val();
                var ytChannelId = $("#ytChannelId").val();
                var ytChannelName = $("#ytChannelName").val();
                if ($.isNullText(linkYtChannel)) {
                    $("#linkYtChannel").focus();
                    displayYtChannelWarn("Warning!", "กรุณาระบุ Channel URL");
                    return false;
                }
                if ($.isNullText(ytChannelId)) {
                    $("#ytChannelId").focus();
                    displayYtChannelWarn("Warning!", "Channel ID ไม่ถูกต้อง");
                    return false;
                }
                if ($.isNullText(ytChannelName)) {
                    $("#ytChannelName").focus();
                    displayYtChannelWarn("Warning!", "กรุณาระบุชื่อช่อง");
                    return false;
                }
                return true;
            }

            function displayYtChannelWarn(tile, desc) {
                $("#displayWarning").html("<p class=\"p-3 mb-2 bg-danger text-white\"><b>" + tile + "</b>&nbsp;&nbsp;&nbsp;" + desc + "</p>");
            }

            function validateForm() {
                var iRequestCoverName = $("#iRequestCoverName").val();
                var iRequestCoverEmail = $('#iRequestCoverEmail').val();
                var iRequestCoverTel = $("#iRequestCoverTel").val();
                var iRequestCoverArtistName = $("#iRequestCoverArtistName").val();
                var iRequestCoverArtistAliasName = $("#iRequestCoverArtistAliasName").val();
                var channelId = $("#channelId").val();
                if (iRequestCoverName === undefined || iRequestCoverName === null || iRequestCoverName === "") {
                    $("#iRequestCoverName").focus();
                    swal("Warning!", "กรุณาระบุชื่อ-นามสกุล ผู้ขอลิขสิทธิ์", "error");
                    return false;
                }

                if (iRequestCoverTel === undefined || iRequestCoverTel === null || iRequestCoverTel === "") {
                    $("#iRequestCoverTel").focus();
                    swal("Warning!", "กรุณาระบุเบอร์โทรศัพท์", "error");
                    return false;
                }

                if (iRequestCoverEmail != null && iRequestCoverEmail != "") {
                    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var isEmail = re.test(String(iRequestCoverEmail).toLowerCase());
                    if (!isEmail) {
                        swal("Warning!", "อีเมลให้ถูกต้อง", "error");
                        return false;
                    }
                } else {
                    swal("Warning!", "กรุณาระบุอีเมล", "error");
                    return false;
                }
                if (iRequestCoverEmail === undefined || iRequestCoverEmail === null || iRequestCoverEmail === "") {
                    $("#iRequestCoverEmail").focus();
                    swal("Warning!", "กรุณาระบุอีเมล", "error");
                    return false;
                }
//                if (iRequestCoverArtistName === undefined || iRequestCoverArtistName === null || iRequestCoverArtistName === "") {
//                    $("#iRequestCoverArtistName").focus();
//                    swal("Warning!", "กรุณาระบุชื่อ-นามสกุลศิลปิน", "error");
//                    return false;
//                }

                if (iRequestCoverArtistAliasName === undefined || iRequestCoverArtistAliasName === null || iRequestCoverArtistAliasName === "") {
                    $("#iRequestCoverArtistAliasName").focus();
                    swal("Warning!", "กรุณาระบุชื่อนามแฝงศิลปิน", "error");
                    return false;
                }

                if (TemplistMusicSelectedArr.length === 0) {
                    swal("Warning!", "กรุณาเลือกเพลง Cover", "error");
                    return false;
                }


                var radioValue = $("input[name='radioYoutubeChannel']:checked").val();
                if (radioValue === "1") { //DEFAULT CHANNEL
                    if (TemplistChannelArr.length === 0) {
                        swal("Warning!", "กรุณาเลือกช่อง Youtube ที่จะลง Cover", "error");
                        return false;
                    }
                } else {
                    if (listAddChannelArr.length === 0) {
                        swal("Warning!", "กรุณาเลือกช่อง Youtube ที่จะลง Cover", "error");
                        return false;
                    }
                }

                return true;
            }

            function eventRadioYoutubeChannelCheck() {
                var radioValue = $("input[name='radioYoutubeChannel']:checked").val();
                listAddChannelArr = [];
                if (radioValue == "1") { //DEFAULT CHANNEL   
                    if (TemplistChannelArr != null && TemplistChannelArr.length > 0) {
                        for (var i = 0; i < TemplistChannelArr.length; i++) {
                            var ytChannelName = TemplistChannelArr[i].channelName;
                            var ytChannelId = TemplistChannelArr[i].channelId;
                            listAddChannelArr.push({
                                'linkYtChannel': "",
                                'channelId': ytChannelId,
                                'channelName': ytChannelName
                            });
                        }
                    }
                    $('#divSelectYoutubeChannel').html("");
                    $("#iRequestCoverChannel").show();
                    $("#divBtnAddChannel").hide();
                } else {
                    $('#divSelectYoutubeChannel').html("");
                    $("#iRequestCoverChannel").hide();
                    $("#divBtnAddChannel").show();
                }
                diplayListYoutubeChannel();
            }

            function sendSubmitRequest() {
                if (validateForm()) {
                    var prefixName = $("#selectPrefixName").val();
                    var requestName = $("#iRequestCoverName").val();
                    var requestemail = $('#iRequestCoverEmail').val();
                    var requestAccManagerId = $('#channelAccManagerUser').val();
                    var requestTel = $("#iRequestCoverTel").val();
                    var requestArtistName = $("#iRequestCoverArtistName").val();
                    var requestArtistAliasName = prefixName + " " + requestName;
                    var remark = $("#iRemark").val();
                    var listMusicInfo = {"list_music": TemplistMusicSelectedArr};
                    listMusicInfo = JSON.stringify(listMusicInfo);
                    var listChannelInfo;
                    var radioValue = $("input[name='radioYoutubeChannel']:checked").val();
                    if (radioValue === "1") { //DEFAULT CHANNEL
                        listChannelInfo = {"list_channel": TemplistChannelArr};
                    } else {
                        listChannelInfo = {"list_channel": listAddChannelArr};
                    }
                    listChannelInfo = JSON.stringify(listChannelInfo);
                    var datas = ({
                        mode: "SUBMIT_REQUEST_MUSIC_CONTENT_COVER",
                        prefixName: prefixName,
                        requestName: requestName,
                        requestemail: requestemail,
                        channelAccManagerUser: requestAccManagerId,
                        requestTel: requestTel,
                        requestArtistName: requestArtistName,
                        requestArtistAliasName: requestArtistAliasName,
                        remark: remark,
                        listMusicInfo: listMusicInfo,
                        listChannelInfo: listChannelInfo
                    });
                    $.ajax({
                        cache: false,
                        type: 'POST',
                        data: datas,
                        dataType: 'json',
                        url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                        beforeSend: function () {
                        }, success: function (data) {
                            var status = data.status;
                            if (status) {
                                swal({
                                    title: "สำเร็จ!",
                                    text: data.message,
                                    type: "success"
                                }, function () {
                                    addRequestSuccess();
                                });
                            } else {
                                swal("ขออภัย!", data.message, "error");
                            }
                        }, complete: function () {
                        }
                    });
                }
            }

            function autoRequestCoverName() {
                var name = $("#selectPrefixName option:selected").text() + "" + $('#iRequestCoverName').val();
                $('#iRequestCoverArtistName').val(name);
            }

            function cancelRequestMusicCover() {
                window.location = "./index.jsp";
            }
            function addRequestSuccess() {
                window.location = "./requestList.jsp";
            }

        </script> 
    </tiles:put>

    <tiles:put name="page.body.head" type="String"></tiles:put>
    <tiles:put name="page.body.data" type="String"> 
        <input type="hidden" name="channelId" id="channelId" value="<%=channelId%>"/>  

        <div class="row justify-content-center"> 
            <div class="col-lg-10 col-md-10 col-sm-12">
                <div class="box"> 
                    <div class="box-header"> 
                        <h5>
                            <!--                                    <i class="fe fa-1x fe-shopping-cart text-success"></i> 
                                                                <span class="">New Music Request</span>-->
                        </h5>
                        <div class="box-actions"></div>
                    </div>  
                    <div class="box-body">
                        <div class="row justify-content-md-center">
                            <div class="col-lg-8 col-md-8 col-sm-12"> 
                                <div class="container">
                                    <div class="form-group row">
                                        <label for="Name" class="form-label col-lg-3 col-md-3 col-sm-12 ">ชื่อ-นามสุกล(ศิลปิน)</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12"> 
                                            <div class="input-group">
                                                <div class="input-group-addon ">
                                                    <select id="selectPrefixName" class="form-control   text-center">
                                                        <option value="นาย" selected>นาย</option>
                                                        <option value="นาง" >นาง</option>
                                                        <option value="นางสาว" >นางสาว</option>
                                                    </select>
                                                </div>
                                                <input id="iRequestCoverName" type="text" class="form-control " onchange="autoRequestCoverName()" placeholder="ชื่อ-นามสกุล ภาษาไทย"/>
                                            </div>  
                                        </div> 
                                    </div> 
                                    <div class="form-group row">
                                        <label for="ArtistAliasName" class="form-label col-lg-3 col-md-3 col-sm-12">ชื่อที่ใช้ในวงการ(ศิลปิน)</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <input id="iRequestCoverArtistAliasName" type="text" class="form-control " placeholder="นามแฝง"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Tel" class="form-label col-lg-3 col-md-3 col-sm-12">เบอร์โทรศัพท์</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <input id="iRequestCoverTel" type="text" class="form-control " placeholder="เบอร์โทรศัพท์ 10 หลัก"/>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="Email" class="form-label col-lg-3 col-md-3 col-sm-12">Email</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <input id="iRequestCoverEmail" type="text" class="form-control " placeholder="อีเมล"/>
                                        </div>
                                    </div> 
                                    <div class="form-group row">
                                        <label for="ผู้ติดต่อ/ประสานงาน" class="form-label col-lg-3 col-md-3 col-sm-12">ผู้ดูแล/ติดต่อ/ประสานงาน</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <select class="form-control" id="channelAccManagerUser" name="channelAccManagerUser">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div> 
                                    <!--                                    <div class="form-group row">
                                                                            <label for="ArtistName" class="form-label col-3">ชื่อศิลปิน Cover</label>
                                                                            <div class="col-9">
                                                                                <input id="iRequestCoverArtistName" type="text" class="form-control " placeholder="ชื่อศิลปิน"/> 
                                                                            </div>
                                                                        </div>-->

                                    <div class="form-group row">
                                        <label for="Channel" class="form-label col-lg-3 col-md-3 col-sm-12">Cover ลงในช่อง Youtube</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12">
                                            <!--<input id="iRequestCoverChannel" type="text" class="form-control " placeholder="" disabled=""/>-->

                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="radioYoutubeChannel" id="radioYoutubeChannelDefault" value="1" checked onclick="eventRadioYoutubeChannelCheck()">
                                                <label class="form-check-label" for="radioYoutubeChannelDefault"> Default Channel </label>
                                                <!--<input id="iRequestCoverChannel" type="text" class="form-control " placeholder="" disabled=""/>-->
                                            </div>
                                            <div class="form-check">
                                                <input class="form-check-input" type="radio" name="radioYoutubeChannel" id="radioYoutubeChannelOther" value="2" onclick="eventRadioYoutubeChannelCheck()">
                                                <label class="form-check-label" for="radioYoutubeChannelOther"> Other Channel </label>
                                                <button type="button" id="divBtnAddChannel" class="btn btn-success btn-sms " data-toggle="modal" data-target="#ytChannelModal" style="display: none;"><small><i class="fa fa-plus-circle fa-lg" aria-hidden="true"></i> Add Channel </small></button>
                                            </div>   
                                            <div id="divSelectYoutubeChannel" class="pt-2"></div>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="remark" class="form-label col-lg-3 col-md-3 col-sm-12">หมายเหตุ</label>
                                        <div class="col-lg-9 col-md-9 col-sm-12"> 
                                            <textarea class="form-control " id="iRemark" rows="2"></textarea>
                                        </div>
                                    </div>
                                    <div class="text-partition text-black-25">
                                        <b>เพลงที่เลือก</b>
                                    </div> 
                                    <div id="divMusicSelectedInner" class="table-responsive">
                                        <div class="text-center"><h5 class="text-black-25 p-xl-5">No Music Request</h5></div>
                                    </div>   
                                </div>
                            </div> 
                        </div> 
                    </div>
                    <div class="box-footer">
                        <div class="pull-right"> 
                            <button type="button" onclick="cancelRequestMusicCover()" class="btn btn-danger"> ยกเลิก</button> 
                            <button type="button" onclick="sendSubmitRequest()" class="btn btn-info"> ยืนยัน</button> 
                        </div> 
                    </div>
                </div>  
            </div> 
        </div>   

        <!-- Add Youtube Channel Modal -->
        <div class="modal fade" tabindex="-1" id="ytChannelModal">
            <div class="modal-dialog modal-dialog-youtube">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><b>Youtube Channel Dialog</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modal-body-youtube"> 
                        <div class="container">
                            <div class="row justify-content-md-center">
                                <div class="col-lg-10 col-md-10 col-lg-12 pt-10">
                                    <div class="form-group row">
                                        <label class="form-label text-right col-lg-3 col-md-3 col-sm-12">Link Channel </label>
                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                            <input type="text" class="form-control " name="linkYtChannel" id="linkYtChannel" placeholder="กรอก Link Youtube Channel">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="form-label text-right col-lg-3 col-md-3 col-sm-12">Channel ID </label>
                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                            <input type="text" class="form-control " name="ytChannelId" id="ytChannelId" disabled="" placeholder="กรอก Youtube Channel Id">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="form-label text-right col-lg-3 col-md-3 col-sm-12">Channel Name </label>
                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                            <input type="text" class="form-control " name="ytChannelName" id="ytChannelName" placeholder="กรอกชื่อ Youtube Channel">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-lg-3 col-md-3 col-sm-12"></div>
                                        <div class="col-lg-8 col-md-8 col-sm-12">
                                            <div id="displayWarning" class=" text-danger"></div>
                                        </div>
                                    </div>
                                </div> 
                            </div> 
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary"  data-dismiss="modal"> Close </button>
                        <button type="button" class="btn btn-info" onclick="addYoutubeChannelCover()"> Add Channel </button>

                    </div>
                </div>
            </div>
        </div>

        <style>
            .form-group {
                margin-bottom:0.75rem;
            }
            .form-label-sm[class*="col-"] {
                padding-top:6px;
            }
            .modal-dialog {
                max-width: 800px;
                margin: 30px auto;
            } 
            .modal-body {
                position:relative;
                padding:0px;
            }
            .close {
                position:absolute;
                right:-30px;
                top:0;
                z-index:999;
                font-size:2rem;
                font-weight: normal;
                color:#fff;
                opacity:1;
            }

            .btn-sms {
                font-size:12px;
                padding:2px 5px;
            }

        </style>

    </tiles:put>
</tiles:insert>
