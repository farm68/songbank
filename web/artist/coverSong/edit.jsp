<%-- 
    Document   : contentMusicCopyright
    Created on : Sep 14, 2021, 4:46:43 PM
    Author     : jiranuwat
--%> 
<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.util.ThaiConvert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<!DOCTYPE html>

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
%>

<%//     
    String channelId = "";
    SessionUserInfo sessionUserInfo = null;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        channelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
    } catch (Exception ex) {
    }

    String requestId = "";
    try {
        requestId = request.getParameter("reqId");
    } catch (Exception ex) {
    }

%>
<tiles:insert page="/assets/template/mainTemplate.jsp" flush="true">
    <tiles:put name="page.title" type="String" >Content Music Cover</tiles:put>
    <tiles:put name="page.body.script" type="String">   
        <script type="text/javascript">
//          ###################### DEFAULT PARAMETER  #######################  
            var TemplistMusicSelectedArr = [];
            var TemplistChannelArr = [];
            var TempDataList;
            const htmlLoading = '<div class="text-center  p-xl-5"><img src="<%=request.getContextPath()%>/assets/images/loading-dot/64x64.gif" alt=""></div>';
//          #################################################################      
            $(document).ready(function () {
                $("#musicCartHeader").hide();
                getRequestMusicCoverLog();
                var input = document.getElementById("iKeywordSearch");
                input.addEventListener("keyup", function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        search();
                    }
                });
            });
            function getRequestMusicCoverLog() {
                var requestId = $("#requestId").val();
                var datas = ({
                    mode: "GET_REQUEST_MUSIC_COVER_LOG",
                    requestId: requestId,
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                    beforeSend: function () {
                    }, success: function (data) {
                        var listData = data.drList;
                        var listChannelCover = listData.listChannelCover;
                        var listMusicCover = listData.listMusicCover;
                        var prefixName = listData.prefixName;
                        var name = listData.name;
                        var artistAliasName = listData.artistAliasName;
                        var artistName = listData.artistName;
                        var email = listData.email;
                        var tel = listData.tel;
                        var remark = listData.actionRemark || "";
                        var publishTime = listData.publishTime || "-";
                        var createTime = listData.createTime || "-";
                        var actionTime = listData.actionTime || "-";
                        var updateTime = listData.updateTime || "-";
                        var reqDocUrl = listData.reqDocUrl;
                        var ackDocUrl = listData.ackDocUrl;
                        var requestId = listData.requestId;
                        if (listMusicCover != null && listMusicCover.length > 0) {
                            for (var mCoverIndex = 0; mCoverIndex < listMusicCover.length; mCoverIndex++) {
                                var musicInfo = listMusicCover[mCoverIndex].musicInfo;
                                TemplistMusicSelectedArr.push(musicInfo);
                            }
                        }

                        var channelName = "";
                        if (listChannelCover != null && listChannelCover.length > 0) {
                            for (var cCoverIndex = 0; cCoverIndex < listChannelCover.length; cCoverIndex++) {
                                var channelInfo = listChannelCover[cCoverIndex].channelInfo;
                                channelName = listChannelCover[cCoverIndex].channelName;
                                TemplistChannelArr.push(channelInfo);
                            }
                        }


                        if (listData != null) {
                            $('#iRequestCoverName').val(name);
                            $('#iRequestCoverArtistName').val(artistAliasName);
                            $('#iRequestCoverArtistAliasName').val(artistAliasName);
                            $('#iRequestCoverEmail').val(email);
                            $('#iRequestCoverTel').val(tel);
//                            $('#iRequestCoverChannel').val(channelName);
                            $('#iRemark').val(remark);
                            $("#selectPrefixName").val(prefixName).change();
                        }
                        diplayMusicSelectedInner();
 
                        diplayListYoutubeChannel();
                    }, complete: function () {
                        search();
                    }
                });
            }

            function search(pageIndex) {
                var iKeywordSearch = $("#iKeywordSearch").val();
                var datas = ({
                    mode: "LIST_SEARCH_MUSIC_CONTENT_COVER",
                    keywordSearch: encodeURIComponent(iKeywordSearch),
                    pageIndex: pageIndex,
                    limit: 6
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                    beforeSend: function () {
                        $('#resultSearchDataForm').html(htmlLoading);
                    }, success: function (data) {
                        var length = "";
                        try {
                            length = data.drList.length;
                        } catch (Error) {
                        }

                        if (data != '' && length > 0) {
                            tempDataList = data.drList;
                            manipulateDisplay(data);
                        } else {
                            var row = '<div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>';
                            $('#resultSearchDataForm').html(row);
                        }
                    }, complete: function () {
                    }
                });
            }

            function manipulateDisplay(data) {
                var row = "";
                var dataSet = data.drList;
                row += '<table class="table table-bordered table-striped" > ';
                var labelId = '';
                if (dataSet.length > 0) {
                    row += '<tbody>';
                    var numberCount = data.fCurrentPageAmount;
                    for (var i = 0; i < dataSet.length; i++) {
                        var albumNameEn = dataSet[i].albumNameEn;
                        var albumNameTh = dataSet[i].albumNameTh;
                        var artistNameEn = dataSet[i].artistNameEn;
                        var artistNameTh = dataSet[i].artistNameTh;
                        var compLyricEn = dataSet[i].compLyricEn;
                        var compLyricTh = dataSet[i].compLyricTh;
                        var compMelodyEn = dataSet[i].compMelodyEn;
                        var compMelodyTh = dataSet[i].compMelodyTh;
                        var compOriginalVocalEn = dataSet[i].compOriginalVocalEn;
                        var compOriginalVocalTh = dataSet[i].compOriginalVocalTh;
                        var compOwnerEn = dataSet[i].compOwnerEn;
                        var compOwnerMasterEn = dataSet[i].compOwnerMasterEn;
                        var compOwnerMasterTh = dataSet[i].compOwnerMasterTh;
                        var compOwnerMelodyEn = dataSet[i].compOwnerMelodyEn;
                        var compOwnerMelodyTh = dataSet[i].compOwnerMelodyTh;
                        var compOwnerTh = dataSet[i].compOwnerTh;
                        var genreId = dataSet[i].genreId;
                        var genreName = dataSet[i].genreName;
                        var isrc = dataSet[i].isrc;
                        var labelNameEn = dataSet[i].labelNameEn;
                        var labelNameTh = dataSet[i].abelNameTh;
                        var musicId = dataSet[i].musicId;
                        var shortLyrics = dataSet[i].shortLyrics;
                        var thumbnailUrl = dataSet[i].thumbnailUrl;
                        var trackNameEn = dataSet[i].trackNameEn;
                        var trackNameTh = dataSet[i].trackNameTh;
                        var upcCode = dataSet[i].upcCode;
                        var urlMaster = dataSet[i].urlMaster;
                        var lyrics = dataSet[i].lyrics;
                        var youtubeUrl = "";
                        if (urlMaster != null && urlMaster.length > 0) {
                            try {
                                var strIndex = parseInt(urlMaster.indexOf("=") + 1);
                                var youtubeId = urlMaster.substr(strIndex, urlMaster.length);
                                youtubeUrl = "https://www.youtube.com/embed/" + youtubeId;
                            } catch (error) {
                            }
                        }

                        var trackName = trackNameTh;
                        if (trackNameEn != null && trackNameEn != "") {
                            trackName = trackName + " (" + trackNameEn + ")";
                        }

                        var isMusicSelected = false;
                        if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                            for (var j = 0; j < TemplistMusicSelectedArr.length; j++) {
                                if (musicId == TemplistMusicSelectedArr[j].musicId) {
                                    isMusicSelected = true;
                                    break;
                                }
                            }
                        }

                        var composcer = "";
                        if (compLyricTh == compMelodyTh) {
                            composcer = compLyricTh;
                        } else {
                            composcer = compLyricTh + "/" + compMelodyTh;
                        }

                        var btnAddId = "btnAdd" + musicId;
                        var tagAction = '';
                        tagAction += '<div class="btn-group-vertical btn-sm" role="group" aria-label="Basic example">';
                        if (isMusicSelected) {
                            tagAction += '<button id="' + btnAddId + '" class="btn btn-sm btn-info" onclick="addMusicCover(' + i + ')" disabled=""><i class="fe fe-shopping-cart"></i><span class="text-sm"> Add </span></button>';
                        } else {
                            tagAction += '<button id="' + btnAddId + '" class="btn btn-sm btn-info" onclick="addMusicCover(' + i + ')"><i class="fe fe-shopping-cart"></i><span class="text-sm"> Add </span></button>';
                        }

//                        var btnLyricsId = "btnLyrics" + musicId;
//                        if (lyrics != null && lyrics != "") {
//                            tagAction += '<button id="' + btnLyricsId + '" class="btn btn-sm btn-secondary" onclick="initMusicLyricsModal(' + i + ')"><i class="fe fe-file-text"></i><span class="text-sm"> Lyrics </span></button>';
//                        }

                        if (youtubeUrl != "") {
                            tagAction += '<button class="video-btn btn btn-sm btn-warning" data-toggle="modal" data-src="' + youtubeUrl + '" data-target="#playYoutubeModal"><i class="fe fe-film"></i><span class="text-sm"> Youtube </span></button>';
                        }
                        tagAction += '</div>';
                        row += ' <tr> ';
                        row += ' <td class="text-center" style="vertical-align: middle;width: 60px;"> ';
                        row += ' <img src="' + thumbnailUrl + '" class="rounded" style="width: 60px;" alt="">';
                        row += ' </td>';
                        row += ' <td style="vertical-align: middle;"> ';
                        row += ' <div class="text-overflow-lg"><b>เพลง : </b>' + trackName + '</div>';
                        row += ' <div class="text-overflow-lg"><b>อัลบัม : </b>' + albumNameTh + '</div>  ';
                        row += ' <div class="text-overflow-lg"><b>ศิลปิน : </b>' + artistNameTh + '</div>  ';
                        row += ' <div class="text-overflow-lg"><b>คำร้อง/ทำนอง : </b>' + composcer + '</div>  ';
                        row += ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + tagAction + ' </td>';
                        row += ' </tr> ';
                        numberCount++;
                    }
                    row += '</tbody>';
                }

                row += '<tfoot>';
                var fCurrentPageAmount = data.fCurrentPageAmount;
                var tCurrentPageAmount = data.tCurrentPageAmount;
                var totalAmount = data.totalAmount;
                row += '<tr class="dataRow">';
                row += '<td colspan="10" class="text-right">';
                if (fCurrentPageAmount > 0) {
                    row += '<span id="displayItem"> Displaying ' + addCommas(fCurrentPageAmount) + ' to ' + addCommas(tCurrentPageAmount) + ' of ' + addCommas(totalAmount) + ' items </span>';
                }
                row += '<div id="paggingFormSearchContent"></div>';
                row += '</td>';
                row += '</tr>';
                row += '</tfoot>';
                row += '</table>';
                $('#resultSearchDataForm').html(row);
                initContentYoutube();
                manipulatePageSearch(data, "paggingFormSearchContent", "search");
            }

            function initContentYoutube() {
                var videoSrc;
                $('.video-btn').click(function () {
                    videoSrc = $(this).data("src");
                });
                $('#playYoutubeModal').on('shown.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
                });
                $('#playYoutubeModal').on('hide.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc);
                });
            }

            function initMusicLyricsModal(index) {
                var trackNameEn = tempDataList[index].trackNameEn;
                var trackNameTh = tempDataList[index].trackNameTh;
                var trackName = trackNameTh;
                if (trackNameEn != null && trackNameEn != "") {
                    trackName = trackName + " (" + trackNameEn + ")";
                }

                var mLyrics = tempDataList[index].lyrics;
                $("#musicLyricsTitle").text(trackName);
                $("#musicLyrics").text(mLyrics);
                $('#musicLyricsModal').modal('show');
            }

            function addMusicCover(index) {
                var musicId = tempDataList[index].musicId;
                var elementBtnAddId = "btnAdd" + musicId;
                document.getElementById(elementBtnAddId).disabled = true;
                TemplistMusicSelectedArr.push(tempDataList[index]);
                diplayMusicSelectedInner();
            }

            function removeMusicCover(index) {
                var tempArr = [];
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        if (index != i) {
                            tempArr.push(TemplistMusicSelectedArr[i]);
                        } else {
                            try {
                                var musicId = TemplistMusicSelectedArr[i].musicId;
                                var elementBtnAddId = "btnAdd" + musicId;
                                document.getElementById(elementBtnAddId).disabled = false;
                            } catch (err) {
                            }
                        }
                    }
                }
                TemplistMusicSelectedArr = tempArr;
                diplayMusicSelectedInner();
            }

            function diplayMusicSelectedInner() {
                var index = 1;
                var row = "";
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    row += '<table class="table table-bordered table-striped" > ';
                    row += '<tbody>';
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        var albumNameEn = TemplistMusicSelectedArr[i].albumNameEn;
                        var albumNameTh = TemplistMusicSelectedArr[i].albumNameTh;
                        var artistNameEn = TemplistMusicSelectedArr[i].artistNameEn;
                        var artistNameTh = TemplistMusicSelectedArr[i].artistNameTh;
                        var compLyricEn = TemplistMusicSelectedArr[i].compLyricEn;
                        var compLyricTh = TemplistMusicSelectedArr[i].compLyricTh;
                        var compMelodyEn = TemplistMusicSelectedArr[i].compMelodyEn;
                        var compMelodyTh = TemplistMusicSelectedArr[i].compMelodyTh;
                        var compOriginalVocalEn = TemplistMusicSelectedArr[i].compOriginalVocalEn;
                        var compOriginalVocalTh = TemplistMusicSelectedArr[i].compOriginalVocalTh;
                        var compOwnerEn = TemplistMusicSelectedArr[i].compOwnerEn;
                        var compOwnerMasterEn = TemplistMusicSelectedArr[i].compOwnerMasterEn;
                        var compOwnerMasterTh = TemplistMusicSelectedArr[i].compOwnerMasterTh;
                        var compOwnerMelodyEn = TemplistMusicSelectedArr[i].compOwnerMelodyEn;
                        var compOwnerMelodyTh = TemplistMusicSelectedArr[i].compOwnerMelodyTh;
                        var compOwnerTh = TemplistMusicSelectedArr[i].compOwnerTh;
                        var genreId = TemplistMusicSelectedArr[i].genreId;
                        var genreName = TemplistMusicSelectedArr[i].genreName;
                        var isrc = TemplistMusicSelectedArr[i].isrc;
                        var labelNameEn = TemplistMusicSelectedArr[i].labelNameEn;
                        var abelNameTh = TemplistMusicSelectedArr[i].abelNameTh;
                        var musicId = TemplistMusicSelectedArr[i].musicId;
                        var shortLyrics = TemplistMusicSelectedArr[i].shortLyrics;
                        var thumbnailUrl = TemplistMusicSelectedArr[i].thumbnailUrl;
                        var trackNameEn = TemplistMusicSelectedArr[i].trackNameEn;
                        var trackNameTh = TemplistMusicSelectedArr[i].trackNameTh;
                        var upcCode = TemplistMusicSelectedArr[i].upcCode;
                        var trackName = trackNameTh;
                        if (trackNameEn != null && trackNameEn != "") {
                            trackName = trackName + " (" + trackNameEn + ")";
                        }

                        var tagAction = '<span onclick="removeMusicCover(' + i + ')"  style="cursor:pointer;"><i class="fe fa-2x fe-x text-danger"></i></span>';
                        row += ' <tr> ';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + index + ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;width: 45px;"> ';
                        row += ' <img src="' + thumbnailUrl + '" class="rounded" style="width: 45px;" alt="">';
                        row += ' </td>';
                        row += ' <td style="vertical-align: middle;"> ';
                        row += ' <div class="text-overflow-lg"><b>เพลง : </b>' + trackName + '</div>';
                        row += ' <div class="text-overflow-lg"><b>อัลบัม : </b>' + albumNameTh + '</div>  ';
                        row += ' <div class="text-overflow-lg"><b>ศิลปิน : </b>' + artistNameTh + '</div>  ';
                        row += ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + tagAction + ' </td>';
                        row += ' </tr> ';
                        index++;
                    }
                    row += '</tbody>';
                    row += '</table>';
                } else {
                    row = '<div class="text-center"><h5 class="text-black-25 p-xl-2">No Music Request</h5></div>';
                }
                $('#divMusicSelectedInner').html(row);
            }

            function diplayListYoutubeChannel() {
                $('#divSelectYoutubeChannel').html("");
                var row = "";
                var num = 1;
                if (TemplistChannelArr != null && TemplistChannelArr.length > 0) {
                    for (var jj = 0; jj < TemplistChannelArr.length; jj++) {
                        var linkYtChannel = TemplistChannelArr[jj].linkYtChannel;
                        var ytChannelId = TemplistChannelArr[jj].channelId;
                        var ytChannelName = TemplistChannelArr[jj].channelName;
                        var actionDelTag = 'removeYoutubeChannelCover(' + jj + ')';
                        row += '<tr class="dataChoice" >';
                        row += '<td class="text-center" style="vertical-align: middle;">' + num + '</td>';
                        row += '<td>';
                        row += '<div class=" text-black-75"><b>Channel Name</b> : <span>' + ytChannelName + '</span></div>';
                        row += '<div class=" text-black-75"><b>Channel ID</b>   : <span>' + ytChannelId + '</span></div>';
                        row += '</td>';
                        row += '<td class="text-center" style="vertical-align: middle;">' +
                                '<div class="btn-group-vertical">' +
//                                '<button type="button" class="btn btn-sm btn-warning" onclick="' + actionEditTag + '"><i class="fa fa-pencil" aria-hidden="true"></i></button>' +
                                '<button type="button" class="btn btn-sm btn-danger" onclick="' + actionDelTag + '"><i class="fa fa-trash" aria-hidden="true"></i></button>' +
                                '</div>'
                                + '</td>';
                        row += '</tr>';
                        num++;
                    }

                    var tmpRow = "<table class=\"table table-striped table-bordered\">";
                    tmpRow += row;
                    tmpRow += "</table>";
                    $('#divSelectYoutubeChannel').html(tmpRow);
                } else {
                    $('#divSelectYoutubeChannel').html('<div class="text-center"><h6 class="p-xl-2" style="color:#C1C1C1">No List Channel</h6></div>');
                }
            }

            function validateForm() {
                var iRequestCoverName = $("#iRequestCoverName").val();
                var iRequestCoverEmail = $('#iRequestCoverEmail').val();
                var iRequestCoverTel = $("#iRequestCoverTel").val();
//                var iRequestCoverArtistName = $("#iRequestCoverArtistName").val();
                var iRequestCoverArtistAliasName = $("#iRequestCoverArtistAliasName").val();
                var channelId = $("#channelId").val();
                if (iRequestCoverName === undefined || iRequestCoverName === null || iRequestCoverName === "") {
                    $("#iRequestCoverName").focus();
                    swal("Warning!", "กรุณาระบุชื่อ-นามสกุล ผู้ขอลิขสิทธิ์", "error");
                    return false;
                }

                if (iRequestCoverTel === undefined || iRequestCoverTel === null || iRequestCoverTel === "") {
                    $("#iRequestCoverTel").focus();
                    swal("Warning!", "กรุณาระบุเบอร์โทรศัพท์", "error");
                    return false;
                }

                if (iRequestCoverEmail != null && iRequestCoverEmail != "") {
                    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                    var isEmail = re.test(String(iRequestCoverEmail).toLowerCase());
                    if (!isEmail) {
                        swal("Warning!", "อีเมลให้ถูกต้อง", "error");
                        return false;
                    }
                } else {
                    swal("Warning!", "กรุณาระบุอีเมล", "error");
                    return false;
                }
                if (iRequestCoverEmail === undefined || iRequestCoverEmail === null || iRequestCoverEmail === "") {
                    $("#iRequestCoverEmail").focus();
                    swal("Warning!", "กรุณาระบุอีเมล", "error");
                    return false;
                }
//                if (iRequestCoverArtistName === undefined || iRequestCoverArtistName === null || iRequestCoverArtistName === "") {
//                    $("#iRequestCoverArtistName").focus();
//                    swal("Warning!", "กรุณาระบุชื่อ-นามสกุลศิลปิน", "error");
//                    return false;
//            }

                if (iRequestCoverArtistAliasName === undefined || iRequestCoverArtistAliasName === null || iRequestCoverArtistAliasName === "") {
                    $("#iRequestCoverArtistAliasName").focus();
                    swal("Warning!", "กรุณาระบุชื่อนามแฝงศิลปิน", "error");
                    return false;
                }

                if (TemplistMusicSelectedArr.length === 0) {
                    swal("Warning!", "กรุณาเลือกเพลง Cover", "error");
                    return false;
                }
                return true;
            }

            function sendEditRequest() {
                if (validateForm()) {
                    var requestId = $("#requestId").val();
                    var prefixName = $("#selectPrefixName").val();
                    var requestName = $("#iRequestCoverName").val();
                    var requestemail = $('#iRequestCoverEmail').val();
                    var requestTel = $("#iRequestCoverTel").val();
//                    var requestArtistName = $("#iRequestCoverArtistName").val();
                    var requestArtistAliasName = $("#iRequestCoverArtistAliasName").val();
                    var remark = $("#iRemark").val();
                    var listMusicInfo = {"list_music": TemplistMusicSelectedArr};
                    listMusicInfo = JSON.stringify(listMusicInfo);
                    var listChannelInfo = {"list_channel": TemplistChannelArr};
                    listChannelInfo = JSON.stringify(listChannelInfo);
                    var datas = ({
                        mode: "EDIT_REQUEST_MUSIC_CONTENT_COVER",
                        requestId: requestId,
                        prefixName: prefixName,
                        requestName: requestName,
                        requestemail: requestemail,
                        requestTel: requestTel,
                        requestArtistName: requestName,
                        requestArtistAliasName: requestArtistAliasName,
                        remark: remark,
                        listMusicInfo: listMusicInfo,
                        listChannelInfo: listChannelInfo
                    });
                    $.ajax({
                        cache: false,
                        type: 'POST',
                        data: datas,
                        dataType: 'json',
                        url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                        beforeSend: function () {
                        }, success: function (data) {
                            var status = data.status;
                            if (status) {
                                swal({
                                    title: "สำเร็จ!",
                                    text: data.message,
                                    type: "success"
                                }, function () {
                                    cancelRequestMusicCover();
                                });
                            } else {
                                swal("ขออภัย!", data.message, "error");
                            }
                        }, complete: function () {

                        }
                    });
                }
            }

            function autoRequestCoverName() {
                var name = $("#selectPrefixName option:selected").text() + "" + $('#iRequestCoverName').val();
                $('#iRequestCoverArtistName').val(name);
            }

            function cancelRequestMusicCover() {
                window.location = "./requestList.jsp";
            }

        </script> 
    </tiles:put>

    <tiles:put name="page.body.head" type="String"></tiles:put>
    <tiles:put name="page.body.data" type="String"> 
        <input type="hidden" name="channelId" id="channelId" value="<%=channelId%>"/>  
        <input type="hidden" name="requestId" id="requestId" value="<%=requestId%>"/>  
        <div class="row justify-content-center"> 
            <div class="col-lg-10 col-md-10 col-sm-12">
                <div class="box"> 
                    <div class="box-header"> 
                        <h5>
                            <i class="fe fa-1x fe-edit text-warning"></i> 
                            <span class="font-weight-bold">แก้ไข</span>
                        </h5>
                        <div class="box-actions"></div>
                    </div>  
                    <div class="box-body">
                        <div class="row justify-content-center">
                            <!--            <div class="col-lg-6">
                                            <div class="box">
                                                <div class="box-header"> 
                                                    <h5>Select Music Cover</h5>
                                                    <div class="box-actions"> 
                                                        <input id="iKeywordSearch" type="text" class="form-control" placeholder="คำค้น">  
                                                        <button class="box-actions-item" onclick="search()"><i class="fas fa-2x fa-search"></i></button> 
                                                    </div>
                                                </div>
                                                <div class="box-body">
                                                    <div id="resultSearchDataForm">
                                                        <div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>
                                                    </div>
                                                </div> 
                                            </div>
                                        </div> -->
                            <!--        </div>
                                    <div class="row">-->
                            <div class="col-lg-8 col-md-8 col-sm-12"> 
                                <!--                <ul class="nav nav-tabs" id="musicCoverTab" role="tablist">
                                                    <li class="nav-item" role="presentation">
                                                        <div class="nav-link active" id="musicRequestTab" data-toggle="tab" href="#musicRequestContentTab" role="tab" aria-controls="music reqeust" aria-selected="true" style="cursor: pointer;">
                                                            <i class="fe fa-2x fe-edit text-warning"></i> 
                                                            <span class="font-weight-bold">Edit Music Request</span>
                                                        </div>
                                                    </li> 
                                                </ul>-->
                                <!--                <div class="box tab-content" id="myTabContent">
                                                    <div class=" tab-pane fade show active" id="musicRequestContentTab" role="tabpanel" aria-labelledby="music-tab">-->
                                <div id="divMusicSelected">
                                    <!--<h5 class="text-black-25 p-xl-5">No Music Request</h5>--> 
                                    <div class="box-body">
                                        <div class="container">
                                            <div class="form-group row">
                                                <label for="Name" class="form-label col-lg-3 col-md-3 col-sm-12">ชื่อ-นามสุกล(ศิลปิน)</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12"> 
                                                    <div class="input-group">
                                                        <div class="input-group-addon">
                                                            <select id="selectPrefixName" class="form-control text-center">
                                                                <option value="นาย" selected>นาย</option>
                                                                <option value="นาง" >นาง</option>
                                                                <option value="นางสาว" >นางสาว</option>
                                                            </select>
                                                        </div>
                                                        <input id="iRequestCoverName" type="text" class="form-control" onchange="autoRequestCoverName()" placeholder="ชื่อ-นามสกุล ภาษาไทย"/>
                                                    </div>  
                                                </div> 
                                            </div> 
                                            <div class="form-group row">
                                                <label for="ArtistAliasName" class="form-label col-lg-3 col-md-3 col-sm-12">ชื่อที่ใช้ในวงการ(ศิลปิน)</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <input id="iRequestCoverArtistAliasName" type="text" class="form-control" placeholder="นามแฝง"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="Tel" class="form-label col-lg-3 col-md-3 col-sm-12">เบอร์โทรศัพท์</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <input id="iRequestCoverTel" type="text" class="form-control" placeholder="เบอร์โทรศัพท์ 10 หลัก"/>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="Email" class="form-label col-lg-3 col-md-3 col-sm-12">Email</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <input id="iRequestCoverEmail" type="text" class="form-control" placeholder="อีเมล"/>
                                                </div>
                                            </div>  

                                            <div class="form-group row">
                                                <label for="Channel" class="form-label col-lg-3 col-md-3 col-sm-12">Cover ลงในช่อง Youtube</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12">
                                                    <!--                                                    <input id="iRequestCoverChannel" type="text" class="form-control" placeholder="" disabled=""/>
                                                                                                        <div id="divChannelId" class="text-sm text-danger"></div>-->
                                                    <div id="divSelectYoutubeChannel" class="pt-2"></div>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="remark" class="form-label col-lg-3 col-md-3 col-sm-12">หมายเหตุ</label>
                                                <div class="col-lg-9 col-md-9 col-sm-12"> 
                                                    <textarea class="form-control" id="iRemark" rows="2"></textarea>
                                                </div>
                                            </div>
                                            <div class="text-partition text-black-25">
                                                <b>เพลงที่เลือก</b>
                                            </div>
                                            <div id="divMusicSelectedInner" class="table-responsive">
                                                <div class="text-center"><h5 class="text-black-25 p-xl-5">No Music Request</h5></div>
                                            </div> 
                                        </div> 
                                    </div> 
                                </div>
                                <!--</div>--> 
                                <!--                                <div class=" tab-pane fade" id="orderRequestContentTab" role="tabpanel" aria-labelledby="order-tab">
                                                                    <div id="resultSearchOrderRequestData" class="text-center">
                                                                        <h5 class="text-black-25 p-xl-5">No Order Request</h5>
                                                                    </div>
                                                                </div>  -->
                                <!--</div>--> 
                            </div>  
                        </div>  
                    </div>  
                    <div class="box-footer">
                        <div class="pull-right"> 
                            <button type="button" onclick="cancelRequestMusicCover()" class="btn btn-danger"> ยกเลิก</button> 
                            <button type="button" onclick="sendEditRequest()" class="btn btn-warning"> ยืนยันแก้ไข</button> 
                        </div> 
                    </div>
                </div>  
            </div>  
        </div>  

        <!-- Play Youtube Modal -->
        <div class="modal fade" id="playYoutubeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content"> 
                    <div class="modal-body"> 
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>        
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                        </div> 
                    </div> 
                </div>
            </div>
        </div> 


        <!-- Music Lyrics Modal -->
        <div class="modal" tabindex="-1" id="musicLyricsModal">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><b>เพลง  :: </b><span id="musicLyricsTitle"></span></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="container"><pre><p id="musicLyrics" class="text-center font-weight-normal p-2"></p></pre></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .form-group {
                margin-bottom:0.75rem;
            }
            .form-label-sm[class*="col-"] {
                padding-top:6px;
            }
            .modal-dialog {
                max-width: 800px;
                margin: 30px auto;
            } 
            .modal-body {
                position:relative;
                padding:0px;
            }
            .close {
                position:absolute;
                right:-30px;
                top:0;
                z-index:999;
                font-size:2rem;
                font-weight: normal;
                color:#fff;
                opacity:1;
            }
        </style>

    </tiles:put>
</tiles:insert>
