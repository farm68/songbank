<%-- 
    Document   : index.jsp
    Created on : Sep 14, 2021, 4:46:43 PM
    Author     : jiranuwat
--%> 

<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.util.ThaiConvert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<!DOCTYPE html>

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
%>

<%//     
    String channelId = "";
    SessionUserInfo sessionUserInfo = null;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        channelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
    } catch (Exception ex) {
    }
%>
<tiles:insert page="/assets/template/mainTemplate.jsp" flush="true">
    <tiles:put name="page.title" type="String" >Content Music Cover</tiles:put>
    <tiles:put name="page.body.script" type="String">   
        <script type="text/javascript">
//          ###################### DEFAULT PARAMETER  #######################  
            var TempMusicCategoryArr = [];
            var TemplistMusicSelectedArr = [];
            var TemplistChannelArr = [];
            var TempDataListTopMetro;
            var TempDataListTopThaiGroove;
            var TempDataListTopThaiPop;
            var TempDataListTopThaiLukthung;
            var TempDataListTopThaiSouth;
            var TempDataList;
            const htmlLoading = '<div class="text-center  p-xl-5"><img src="<%=request.getContextPath()%>/assets/images/loading-dot/64x64.gif" alt=""></div>';
//          #################################################################      
            $(document).ready(function () {
                searchByCategory('#resultSearchTopThaiMetro', <%=Constants.MUSIC_CATEGORY_METRO%>);
                searchByCategory('#resultSearchTopThaiGroove', <%=Constants.MUSIC_CATEGORY_THAI_GROOVE%>);
                searchByCategory('#resultSearchTopPop', <%=Constants.MUSIC_CATEGORY_THAI_STRING%>);
                searchByCategory('#resultSearchTopLukthung', <%=Constants.MUSIC_CATEGORY_THAI_LUKTHUNG%>);
                searchByCategory('#resultSearchSongSouth', <%=Constants.MUSIC_CATEGORY_THAI_SOUTERN%>);
                search();
                var input = document.getElementById("iKeywordSearch");
                input.addEventListener("keyup", function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        search();
                    }
                });
                listMusicComposer();
                listMusicCategoryInfo();
            });

            function listMusicCategoryInfo() {
                var datas = ({
                    mode: "LIST_MUSIC_CATEGORY"
                    , isNotAll: 1
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/GenerateListSelectServlet",
                    beforeSend: function () {
                    }, success: function (data) {
                        var length = "";
                        try {
                            length = data.length;
                        } catch (Error) {
                        }
                        var listData = data;
                        if (listData != '' && length > 0) {
                            var htmlText = '';
                            for (var i = 0; i < length; i++) {
                                var mcId = listData[i].id;
                                var mcText = listData[i].text;

                                htmlText += '<li> <label> <input id="checkboxMusicCategory' + mcId + '" name="checkboxMusicCategory" onchange="eventCheckBoxMusicCategory()" type="checkbox" value="' + mcId + '"><span id="txtCategoryName' + mcId + '">' + mcText + '</span></label></li> ';
                            }
                            $('#divDropdownMusicCategory').html(htmlText);
                            eventCheckBoxMusicCategory();
                        }
                    }, complete: function () {
                    }
                });
            }


            function searchByCategory(divResult, categoryId) {
//                alert(divResult +" :: "+ categoryId);
                var datas = ({
                    mode: "LIST_SEARCH_MUSIC_CONTENT_COVER",
                    listMusicCategoryId: categoryId,
                    limit: 60
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                    beforeSend: function () {
                        $(divResult).html(htmlLoading);
                    }, success: function (data) {

                        var length = 0;
                        try {
                            length = data.drList.length;
                        } catch (Error) {
                        }

                        if (data != null && length > 0) {
                            if(categoryId == <%=Constants.MUSIC_CATEGORY_METRO%>){
                                TempDataListTopMetro = data.drList;
                            }else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_GROOVE%>) {
                                TempDataListTopThaiGroove = data.drList;
                            } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_STRING%>) {
                                TempDataListTopThaiPop = data.drList;
                            } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_LUKTHUNG%>) {
                                TempDataListTopThaiLukthung = data.drList;
                            } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_SOUTERN%>) {
                                TempDataListTopThaiSouth = data.drList;
                            }

                            var dataSet = data.drList;
                            var row = '<div class="table-responsive"><table class="table  table-striped" > ';
                            var labelId = '';
                            if (dataSet.length > 0) {
                                row += '<tbody>';
                                var numberCount = data.fCurrentPageAmount;
                                var index = 1;
                                for (var i = 0; i < dataSet.length; i++) {
                                    var albumNameEn = dataSet[i].albumNameEn;
                                    var albumNameTh = dataSet[i].albumNameTh;
                                    var artistNameEn = dataSet[i].artistNameEn;
                                    var artistNameTh = dataSet[i].artistNameTh;
                                    var compLyricEn = dataSet[i].compLyricEn;
                                    var compLyricTh = dataSet[i].compLyricTh;
                                    var compMelodyEn = dataSet[i].compMelodyEn;
                                    var compMelodyTh = dataSet[i].compMelodyTh;
                                    var compOriginalVocalEn = dataSet[i].compOriginalVocalEn;
                                    var compOriginalVocalTh = dataSet[i].compOriginalVocalTh;
                                    var compOwnerEn = dataSet[i].compOwnerEn;
                                    var compOwnerMasterEn = dataSet[i].compOwnerMasterEn;
                                    var compOwnerMasterTh = dataSet[i].compOwnerMasterTh;
                                    var compOwnerMelodyEn = dataSet[i].compOwnerMelodyEn;
                                    var compOwnerMelodyTh = dataSet[i].compOwnerMelodyTh;
                                    var compOwnerTh = dataSet[i].compOwnerTh;
                                    var genreId = dataSet[i].genreId;
                                    var genreName = dataSet[i].genreName;
                                    var isrc = dataSet[i].isrc;
                                    var labelNameEn = dataSet[i].labelNameEn;
                                    var labelNameTh = dataSet[i].labelNameTh;
                                    var musicId = dataSet[i].musicId;
                                    var shortLyrics = dataSet[i].shortLyrics;
                                    var thumbnailUrl = dataSet[i].thumbnailUrl;
                                    var trackNameEn = dataSet[i].trackNameEn;
                                    var trackNameTh = dataSet[i].trackNameTh;
                                    var upcCode = dataSet[i].upcCode;
                                    var urlMaster = dataSet[i].urlMaster;
                                    var lyrics = dataSet[i].lyrics;
                                    var youtubeUrl = "";
                                    if (urlMaster != null && urlMaster.length > 0) {
                                        try {
                                            var strIndex = parseInt(urlMaster.indexOf("=") + 1);
                                            var youtubeId = urlMaster.substr(strIndex, urlMaster.length);
                                            youtubeUrl = "https://www.youtube.com/embed/" + youtubeId;
                                        } catch (error) {
                                        }
                                    }

                                    var trackName = trackNameTh;
//                                    if (trackNameEn != null && trackNameEn != "") {
//                                        trackName = trackName + " (" + trackNameEn + ")";
//                                    }

                                    var isMusicSelected = false;
                                    if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                                        for (var j = 0; j < TemplistMusicSelectedArr.length; j++) {
                                            if (musicId == TemplistMusicSelectedArr[j].musicId) {
                                                isMusicSelected = true;
                                                break;
                                            }
                                        }
                                    }

                                    var composcer = "";
                                    if (compLyricTh == compMelodyTh) {
                                        composcer = compLyricTh;
                                    } else {
                                        composcer = compLyricTh + "/" + compMelodyTh;
                                    }

                                    var btnAddId = "btnAdd" + musicId + categoryId;
                                    var tagAction = '';
                                    if (youtubeUrl != "") {
                                        tagAction += '<button class="video-btn btn btn-sms btn-light" data-toggle="modal" data-src="' + youtubeUrl + '" data-target="#playYoutubeModal"><i class="fe fe-play-circle"></i> เล่นเพลง </button>';
                                    } else {
                                        tagAction += '<button class="video-btn btn btn-sms btn-light" disabled=""><i class="fe fe-play-circle"></i> เล่นเพลง </button>';
                                    }

                                    var btnLyricsId = "btnLyrics" + musicId + categoryId;
                                    if (lyrics !== null && lyrics !== "") {
                                        tagAction += ' | <button id="' + btnLyricsId + '" class="video-btn btn btn-sms btn-light" onclick="initMusicLyricsModal(' + i + ',' + categoryId + ')"><i class="fe fe-file-text"></i> เนื้อร้อง </button>';
                                    } else {
                                        tagAction += ' | <button id="' + btnLyricsId + '" class="video-btn btn btn-sms btn-light" disabled=""><i class="fe fe-file-text"></i> เนื้อร้อง </button>';
                                    }

                                    var tagActionSelect = '';
                                    if (isMusicSelected) {
                                        tagActionSelect += '<button id="' + btnAddId + '" class="btn btn-sm btn-info btn-block" onclick="addMusicCover(' + i + ',' + categoryId + ')" disabled=""><span class="text-white"><i class="fa fa-microphone text-white"></i> เลือก </span></button>';
                                    } else {
                                        tagActionSelect += '<button id="' + btnAddId + '" class="btn btn-sm btn-info btn-block" onclick="addMusicCover(' + i + ',' + categoryId + ')"><span  class="text-white"><i class="fa fa-microphone text-white"></i> เลือก </span></button>';
                                    }
                                    tagAction += '</div>';
                                    row += ' <tr> ';
//                                    row += ' <td class="text-black-50 text-center" style="vertical-align: middle;"><b>' + index + '</b></td>  ';
                                    row += ' <td class="text-center" style="vertical-align: middle;width: 60px;"> ';
                                    row += ' <img src="' + thumbnailUrl + '" class="rounded" style="width: 60px;" alt="">';
                                    row += ' </td>';
                                    row += ' <td style="vertical-align: middle;"> ';
                                    row += ' <div class="text-overflow-lg">' + trackName + '</div>';
                                    row += ' <div class="text-overflow-lg text-sm">' + artistNameTh + '</div>  ';
                                    row += ' <div class="">' + tagAction + '</div>  ';
                                    row += ' </td>';
                                    row += ' <td class="text-center" style="vertical-align: middle;width: 80px;">' + tagActionSelect + ' </td>';
                                    row += ' </tr> ';

                                    index++;
                                }
                                row += '</tbody>';
                            }

                            row += '</table>';
                            row += '</div>';
                            $(divResult).html(row);
                            initContentYoutube();
                        } else {
                            var row = '<div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>';
                            $(divResult).html(row);
                        }
                    }, complete: function () {
                    }
                });
            }

            function search(pageIndex) {
                var listMusicCategoryId = "";
                if (TempMusicCategoryArr != null && TempMusicCategoryArr.length > 0) {
                    for (var i = 0; i < TempMusicCategoryArr.length; i++) {
                        if (i == 0) {
                            listMusicCategoryId = TempMusicCategoryArr[i];
                        } else {
                            listMusicCategoryId = listMusicCategoryId + ',' + TempMusicCategoryArr[i];
                        }
                    }
                }
                var iKeywordSearch = $("#iKeywordSearch").val();
                var datas = ({
                    mode: "LIST_SEARCH_MUSIC_CONTENT_COVER",
                    keywordSearch: encodeURIComponent(iKeywordSearch),
                    listMusicCategoryId: listMusicCategoryId,
                    pageIndex: pageIndex,
                    limit: 80
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                    beforeSend: function () {
                        $('#resultSearchDataForm').html(htmlLoading);
                    }, success: function (data) {
                        var length = "";
                        try {
                            length = data.drList.length;
                        } catch (Error) {
                        }

                        if (data != '' && length > 0) {
                            tempDataList = data.drList;
                            manipulateDisplay(data);
                        } else {
                            var row = '<div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>';
                            $('#resultSearchDataForm').html(row);
                        }
                    }, complete: function () {
                    }
                });
            }

            function manipulateDisplay(data) {
                var row = "";
                var dataSet = data.drList;
                var labelId = '';
                if (dataSet.length > 0) {
                    row += '<div class="row">'; 
                    var numberCount = data.fCurrentPageAmount;
                    for (var i = 0; i < dataSet.length; i++) {
                        var albumNameEn = dataSet[i].albumNameEn;
                        var albumNameTh = dataSet[i].albumNameTh;
                        var artistNameEn = dataSet[i].artistNameEn;
                        var artistNameTh = dataSet[i].artistNameTh;
                        var compLyricEn = dataSet[i].compLyricEn;
                        var compLyricTh = dataSet[i].compLyricTh;
                        var compMelodyEn = dataSet[i].compMelodyEn;
                        var compMelodyTh = dataSet[i].compMelodyTh;
                        var compOriginalVocalEn = dataSet[i].compOriginalVocalEn;
                        var compOriginalVocalTh = dataSet[i].compOriginalVocalTh;
                        var compOwnerEn = dataSet[i].compOwnerEn;
                        var compOwnerMasterEn = dataSet[i].compOwnerMasterEn;
                        var compOwnerMasterTh = dataSet[i].compOwnerMasterTh;
                        var compOwnerMelodyEn = dataSet[i].compOwnerMelodyEn;
                        var compOwnerMelodyTh = dataSet[i].compOwnerMelodyTh;
                        var compOwnerTh = dataSet[i].compOwnerTh;
                        var genreId = dataSet[i].genreId;
                        var genreName = dataSet[i].genreName;
                        var isrc = dataSet[i].isrc;
                        var labelNameEn = dataSet[i].labelNameEn;
                        var labelNameTh = dataSet[i].abelNameTh;
                        var musicId = dataSet[i].musicId;
                        var shortLyrics = dataSet[i].shortLyrics;
                        var thumbnailUrl = dataSet[i].thumbnailUrl;
                        var trackNameEn = dataSet[i].trackNameEn;
                        var trackNameTh = dataSet[i].trackNameTh;
                        var upcCode = dataSet[i].upcCode;
                        var urlMaster = dataSet[i].urlMaster;
                        var lyrics = dataSet[i].lyrics;
                        var youtubeUrl = "";
                        if (urlMaster != null && urlMaster.length > 0) {
                            try {
                                var strIndex = parseInt(urlMaster.indexOf("=") + 1);
                                var youtubeId = urlMaster.substr(strIndex, urlMaster.length);
                                youtubeUrl = "https://www.youtube.com/embed/" + youtubeId;
                            } catch (error) {
                            }
                        }

                        var trackName = trackNameTh;
//                        if (trackNameEn != null && trackNameEn != "") {
//                            trackName = trackName + " (" + trackNameEn + ")";
//                        }

                        var isMusicSelected = false;
                        if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                            for (var j = 0; j < TemplistMusicSelectedArr.length; j++) {
                                if (musicId == TemplistMusicSelectedArr[j].musicId) {
                                    isMusicSelected = true;
                                    break;
                                }
                            }
                        }

                        var composcer = "";
                        if (compLyricTh == compMelodyTh) {
                            composcer = compLyricTh;
                        } else {
                            composcer = compLyricTh + "/" + compMelodyTh;
                        }


                        var tagBtnYt_lyrics = '';
                        if (youtubeUrl != "") {
                            tagBtnYt_lyrics += '<button class="video-btn btn btn-sm btn-light text-sm" data-toggle="modal" data-src="' + youtubeUrl + '" data-target="#playYoutubeModal"><i class="fe fe-play-circle"></i> เล่นเพลง </button>';
                        } else {
                            tagBtnYt_lyrics += '<button class="video-btn btn btn-sm btn-light text-sm" disabled=""><i class="fe fe-play-circle"></i> เล่นเพลง </button>';
                        }

                        var btnLyricsId = "btnLyrics" + musicId;
                        if (lyrics !== null && lyrics !== "") {
                            tagBtnYt_lyrics += '|<button id="' + btnLyricsId + '" class="video-btn btn btn-sm btn-light text-sm" onclick="initMusicLyricsModal(' + i + ',)"><i class="fe fe-file-text"></i> เนื้อร้อง </button>';
                        } else {
                            tagBtnYt_lyrics += '|<button id="' + btnLyricsId + '" class="video-btn btn btn-sm btn-light text-sm" disabled=""><i class="fe fe-file-text"></i> เนื้อร้อง </button>';
                        }

                        var btnAddId = "btnAdd" + musicId;
                        var tagAction = '';
                        if (isMusicSelected) {
                            tagAction += '<button id="' + btnAddId + '" class="btn btn-sm btn-info btn-block" onclick="addMusicCover(' + i + ',)" disabled=""><span class="text-white"><i class="fa fa-microphone text-white"></i> เลือก </span></button>';
                        } else {
                            tagAction += '<button id="' + btnAddId + '" class="btn btn-sm btn-info btn-block" onclick="addMusicCover(' + i + ',)"><span  class="text-white"><i class="fa fa-microphone text-white"></i> เลือก </span></button>';
                        }
                        var tmpTrackName = trackName + " - " + artistNameTh;
                        row += ' <div class="col-lg-3 col-md-4 col-sm-6 col-xs-6" style="padding:5px">';
                        row += '        <div class="box" style="padding:5px">';
                        row += '            <div class="text-center"><img class=" img-thumbnail mx-auto d-block"  src="' + thumbnailUrl + '"   alt="' + tmpTrackName + '"/> </div>';
                        row += '            <div style="padding-bottom:10px;">';
                        row += '                 <div class="text-center text-overflow font-weight-bold"> ' + trackName + '</div>';
                        row += '                 <div class="text-center text-overflow text-sm"> ' + artistNameTh + '</div>';
                        row += '            </div>';
                        row += '            <div class="text-center"> ' + tagBtnYt_lyrics + ' </div>';
                        row += '            <div class="text-center"> ' + tagAction + ' </div>';
                        row += '        </div>';
                        row += ' </div>';
                    }

                    var fCurrentPageAmount = data.fCurrentPageAmount;
                    var tCurrentPageAmount = data.tCurrentPageAmount;
                    var totalAmount = data.totalAmount;



                    row += '<div class="text-end text-secondary text-right col-12">';
                    if (fCurrentPageAmount > 0) {
                        row += '<span id="displayItem" class=""> Displaying ' + addCommas(fCurrentPageAmount) + ' to ' + addCommas(tCurrentPageAmount) + ' of ' + addCommas(totalAmount) + ' items </span>';
                    }
                    row += '<div id="paggingFormSearchContent"></div>';

                    row += '</div>';


                    row += '</div> ';
                }
                $('#resultSearchDataForm').html(row);

                if (data.totalAmount > 0) {
                    manipulatePageSearch(data, "paggingFormSearchContent", "search");
                }
                initContentYoutube();
            }

            function initContentYoutube() {
                var videoSrc;
                $('.video-btn').click(function () {
                    videoSrc = $(this).data("src");
                });
                $('#playYoutubeModal').on('shown.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc + "?autoplay=1&amp;modestbranding=1&amp;showinfo=0");
                });
                $('#playYoutubeModal').on('hide.bs.modal', function (e) {
                    $("#video").attr('src', videoSrc);
                });
            }

            function initMusicLyricsModal(index, categoryId) {
                var listDataMusic = "";
                var musicId = "";
                if(categoryId == <%=Constants.MUSIC_CATEGORY_METRO%>){ 
                    musicId = TempDataListTopMetro[index].musicId;
                    listDataMusic = TempDataListTopMetro[index];
                }else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_GROOVE%>) {
                    musicId = TempDataListTopThaiGroove[index].musicId;
                    listDataMusic = TempDataListTopThaiGroove[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_STRING%>) {
                    musicId = TempDataListTopThaiPop[index].musicId;
                    listDataMusic = TempDataListTopThaiPop[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_LUKTHUNG%>) {
                    musicId = TempDataListTopThaiLukthung[index].musicId;
                    listDataMusic = TempDataListTopThaiLukthung[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_SOUTERN%>) {
                    musicId = TempDataListTopThaiSouth[index].musicId;
                    listDataMusic = TempDataListTopThaiSouth[index];
                } else {
                    musicId = tempDataList[index].musicId;
                    listDataMusic = tempDataList[index];
                }

                var artistNameTh = listDataMusic.artistNameTh;
                var trackNameTh = listDataMusic.trackNameTh;
                var compLyricTh = listDataMusic.compLyricTh;
                var compMelodyTh = listDataMusic.compMelodyTh;
                var urlMaster = listDataMusic.urlMaster;
                var mLyrics = listDataMusic.lyrics || "";

                var tagLyrics = '';
                tagLyrics += '<b> เพลง : ' + trackNameTh + '</b><br/>';
                tagLyrics += '<b> ศิลปิน : ' + artistNameTh + '</b><br/>';
                if (compLyricTh != null && compLyricTh != "") {
                    tagLyrics += '<b> เนื้อร้อง : ' + compLyricTh + '</b><br/>';
                }
                if (compMelodyTh != null && compMelodyTh != "") {
                    tagLyrics += '<b> ทำนอง : ' + compMelodyTh + '</b><br/>';
                }

                if (mLyrics != null && mLyrics != "") {
                    tagLyrics += '<br/><pre>' + mLyrics + '</pre>'
                }

                tagLyrics += '<br/>=======================================<br/>';
                tagLyrics += 'ศิลปินต้นฉบับ : ' + artistNameTh + '<br/>';
                if (compLyricTh != null && compLyricTh != "") {
                    tagLyrics += 'ผู้แต่งเนื้อร้อง : ' + compLyricTh + '<br/>';
                }
                if (compMelodyTh != null && compMelodyTh != "") {
                    tagLyrics += 'ผู้แต่งทำนอง : ' + compMelodyTh + '<br/>';
                }

                if (urlMaster != null && urlMaster != "") {
                    tagLyrics += 'ลิงค์ URL เพลงต้นฉบับ : ' + urlMaster + '<br/>';
                }

                tagLyrics += 'ผู้อนุญาต [ Cover ] แบบถูกต้องลิขสิทธิ์ : Digital One Solution Co. Ltd.';


                var tagBtnSave = "<button type=\"button\" class=\"btn btn-info\" onclick=\"exportMusicLyricTextFile('" + musicId + "')\">Save</button>";

                $("#divSaveLyricBtn").html(tagBtnSave);
                $("#musicLyrics").html(tagLyrics);
                $('#musicLyricsModal').modal('show');
            }

            function addMusicCover(index, categoryId) {
                var listDataMusic = "";
                var musicId = "";
                if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_GROOVE%>) {
                    musicId = TempDataListTopThaiGroove[index].musicId;
                    listDataMusic = TempDataListTopThaiGroove[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_STRING%>) {
                    musicId = TempDataListTopThaiPop[index].musicId;
                    listDataMusic = TempDataListTopThaiPop[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_LUKTHUNG%>) {
                    musicId = TempDataListTopThaiLukthung[index].musicId;
                    listDataMusic = TempDataListTopThaiLukthung[index];
                } else if (categoryId == <%=Constants.MUSIC_CATEGORY_THAI_SOUTERN%>) {
                    musicId = TempDataListTopThaiSouth[index].musicId;
                    listDataMusic = TempDataListTopThaiSouth[index];
                } else {
                    musicId = tempDataList[index].musicId;
                    listDataMusic = tempDataList[index];
                }

                var musicPublishingId = listDataMusic.musicPublishingId;
                var albumNameEn = listDataMusic.albumNameEn;
                var albumNameTh = listDataMusic.albumNameTh;
                var artistNameEn = listDataMusic.artistNameEn;
                var artistNameTh = listDataMusic.artistNameTh;
                var labelNameEn = listDataMusic.labelNameEn;
                var abelNameTh = listDataMusic.abelNameTh;
                var musicId = listDataMusic.musicId;
                var thumbnailUrl = listDataMusic.thumbnailUrl;
                var trackNameEn = listDataMusic.trackNameEn;
                var trackNameTh = listDataMusic.trackNameTh;
                var upcCode = listDataMusic.upcCode;
                var urlMaster = listDataMusic.urlMaster;
                var coverOwnerPercentSharing = listDataMusic.coverOwnerPercentSharing;
                var coverPartnerPercentSharing = listDataMusic.coverPartnerPercentSharing;

                var jsonListMusic = new Object();
                jsonListMusic.musicPublishingId = musicPublishingId;
                jsonListMusic.musicId = musicId;
                jsonListMusic.trackNameTh = trackNameTh;
                jsonListMusic.trackNameEn = trackNameEn;
//                json.genreId = genreId;
//                json.genreName = genreName;
                jsonListMusic.artistNameTh = artistNameTh;
                jsonListMusic.artistNameEn = artistNameEn;
                jsonListMusic.upcCode = upcCode;
                jsonListMusic.albumNameTh = albumNameTh;
                jsonListMusic.albumNameEn = albumNameEn;
                jsonListMusic.labelNameTh = abelNameTh;
                jsonListMusic.labelNameEn = labelNameEn;
                jsonListMusic.urlMaster = urlMaster;
                jsonListMusic.thumbnailUrl = thumbnailUrl;
                jsonListMusic.coverOwnerPercentSharing = coverOwnerPercentSharing;
                jsonListMusic.coverPartnerPercentSharing = coverPartnerPercentSharing;

                TemplistMusicSelectedArr.push(jsonListMusic);
                try {
                    var elementBtnAddId = "btnAdd" + musicId + categoryId;
                    document.getElementById(elementBtnAddId).disabled = true;
                } catch (error) {
                }

                try {
                    var elementBtnAddId = "btnAdd" + musicId;
                    document.getElementById(elementBtnAddId).disabled = true;
                } catch (error) {
                }

                var totalSelect = TemplistMusicSelectedArr.length;
                if (totalSelect > 0) {
                    $("#badgeTotalSelectMusicCover").text(totalSelect);
                } else {
                    $("#badgeTotalSelectMusicCover").text('');
                }
                diplayMusicSelectedInner();
            }

            function removeMusicCover(index) {
                var tempArr = [];
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        if (index != i) {
                            tempArr.push(TemplistMusicSelectedArr[i]);
                        } else {
                            try {
                                var musicId = TemplistMusicSelectedArr[i].musicId;
                                var elementBtnAddId = "btnAdd" + musicId;
                                document.getElementById(elementBtnAddId).disabled = false;
                            } catch (err) {
                            }
                        }
                    }
                }
                TemplistMusicSelectedArr = tempArr;
                var totalSelect = TemplistMusicSelectedArr.length;
                if (totalSelect > 0) {
                    $("#badgeTotalSelectMusicCover").text(totalSelect);
                } else {
                    $("#badgeTotalSelectMusicCover").text('');
                }
                diplayMusicSelectedInner();
            }

            function diplayMusicSelectedInner() {
                var index = 1;
                var row = "";
                if (TemplistMusicSelectedArr != null && TemplistMusicSelectedArr.length > 0) {
                    row += '<table class="table table-bordered table-striped" > ';
                    row += '<tbody>';
                    for (var i = 0; i < TemplistMusicSelectedArr.length; i++) {
                        var albumNameEn = TemplistMusicSelectedArr[i].albumNameEn;
                        var albumNameTh = TemplistMusicSelectedArr[i].albumNameTh;
                        var artistNameEn = TemplistMusicSelectedArr[i].artistNameEn;
                        var artistNameTh = TemplistMusicSelectedArr[i].artistNameTh;
                        var compLyricEn = TemplistMusicSelectedArr[i].compLyricEn;
                        var compLyricTh = TemplistMusicSelectedArr[i].compLyricTh;
                        var compMelodyEn = TemplistMusicSelectedArr[i].compMelodyEn;
                        var compMelodyTh = TemplistMusicSelectedArr[i].compMelodyTh;
                        var compOriginalVocalEn = TemplistMusicSelectedArr[i].compOriginalVocalEn;
                        var compOriginalVocalTh = TemplistMusicSelectedArr[i].compOriginalVocalTh;
                        var compOwnerEn = TemplistMusicSelectedArr[i].compOwnerEn;
                        var compOwnerMasterEn = TemplistMusicSelectedArr[i].compOwnerMasterEn;
                        var compOwnerMasterTh = TemplistMusicSelectedArr[i].compOwnerMasterTh;
                        var compOwnerMelodyEn = TemplistMusicSelectedArr[i].compOwnerMelodyEn;
                        var compOwnerMelodyTh = TemplistMusicSelectedArr[i].compOwnerMelodyTh;
                        var compOwnerTh = TemplistMusicSelectedArr[i].compOwnerTh;
                        var genreId = TemplistMusicSelectedArr[i].genreId;
                        var genreName = TemplistMusicSelectedArr[i].genreName;
                        var isrc = TemplistMusicSelectedArr[i].isrc;
                        var labelNameEn = TemplistMusicSelectedArr[i].labelNameEn;
                        var abelNameTh = TemplistMusicSelectedArr[i].abelNameTh;
                        var musicId = TemplistMusicSelectedArr[i].musicId;
                        var shortLyrics = TemplistMusicSelectedArr[i].shortLyrics;
                        var thumbnailUrl = TemplistMusicSelectedArr[i].thumbnailUrl;
                        var trackNameEn = TemplistMusicSelectedArr[i].trackNameEn;
                        var trackNameTh = TemplistMusicSelectedArr[i].trackNameTh;
                        var upcCode = TemplistMusicSelectedArr[i].upcCode;
                        var trackName = trackNameTh;
                        if (trackNameEn != null && trackNameEn != "") {
                            trackName = trackName + " (" + trackNameEn + ")";
                        }

                        var tagAction = '<span onclick="removeMusicCover(' + i + ')"  style="cursor:pointer;"><i class="fe fa-2x fe-x text-danger"></i></span>';
                        row += ' <tr> ';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + index + ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;width: 45px;"> ';
                        row += ' <img src="' + thumbnailUrl + '" class="rounded" style="width: 45px;" alt="">';
                        row += ' </td>';
                        row += ' <td style="vertical-align: middle;"> ';
                        row += ' <div class=""><b>เพลง : </b>' + trackName + '</div>';
                        row += ' <div class=""><b>อัลบัม : </b>' + albumNameTh + '</div>  ';
                        row += ' <div class=""><b>ศิลปิน : </b>' + artistNameTh + '</div>  ';
                        row += ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + tagAction + ' </td>';
                        row += ' </tr> ';
                        index++;
                    }

                    row += '</tbody>';



                    row += '</table>';
                } else {
                    row = '<div class="text-center"><h5 class="text-black-25 p-xl-2">No Music Request</h5></div>';
                }
                $('#divMusicSelectedInner').html(row);
                $('#musicConfirmModal').modal('show');
            }

            function confirmListMusicCover() {
                var json = JSON.stringify(TemplistMusicSelectedArr);
                json = Base64.encode(encodeURIComponent(json));
                window.location = "./add.jsp?minfo=" + json;
            }

            var nextPage = '<button type="button" class="btn btn-secondary btn-sm"><i class="fas fa-step-forward"></i></button>';
            var previosPage = '<button type="button" class="btn btn-secondary btn-sm"><i class="fas fa-step-backward"></i></button>';

            function manipulatePageSearch(data, idResult, isFunction) {
                var PAGE_NUMBER_LIMIT = 5;
                var resultTmp = '';
                var pageIndex = data.pageIndex;
                var totalPage = data.totalPage;
                var totalAmount = data.totalAmount;
                var fCurrentPageAmount = data.fCurrentPageAmount;
                var tCurrentPageAmount = data.tCurrentPageAmount;

                resultTmp += '<nav aria-label="...">';
                resultTmp += '<ul class="pagination justify-content-end">';

                var moreURL = '' + isFunction + '(#pageIndex#)';

                if (totalPage > 1) {
                    totalPage += 1;
                    var firstPageBox = 0;
                    var totalPageBox = 0;
                    if (pageIndex > PAGE_NUMBER_LIMIT) {
                        if (pageIndex % PAGE_NUMBER_LIMIT == 0) {
                            firstPageBox = (Math.floor((pageIndex - 1) / PAGE_NUMBER_LIMIT) * PAGE_NUMBER_LIMIT) + 1;
                        } else {
                            firstPageBox = (Math.floor(pageIndex / PAGE_NUMBER_LIMIT) * PAGE_NUMBER_LIMIT) + 1;
                        }
                    } else {
                        firstPageBox = 1;
                    }

                    totalPageBox = firstPageBox + (PAGE_NUMBER_LIMIT - 1);
                    if (totalPageBox >= totalPage) {
                        totalPageBox = totalPage;
                    }
                    if (pageIndex > 1) {
                        tmp = moreURL.replace("#pageIndex#", pageIndex - 1);
                        resultTmp += '<li class="page-item" onclick="' + tmp + '">' + previosPage + '</li>';
                    } else {
                        resultTmp += '&nbsp;'; // previosPage + 
                    }

                    resultTmp += '<select class="form-select form-select-sm" style="width:60px;" id="changePage" name="changePage" onchange="' + isFunction + '(this.value)">';

                    for (var i = 1; i < totalPage; i++) {
                        if (i == pageIndex) {
                            resultTmp += "<option selected value=\"" + i + "\">" + i + "</option>";
                        } else {
                            resultTmp += "<option  value=\"" + i + "\">" + i + "</option>";
                        }
                    }
                    resultTmp += '</select>';

                    if (pageIndex < totalPage - 1) {
                        var tmp = moreURL.replace("#pageIndex#", pageIndex + 1);
                        resultTmp += '<li class="page-item" onclick="' + tmp + '">' + nextPage + '</li>';
                    } else {
                        resultTmp += '&nbsp;'; //+ nextPage;
                    }
                }

                resultTmp += '</ul>';
                resultTmp += '</nav>';
                $('#' + idResult).html(resultTmp);
            }

            function gotoUrl(url) {
                window.location = '' + url;
            }

            function addCommas(nStr)
            {
                nStr += '';
                var x = nStr.split('.');
                var x1 = x[0];
                var x2 = x.length > 1 ? '.' + x[1] : '';
                var rgx = /(\d+)(\d{3})/;
                while (rgx.test(x1)) {
                    x1 = x1.replace(rgx, '$1' + ',' + '$2');
                }
                return x1 + x2;
            }


            function eventCheckBoxMusicCategory() {
                TempMusicCategoryArr = [];
                var TempMusicCategoryName = [];
                var htmlListCategoryName = '';
                var index = 0;
                $("input:checkbox[name=checkboxMusicCategory]:checked").each(function () {
                    TempMusicCategoryArr.push($(this).val());
                    TempMusicCategoryName.push()
                    htmlListCategoryName += '&nbsp; <span class="badge  arrow-left text-info">' + $('#txtCategoryName' + $(this).val()).text() + ' <i class="fas fa-times" style="cursor: pointer;" onclick="removeCheckBoxMusicCategory(' + $(this).val() + ')"></i></span> ';
                    index++;
                });
                $('#divCategoryName').html(htmlListCategoryName);
            }

            function removeCheckBoxMusicCategory(value) {
                document.getElementById("checkboxMusicCategory" + value).checked = false;
                TempMusicCategoryArr = [];
                var TempMusicCategoryName = [];
                var htmlListCategoryName = '';
                var index = 0;
                $("input:checkbox[name=checkboxMusicCategory]:checked").each(function () {
                    TempMusicCategoryArr.push($(this).val());
                    TempMusicCategoryName.push()
                    htmlListCategoryName += '&nbsp; <span class="badge  arrow-left text-info">' + $('#txtCategoryName' + $(this).val()).text() + ' <i class="fas fa-times" style="cursor: pointer;" onclick="removeCheckBoxMusicCategory(' + $(this).val() + ')"></i></span> ';
                    index++;
                });
                $('#divCategoryName').html(htmlListCategoryName);
                search(); 
            }

            function exportMusicLyricTextFile(musicId) {
                var url = "<%=request.getContextPath()%>/ExportToPlainTextServlet?mode=EXPORT_MUSIC_LYRICS&musicId=" + musicId;
                window.open(url, "music lyrics");
                $('#musicLyricsModal').modal('toggle');
                return;
            }

            function listMusicComposer() {
                var datas = ({mode: "LIST_MUSIC_COMPOSER_SUGGEST", isNotAll: 1});
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: CONTEXT_PATH + "/GenerateListSelectServlet",
                    beforeSend: function () {
                        //$('#resultTableReportForm').html(waitingMsg);
                    }, success: function (data) {
                        var arraySuggestComposer = [];
                        if (data !== '' && data.length > 0) {
                            for (var i = 0; i < data.length; i++) {
                                arraySuggestComposer.push(data[i].text);
                            }
                            autocomplete(document.getElementById("iKeywordSearch"), arraySuggestComposer);
                        }
                    }, complete: function () {
                    }
                });
            }

            function autocomplete(inp, arr) {
                /*the autocomplete function takes two arguments,
                 the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                /*execute a function when someone writes in the text field:*/
                inp.addEventListener("input", function (e) {
                    var a, b, i, val = this.value;
                    /*close any already open lists of autocompleted values*/
                    closeAllLists();
                    if (!val) {
                        return false;
                    }
                    currentFocus = -1;
                    /*create a DIV element that will contain the items (values):*/
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items rounds");
                    /*append the DIV element as a child of the autocomplete container:*/
                    this.parentNode.appendChild(a);
                    /*for each item in the array...*/
                    for (i = 0; i < arr.length; i++) {
                        /*check if the item starts with the same letters as the text field value:*/
                        if (arr[i].toUpperCase().includes(val.toUpperCase())) {
                            /*create a DIV element for each matching element:*/
                            b = document.createElement("DIV");
                            b.setAttribute("style", "padding-top:10px;padding-bottom:10px;");
                            /*make the matching letters bold:*/
//                            b.innerHTML = "<span><mark>" + arr[i].substr(arr[i].indexOf(val.toUpperCase()), val.length) + "</mark>";
                            b.innerHTML += "<span>" + arr[i] + "</span>";
                            /*insert a input field that will hold the current array item's value:*/
                            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                            /*execute a function when someone clicks on the item value (DIV element):*/
                            b.addEventListener("click", function (e) {
                                /*insert the value for the autocomplete text field:*/
                                inp.value = this.getElementsByTagName("input")[0].value;
                                /*close the list of autocompleted values,
                                 (or any other open lists of autocompleted values:*/
                                closeAllLists();
                            });
                            a.appendChild(b);
                        }
                    }
                });
                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x)
                        x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                         increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                         decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x)
                                x[currentFocus].click();
                        }
                    }
                });
                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x)
                        return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length)
                        currentFocus = 0;
                    if (currentFocus < 0)
                        currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }
                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                     except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }
                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }

        </script> 
        <style type="text/css">
            .topbar{
                display:block;
                /*width:100%;*/
                height:450px;
                /*background-color:#475;*/
                overflow:scroll;
            }
            .img{
                height: 200px;
            }
        </style>
    </tiles:put>

    <tiles:put name="page.body.head" type="String"></tiles:put>
    <tiles:put name="page.body.data" type="String"> 
        <input type="hidden" name="channelId" id="channelId" value="<%=channelId%>"/>  

        <div class="row">
            <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12 order-lg-last order-md-last order-sm-first">
                <div class="box" >
                    <div class="box-header">  
                        <span class="font-weight-bold"><i class="fe fe-thumbs-up"></i> Top Recommend</span>
                    </div>

                    <ul class="nav nav-tabs nav-fill" id="myTab" role="tablist">
                        <li class="nav-item" role="presentation" >
                            <a class="nav-link active font-weight-bold" id="thaiMetro-tab" data-toggle="tab" href="#thaiMetro" role="tab" aria-controls="thaiMetro" aria-selected="true" style="background:rgba(186, 23, 35, 0.3);border-bottom:1px solid rgba(186, 23, 35, 0.3);color:#5F0C12;">รวมเพลง แจ้ ดนุพล</a>
                        </li>
                        <li class="nav-item" role="presentation" >
                            <a class="nav-link font-weight-bold" id="thaiGroove-tab" data-toggle="tab" href="#thaiGroove" role="tab" aria-controls="thaiGroove" aria-selected="false" style="background:rgba(56, 132, 255, 0.3);border-bottom:1px solid rgba(56, 132, 255, 0.3);color:#0050D1;">Thai Groove</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link font-weight-bold" id="musicString-tab" data-toggle="tab" href="#musicString" role="tab" aria-controls="musicString" aria-selected="false" style="background:rgba(245, 132, 36, 0.3);border-bottom:1px solid rgba(245, 132, 36, 0.3);color:#AB5308;">ไทยสตริง</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link font-weight-bold" id="musicCountry-tab" data-toggle="tab" href="#musicCountry" role="tab" aria-controls="musicCountry" aria-selected="false" style="background:rgba(56, 193, 47, 0.3);border-bottom:1px solid rgba(56, 193, 47, 0.3);color:#206F1B;">ลูกทุ่ง</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link font-weight-bold" id="musicSouth-tab" data-toggle="tab" href="#musicSouth" role="tab" aria-controls="musicSouth" aria-selected="false" style="background:rgba(116, 129, 141, 0.3);border-bottom:1px solid rgba(116, 129, 141, 0.3);color:#464E55; ">เพลงใต้</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="thaiMetro" role="tabpanel" aria-labelledby="thaiMetro-tab">
                            <div id="resultSearchTopThaiMetro" style="padding:10px 25px 10px 25px;color:#5F0C12;background:rgba(186, 23, 35, 0.1);"> 
                                <div class="text-center"><h5 class="text-black-25 p-xl-10">No List Music</h5></div> 
                            </div>
                        </div>
                        <div class="tab-pane fade" id="thaiGroove" role="tabpanel" aria-labelledby="thaiGroove-tab">
                            <div id="resultSearchTopThaiGroove" style="padding:10px 25px 10px 25px;color:#0050D1;background:rgba(56, 132, 255, 0.1);min-height:70vh">
                                <div class="text-center"><h5 class="text-black-25 p-xl-10">No List Music</h5></div> 
                            </div>
                        </div>
                        <div class="tab-pane fade" id="musicString" role="tabpanel" aria-labelledby="musicString-tab">
                            <div id="resultSearchTopPop" style="padding:10px 25px 10px 25px;color:#AB5308;background:rgba(245, 132, 36, 0.1);">
                                <div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div> 
                            </div>
                        </div>
                        <div  class="tab-pane fade" id="musicCountry" role="tabpanel" aria-labelledby="musicCountry-tab">
                            <div id="resultSearchTopLukthung" style="padding:10px 25px 10px 25px;color:#206F1B;background:rgba(56, 193, 47, 0.1);">
                                <div id="resultSearchSongSouth" class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div> 
                            </div>
                        </div> 
                        <div class="tab-pane fade" id="musicSouth" role="tabpanel" aria-labelledby="musicSouth-tab">
                            <div id="resultSearchSongSouth"style="padding:10px 25px 10px 25px;color:#464E55;background:rgba(116, 129, 141, 0.3);min-height:70vh">
                                <div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
            <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12  order-lg-first order-md-first order-sm-last">
                <div class="box box-secondary" style="min-height:80vh;">
                    <div class="box-body" style="background:rgba(116, 129, 141, 0.3);border-bottom:1px solid rgba(116, 129, 141, 0.3);color:#464E55;"> 
                        <div class="p-xl-3 row justify-content-center" style="width: 100%;">
                            <div class="col-lg-12 col-md-12 col-sm-12"> 
                                <div class="input-group">  
                                    <div class="dropdown">
                                        <a href="#"  data-dropdown class="btn btn-lg btn-info "id="dropdownMenuMusicCat">
                                            <span class=" font-weight-normal" style="font-size:14px;">ประเภทเพลง <b><i class="fe fe-chevron-down"></i></b></span> 
                                        </a>
                                        <div class="dropdown-menu checkbox-menu allow-focus" aria-labelledby="dropdownMusicCategory" id="divDropdownMusicCategory" onchange="search()">
                                            <li> <label> <input id="checkboxMusicCategory" name="checkboxMusicCategory" type="checkbox" value=""></label></li> 
                                        </div>
                                    </div>
                                    <input id="iKeywordSearch" type="text" class="form-control form-control-lg rounded" placeholder="คำค้น">  
                                    <button class="box-actions-item" onclick="search()"><i class="fas fa-2x fa-search"></i></button> 
                                </div>
                            </div>
                            <div col-lg-12 col-md-12 col-sm-12"> 
                                <span id="divCategoryName"></span>
                            </div>
                        </div>
                    </div>
                    <div class="box-body" style="min-height:70vh">
                        <div id="resultSearchDataForm">
                            <div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>
                        </div>
                    </div> 
                </div>
            </div>  
        </div>  

        <!-- Play Youtube Modal -->
        <div class="modal fade" id="playYoutubeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-youtube" role="document">
                <div class="modal-content"> 
                    <div class="modal-body modal-body-youtube"> 
                        <button type="button" class="close close-youtube" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>        
                        <!-- 16:9 aspect ratio -->
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe class="embed-responsive-item" src="" id="video"  allowscriptaccess="always" allow="autoplay"></iframe>
                        </div> 
                    </div> 
                </div>
            </div>
        </div>  

        <!-- Music Confirm Modal -->
        <div class="modal fade" tabindex="-1" id="musicConfirmModal">
            <div class="modal-dialog modal-dialog-youtube">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"><b>รายชื่อเพลงที่เลือก</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body modal-body-youtube">
                        <div id="divMusicSelectedInner" class="container p-xl-3">
                            <div class="text-center"><h5 class="text-black-25 p-xl-5">No Music Request</h5></div>
                        </div> 
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning"  data-dismiss="modal"><i class="fe fe-arrow-left-circle"></i>เลือกเพลงเพิ่มเติม</button>
                        <button type="button" class="btn btn-info" onclick="confirmListMusicCover()">ยืนยันเพลงที่เลือก</button>
                    </div>
                </div>
            </div>
        </div>

        <!-- Music Lyrics Modal -->
        <div class="modal" tabindex="-1" id="musicLyricsModal">
            <div class="modal-dialog modal-dialog-youtube">
                <div class="modal-content">
                    <!--                    <div class="modal-header">
                                            <h5 class="modal-title"><b>เพลง  :: </b><span id="musicLyricsTitle"></span></h5>
                    -->                        <button type="button" class="close close-youtube" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button><!--
                </div>-->
                    <div class="modal-body modal-body-youtube">
                        <div class="container"> <p id="musicLyrics" class="text-left font-weight-normal p-2"></p> </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <div id="divSaveLyricBtn"></div>
                    </div>
                </div>
            </div>
        </div>

        <style>
            .table td {
                padding:5px 5px;
                text-align:left;
            }
            .form-group {
                margin-bottom:0.75rem;
            }
            .form-label-sm[class*="col-"] {
                padding-top:6px;
            }
            .modal-dialog-youtube {
                max-width: 800px;
                margin: 30px auto;
            } 
            .modal-body-youtube {
                position:relative;
                padding:0px;
            }
            .close-youtube {
                position:absolute;
                right:-30px;
                top:0;
                z-index:999;
                font-size:2rem;
                font-weight: normal;
                color:#fff;
                opacity:1;
            }
            .checkbox-menu li label {
                display: block;
                padding: 3px 20px;
                clear: both;
                font-weight: normal;
                line-height: 1.42857143; 
                white-space: nowrap;
                margin:0;
                transition: background-color .4s ease;
            }
            .checkbox-menu li input {
                margin: 0px 5px;
                top: 2px;
                position: relative;
            }

            .checkbox-menu li.active label {
                background-color: #cbcbff;
                font-weight:bold;
            }

            .checkbox-menu li label:hover,
            .checkbox-menu li label:focus {
                background-color: #f5f5f5;
            }

            .checkbox-menu li.active label:hover,
            .checkbox-menu li.active label:focus {
                background-color: #b8b8ff;
            }

            /*            .badge {
                            background:#FFFFFF;
                            border:1px solid #F5F7F9;
                            border-radius:4px;
                            box-shadow:rgba(0, 0, 0, 0.1) 0 0 15px;
                            color:#74818D;
                            font-size:14px;
                            font-weight:700;
                            min-width:17px;
                            padding:3 8px;
                            text-align:center;
                        }*/
            .btn-sms {
                font-size:12px;
                padding:2px 5px;
            }

            .autocomplete {
                /*the container must be positioned relative:*/
                position: relative;
                display: inline-block;
            }
            .autocomplete-items {
                position: absolute; 
                border-bottom: none;
                border-top: none;
                z-index: 99;
                /*position the autocomplete items to be the same width as the container:*/
                top: 100%;
                left: 10%;
                right: 0;
                padding: 0rem 0.75rem;
            }
            .autocomplete-items div { 
                cursor: pointer; 
                width: 100%;
                background-color: #FFFFFF; 
                border-bottom: 1px solid #d4d4d4; 
                padding: 0rem 0.75rem;
            }
            .autocomplete-items div:hover {
                /*when hovering an item:*/
                background-color: #b4d7f9;  
            }  
            .autocomplete-active {
                /*when navigating through the items using the arrow keys:*/
                background-color: DodgerBlue !important; 
                color: #e9e9e9; 
            }
        </style>

    </tiles:put>
</tiles:insert>
