<%-- 
    Document   : requestCover
    Created on : Sep 28, 2021, 11:27:42 AM
    Author     : jiranuwat
--%>

<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.util.ThaiConvert"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>  
<!DOCTYPE html>

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
%>

<%//    
    String thumbnailPath = request.getContextPath() + "/assets/images/icons/avatar.png";
    String channelId = "";
    SessionUserInfo sessionUserInfo = null;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        channelId = sessionUserInfo.getYoutubeChannelInfo().getChannelId();
    } catch (Exception ex) {
    }
%>
<tiles:insert page="/assets/template/mainTemplate.jsp" flush="true">
    <tiles:put name="page.title" type="String" >Content Music Cover</tiles:put>
    <tiles:put name="page.body.script" type="String">   
        <script type="text/javascript">
//          ###################### DEFAULT PARAMETER  #######################  
            var TemplistMusicSelectedArr = [];
            var TemplistChannelArr = [];
            var TempDataList;
            const htmlLoading = '<div class="text-center  p-xl-5"><img src="<%=request.getContextPath()%>/assets/images/loading-dot/64x64.gif" alt=""></div>';
//          #################################################################      
            $(document).ready(function () {
                 $("#musicCartHeader").hide();
                searchOrder();
                selectOptionListArtist();
                var input = document.getElementById("iKeywordSearch");
                input.addEventListener("keyup", function (event) {
                    if (event.keyCode === 13) {
                        event.preventDefault();
                        searchOrder();
                    }
                });
            });

            function selectOptionListArtist() {
                var datas = ({
                    mode: 'LIST_COVER_SONG_STATUS'
//                    , isNotAll: 1
                });
                var id = "selectStatusOption";
                var url = "<%=request.getContextPath()%>/GenerateListSelectServlet";
                var selectValue = "";
                ajax_generate_option(id, url, datas, selectValue);
//                ajax_generate_option_callfnc(id, url, datas, selectValue, "setSelectSearch(\"" + id + "\")");
            }

            function searchOrder(pageIndex) {
                var iKeywordSearch = $("#iKeywordSearch").val();
                var channelId = $("#channelId").val();
                var statusId = $("#selectStatusOption").val();
                var datas = ({
                    mode: "LIST_REQUEST_MUSIC_COVER_LOG",
                    keywordSearch: encodeURIComponent(iKeywordSearch),
                    channelId: channelId,
                    statusId: statusId,
                    pageIndex: pageIndex,
                    limit: 6
                });
                $.ajax({
                    cache: false,
                    type: 'POST',
                    data: datas,
                    dataType: 'json',
                    url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                    beforeSend: function () {
                        $('#resultSearchOrderRequestData').html(htmlLoading);
                    }, success: function (data) {
                        var length = "";
                        try {
                            length = data.drList.length;
                        } catch (Error) {
                        }

                        if (data != '' && length > 0) {
                            manipulateDisplayOrder(data);
                        } else {
                            var row = '<div class="text-center"><h5 class="text-black-25 p-xl-5">No List Music</h5></div>';
                            $('#resultSearchOrderRequestData').html(row);
                        }
                    }, complete: function () {
                    }
                });
            }

            function manipulateDisplayOrder(data) {
                var row = "";
                var dataSet = data.drList;
                row += '<div class="table-responsive"><table class="table table-bordered table-striped" > ';
                if (dataSet.length > 0) {

                    row += '<thead class="thead-dark"> ';
                    row += '<tr style="height: 30px;" > ';
                    row += '<th class="text-center" style="width: 5%;">#</th> ';
                    row += '<th class="text-center" style="width: 25%;">Info.</th> ';
                    row += '<th class="text-center" style="width: 35%;">Music Cover</th> ';
//                    row += '<th class="text-center" style="width: 15%;">remark</th> ';
                    row += '<th class="text-center" style="width: 20%;">status</th> ';
//                    row += '<th class="text-center" style="width: 15%;">Date Time</th> ';
                    row += '<th class="text-center" style="width: 15%;" >Action</th> ';
                    row += '</tr> ';
                    row += '</thead> ';
                    row += '<tbody>';
                    var numberCount = data.fCurrentPageAmount;
                    var num = 1;
                    for (var i = 0; i < dataSet.length; i++) {
                        var name = dataSet[i].name;
                        var artistAliasName = dataSet[i].artistAliasName;
                        var artistName = dataSet[i].artistName;
                        var email = dataSet[i].email;
                        var tel = dataSet[i].tel;
                        var remark = dataSet[i].actionRemark || "";

                        var listChannelCoverArr = dataSet[i].listChannelCover;
                        var listMusicCoverArr = dataSet[i].listMusicCover;

                        var publishTime = dataSet[i].publishTime || "-";
                        var createTime = dataSet[i].createTime || "-";
                        var actionTime = dataSet[i].actionTime || "-";
                        var updateTime = dataSet[i].updateTime || "-";

                        var reqDocUrl = dataSet[i].reqDocUrl;
                        var ackDocUrl = dataSet[i].ackDocUrl;
                        var requestId = dataSet[i].requestId;
                        var statusId = dataSet[i].statusId;

                        var isUserAction = false;
                        var isUserDownload = false;
                        var statusName = getStatusName(statusId);
                        var tagStatus = "";
                        if (statusId == 0) {
                            isUserAction = true;
                            tagStatus = '<div class="font-weight-bold"><span class="status-icon bg-secondary"></span> ' + statusName + '</divL>';
                        } else if (statusId == 2) {
                            isUserDownload = true;
                            tagStatus = '<div class="font-weight-bold"><span class="status-icon bg-warning"></span> ' + statusName + '</divL>';
                        } else if (statusId == -999 || statusId == -998) {
                            tagStatus = '<div class="font-weight-bold text-danger"><span class="status-icon bg-danger"></span> ' + statusName + '</divL>';
                        } else if (statusId == 1) {
                            isUserDownload = true;
                            tagStatus = '<div class="font-weight-bold text-success"><span class="status-icon bg-success"></span> ' + statusName + '</divL>';
                        }


                        var tagMusic = "";
                        if (listMusicCoverArr != null && listMusicCoverArr.length > 0) {
                            for (var mCoverIndex = 0; mCoverIndex < listMusicCoverArr.length; mCoverIndex++) {
                                var musicInfo = listMusicCoverArr[mCoverIndex].musicInfo;
                                var priority = listMusicCoverArr[mCoverIndex].priority;

                                if (musicInfo != null) {
                                    var trackNameTh = musicInfo.trackNameTh;
                                    var artistNameTh = musicInfo.artistNameTh;
                                    var trackName = trackNameTh;
                                    if (artistNameTh != null && artistNameTh != "") {
                                        trackName = priority + ". " + trackName + " - " + artistNameTh + " ";
                                    }
                                    tagMusic += '<div class="text-overflow ">' + trackName + '</div>';
                                }
                            }
                        }

                        var channelName = "";
                        if (listChannelCoverArr != null && listChannelCoverArr.length > 0) {
                            for (var cCoverIndex = 0; cCoverIndex < listChannelCoverArr.length; cCoverIndex++) {
                                channelName = listChannelCoverArr[cCoverIndex].channelName;
                                if (cCoverIndex == 0) {
                                    channelName = channelName;
                                } else {
                                    channelName = "," + channelName;
                                }
                            }
                        }


                        var tagAction = '';
                        tagAction += '<div class="btn-group-vertical btn-sm" role="group" aria-label="Basic example">';
                        if (isUserAction) {
                            tagAction += '<button class="btn btn-sm btn-warning" onclick="editRequestMusicCover(' + requestId + ')"  ><i class="fe fe-edit"></i><span class=""> แก้ไข </span></button>';
                            tagAction += '<button class="btn btn-sm btn-danger" onclick="cancelRequestMusicCover(' + requestId + ')"><i class="fe fe-x"></i><span class=""> ยกเลิก</span></button>';
                        }  
                         
                                if (reqDocUrl != null && reqDocUrl != "") {
                                    tagAction += '<a href="' + reqDocUrl + '" class="btn btn-sm btn-secondary" target="_blank" download="request_form""  ><i class="fe fe-download"></i><span class=""> เอกสารคำร้อง</span></a>';
                                }
                                if (ackDocUrl != null && ackDocUrl != "") {
                                    tagAction += '<a href="' + ackDocUrl + '" class="btn btn-sm btn-info" target="_blank" download="approve_form"><i class="fe fe-download"></i><span class=""> เอกสารอนุมัติ </span></a>';
                                }
                          
                         
                        tagAction += '</div>';
                        row += ' <tr> ';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + num + '</td>';
                        row += '    <td style=""> ';
                        row += '    <div class="text-overflow"><b>ID : </b><small>' + requestId + '</small></div>  ';
                        row += '    <div class="text-overflow"><b>ชื่อ : </b>' + name + '</div>';
                        row += '    <div class="text-overflow"><b>ศิลปิน Cover : </b>' + artistName + ' (' + artistAliasName + ')</div>  ';
                        row += '    <div class="text-overflow"><b>ช่อง Youtube : </b>' + channelName + '</div>  ';
                        row += ' </td>';
                        row += ' <td class="" style="">' + tagMusic + '</td>';
//                        row += ' <td  ><pre>' + remark + '<pre></td>';
                        row += ' <td class=" " style="vertical-align: middle;">' + tagStatus + '</td>';
//                        row += ' <td style="vertical-align: middle;">';
//                        row += '    <div class="text-overflow "><b>Request : </b>' + createTime + '</div>  ';
//                        row += '    <div class="text-overflow "><b>Approve : </b>' + actionTime + '</div>  ';
//                        row += ' </td>';
                        row += ' <td class="text-center" style="vertical-align: middle;">' + tagAction + ' </td>';
                        row += ' </tr> ';
                        numberCount++;
                        num++;
                    }
                    row += '</tbody>';
                }

                row += '<tfoot>';
                var fCurrentPageAmount = data.fCurrentPageAmount;
                var tCurrentPageAmount = data.tCurrentPageAmount;
                var totalAmount = data.totalAmount;
                row += '<tr class="dataRow">';
                row += '<td colspan="10" class="text-right">';
                if (fCurrentPageAmount > 0) {
                    row += '<span id="displayItem"> Displaying ' + addCommas(fCurrentPageAmount) + ' to ' + addCommas(tCurrentPageAmount) + ' of ' + addCommas(totalAmount) + ' items </span>';
                }
                row += '<div id="paggingFormSearchOrderRequest"></div>';
                row += '</td>';
                row += '</tr>';
                row += '</tfoot>';
                row += '</table>';
                 row += '</div>';
                $('#resultSearchOrderRequestData').html(row);
                initContentYoutube();
                manipulatePageSearch(data, "paggingFormSearchOrderRequest", "searchOrder");
            }

            function getStatusName(statusId) {
                var statusDetail = {
                    '-999': 'ไม่อนุมัติ',
                    '-998': 'ยกเลิกคำขออนุมัติ',
                    '0': 'ยื่นเรื่อง',
                    '1': 'คำขออนุมัติ',
                    '2': 'รอการอนุมัติ'
                };
                if (statusDetail.hasOwnProperty(statusId)) {
                    return statusDetail[statusId];
                } else {
                    return statusId;
                }
            }

            function createRequestMusicCover() {
                window.location = "./add.jsp";
            }

            function editRequestMusicCover(requestId) {
                window.location = "./edit.jsp?reqId=" + requestId;
            }

            function cancelRequestMusicCover(requestId) {
                swal({
                    title: "",
                    text: "คุณต้องการยกเลิกคำขอ Cover เพลง?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "ยืนยันยกเลิก",
                    cancelButtonText: "ไม่ขอบคุณ",
                    closeOnConfirm: false
                }, function () {
                    var statusId = -998;
                    var datas = ({
                        mode: "UPDATE_MUSIC_COVER_STATUS",
                        statusId: statusId,
                        requestId: requestId
                    });
                    $.ajax({
                        cache: false,
                        type: 'POST',
                        data: datas,
                        dataType: 'json',
                        url: "<%=request.getContextPath()%>/ManageContentMusicCoverServlet",
                        beforeSend: function () {
                            $('#resultSearchOrderRequestData').html(htmlLoading);
                        }, success: function (data) {
                            if (data.status) {
                                swal("สำเร็จ!", data.message, "success");
                                searchOrder();
                            } else {
                                swal("!", data.message, "error");
                            }
                        }, complete: function () {
                        }
                    });
                });
            }

        </script> 
    </tiles:put>

    <tiles:put name="page.body.head" type="String"></tiles:put>
    <tiles:put name="page.body.data" type="String"> 
        <input type="hidden" name="channelId" id="channelId" value="<%=channelId%>"/>   

        <div class="row">
            <div class="col-lg-12">
                <div class="box"> 
                    <div class="box-header"> 
                        <h5>
                            <i class="fe fa-1x fe-clipboard text-warning"></i> 
                            <span class="font-weight-bold">Order Request</span>
                        </h5>
<!--                        <div class="box-actions"> 
                            <div class="text-right"> 
                                <button type="button" onclick="createRequestMusicCover()" class="btn btn-success"><i class="fe fa-1x fe-plus"></i> New Request</button> 
                            </div>
                        </div>-->
                    </div>
                    <div class="box-body"> 
                        <div class="row">  
<!--                            <div class="col-3"></div>
                            <div class="col-4"></div>-->
                            <div class="col-lg-3 col-md-3 col-sm-12"> 
                                <select class="form-control" id="selectStatusOption" name="selectStatusOption" onchange="searchOrder()">
                                    <option value="">+++ all status +++</option>
                                </select>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-12">
                                <div class="form-group">  
                                    <div class="input-group"> 
                                        <input id="iKeywordSearch" type="text" class="form-control" placeholder="คำค้น"/> 
                                        <div class="input-group-icon" onclick="searchOrder()" style="cursor:pointer;">
                                            <i class="fas fa-2x fa-search"></i>
                                        </div> 
                                    </div>  
                                </div> 
                            </div>
                        </div>


                        <div id="resultSearchOrderRequestData" class="text-center"> 
                            <h5 class="text-black-25 p-xl-5">No Order Request</h5>
                        </div> 

                    </div> 
                </div>
            </div>
        </div> 


        <style>
            .form-group {
                margin-bottom:0.75rem;
            }
            .form-label-sm[class*="col-"] {
                padding-top:6px;
            } 
        </style>

    </tiles:put>
</tiles:insert>
