var WAITING_MSG = '<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br/><span>Please wait...</span></div>';
var waitingMsg = '<div class="text-center"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i><br/><span>Please wait...</span></div>';
var CONTEXT_PATH = location.pathname.substring(0, location.pathname.indexOf("/", 1));

var isAdd = 0;
var isEdit = 0;
var isDelete = 0;
var menuId = 0;

$.fn.enterKey = function (fnc) {
    return this.each(function () {
        $(this).keypress(function (ev) {
//                        var keycode = (ev.keyCode ? ev.keyCode : ev.which);
//                        if (keycode == '13') {
//                            fnc.call(this, ev);
//                        }
            if (ev.key === "Enter") {
                fnc.call(this, ev);
            }
        })
    })
}

$(function () {
    performAuthorizeAccess();
    $('.inputNumber').keyup(function () {
        this.value = this.value.replace(/[^0-9\.]/g, '');
    });

    /*
     $.extend(true, $.fn.dataTable.defaults, {
     //                     "paging":   false,
     //        "ordering": false,
     //        "info":     false
     "searching": false,
     "fixedHeader": true,
     //                    "ordering": false,
     "columnDefs": [{
     "targets": 'no-sort',
     "orderable": false,
     }]
     });
     */
//                showMessageBox("error", "Success", "Success Message")
});

function performParamChangePage(paramAppend) {
//                var menuId = $.getParamFromToken("mid");
//                var isAdd = $.parseTxtBool2Bit($.getParamFromToken("iA"));
//                var isEdit = $.parseTxtBool2Bit($.getParamFromToken("iE"));
//                var isDelete = $.parseTxtBool2Bit($.getParamFromToken("iD"));

    var paramJson = $.getQueryStringToJSON();
    var menuId = paramJson.mid;
    var isAdd = 1;
    var isEdit = 1;
    var isDelete = 1;

    var tmpResult = "mid=" + menuId + "&iA=" + isAdd + "&iE=" + isEdit + "&iD=" + isDelete;
    if (!$.isNullText(paramAppend)) {
        tmpResult += "&" + paramAppend;
    }
    return "?" + tmpResult;//$.performParam2Token(tmpResult);
}


function performAuthorizeAccess() {

    var paramJson = $.getQueryStringToJSON();
    var menuId = paramJson.mid;

    var isAdd = $.parseTxtBool2Bit(paramJson.iA);
    var isEdit = $.parseTxtBool2Bit(paramJson.iE);
    var isDelete = $.parseTxtBool2Bit(paramJson.iD);

    console.log("[menuId][" + menuId + "], [isAdd][" + isAdd + "], [isEdit][" + isEdit + "], [isDelete][" + isDelete + "]");
    if (isAdd === 1) {
        $(".btnAdd").show();
    } else {
        $(".btnAdd").hide();
    }
    if (isEdit === 1) {
        $(".btnEdit").show();
    } else {
        $(".btnEdit").hide();
    }
    if (isDelete === 1) {
        $(".btnDelete").show();
    } else {
        $(".btnDelete").hide();
    }
}

function checkNullForm(idCheck, textWarning) {
    var tmpResult = $("#" + idCheck).val();
    if ($.isNullText(tmpResult)) {
        showMessageBox("warning", "", "" + textWarning);
        return false;
    }
    return true;
}

function showMessageBox(type, title, message, callfnc) {
//                swal("Hey, Good job !!", "You clicked the button !!", "success");
//                swal("Oops...", "Something went wrong !!", "error");
//type = success, error, warning, info
    swal({
        dangerMode: true,
        title: "" + title,
        text: "" + message,
        type: "" + type,
        closeOnClickOutside: true,
        closeOnEsc: true
    },
            function () {
                if (!$.isNullText(callfnc)) {
                    jQuery.globalEval(callfnc);
                }
            });
}

function showMessageConfirmBox(title, message, confirmText, callfnc) {
    swal({
//                    title: "Are you sure to delete ?",
        title: "" + title,
//                    text: "You will not be able to recover this imaginary file !!",
        text: "" + message,
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
//                    confirmButtonText: "Yes, delete it !!",
        confirmButtonText: "" + confirmText,
        closeOnConfirm: true
    },
            function () {
//                    swal("Deleted !!", "Hey, your imaginary file has been deleted !!", "success");
                jQuery.globalEval(callfnc);
            });
}


function showMessageBar(type, title, message) {
    if (type === "success") {
        toastr.success('' + message, '' + title, {
            "positionClass": "toast-bottom-full-width",
//                            "positionClass": "toast-top-center",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "error") {
        toastr.error('' + message, '' + title, {
            "positionClass": "toast-bottom-full-width",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "warning") {
        toastr.warning('' + message, '' + title, {
            "positionClass": "toast-bottom-full-width",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "info") {
        toastr.info('' + message, '' + title, {
            "positionClass": "toast-bottom-full-width",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    }
}

function showMessageMiniBar(type, title, message) {
    if (type === "success") {
        toastr.success('' + message, '' + title, {
            "positionClass": "toast-bottom-right",
//                            "positionClass": "toast-top-center",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "error") {
        toastr.error('' + message, '' + title, {
            "positionClass": "toast-bottom-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "warning") {
        toastr.warning('' + message, '' + title, {
            "positionClass": "toast-bottom-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    } else if (type === "info") {
        toastr.info('' + message, '' + title, {
            "positionClass": "toast-bottom-right",
            timeOut: 5000,
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
            "tapToDismiss": false
        });
    }
}

function goBack() {
    window.history.go(-1);
}

function cancel() {
    var queryStr = performParamChangePage();
    window.location = "./index.jsp" + queryStr;
}

function cancel2Root() {
    var queryStr = performParamChangePage();
    window.location = "../index.jsp" + queryStr;
}

function returnHome() {
    var queryStr = performParamChangePage();
    window.location = "./index.jsp" + queryStr;
}

//            var nextPage = '<img src="<%=request.getContextPath()%>/css/images/msg/next.gif" />';
var nextPage = '<button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-step-forward"></i></button>';
//            var previosPage = '<img src="<%=request.getContextPath()%>/css/images/msg/fw.gif" />';
var previosPage = '<button type="button" class="btn btn-secondary btn-sm"><i class="fa fa-step-backward"></i></button>';



function manipulatePageSearch(data, idResult, isFunction) {

    var PAGE_NUMBER_LIMIT = 5;
    var resultTmp = '';
    var pageIndex = data.pageIndex;
    var totalPage = data.totalPage;
    var totalAmount = data.totalAmount;
    var fCurrentPageAmount = data.fCurrentPageAmount;
    var tCurrentPageAmount = data.tCurrentPageAmount;


    var moreURL = '' + isFunction + '(#pageIndex#)';

    if (totalPage > 1) {
        totalPage += 1;
        var firstPageBox = 0;
        var totalPageBox = 0;
        if (pageIndex > PAGE_NUMBER_LIMIT) {
            if (pageIndex % PAGE_NUMBER_LIMIT == 0) {
                firstPageBox = (Math.floor((pageIndex - 1) / PAGE_NUMBER_LIMIT) * PAGE_NUMBER_LIMIT) + 1;
            } else {
                firstPageBox = (Math.floor(pageIndex / PAGE_NUMBER_LIMIT) * PAGE_NUMBER_LIMIT) + 1;
            }
        } else {
            firstPageBox = 1;
        }

        totalPageBox = firstPageBox + (PAGE_NUMBER_LIMIT - 1);
        if (totalPageBox >= totalPage) {
            totalPageBox = totalPage;
        }
        if (pageIndex > 1) {
            tmp = moreURL.replace("#pageIndex#", pageIndex - 1);
            resultTmp += '<span onclick="' + tmp + '" class="anotherPage">' + previosPage + '</span>&nbsp;';
        } else {
            resultTmp += // previosPage +
                    '&nbsp;';
        }

        resultTmp += '<select class="form-select form-select-sm" style="width: 60px;" id="changePage" name="changePage" onchange="' + isFunction + '(this.value)">';

        for (var i = 1; i < totalPage; i++) {
            if (i == pageIndex) {
                resultTmp += "<option selected value=\"" + i + "\">" + i + "</option>";
            } else {
                resultTmp += "<option  value=\"" + i + "\">" + i + "</option>";
            }
        }
        resultTmp += '</select>';

        if (pageIndex < totalPage - 1) {
            var tmp = moreURL.replace("#pageIndex#", pageIndex + 1);
            resultTmp += '&nbsp;<span onclick="' + tmp + '" class="anotherPage" >' + nextPage + '</span>&nbsp;';
        } else {
            resultTmp += '&nbsp;'; //+ nextPage;
        }
    }

//                alert("pageIndex: " + pageIndex
//                        + "\ntotalPage: " + totalPage
//                        + "\ntotalAmount: " + totalAmount
//                        + "\nfCurrentPageAmount: " + fCurrentPageAmount
//                        + "\ntCurrentPageAmount: " + tCurrentPageAmount
//                        );



    $('#' + idResult).html(resultTmp);
}

function gotoUrl(url) {
    window.location = '' + url;
}


function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

var i = "0";
var temptitle = ""
function titler(message) {
    if (!document.all && !document.getElementById)
        return
    document.title = temptitle + message.charAt(i)
    temptitle = temptitle + message.charAt(i)
    i++
    if (i == message.length) {
        i = "0"
        temptitle = ""
        setTimeout("titler('" + message + "')", 3000)
    } else {
        setTimeout("titler('" + message + "')", 200)
    }
}


function countchr(obj, x) {
    var form = document.forms[0];
    x.value = obj.value.length;
}
function confirmLogout() {
    input_box = confirm("Are you sure logout?");
    if (input_box == true) {
        window.location = "<%=request.getContextPath()%>/authenticationAction.do?mode=logout";
    }
}


function performCalendar(idCal, dateFormat) {
    if ($.isNullText(dateFormat)) {
        dateFormat = "dd/mm/yyyy hh:ii";
    }

    if (dateFormat === "dd/mm/yyyy") {
        $("#" + idCal).datetimepicker({
            language: 'th',
            format: dateFormat,
////                    format: "dd/mm/yyyy",
            autoclose: true,
            fontAwesome: true,
            pickTime: false,
////                    ,
            altFieldTimeOnly: false,
////                    startDate: "2013-02-14 10:00",
            weekStart: 1,
            todayBtn: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            minView: 3,
            showMeridian: 1
        });
    } else if (dateFormat === "mm/yyyy") {
        $("#" + idCal).datetimepicker({
            language: 'th',
            format: 'mm/yyyy',
            autoclose: true,
            fontAwesome: true,
            pickTime: false,
            altFieldTimeOnly: false,
            startView: 3,
            forceParse: 0,
            minView: 3,
            showMeridian: 1
        });
    } else {

        $("#" + idCal).datetimepicker({
            language: 'th',
            format: dateFormat,
////                    format: "dd/mm/yyyy",
            autoclose: true,
            fontAwesome: true,
            pickTime: true,
////                    ,
            altFieldTimeOnly: false,
////                    startDate: "2013-02-14 10:00",
            weekStart: 1,
            todayBtn: 1,
            todayHighlight: 1,
            startView: 2,
            forceParse: 0,
            minView: 0,
            showMeridian: 1
        });
    }
}

$.wait = function (ms) {
    var defer = $.Deferred();
    setTimeout(function () {
        defer.resolve();
    }, ms);
    return defer;
};

function setSelectSearch(id) {

    $('#' + id).selectpicker("destroy");
    $('#' + id).selectpicker({
        liveSearch: true,
        actionsBox: true,
        showTick: true,
        maxOptions: 1
    });
}
function manipulateDisplayText(data, displayWhenNull) {
    var result = data;

    if (displayWhenNull === undefined || displayWhenNull === null || displayWhenNull === "") {
        displayWhenNull = "-";
    }

    if (result === undefined || result === null || result === "") {
        result = "-";
    }

    return result;
}
