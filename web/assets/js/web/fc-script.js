/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    generate_option = (function (id, url, data, selectValue) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                var i = 0, o = null;
                $(id).empty();
                var options = "";
                for (i = 0; i < j.length; i++) {
                    if (selectValue != undefined) {
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }
                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    }
                }
                $(id).html(options);
            },
            error: function (xhr, desc, er) {
            }
        });
    });

    generate_option_svalue_isAll = (function (id, url, data, selectValue) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                var i = 0, o = null;
                $(id).empty();
                var options = "<option  value=\"-1\"> All </option>";
                for (i = 0; i < j.length; i++) {
                    if (selectValue != undefined) {
                        if (j[i].sValue == selectValue) {
                            options += "<option selected value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }
                        if (isActive) {
                            options += "<option selected value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        }
                    }
                }
                $(id).html(options);
            },
            error: function (xhr, desc, er) {
            }
        });
    });

    generate_option_svalue = (function (id, url, data, selectValue) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                var i = 0, o = null;
                $(id).empty();
                var options = "";
                for (i = 0; i < j.length; i++) {
                    if (selectValue != undefined) {
                        if (j[i].sValue == selectValue) {
                            options += "<option selected value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }
                        if (isActive) {
                            options += "<option selected value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].sValue + "\">" + j[i].valueName + "</option>";
                        }
                    }
                }
                $(id).html(options);
            },
            error: function (xhr, desc, er) {
            }
        });
    });

    generate_option_list = (function (idList, url, data, selectValue) {
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                for (var indexId = 0; indexId < idList.length; indexId++) {
                    var id = "#" + idList[indexId];
                    var i = 0, o = null;
                    $(id).empty();
                    var options = "";
                    for (i = 0; i < j.length; i++) {
                        if (selectValue != undefined) {
                            if (j[i].id == selectValue) {
                                options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            } else {
                                options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            }
                        } else {
                            var isActive = false;
                            try {
                                isActive = j[i].is_active;
                            } catch (Error) {
                            }

                            if (isActive) {
                                options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            } else {
                                options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            }
                        }
                    }
                    $(id).html(options);
                }
            },
            error: function (xhr, desc, er) {
            }
        });
    });

    generate_option_callfnc = (function (id, url, data, selectValue, callfnc) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                var options = [], i = 0, o = null;
                $(id).empty();
                var options = "";
                for (i = 0; i < j.length; i++) {
                    if (selectValue != undefined && selectValue != "") {
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }

                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    }
                }
                $(id).html(options);
                jQuery.globalEval(callfnc)
            },
            error: function (xhr, desc, er) {
            }
        });
    });

    callfnc_data = (function (id, url, data) {
        var x = null;
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {
                x = j;
            }, error: function (xhr, desc, er) {}
        });
        return x;
    });
})(jQuery);

