/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.getQueryString = (function () {
        var params = (window.location.search.split('?')[1] || '').split('&').join("&");
        return params;
    });

    $.getQueryStringToJSON = (function () {
        var qStr = window.location.search.replace(/(.*?\?)/, '');
        var qArr = qStr.split("&");
        stack = {};
        for (var i in qArr) {
            var tmpTxt = qArr[i];
            try {
//                alert(tmpTxt);
                var a = tmpTxt.split("=");
                var name = a[0],
                        value = isNaN(a[1]) ? a[1] : parseFloat(a[1]);
                if (name.match(/(.*?)\[(.*?)]/)) {
                    name = RegExp.$1;
                    name2 = RegExp.$2;
                    //alert(RegExp.$2)
                    if (name2) {
                        if (!(name in stack)) {
                            stack[name] = {};
                        }
                        stack[name][name2] = value;
                    } else {
                        if (!(name in stack)) {
                            stack[name] = [];
                        }
                        stack[name].push(value);
                    }
                } else {
                    stack[name] = value;
                }
            }
            catch (Error) {
//                alert(Error.responseTxt)
            }
        }
        return stack;
    });

    $.checkTextNumber$ = (function (value) {
        var xpr = /^[0-9]+$/;
        return xpr.test(value);
    });

    $.checkTextNoScript = (function (value) {
        var xpr = /[!#$&*=\';/|\"<>]/;
        return !xpr.test(value);
    });


    $.checkIdCard = (function (value) {
        if (value.length != 13) {
            return false;
        }
        var sum = 0;
        for (var i = 0; i < 12; i++)
        {
            sum += value.charAt(i) * (13 - i);
        }
        return value.charAt(12) == ((11 - (sum % 11)) % 10);
    });

    $.checkTextId = (function (value) {
        var xpr = /[!#$&*=\-\';/|\"<>]/;
        return !xpr.test(value);
    });



    $.isValidEmail = (function (value) {
        var filter = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;

        if (filter.test(value)) {
            return true;
        } else {
            return false;
        }
    });

    $.removeSemiColon = (function () {
        var value = $(this).val();
        if (value.indexOf(';') != -1) {
            $(this).val(value.replace(/;/, ''));
        }
    });

    $.isNullText = (function (val) {
        if (val === undefined||val === "undefined" || val === null || val === "" || val === "null" || val === "NULL") {
            return true;
        }
        return false;
    });



$.parseTxtBool2Bit = (function (val) {
var res = 0;
if (val !== undefined && val !== null && val !== "" && val !== "null" && val !== "NULL" && val !== "FALSE" && val !== "false" && val !== "0" && val !== 0) {
res = 1;
}
return res;
});
})(jQuery);
//var param= $.QueryString();
//alert(param)





