/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.isInteger = (function (s)
    {
        var i;
        for (i = 0; i < s.length; i++)
        {
            // Check that current character is number.
            var c = s.charAt(i);
            if (((c < "0") || (c > "9")))
                return false;
        }

        // All characters are numbers.
        return true;
    });

    $.onlyInteger = (function (e)
    {
        var keynum
        var keychar
        var numcheck

        if (window.event) // IE
        {
            keynum = e.keyCode
        }
        else if (e.which) // Netscape/Firefox/Opera
        {
            keynum = e.which
        }
        keychar = String.fromCharCode(keynum)
        numcheck = /[\d\b\.]/
        return numcheck.test(keychar)
    });

    $.twoDigitsNumber = (function (e)
    {
        e = parseInt(e, 10);
        if (e < 10) {
            return "0" + e;
        }
        return e;
    });



    $.addCommas = (function (nStr)
    {
        nStr += '';
        x = nStr.split('.');
        x1 = x[0];
        x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
        }
        return x1 + x2;
    });

    addCommas = (function (nStr)
    {
        return $.addCommas(nStr);
    });


})(jQuery);

