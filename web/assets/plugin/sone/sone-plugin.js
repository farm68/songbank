/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {

    ajax_generate_option = (function (id, url, data, selectValue) {

        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {

                    //                    if(selectValue!=undefined && selectValue!=""){
                    if (selectValue != undefined) {
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }

                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }

                    }
                }
                $(id).html(options);
            },
            error: function (xhr, desc, er) {
//                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });



    ajax_generate_option_list = (function (idList, url, data, selectValue) {


        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                for (var indexId = 0; indexId < idList.length; indexId++) {

                    var id = "#" + idList[indexId];

                    var i = 0, o = null;
                    $(id).empty();
                    var options = "";

                    for (i = 0; i < j.length; i++) {

                        //                    if(selectValue!=undefined && selectValue!=""){
                        if (selectValue != undefined) {
                            if (j[i].id == selectValue) {
                                options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            } else {
                                options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            }
                        } else {
                            var isActive = false;
                            try {
                                isActive = j[i].is_active;
                            } catch (Error) {
                            }

                            if (isActive) {
                                options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            } else {
                                options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                            }

                        }
                    }
                    $(id).html(options);
                }
            },
            error: function (xhr, desc, er) {
//                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });



    ajax_generate_option_callfnc = (function (id, url, data, selectValue, callfnc) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var options = [], i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {
                    if (selectValue != undefined && selectValue != "") {
                        //                    if(selectValue!=undefined ){
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }

                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + j[i].text + "</option>";
                        }

                    }

                }
                $(id).html(options);
                jQuery.globalEval(callfnc)
            },
            error: function (xhr, desc, er) {
//                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });

    ajax_generate_checkbox = (function (id, name, url, data, numrow, listSelectValue, fncCall) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();

        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var options = [], i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {
                    var tmpName = name + j[i].id;
//                    if (bsVersion != null && bsVersion != undefined && bsVersion == "4") { 
                    options += '<label class="form-checkbox-custom">';
                    options += '<input type="checkbox" class="form-check-input" id="' + tmpName + '" name="' + name + '" value="' + j[i].id + '"><span class="form-label text-sm">' + j[i].text + '</span>';
                    options += '</label>&nbsp;&nbsp;';

//                        options += '<div class="form-check-inline">';
//                        options += '<label class="form-check-label">';
//                        options += '<input type="checkbox" class="form-check-input" id="' + tmpName + '" name="' + name + '" value="' + j[i].id + '">' + j[i].text;
//                        options += '</label>';
//                        options += '</div>';

//                    } else {
//
//
//                        options += '<div class="form-check form-check-inline">';
//                        options += '<input class="form-check-input" type="checkbox" id="' + tmpName + '" name="' + name + '" value="' + j[i].id + '">';
//                        options += '&nbsp;&nbsp;<label class="form-check-label" for="' + tmpName + '">' + j[i].text + '</label>';
//                        options += '</div>';
//                    }
                    if ((i + 1) % numrow == 0) {
//                        options += "<div>&nbsp;</div>"
                    }
                }
                $(id).html(options);
                var selectValue = []
                if (listSelectValue != undefined && listSelectValue != null && listSelectValue != "") {
                    selectValue = listSelectValue.split(",");
                }
                for (var k = 0; k < selectValue.length; k++) {
                    var tmpName = name + selectValue[k].trim();
                    $("#" + tmpName).prop('checked', true);
                }

                if (fncCall != undefined && fncCall != null && fncCall != "") {
                    $('input[name="' + name + '"]').click(function () {
                        jQuery.globalEval(fncCall)
                    });
                }

//                $('input[name="' + name + '"]').prop('disabled', true);

//                jQuery.globalEval(callfnc)
            },
            error: function (xhr, desc, er) {
//                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });

    handleChoiceCheck = (function (idAll, name) {
        if ($("#" + idAll).is(":checked")) {
//            $("." + tmpClass).attr('checked', true);
//            $('.' + tmpClass + ':checkbox').attr('disabled', 'disabled');
//            
            $("[name=" + name + "]").attr('checked', true);
            $("[name=" + name + "]:checkbox").attr('disabled', 'disabled');
            $("#" + idAll).removeAttr('disabled');
        } else {
//            $("." + tmpClass).attr('checked', false);
//            $('.' + tmpClass + ':checkbox').removeAttr('disabled');

            $("[name=" + name + "]").attr('checked', false);
            $("[name=" + name + "]:checkbox").removeAttr('disabled');
        }
    });

    ajax_generate_radiobox = (function (id, name, url, data, numrow, listSelectValue) {
        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var options = [], i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {

                    var tmpName = name + j[i].id;
                    options += '<div class="form-check form-check-inline">';
                    options += '<input class="form-check-input" type="radio" id="' + tmpName + '" name="' + name + '" value="' + j[i].id + '">';
                    options += '<label class="form-check-label" for="' + tmpName + '">' + j[i].text + '</label>';
                    options += '</div>';

                    if ((i + 1) % numrow == 0) {
                        options += "<div>&nbsp;</div>"
                    }

                }
                $(id).html(options);
//                jQuery.globalEval(callfnc)

                var selectValue = []

                if (listSelectValue != undefined && listSelectValue != null && listSelectValue != "") {
                    selectValue = listSelectValue.split(",");
                }
                for (var k = 0; k < selectValue.length; k++) {
                    var tmpName = name + selectValue[k].trim();
                    $("#" + tmpName).prop('checked', true);
                }
            },
            error: function (xhr, desc, er) {
//                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });

    ajax_check_box_by_reteive_data = (function (name, url, data) {
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            type: 'POST',
            data: data,
            dataType: 'json',
            url: url,
            beforeSend: function () {
            }, success: function (data) {
                var length = "";
                try {
                    length = data.drList.length;
                } catch (Error) {
                }

                if (data != '' && length > 0) {
                    var dataSet = data.drList;

                    if (dataSet.length > 0) {
                        for (var i = 0; i < dataSet.length; i++) {
                            var listSelectValue = dataSet[i].list_msg_id;


//                            alert(name)
                            $('input[name="' + name + '"]:checked').each(function () {
//                                $(this).removeAttr('checked');
                                $(this).prop('checked', false);
                            });

//                            alert(listSelectValue);

                            var selectValue = [];

                            if (listSelectValue != undefined && listSelectValue != null && listSelectValue != "") {
                                selectValue = listSelectValue.split(",");
                            }
                            for (var k = 0; k < selectValue.length; k++) {
                                var tmpName = name + selectValue[k].trim();
//                                $("#" + tmpName).attr('checked', true);
//                                alert(selectValue[k]);
                                $("#" + tmpName).prop('checked', true);

                            }

//                            $('input[name="' + name + '"]').prop('readOnly', true);
//                            $('input[name="' + name + '"]').prop('disabled', true);


                        }
                    }
                }
            }, complete: function () {
            }
        });
    });


    ajax_generate_option_show_val = (function (id, url, data, selectValue) {

        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {

                    //                    if(selectValue!=undefined && selectValue!=""){
                    if (selectValue != undefined) {
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }

                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        }

                    }
                }
                $(id).html(options);
            },
            error: function (xhr, desc, er) {
                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });

    ajax_generate_option_show_val_callfnc = (function (id, url, data, selectValue, callfnc) {

        id = "#" + id;
        url += "?xxxx=" + new Date().getTime();
        $.ajax({
            cache: false,
            url: url,
            data: data,
            dataType: 'json',
            success: function (j) {

                var i = 0, o = null;
                $(id).empty();
                var options = "";

                for (i = 0; i < j.length; i++) {

                    //                    if(selectValue!=undefined && selectValue!=""){
                    if (selectValue != undefined) {
                        if (j[i].id == selectValue) {
                            options += "<option selected value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        }
                    } else {
                        var isActive = false;
                        try {
                            isActive = j[i].is_active;
                        } catch (Error) {
                        }

                        if (isActive) {
                            options += "<option selected value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        } else {
                            options += "<option  value=\"" + j[i].id + "\">" + "[" + j[i].id + "] " + j[i].text + "</option>";
                        }

                    }
                }
                $(id).html(options);
                jQuery.globalEval(callfnc)
            },
            error: function (xhr, desc, er) {
                alert(xhr.responseText);
                // add whatever debug you want here.
                //alert("an error occurred");
            }
        });
    });
})(jQuery);

