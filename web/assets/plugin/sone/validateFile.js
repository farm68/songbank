/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function($) {

    $.isImageFile=(function(fname) {
        fname = fname.replace(/^\s|\s$/g, ''); //trims string
        if(fname.match(/([^\/\\]+)\.(jpg|gif)$/i)) {
            return true;
        }
        return false;
    });

    $.isSoundFile=(function(fname) {
        fname = fname.replace(/^\s|\s$/g, ''); //trims string
        if(fname.match(/([^\/\\]+)\.(amr|mp3)$/i)) {
            return true;
        }
        return false;
    });

    $.isVDOFile=(function(fname) {
        fname = fname.replace(/^\s|\s$/g, ''); //trims string
        if(fname.match(/([^\/\\]+)\.(3gp|avi)$/i)) {
            return true;
        }
        return false;
    });

})(jQuery);