function haveThai(text) {
    for(var i = 0; i < text.length; i++) {
        if(isThaiCharacter(text.charCodeAt(i))) return true;
    }
    return false;
}
            
            
function addCommas(nStr)
{
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}
            
            
function isThaiCharacter(charCode){
    if(charCode != 10 && charCode != 13 && charCode != 27 && !(charCode >= 32 && charCode <= 95)
        && !(charCode >= 97 && charCode <= 126) && charCode != 161 && charCode != 163 && charCode != 164
        && charCode != 165 && charCode != 167 && charCode != 191 && charCode != 196 && charCode != 197 && charCode != 198
        && charCode != 199 && charCode != 201 && charCode != 209 && charCode != 214 && charCode != 216 && charCode != 223
        && charCode != 224 && charCode != 228 && charCode != 229 && charCode != 230 && charCode != 231 && charCode != 232
        && charCode != 233 && charCode != 236 && charCode != 241 && charCode != 242 && charCode != 246 && charCode != 248
        && charCode != 249 && charCode != 252 && charCode != 915 && charCode != 916 && charCode != 920 && charCode != 923
        && charCode != 926 && charCode != 928 && charCode != 931 && charCode != 934 && charCode != 936 && charCode != 937
        && charCode != 8364 )
        return true;
    return false;
}

function isInteger(text) {
    return !isEmpty(text) && (text.search(/^-?[0-9]+$/) == 0);
}

function hasNumber(t) {
    var regex = /\d/g;
    return regex.test(t);
}

function isCorrectPinCode(text) {
    var validMobile = /^[0-9]{4}$/;
    return !isEmpty(text) && validMobile.test(text);
}

function containsInvalidSymbol(text){
    var regx = /[!#$&*=\';/|\<>]/;
    return regx.test(text);
}
	
function isInLength(text, length) {
    return text.length <= length;
}

function isPhoneNumber0(text) {
    var regx = /^0[0-9]{9}$/;
    return !isEmpty(text) && regx.test(text);
}
function isPhoneNumber0_isnull(text) {

    var regx = /^0[0-9]/;
    return regx.test(text);
}

function isPhoneNumber6(text) {
    var regx = /^66[0-9]{9}$/;
    return !isEmpty(text) && regx.test(text);
}

function isEmpty(text) {
    return text == null || text == '';
}

function isEmail(text) {
    var regx = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    return !isEmpty(text) && regx.test(text);
}

function isDouble(text) {
    var regx = /^\d{1,9}\.\d{1,9}$/;
    return !isEmpty(text) && regx.test(text);
}
function isCorrectCitizenID(text) {
    var validCitizenID = /^[0-9]{13}$/;
    if(!this.isEmpty(text) && validCitizenID.test(text)){
        return true;
    }else{
        return false;
    }
}
function checkTextNoScript(text){
    var xpr = /[!#$&*=\';/|\"<>]/;
    return !xpr.test(text);
}