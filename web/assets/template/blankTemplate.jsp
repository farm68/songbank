<%-- 
    Document   : blankTemplate
    Created on : Sep 3, 2021, 9:15:39 AM
    Author     : jiranuwat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
    response.setHeader("X-Powered-By", "");

%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:insert attribute="page.title" ignore="true"/></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no"> 
        <link rel="shortcut icon preload" href="<%=request.getContextPath()%>/assets/images/logo/logo.ico" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/main.css"> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/jquery/jquery-3.5.1.min.js"></script>  
        <script src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/popper.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap4-datetimepicker/js/moment.min.js"></script>  
        <%---------------------------------------------%>
        <!--bootstrap-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/css/bootstrap-grid.min.css"> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/bootstrap.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/anchor.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/clipboard.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/holder.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/popper.min.js"></script>  
        <!-- CSS only -->  
        <tiles:insert attribute="page.body.script" />
    </head> 
    <body style="background:#f5f7f9;">
        <div class="wrapper">
            <div class="loader text-primary">
                <i class="fas fa-circle-notch loading"></i>
            </div>  
            <main class="content">
                <div class="container-fluid">  
                    <tiles:insert attribute="page.body.head" />
                    <tiles:insert attribute="page.body.data" />
                </div>
                <jsp:include page="include/footer.jsp" /> 
            </main> 
        </div>  

        <script src="<%=request.getContextPath()%>/assets/js/app.js"></script> 
        <script src="<%=request.getContextPath()%>/assets/js/web/fc-script.js"></script>
    </body>
</html>
