<%-- 
    Document   : header
    Created on : Sep 11, 2020, 11:32:17 AM
    Author     : Jiranuwat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%> 
<%@page import="com.sone.songbank.info.DmsArtistPortalInfo"%>
<%@page import="com.sone.songbank.util.Constants"%>
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%
    String thumbnailPath = request.getContextPath() + "/assets/images/icons/avatar.png";
    SessionUserInfo sessionUserInfo = null;
    DmsArtistPortalInfo artistPortalInfo = null;
    String userDisplay = "";
    String roleName = "";
    String userLonginTime = "";
    Integer accountId = -1;
    boolean isMember = false;
    try {
        sessionUserInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        if (sessionUserInfo != null) {
            isMember = true;
        }
        accountId = sessionUserInfo.getCoverSongAccountInfo().getAccountId();
//        userLonginTime = (String) session.getAttribute(Constants.USER_LOGIN_TIME);
        artistPortalInfo = sessionUserInfo.getArtistPortalInfo();
        String thumbnailPathTmp = null;
        if (artistPortalInfo != null) {
            userDisplay = artistPortalInfo.getArtistNameTh();
            thumbnailPathTmp = artistPortalInfo.getImageUrl();
        }

        userDisplay = sessionUserInfo.getCoverSongAccountInfo().getDisplayName();
        try {
            thumbnailPathTmp = sessionUserInfo.getYoutubeChannelInfo().getChannelThumbnailUrl();
        } catch (Exception ex) {
        }

        if (thumbnailPathTmp != null && !thumbnailPathTmp.trim().equals("")) {
            thumbnailPath = thumbnailPathTmp;
        }

    } catch (Exception ex) {
    }

    String currentUrl = request.getRequestURI();
    boolean isIndexPage = false;
    String isGridViewActive = "";
    String isListViewActive = "";
    if (currentUrl.contains("/artist/coverSong/index")) {
        isIndexPage = true;
        if (currentUrl.contains("/artist/coverSong/index.jsp")) {
            isListViewActive = " active";
        }
        if (currentUrl.contains("/artist/coverSong/indexGrid.jsp")) {
            isGridViewActive = " active";
        }
    }
%>


<header class="header">
    <ul class="header-nav">
        <li>
            <a href="#" data-dropdown class="header-nav-item"><img src="<%=request.getContextPath()%>/assets/images/logoMCN400x400.png" height="60px;" alt=""></a>   
        </li>  
        <%
            if (isMember) {
        %> 
        <li>
            <a href="<%=request.getContextPath()%>/artist/coverSong/index.jsp" data-dropdown class="header-nav-item <%=isListViewActive%>"> 
                <i class="fe fe-list"></i> 
            </a> 
        </li>
        <li>
            <a href="<%=request.getContextPath()%>/artist/coverSong/indexGrid.jsp" data-dropdown class="header-nav-item <%=isGridViewActive%>">  
                <i class="fe fe-grid"></i>  
            </a>
        </li>
        <%
            }
        %>  
    </ul>  
    <ul class="header-nav pull-right">  
        <%
            if (isMember) {
        %> 
        <li id="musicCartHeader">
            <a href="#"  class="header-nav-item" data-toggle="modal" data-target="#musicConfirmModal">
                <i class="fe fe-shopping-cart"></i>
                <span id="badgeTotalSelectMusicCover" class="badge badge-pill badge-primary"></span>
            </a> 
        </li>
        <li>
            <a href="#" data-dropdown class="user-panel" style="text-decoration: none;">
                <div class="user-panel-image">
                    <div class="avatar avatar-sm">
                        <img src="<%=thumbnailPath%>" alt="">
                    </div>
                </div>
                <div class="user-panel-info">
                    <p class="text-sm text-black-75">Welcome,</p>
                    <span class="font-weight-bold"><%=userDisplay%></span>  
                </div> 
            </a> 
            <ul class="dropdown-menu"> 
                <%
                    if (accountId == 1000) {
                %>
                <li><a class="dropdown-item" href="<%=request.getContextPath()%>/admin/account.jsp"><i class="fe fe-user"></i> <span>Account</span></a></li> 
                    <%
                        }
                    %>
                <li><a class="dropdown-item" href="<%=request.getContextPath()%>/artist/coverSong/index.jsp"><i class="fe fe-music"></i> <span>Song Bank</span></a></li> 
                <li><a class="dropdown-item" href="<%=request.getContextPath()%>/artist/coverSong/requestList.jsp"><i class="fe fe-clipboard"></i> <span>Order Request</span></a></li>  

                <li><a class="dropdown-item" href="<%=request.getContextPath()%>/login/logout.jsp"><i class="fas fa-power-off"></i> Logout</a></li>
            </ul>
        </li>
        <%
        } else {
        %>  
        <li>
            <a href="#" class="user-panel"  onclick="showModalMemberAuthen()" style="text-decoration: none"> 
                <div class="user-panel-image">
                    <div class="avatar avatar-sm">
                        <img src="<%=thumbnailPath%>" alt="">
                    </div>
                </div>
                <div class="user-panel-info"> 
                    <span class="text-black-75">เข้าสู่ระบบ</span> 
                </div> 
            </a>  
        </li> 
        <%
            }
        %>  
    </ul>
</header>


<script type="text/javascript">
    var CONTEXT_PATH = location.pathname.substring(0, location.pathname.indexOf("/", 1));
//    document.writeln('<scr' + 'ipt type="text/javascript" src="' + CONTEXT_PATH + '/js/web/authenticated/script.js" ></scr' + 'ipt>');

    var refreshID;
    var timer = 5000;//5s

    $(document).ready(function () {
        refreshID = setInterval("checkSession()", timer);

        checkActiveMenu();
    });

    function gotoUrl(url) {
        window.location = '' + url;
    }
    function checkSession() {
        $.ajax({
            cache: false,
            type: 'POST',
            url: CONTEXT_PATH + "/ManageUserSessionServlet",
            dataType: "JSON",
            beforeSend: function () {
            }, success: function (data) {
                if (!data.status) {
                    clearInterval(refreshID);
                    showMessageBox("error", "Session Timeout", "กรุณาเข้าสู่ระบบใหม่");
//alert("Time out กรุณาเข้าสู่ระบบใหม่");
                    setTimeout("gotoUrl('" + CONTEXT_PATH + "/login/login.jsp');", 3000);
//                    window.location = CONTEXT_PATH + "/login/login.jsp";
                }
            }
        });
    }

    function checkActiveMenu() {
        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
        $(".sidebar ul li a").each(function () {
            var menuLink = $(this).attr('href');
            var menuName = menuLink.substr(menuLink.lastIndexOf("/") + 1);
            if (pgurl.includes(menuName)) {
                $(this).addClass("active_menu");
            }
        });
    }
</script>