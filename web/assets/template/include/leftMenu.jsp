<%-- 
    Document   : leftMenu
    Created on : Sep 11, 2020, 11:32:05 AM
    Author     : Jiranuwat
--%>
<%@page import="com.sone.songbank.info.DmsArtistPortalInfo"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>   
<%@page import="com.sone.songbank.util.Constants"%> 
<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="java.util.ArrayList"%>
<%
    String thumbnailPath = request.getContextPath() + "/assets/images/icons/avatar.png";
    // Set Session JavaScript
    DmsArtistPortalInfo artistPortalInfo = null;
    SessionUserInfo userMInfo = null;
    String displayName = "";
    Integer accountId = -1;
    try {
        userMInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
        artistPortalInfo = userMInfo.getArtistPortalInfo();
        accountId = userMInfo.getCoverSongAccountInfo().getAccountId();
        String thumbnailPathTmp = null;
        if (artistPortalInfo != null) {
            displayName = artistPortalInfo.getArtistNameTh();
            thumbnailPathTmp = artistPortalInfo.getImageUrl();
        }
        if (displayName == null || displayName.trim().equals("")) {
            displayName = userMInfo.getYoutubeChannelInfo().getChannelName();
            if ((displayName == null || displayName.trim().equals(""))) {
                displayName = userMInfo.getYoutubeChannelInfo().getName();
            }
        }

        if (thumbnailPathTmp == null || thumbnailPathTmp.trim().equals("")) {
            try {
                thumbnailPathTmp = userMInfo.getYoutubeChannelInfo().getChannelThumbnailUrl();
            } catch (Exception ex) {
            }
        }

        if (thumbnailPathTmp != null && !thumbnailPathTmp.trim().equals("")) {
            thumbnailPath = thumbnailPathTmp;
        }

    } catch (Exception ex) {
    }
%>

<aside class="sidebar">   
    <nav>
        <ul class="sidebar-list"> 
            <li class="logo logo-lg text-center" >    
                <img class="rounded-circle" src="<%=thumbnailPath%>" alt="">  
            </li>  
            <li><span class="sidebar-list-header text-center" style="font-weight: bold;"><%=displayName%></span></li>
                <%
                    if(accountId==1000){
                        %>
            <li><a class="sidebar-list-item" href="<%=request.getContextPath()%>/admin/account.jsp"><i class="fe fe-user"></i> <span>Account</span></a></li> 
                        <%
                    }
                %>
            <li><a class="sidebar-list-item" href="<%=request.getContextPath()%>/artist/coverSong/index.jsp"><i class="fe fe-music"></i> <span>Song Bank</span></a></li> 
            <li><a class="sidebar-list-item" href="<%=request.getContextPath()%>/artist/coverSong/requestList.jsp"><i class="fe fe-clipboard"></i> <span>Order Request</span></a></li>  
            <!--<li><a class="sidebar-list-item" href="<%=request.getContextPath()%>/artist/calendar.jsp"><i class="fe fe-calendar"></i> <span>Calendar</span></a></li>-->
        </ul>
    </nav>
</aside>

<script type="text/javascript">
    var CONTEXT_PATH = location.pathname.substring(0, location.pathname.indexOf("/", 1));
//    document.writeln('<scr' + 'ipt type="text/javascript" src="' + CONTEXT_PATH + '/js/web/authenticated/script.js" ></scr' + 'ipt>');

    var refreshID;
    var timer = 5000;//5s

    $(document).ready(function () {
        refreshID = setInterval("checkSession()", timer);

        checkActiveMenu();
    });

    function gotoUrl(url) {
        window.location = '' + url;
    }
    function checkSession() {
        $.ajax({
            cache: false,
            type: 'POST',
            url: CONTEXT_PATH + "/ManageUserSessionServlet",
            dataType: "JSON",
            beforeSend: function () {
            }, success: function (data) {
                if (!data.status) {
                    clearInterval(refreshID);
                    showMessageBox("error", "Session Timeout", "กรุณาเข้าสู่ระบบใหม่");
//alert("Time out กรุณาเข้าสู่ระบบใหม่");
                    setTimeout("gotoUrl('" + CONTEXT_PATH + "/login/login.jsp');", 3000);
//                    window.location = CONTEXT_PATH + "/login/login.jsp";
                }
            }
        });
    }

    function checkActiveMenu() {
        var pgurl = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
        $(".sidebar ul li a").each(function () {
            var menuLink = $(this).attr('href');
            var menuName = menuLink.substr(menuLink.lastIndexOf("/") + 1);
            if (pgurl.includes(menuName)) {
                $(this).addClass("active_menu");
            }
        });
    }



</script>
