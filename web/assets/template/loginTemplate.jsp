<%-- 
    Document   : loginTemplate
    Created on : Sep 11, 2020, 11:31:32 AM
    Author     : Jiranuwat
--%>



<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
    response.setHeader("X-Powered-By", "");

%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Song Bank</title>
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <meta name="format-detection" content="telephone=no"> 
        <link rel="shortcut icon preload" href="<%=request.getContextPath()%>/assets/images/logo/logo.ico" />
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700" rel="stylesheet">

        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/main.css">  
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/util.css">
        <link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/assets/css/login.css">
    </head> 
    <body> 
        <tiles:insert attribute="page.body.data" />    
        <script src="<%=request.getContextPath()%>/assets/plugin/jquery/jquery-3.5.1.min.js"></script>
        <!--===============================================================================================-->
        <script src="<%=request.getContextPath()%>/assets/plugin/bootstrap4/js/popper.js"></script>
        <script src="<%=request.getContextPath()%>/assets/plugin/bootstrap4/js/bootstrap.min.js"></script>
        <!--===============================================================================================--> 
        <script src="<%=request.getContextPath()%>/assets/plugin/tilt/tilt.jquery.min.js"></script>
        <!--<script src="<%=request.getContextPath()%>/assets/js/app.js"></script>-->  
    </body>
</html>
