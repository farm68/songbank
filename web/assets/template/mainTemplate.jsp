<%-- 
    Document   : mainTemplate
    Created on : Sep 11, 2020, 11:31:03 AM
    Author     : Jiranuwat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles" %> 

<%
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-control", "no-cache");
    response.setHeader("X-Powered-By", "");

%> 
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><tiles:insert attribute="page.title" ignore="true"/></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta name="format-detection" content="telephone=no"> 
        <link rel="shortcut icon preload" href="<%=request.getContextPath()%>/assets/images/logo/logo.ico" />
        <meta name="google-site-verification" content="CA6duhoP0uq704HFV4Az3gnv3Zne9Qx-d4Ao-2yKEDI" />
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/css/main.css"> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/jquery/jquery-3.5.1.min.js"></script>  
        <!--<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>-->
        <script src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/popper.min.js" ></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap4-datetimepicker/js/moment.min.js"></script>  
        <%---------------------------------------------%>
        <!--bootstrap-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/css/bootstrap.min.css"> 
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/css/bootstrap-grid.min.css"> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/bootstrap.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/anchor.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/clipboard.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/holder.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-4.1.3/js/vendor/popper.min.js"></script>  
        <!-- CSS only -->
        <!-- CSS only -->
        <!--<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">-->

        <!-- JS, Popper.js, and jQuery -->
        <!--<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>-->
        <!--<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>-->

        <%---------------------------------------------%>
        <!--bootstrap4-datetimepicker-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/bootstrap4-datetimepicker/css/bootstrap-datetimepicker.min.css"/> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap4-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap4-datetimepicker/js/bootstrap-weekpicker.min.js"></script>  

        <%---------------------------------------------%>
        <!--bootstrap-select-1.13.14-->
        <link href="<%=request.getContextPath()%>/assets/plugin/bootstrap-select-1.13.14/dist/css/bootstrap-select.min.css" rel="stylesheet" type="text/css" />  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-select-1.13.14/dist/js/bootstrap-select.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-select-1.13.14/dist/js/i18n/defaults-th_TH.min.js"></script>

        <!-- Latest compiled and minified CSS -->
        <%---------------------------------------------%>


        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/sone/forms.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/sone/numeric.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/sone/sone-plugin.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/main.js"></script>

        <%---------------------------------------------%>
        <!--ChartJS-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/chartjs/Chart.min.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/chartjs/chartjs-plugin-datalabels/chartjs-plugin-datalabels.js"></script>
        <%---------------------------------------------%>
        <!--sweetalert-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/sweetalert/css/sweetalert.css">
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/sweetalert/js/sweetalert.min.js"></script>

        <%---------------------------------------------%>
        <!--toastr-->
        <link rel="stylesheet" href="<%=request.getContextPath()%>/assets/plugin/toastr/toastr.min.css">
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/toastr/toastr.init.js"></script>
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/toastr/toastr.min.js"></script>

        <!--/template/ElaAdmin-master/js/lib/datatables/datatables.min.js-->
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/js/lib/datatables/datatables.min.js"></script>


        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/base64/base64.js"></script>  

        <script type="text/javascript">
            $.fn.selectpicker.Constructor.BootstrapVersion = '4';

            function cancle() {
                var paramJson = $.getQueryStringToJSON();
                var menuId = paramJson.mid;
                window.location = "./index.jsp?mid=" + menuId;
            }

            function autocomplete(inp, arr) {
                /*the autocomplete function takes two arguments,
                 the text field element and an array of possible autocompleted values:*/
                var currentFocus;
                /*execute a function when someone writes in the text field:*/
                inp.addEventListener("input", function (e) {
                    var a, b, i, val = this.value;
                    /*close any already open lists of autocompleted values*/
                    closeAllLists();
                    if (!val) {
                        return false;
                    }
                    currentFocus = -1;
                    /*create a DIV element that will contain the items (values):*/
                    a = document.createElement("DIV");
                    a.setAttribute("id", this.id + "autocomplete-list");
                    a.setAttribute("class", "autocomplete-items");
                    /*append the DIV element as a child of the autocomplete container:*/
                    this.parentNode.appendChild(a);
                    /*for each item in the array...*/
                    for (i = 0; i < arr.length; i++) {
                        /*check if the item starts with the same letters as the text field value:*/
                        if (arr[i].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                            /*create a DIV element for each matching element:*/
                            b = document.createElement("DIV");
                            /*make the matching letters bold:*/
                            b.innerHTML = "<strong>" + arr[i].substr(0, val.length) + "</strong>";
                            b.innerHTML += arr[i].substr(val.length);
                            /*insert a input field that will hold the current array item's value:*/
                            b.innerHTML += "<input type='hidden' value='" + arr[i] + "'>";
                            /*execute a function when someone clicks on the item value (DIV element):*/
                            b.addEventListener("click", function (e) {
                                /*insert the value for the autocomplete text field:*/
                                inp.value = this.getElementsByTagName("input")[0].value;
                                /*close the list of autocompleted values,
                                 (or any other open lists of autocompleted values:*/
                                closeAllLists();
                            });
                            a.appendChild(b);
                        }
                    }
                });
                /*execute a function presses a key on the keyboard:*/
                inp.addEventListener("keydown", function (e) {
                    var x = document.getElementById(this.id + "autocomplete-list");
                    if (x)
                        x = x.getElementsByTagName("div");
                    if (e.keyCode == 40) {
                        /*If the arrow DOWN key is pressed,
                         increase the currentFocus variable:*/
                        currentFocus++;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 38) { //up
                        /*If the arrow UP key is pressed,
                         decrease the currentFocus variable:*/
                        currentFocus--;
                        /*and and make the current item more visible:*/
                        addActive(x);
                    } else if (e.keyCode == 13) {
                        /*If the ENTER key is pressed, prevent the form from being submitted,*/
                        e.preventDefault();
                        if (currentFocus > -1) {
                            /*and simulate a click on the "active" item:*/
                            if (x)
                                x[currentFocus].click();
                        }
                    }
                });
                function addActive(x) {
                    /*a function to classify an item as "active":*/
                    if (!x)
                        return false;
                    /*start by removing the "active" class on all items:*/
                    removeActive(x);
                    if (currentFocus >= x.length)
                        currentFocus = 0;
                    if (currentFocus < 0)
                        currentFocus = (x.length - 1);
                    /*add class "autocomplete-active":*/
                    x[currentFocus].classList.add("autocomplete-active");
                }
                function removeActive(x) {
                    /*a function to remove the "active" class from all autocomplete items:*/
                    for (var i = 0; i < x.length; i++) {
                        x[i].classList.remove("autocomplete-active");
                    }
                }
                function closeAllLists(elmnt) {
                    /*close all autocomplete lists in the document,
                     except the one passed as an argument:*/
                    var x = document.getElementsByClassName("autocomplete-items");
                    for (var i = 0; i < x.length; i++) {
                        if (elmnt != x[i] && elmnt != inp) {
                            x[i].parentNode.removeChild(x[i]);
                        }
                    }
                }
                /*execute a function when someone clicks in the document:*/
                document.addEventListener("click", function (e) {
                    closeAllLists(e.target);
                });
            }
            function callCalendar() {
//$('.form_datetime').datetimepicker({format: 'YYYY-MM-DD HH:mm'});
                $('.form_datetime').datetimepicker({format: 'DD/MM/YYYY'});
                moment.locale('en');
                $('#releaseDate').val(moment().format('DD/MM/YYYY'));
            }
            String.prototype.replaceAll = function (search, replacement) {
                var target = this;
                return target.replace(new RegExp(search, 'g'), replacement);
            };

        </script>

        <tiles:insert attribute="page.body.script" />
    </head> 
    <body style="background:#f5f7f9;">
        <div class="wrapper">
            <div class="loader text-primary">
                <i class="fas fa-circle-notch loading"></i>
            </div> 
            <%--<jsp:include page="include/leftMenu.jsp" />--%> 
            <jsp:include page="include/header.jsp" /> 
            <main class="content container-fluid">
                <!--<div class="container-fluid">-->  
                <tiles:insert attribute="page.body.head" />
                <tiles:insert attribute="page.body.data" />
                <!--</div>-->
            </main>
            <%--<jsp:include page="include/footer.jsp" />--%> 
        </div>  

        <script src="<%=request.getContextPath()%>/assets/js/app.js"></script> 
        <script src="<%=request.getContextPath()%>/assets/js/web/fc-script.js"></script>
    </body>
</html>
