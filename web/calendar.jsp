<%-- 
    Document   : newjsp
    Created on : Aug 17, 2021, 1:22:05 PM
    Author     : jiranuwat
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <!--===============================================================================================--> 
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/jquery/jquery-3.6.0.min.js"></script>  
        <script type="text/javascript" src="<%=request.getContextPath()%>/assets/plugin/bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>  
        <!--===============================================================================================--> 
        <script src="https://apis.google.com/js/api.js"></script>
        <!--===============================================================================================--> 
        <script>
            var YOUTUBE_CLIEND_ID = '847138305209-u53f46i8pb8eejcangl58ti1sme3cejj.apps.googleusercontent.com';
            var YOUTUBE_CLIEND_API_URL = 'https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest';
            var YOUTUBE_CLIEND_ANALYTICS_URL = 'https://youtubeanalytics.googleapis.com/$discovery/rest?version=v2';
            var YOUTUBE_SCOPES = 'email profile https://www.googleapis.com/auth/userinfo.profile ' +
                    'https://www.googleapis.com/auth/userinfo.email '+
                    'https://www.googleapis.com/auth/youtube ' +
//                    'https://www.googleapis.com/auth/youtube.channel-memberships.creator '+
                    'https://www.googleapis.com/auth/youtube.force-ssl ' +
                    'https://www.googleapis.com/auth/youtube.readonly ' +
//                    'https://www.googleapis.com/auth/youtube.upload '+
                    'https://www.googleapis.com/auth/youtubepartner ' +
                    'https://www.googleapis.com/auth/youtubepartner-channel-audit ' +
                    'https://www.googleapis.com/auth/yt-analytics.readonly ';
            $(document).ready(function () {
            });
            var GoogleAuth; // Google Auth object.
            var isSignedIn = false;
            function handleClientLoad() {
                // Load the API's client and auth2 modules.
                // Call the initClient function after the modules load. 
                gapi.load('client:auth2', initClient);
            }

            function initClient() {
                gapi.client.init({
                    clientId: YOUTUBE_CLIEND_ID,
                    scope: YOUTUBE_SCOPES
                }).then(function () {
                    GoogleAuth = gapi.auth2.getAuthInstance();
                    var isSignedInGoogle = GoogleAuth.isSignedIn.get();
                    console.log("[GoogleAuth]  Sign-in : " + isSignedInGoogle); //now this always returns correctly
                    var isAuthen = verifySigninStatus();
                    if (!isAuthen) {
                        GoogleAuth.signIn({scope: YOUTUBE_SCOPES})
                                .then(function () {
                                    console.log("[GoogleAuth] Authenticate successful");
                                    verifySigninStatus();
                                }, function (err) {
                                    console.error("[GoogleAuth] Error Authenticate", err);
                                });
                    }
                });
            }

            function loadClient(url) {
//                gapi.client.setApiKey("YOUR_API_KEY");
                return gapi.client.load(url)
                        .then(function () {
                            console.log("[loadClient] loaded for API");
                            execute();
                        }, function (err) {
                            console.error("[loadClient] Error loading GAPI client for API", err);
                        });
            }
 
            function execute() {
                return gapi.client.youtube.channels.list({
                    "mine": true,
//                    "part": 'auditDetails,brandingSettings,contentDetails,contentOwnerDetails,id,localizations,snippet,statistics,status,topicDetails',
                    "part": 'contentDetails,snippet',
//                    "onBehalfOfContentOwner":CONTENT_OWNER_ID
//                    "managedByMe": true,
                    "maxResults": 50
                }).then(function (response) {
                    // Handle the results here (response.result has the parsed body). 
                    console.log("Response", response);
                }, function (err) {
                    console.error("Execute error", err);
                });
            }

            function verifySigninStatus() {
                var GoogleUser = GoogleAuth.currentUser.get();
                var userId = GoogleUser.getId();
                if (userId != null) {
                    const listScopesArr = YOUTUBE_SCOPES.split(" ");
                    for (const scope of listScopesArr) {
                        isSignedIn = GoogleUser.hasGrantedScopes(scope);
                        if (!isSignedIn) {
                            isSignedIn = false;
                            break;
                        }
                    }
                    if (isSignedIn) {
                        console.log("[GoogleUser] Status :: Signed in and have granted access.");
                        console.log("[GoogleUser] getId : " + GoogleUser.getId());
                        console.log("[GoogleUser] getGrantedScopes : " + GoogleUser.getGrantedScopes());
                        console.log("[GoogleUser] getId()       : " + GoogleUser.getBasicProfile().getId());
                        console.log("[GoogleUser] getName()     : " + GoogleUser.getBasicProfile().getName());
                        console.log("[GoogleUser] getImageUrl() : " + GoogleUser.getBasicProfile().getImageUrl());
                        console.log("[GoogleUser] getEmail()    : " + GoogleUser.getBasicProfile().getEmail());
                    } else {
                        console.log("[GoogleUser] Status :: not authorized.");
                    }
                } else {
                    console.log("[GoogleUser] Status :: signed out.");
                }
                console.log("[GoogleUser] isSignedIn : " + isSignedIn + "\n\n");
                return isSignedIn;
            }

            function googleAuthSignOut() {
                GoogleAuth.signOut(); 
                console.log("[GoogleUser] Status :: signed out.");
            }

            function revokeAccess() {
                GoogleAuth.disconnect(); 
                console.log("[GoogleUser] Status :: disconnected.");
            }


        </script> 
    </head> 
    <body>
        <div class="container">
            <h1>Hello World!</h1> 
            <button class="btn btn-primary pull-right" onclick="handleClientLoad()" type ="button">   authorize and load</button>  
            <button class="btn btn-primary pull-right" onclick="googleAuthSignOut()" type ="button">  sign out</button>  
            <button class="btn btn-primary pull-right" onclick="revokeAccess()" type ="button">  revokeAccess</button>  
        </div>
        <br/> 
        <div class="container">
            <div id="result"></div>
        </div> 
    </body>
</html>
