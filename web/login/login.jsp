<%-- 
    Document   : loginTemplate
    Created on : Jul 5, 2019, 11:11:04 AM
    Author     : Jiranuwat
--%>  

<%@page import="com.sone.songbank.util.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="/WEB-INF/struts-tiles.tld" prefix="tiles"%>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>   

<%
    String errMsg = null;
    try {
        errMsg = request.getAttribute(Constants.DESCRIPTION_MESSAGE) + "";
    } catch (Exception e) {
    }
%>

<!DOCTYPE html>
<tiles:insert page="/assets/template/loginTemplate.jsp" flush="true">  
    <tiles:put name="page.body.data" type="String">  
        <script>
            function submitForm() {
                document.getElementById("messageError").textContent = "";
                document.getElementById("authenticationForm").submit();
            }
        </script>
        <div class="limiter">
            <div class="container-login100">
                <div class="wrap-login100">
                    <div class="login100-pic js-tilt" data-tilt>
                        <img src="<%=request.getContextPath()%>/assets/images/logoMCN400x400.png" alt="IMG">
                    </div> 
                    <form class="login100-form validate-form" name="authenticationForm" id="authenticationForm" method="POST" action="<%=request.getContextPath()%>/AuthenticationAction.do" >
                        <input type="hidden" name="mode" id="mode" value="login">
                        <span class="login100-form-title">
                            Song Bank
                        </span> 
                        <div class="wrap-input100 validate-input" data-validate = "Username is required">
                            <input class="input100" type="text"  id="input_username" name="input_username" placeholder="username">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-user" aria-hidden="false"></i>
                            </span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <input class="input100" type="password" id="input_password" name="input_password" placeholder="Password">
                            <span class="focus-input100"></span>
                            <span class="symbol-input100">
                                <i class="fa fa-lock" aria-hidden="true"></i>
                            </span>
                        </div> 
                        <br/>  
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn" onclick="submitForm()" type="submit">
                                Login
                            </button>
                        </div> 
                        <div class="text-center">
                            <small>
                                <%
                                    if (errMsg != null && !errMsg.trim().equals("") && !errMsg.trim().equalsIgnoreCase("null")) {
//                                        out.println("<span class=\"text-danger\" style=\"padding: 10px;background-color: #feefb3;border-width: 1px;border-style: solid;border-color: #ff0101;border-radius: 5px\">");
                                        out.println(" <span id=\"messageError\"><font color=\"red\" ><i class=\"fa fa-exclamation-triangle\"></i> " + errMsg + "</font></span>");
//                                        out.println("</span>");
                                    }
                                %>
                            </small>
                        </div>
                    </form>	
                </div>
            </div> 
        </div>  
    </tiles:put> 
</tiles:insert>

