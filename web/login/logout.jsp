<%-- 
    Document   : logout
    Created on : Jul 11, 2019, 12:19:43 PM
    Author     : Jiranuwat
--%>

<%@page import="com.sone.songbank.info.login.SessionUserInfo"%>
<%@page import="com.sone.songbank.util.Constants"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    session.invalidate();
    SessionUserInfo userInfo = null;
    try {
        userInfo = (SessionUserInfo) session.getAttribute(Constants.USER_SESSION);
    } catch (Exception ex) {
    }

    if (userInfo == null) {
        response.sendRedirect(request.getContextPath() + "/login/login.jsp");
        return;
    }
%> 